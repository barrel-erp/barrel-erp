/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 2.0        02 Nov 2016     jbrox
 *
 */

function dryHop(request, response) {

	if (request.getMethod() == 'GET') { 

		// Get Parameters
		var location = request.getParameter('custparam_location');
		nlapiLogExecution('DEBUG', 'Location', location);
		var recordId = request.getParameter('custparam_recordid');
		nlapiLogExecution('DEBUG', 'Record ID', recordId);

		if (location != 'null') {

			var onHand = 'T';
			var context = nlapiGetContext();

			var dryHopLines = searchDryHopLines(recordId);

			if (dryHopLines) {

				var lineCount = dryHopLines.length;

				// Get Lot Numbered Item Records
			  	var itemRecordsObj = { itemRecords: [], itemInternal: [], itemLineId: [], itemRecordQuantities: [], itemInventoryDetail: [] };
			  	var lineNum = 0;
				for (var l = 0; l < lineCount; l++) {

					itemRecordsObj.itemRecords.push(dryHopLines[l].getText('item', null, 'GROUP'));
					itemRecordsObj.itemInternal.push(dryHopLines[l].getValue('item', null, 'GROUP'));
					// itemRecordsObj.itemLineId.push(record.getLineItemValue('item', 'line', lineNum));
					itemRecordsObj.itemRecordQuantities.push(dryHopLines[l].getValue('quantity', null, 'SUM'));
					// itemRecordsObj.itemInventoryDetail.push(record.viewLineItemSubrecord('item', 'inventorydetail', lineNum));

				}
				var date = new Date();
				var setDate = nlapiDateToString(date);
				nlapiLogExecution('DEBUG', 'date', setDate);

				//nlapiLogExecution('DEBUG', 'Item Record Object', JSON.stringify(itemRecordsObj));

			    // Create Form with Fields
			    var lotReportForm = nlapiCreateForm('Lot Report (Dry Hop)');

			    // Create Field Groups on the Form
			    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
			    lotReportForm.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('inline').setDefaultValue(location);
			    lotReportForm.addField('custpage_date', 'date', 'Date', null, 'primary').setDefaultValue(setDate);
			    lotReportForm.addField('custpage_sublists', 'integer', 'Sublists').setDisplayType('hidden');
			    lotReportForm.addField('custpage_recordid', 'integer', 'Record ID').setDisplayType('hidden');
			    lotReportForm.addField('custpage_recordtype', 'text', 'Record Type').setDisplayType('hidden');

			  	var sublistRecs = [];

			  	for (var i = 0; i < itemRecordsObj.itemRecords.length; i++) {

			  		var sublistName = 'custpage_lotitemdetail' + itemRecordsObj.itemInternal[i];
			  		sublistRecs.push(sublistName);
			  		// nlapiLogExecution('DEBUG', 'Sublist Name', sublistName);
				    var lotItemDetail = lotReportForm.addSubList(sublistName, 'inlineeditor', itemRecordsObj.itemRecords[i] + ' (' + itemRecordsObj.itemRecordQuantities[i] + ')');
				    lotItemDetail.addField('custpage_sublistname', 'text', 'Sublist Name').setDisplayType('hidden');
				    lotItemDetail.addField('custpage_item', 'select', 'Item', '-10').setDisplayType('hidden');
				    lotItemDetail.addField('custpage_lot', 'text', 'Lot Number').setDisplayType('disabled');
				    lotItemDetail.addField('custpage_hopalpha', 'percent', 'Alpha Acid %').setDisplayType('disabled');
				    lotItemDetail.addField('custpage_lotid', 'integer', 'Lot ID').setDisplayType('hidden');
				    lotItemDetail.addField('custpage_expirationdate', 'date', 'Expiration Date').setDisplayType('disabled');
				    lotItemDetail.addField('custpage_onhand', 'float', 'On Hand').setDisplayType('disabled');
					lotItemDetail.addField('custpage_available', 'float', 'Available').setDisplayType('disabled');
					lotItemDetail.addField('custpage_isonhand', 'checkbox', 'Is On Hand').setDisplayType('hidden');
					lotItemDetail.addField('custpage_quantity', 'float', 'Quantity');
					// lotItemDetail.addField('custpage_cumulativequantity', 'float', 'Cumulative Quantity');
					lotItemDetail.addField('custpage_selectlot', 'checkbox', 'Select Lot').setMandatory(true);

						var inventoryNumbers = searchInventoryNumbers(itemRecordsObj.itemInternal[i], onHand, location);
						// if (itemRecordsObj.itemInventoryDetail[i]) { var inventoryDetail = searchInventoryDetail(recordId, itemRecordsObj.itemInventoryDetail[i].id, null); }
						var setLineNum = 0;
						var setLotLine = 'T';
						var setQuantity = 0;
						var previousSetQuantity = 0;
						var totalLineQuantity = itemRecordsObj.itemRecordQuantities[i];
						var remainingLineQuantity = itemRecordsObj.itemRecordQuantities[i];
						var cumulativeQuantity = 0;

						if (inventoryNumbers) {
						
							for (var n = 0; n < inventoryNumbers.length; n++) {

								// if (!itemRecordsObj.itemInventoryDetail[i]) {

									previousSetQuantity = parseFloat(setQuantity) + parseFloat(previousSetQuantity);
									// if (itemRecordsObj.itemInternal[i] == 209) { nlapiLogExecution('AUDIT', 'Grain 2Row', 'setQuantity: ' + ' | ' + setQuantity + ' previousSetQuantity: ' + ' | ' + previousSetQuantity); }
									setQuantity = findSetQuantity(totalLineQuantity, remainingLineQuantity, inventoryNumbers[n].getValue('quantityavailable'));
									nlapiLogExecution('AUDIT', 'Set Quantity | Remaining Line Quantity', setQuantity + ' | ' + remainingLineQuantity);
									if (n > 0) { 
										// remainingLineQuantity = parseFloat(remainingLineQuantity) - parseFloat(setQuantity); 
										setQuantity = parseFloat(setQuantity) - parseFloat(previousSetQuantity);
										setQuantity = setQuantity.toFixed(2);
										// setQuantity = parseFloat(remainingLineQuantity) - parseFloat(setQuantity);
									}
									nlapiLogExecution('AUDIT', 'Set Quantity | Remaining Line Quantity', setQuantity + ' | ' + remainingLineQuantity);
									if (remainingLineQuantity < 0) { setQuantity = 0; setLotLine = 'F'; } 
									if (setQuantity <= 0) { setQuantity = 0; setLotLine = 'F'; }
									setLineNum = n + 1;

									lotItemDetail.setLineItemValue('custpage_item', setLineNum, itemRecordsObj.itemInternal[i]);
									lotItemDetail.setLineItemValue('custpage_sublistname', setLineNum, sublistName);
									lotItemDetail.setLineItemValue('custpage_lot', setLineNum, inventoryNumbers[n].getValue('inventorynumber'));
									lotItemDetail.setLineItemValue('custpage_hopalpha', setLineNum, inventoryNumbers[n].getValue('custitemnumber_h_berp_alpha_acid'));
									lotItemDetail.setLineItemValue('custpage_lotid', setLineNum, inventoryNumbers[n].getValue('internalid'));
								    lotItemDetail.setLineItemValue('custpage_expirationdate', setLineNum, inventoryNumbers[n].getValue('expirationdate'));
								    lotItemDetail.setLineItemValue('custpage_onhand', setLineNum, inventoryNumbers[n].getValue('quantityonhand'));
									lotItemDetail.setLineItemValue('custpage_available', setLineNum, inventoryNumbers[n].getValue('quantityavailable'));
									lotItemDetail.setLineItemValue('custpage_isonhand', setLineNum, inventoryNumbers[n].getValue('isonhand'));
									lotItemDetail.setLineItemValue('custpage_quantity', setLineNum, setQuantity);
									// lotItemDetail.setLineItemValue('custpage_cumulativequantity', setLineNum, setQuantity);				
									lotItemDetail.setLineItemValue('custpage_selectlot', setLineNum, setLotLine);
									setLotLine = 'T';

							}

						}

				}

				lotReportForm.setFieldValues({custpage_sublists: itemRecordsObj.itemRecords.length, custpage_location: location, custpage_recordid: recordId, custpage_recordtype: recordType});

				lotReportForm.addSubmitButton('Submit');

			    response.writePage(lotReportForm);

		   	}

		} else {

			// Create Form with Fields
		    var lotReportForm = nlapiCreateForm('Lot Report');

		    var noLocationError = 'USER ERROR: Please enter a value for Location.';

		    // Create Field Groups on the Form
		    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
		    lotReportForm.addField('custpage_nolocationerror', 'textarea', null, null, 'primary').setDisplayType('inline').setDefaultValue(noLocationError);
		    lotReportForm.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('inline').setDefaultValue(location);

		     var lotItemDetail = lotReportForm.addSubList('custpage_lotreport', 'inlineeditor', 'Lot Report');
		    lotItemDetail.addField('custpage_sublistname', 'text', 'Sublist Name').setDisplayType('hidden');
		    lotItemDetail.addField('custpage_item', 'select', 'Item', '-10').setDisplayType('hidden');
		    lotItemDetail.addField('custpage_lot', 'text', 'Lot Number').setDisplayType('disabled');
		    lotItemDetail.addField('custpage_lotid', 'integer', 'Lot ID').setDisplayType('hidden');
		    lotItemDetail.addField('custpage_expirationdate', 'date', 'Expiration Date').setDisplayType('disabled');
		    lotItemDetail.addField('custpage_onhand', 'float', 'On Hand').setDisplayType('disabled');
			lotItemDetail.addField('custpage_available', 'float', 'Available').setDisplayType('disabled');
			lotItemDetail.addField('custpage_isonhand', 'checkbox', 'Is On Hand').setDisplayType('hidden');
			// lotItemDetail.addField('custpage_quantity', 'float', 'Quantity');
			lotItemDetail.addField('custpage_selectlot', 'checkbox', 'Select Lot').setMandatory(true);

			response.writePage(lotReportForm);

		}

	} else {

		// Get Parameters
		var sublistRecs = request.getParameter('custpage_sublists');
		var location = request.getParameter('custpage_location');
		var recordId = request.getParameter('custpage_recordid');
		var recordType = request.getParameter('custpage_recordtype');
		var date = request.getParameter('custpage_date');
		nlapiLogExecution('DEBUG', 'date', date);
		var context = nlapiGetContext();
		var itemLineNum = 0;

		var dryHopLines = searchDryHopLines(recordId);

		if (dryHopLines) {

			var lineCount = dryHopLines.length;

			// Get Lot Numbered Item Records
		  	var itemRecordsObj = { itemRecords: [], itemInternal: [], itemLineId: [], itemRecordQuantities: [], itemInventoryDetail: [] };
		  	var lineNum = 0;
			for (var l = 0; l < lineCount; l++) {

				itemRecordsObj.itemRecords.push(dryHopLines[l].getText('item', null, 'GROUP'));
				itemRecordsObj.itemInternal.push(dryHopLines[l].getValue('item', null, 'GROUP'));
				// itemRecordsObj.itemLineId.push(record.getLineItemValue('item', 'line', lineNum));
				itemRecordsObj.itemRecordQuantities.push(dryHopLines[l].getValue('quantity', null, 'SUM'));
				// itemRecordsObj.itemInventoryDetail.push(record.viewLineItemSubrecord('item', 'inventorydetail', lineNum));

			}

		}

		for (var s = 0; s < itemRecordsObj.itemInternal.length; s++) {

			var prodOrders = searchBatchProductionOrdersWithDryHop(recordId, itemRecordsObj.itemInternal[s]);

			// var sublistName = 'custpage_lotitemdetail';
			// sublistName = sublistName + lineItem;
			// nlapiLogExecution('AUDIT', 'Sublist Name Line ID', sublistName);
			// var itemLineCount = request.getLineItemCount(sublistName);
			// nlapiLogExecution('AUDIT', 'Sublist Line Count', itemLineCount);

			if (prodOrders) {

				for (var p = 0; p < prodOrders.length; p++) {

					var tranRecord = nlapiLoadRecord('workorder', prodOrders[p].getValue('internalid'), {recordmode: 'dynamic'});
					var linecount = tranRecord.getLineItemCount('item');
					var lineNum = 0;
					var itemLine = 0;
					var firstItem = 'F';
					var previousItemSelected = '';
					var prodOrderLength = prodOrders.length;
					nlapiLogExecution('DEBUG', 'Prod Length', prodOrderLength);

					for (var l = 0; l < linecount; l++) {

						lineNum = l + 1;
						tranRecord.selectLineItem('item', lineNum);
						var lineItem = tranRecord.getCurrentLineItemValue('item', 'item');

						if (itemRecordsObj.itemInternal[s] == lineItem && tranRecord.getCurrentLineItemValue('item', 'custcol_h_berp_addition_type') == 2) {

							var lineId = tranRecord.getCurrentLineItemValue('item', 'line');
							var lineQuantity = tranRecord.getCurrentLineItemValue('item', 'quantity');
							var sublistName = 'custpage_lotitemdetail';
							sublistName = sublistName + lineItem;
							nlapiLogExecution('AUDIT', 'Sublist Name Line ID', sublistName);
							var itemLineCount = request.getLineItemCount(sublistName);
							nlapiLogExecution('AUDIT', 'Sublist Line Count', itemLineCount);

							var lotSublistName = request.getLineItemValue(sublistName, 'custpage_sublistname', 1);
							var inventoryDetailExists = tranRecord.viewCurrentLineItemSubrecord('item', 'inventorydetail');
							// nlapiLogExecution('DEBUG', 'Inventory Detail', inventoryDetailExists);

							if (sublistName == lotSublistName) {
								
								if (inventoryDetailExists) { var subrecord2 = tranRecord.removeCurrentLineItemSubrecord('item', 'inventorydetail'); tranRecord.commitLineItem('item'); var recid = nlapiSubmitRecord(tranRecord, true); var tranRecord = nlapiLoadRecord('workorder', prodOrders[p].getValue('internalid'), {recordmode: 'dynamic'}); tranRecord.selectLineItem('item', lineNum);  } 

								var subrecord2 = tranRecord.createCurrentLineItemSubrecord('item', 'inventorydetail');
				
								nlapiLogExecution('DEBUG', 'Subrecord', JSON.stringify(subrecord2));
								var lineSelected = 'F';
								
								for (var i = 0; i < itemLineCount; i++) {

									nlapiLogExecution('DEBUG', 'itemLineCount', itemLineCount);

									itemLineNum = i + 1;
									
									if (request.getLineItemValue(sublistName, 'custpage_selectlot', itemLineNum) == 'T') {

										lineSelected = 'T';
										// nlapiLogExecution('DEBUG', 'sublistName', sublistName);
										// nlapiLogExecution('DEBUG', 'i', i);
										var lotToSet = request.getLineItemValue(sublistName, 'custpage_lotid', itemLineNum);
										// nlapiLogExecution('DEBUG', 'lotToSet', lotToSet);
										var quantityToSet = request.getLineItemValue(sublistName, 'custpage_quantity', itemLineNum);
										nlapiLogExecution('DEBUG', 'quantityToSet', quantityToSet + ' / ' + prodOrderLength);
										var lineQuantityToSet = quantityToSet / prodOrderLength;
										nlapiLogExecution('DEBUG', 'quantityToSet Divided', lineQuantityToSet);

										subrecord2.selectNewLineItem('inventoryassignment');
										subrecord2.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotToSet);
										subrecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', lineQuantityToSet);
										subrecord2.commitLineItem('inventoryassignment');

									}

								}

								if (lineSelected == 'T') { subrecord2.commit(); } else { subrecord2.cancel(); }

							}

						}
							
						tranRecord.commitLineItem('item');

					}

					var recid = nlapiSubmitRecord(tranRecord, true);

					var productionIssue = nlapiTransformRecord('workorder', prodOrders[p].getValue('internalid'), 'workorderissue');
					productionIssue.setFieldValue('trandate', date);
					var lineCount = productionIssue.getLineItemCount('component');
					nlapiLogExecution('DEBUG', 'Line Count', lineCount);
					var lineNum = 0;

					for (var t = 0; t < lineCount; t++) {

						lineNum++;
						var lineQuantity = productionIssue.getLineItemValue('component', 'quantity', lineNum);
						productionIssue.setLineItemValue('component', 'quantity', lineNum, lineQuantity);

					}

					try {

						var productionIssueId = nlapiSubmitRecord(productionIssue, true);

					} catch(e) {

						nlapiLogExecution('ERROR', 'Production Order', prodOrders[p].getValue('internalid') + ' ' + e.getCode() + e.getDetails());

					}

				}
			
			}

		}

	}

	nlapiSubmitField('customrecord_h_berp_prod_batch', recordId, 'custrecord_h_berp_prb_dry_hop', 'T');

	response.write('<script>window.close();</script>');

}