/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Aug 2017     amillen          
 *
 */

/**
 * @NApiVersion 2.0
 * @NScriptType MapReduceScript
 */
define(['N/search', 'N/record', 'N/email', 'N/runtime', 'N/error'],
    function(search, record, email, runtime, error)
    {
        function handleErrorAndSendNotification(e, stage)
        {
            log.error('Stage: ' + stage + ' failed', e);

            var author = -5;
            var recipients = 'ashley.millen@heincpa.com';
            var subject = 'Map/Reduce script ' + runtime.getCurrentScript().id + ' failed for stage: ' + stage;
            var body = 'An error occurred with the following information:\n' +
                       'Error code: ' + e.name + '\n' +
                       'Error msg: ' + e.message;

            email.send({
                author: author,
                recipients: recipients,
                subject: subject,
                body: body
            });
        }

        function handleErrorIfAny(summary)
        {
            var inputSummary = summary.inputSummary;
            var mapSummary = summary.mapSummary;
            var reduceSummary = summary.reduceSummary;

            if (inputSummary.error)
            {
                var e = error.create({
                    name: 'INPUT_STAGE_FAILED',
                    message: inputSummary.error
                });
                handleErrorAndSendNotification(e, 'getInputData');
            }

            handleErrorInStage('map', mapSummary);
            handleErrorInStage('reduce', reduceSummary);
        }

        function handleErrorInStage(stage, summary)
        {
            var errorMsg = [];
            summary.errors.iterator().each(function(key, value){
                var msg = 'Failure to accept payment from customer id: ' + key + '. Error was: ' + JSON.parse(value).message + '\n';
                errorMsg.push(msg);
                return true;
            });
            if (errorMsg.length > 0)
            {
                var e = error.create({
                    name: 'RECORD_TRANSFORM_FAILED',
                    message: JSON.stringify(errorMsg)
                });
                handleErrorAndSendNotification(e, stage);
            }
        }

        function createSummaryRecord(summary)
        {
            try
            {
                var seconds = summary.seconds;
                var usage = summary.usage;
                var yields = summary.yields;

                var rec = record.create({
                    type: 'customrecord_summary',
                });

                rec.setValue({
                    fieldId : 'name',
                    value: 'Summary for M/R script: ' + runtime.getCurrentScript().id
                });

                rec.setValue({
                    fieldId: 'custrecord_time',
                    value: seconds
                });
                rec.setValue({
                    fieldId: 'custrecord_usage',
                    value: usage
                });
                rec.setValue({
                    fieldId: 'custrecord_yields',
                    value: yields
                });

                rec.save();
            }
            catch(e)
            {
                handleErrorAndSendNotification(e, 'summarize');
            }
        }

  

        function getInputData()
        {
            return search.create({
                type: "customrecord_h_berp_subscript",
                filters: [ 
                          ["custrecord_h_berp_subscript_suspend","is","F"],
                           "AND", 
                         ["custrecord_h_berp_subscript_cancelled","is","F"],
                         "AND", 
                        ["custrecord_h_berp_subscript_process_date","on","today"]
                        ],
                columns: ["custrecord_h_berp_subscript_cust_name", "custrecord_h_berp_subscript_first_time", "custrecord_h_berp_subscript_frequency", "custrecord_h_berp_subscript_tier"],
                title: 'Bottle Club Process Subscriptions'
            });
        }

        function map(context)
        {
            var searchResult = JSON.parse(context.value);
            //log.debug("search results", searchResult);
            var subscriptionId = searchResult.id;
            
            var customerId = searchResult.values.custrecord_h_berp_subscript_cust_name.value;
            var subTier = searchResult.values.custrecord_h_berp_subscript_tier.value;
        
            context.write(subscriptionId, subTier);
        }

        function reduce(context)
        {
           
    

        var subscriptionId = context.key;
        //log.debug('context', context.key + ', ' + subscriptionId);


        var subRec = record.load({type:'customrecord_h_berp_subscript', id: subscriptionId});
        var tierLevel = subRec.getValue('custrecord_h_berp_subscript_tier');
        var firstTime = subRec.getValue('custrecord_h_berp_subscript_first_time');
        var custId = subRec.getValue('custrecord_h_berp_subscript_cust_name');
        //log.debug('vals from subrec ', tierLevel + ', ' + firstTime + ', ' + custId);

        var tierRec = record.load({type: 'customrecord_h_berp_subscript_tier', id: tierLevel});
        if(firstTime == true){
            var estimateId = tierRec.getValue('custrecord_h_berp_tier_first_mstr_est');
            subRec.setValue('custrecord_h_berp_subscript_first_time', false);
            subRec.save();
        }
        if(firstTime == false){
            var estimateId = tierRec.getValue('custrecord_h_berp_mstr_est');
        }
        //log.debug('estimateid', estimateId);
        var so_rec = record.transform({fromType: record.Type.ESTIMATE, fromId: estimateId, toType: record.Type.SALES_ORDER, isDynamic: true});
        so_rec.setValue({ fieldId: 'entity', value: custId});
        var so_id = so_rec.save();
        //log.debug('sales ord', so_id);
        context.write(so_id);

        }

        function summarize(summary)
        {
            handleErrorIfAny(summary);
            createSummaryRecord(summary);
        }

        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce,
            summarize: summarize
        };
    }); 