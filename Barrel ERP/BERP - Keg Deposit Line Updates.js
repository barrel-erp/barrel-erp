function runKegDepositLineUpdates() {
	
	var context = nlapiGetContext();
	var transactionId = context.getSetting('SCRIPT', 'custscript_h_berp_transaction');
	var transactionType = context.getSetting('SCRIPT', 'custscript_h_berp_transaction_type');
	nlapiLogExecution('DEBUG', 'Transaction ID | Type', transactionId + ' | ' + transactionType);

	if (transactionId && transactionType) {

		var recordObj = nlapiLoadRecord(transactionType, transactionId);
		var recordStatus = recordObj.getFieldValue('status');
		nlapiLogExecution('DEBUG', 'Record Loaded | Record Status', recordStatus);

		if (recordStatus != 'Pending Billing/Partially Fulfilled' && recordStatus != 'Pending Billing' && recordStatus != 'Billed') {

			var lineCount = recordObj.getLineItemCount('item');
			var lineNum = 0;

			var packageTypeLines = searchPackageTypeLines(transactionId);
			nlapiLogExecution('DEBUG', 'packageTypeLines', JSON.stringify(packageTypeLines));

			for (var i = 0; i < lineCount; i++) {

				lineNum++;
				recordObj.selectLineItem('item', lineNum);
				var item = recordObj.getCurrentLineItemValue('item', 'item');
				var lineNumber = recordObj.getCurrentLineItemValue('item', 'line');
				nlapiLogExecution('DEBUG', 'Item | Line Number', item + ' | ' + lineNumber);
				var kegDepositItem = searchKegDepositItems(item);
				recordObj.commitLineItem('item');
				
				if (kegDepositItem) { nlapiLogExecution('DEBUG', 'Removing Line Item', lineNum); recordObj.removeLineItem('item', lineNum); lineNum--; }

			}
			var newLineCount = 0;  //use to track new lines
			for (var i = 0; i < packageTypeLines.length; i++) {

				var depositItem = nlapiLookupField('customrecord_h_berp_package', packageTypeLines[i].getValue('custitem_h_berp_packagetype', 'item', 'GROUP'), 'custrecord_h_berp_pt_deposititem');
				nlapiLogExecution('DEBUG', 'Thnking about adding Deposit Item', depositItem);
				nlapiLogExecution('DEBUG', 'does the sum work?', packageTypeLines[i].getValue('quantity', null, 'SUM') );
              if ( depositItem.length > 0 )
                {
                  nlapiLogExecution('DEBUG', 'Adding Deposit Item', depositItem);
                  recordObj.selectNewLineItem('item');
                  recordObj.setCurrentLineItemValue('item', 'item', depositItem);
                  recordObj.setCurrentLineItemValue('item', 'quantity', packageTypeLines[i].getValue('quantity', null, 'SUM'));
                  recordObj.commitLineItem('item');
                  newLineCount += 1;
                }

			}

		}
      //check new line count to make sure there are lines to submit
	    nlapiLogExecution('DEBUG', 'New Line Count', newLineCount);
        if ( newLineCount > 0 )
          {
            var recordId = nlapiSubmitRecord(recordObj, true);
            nlapiLogExecution('DEBUG', 'Record Submitted', recordId);
          }
	}

}