/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Dec 2015     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type) {
   
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord() {

    return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Boolean} True to continue changing field value, false to abort value change
 */
function clientValidateField(type, name, linenum) {
   
    return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum) {

	var productionBatch = nlapiGetFieldValue('custpage_prodbatchto');
	var location = nlapiGetFieldValue('custpage_location');

	if (productionBatch) {

		var recipe = searchBatchInfo(productionBatch);
		var targetOG = recipe[0].getValue('custitem_h_berp_targetog_p', 'item', 'GROUP');
		var batchVolume = recipe[0].getValue('quantity', null, 'SUM');
		recipe = recipe[0].getValue('item', null, 'GROUP');

	}

	if (name == 'custpage_prodbatchto') {

		var recipe = searchBatchInfo(productionBatch);
		var targetOG = recipe[0].getValue('custitem_h_berp_targetog_p', 'item', 'GROUP');
		var batchVolume = recipe[0].getValue('quantity', null, 'SUM');
		var recipeItem = recipe[0].getValue('item', null, 'GROUP');
		nlapiSetFieldValue('custpage_target_orig_gravity', targetOG);
		nlapiSetFieldValue('custpage_batch_volume', batchVolume);
		nlapiSetFieldValue('custpage_recipe', recipeItem);
		var manufacturingOperationTasks = searchBatchOperationTasks(productionBatch);
		var allFermTanks = searchAllFermentationTanks(location);
		var fermTanks = [];

		for (var a = 0; a < allFermTanks.length; a++) { fermTanks.push(allFermTanks[a].getValue('internalid')); }

		if (manufacturingOperationTasks) {

			for (var i = 0; i < manufacturingOperationTasks.length; i++) {

				var currentManufacturingWorkCenter = manufacturingOperationTasks[i].getValue('manufacturingworkcenter', 'manufacturingOperationTask', 'GROUP');

				if (fermTanks.indexOf(currentManufacturingWorkCenter) > -1) {

					nlapiSetFieldValue('custpage_prodbatchtotank', allFermTanks[fermTanks.indexOf(currentManufacturingWorkCenter)].getValue('custentity_h_berp_tank_name'));

				}

			}

		}

	}

	if (name == 'custpage_sl_quantity') {

		var liveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_live');
		var preHarvestLiveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_live');
		var preHarvestQty = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_qty');
		var additionalYeast = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity');
		
		// if (liveCellCount && preHarvestLiveCellCount && targetOG) {
			
		// 	// Calculate the Average Live Cells from the 1st and 2nd readings
		// 	var avgLiveCells = (parseFloat(liveCellCount) + parseFloat(preHarvestLiveCellCount)) / 2;
		// 	nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_avg_live', avgLiveCells);

		// 	// Calculate the Cells Per Millileter
		// 	var cellsPerMil = parseFloat(avgLiveCells) * 5000000;
		// 	cellsPerMil = setNumber(parseFloat(cellsPerMil), 2);
		// 	nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil', cellsPerMil);

		// 	// Calculate the Cells per Gallon
		// 	var cellsPerGa = parseFloat(cellsPerMil) * 3800 * parseFloat(additionalYeast);
		// 	cellsPerGa = setNumber(parseFloat(cellsPerGa), 2);
		// 	nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_gal', cellsPerGa);

		// 	// Calculate the Cells per Millileter Required
		// 	var cellsPerMilReq = parseFloat(targetOG) * 1400000;
		// 	cellsPerMilReq = setNumber(parseFloat(cellsPerMilReq), 2);
		// 	nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil_req', cellsPerMilReq);

		// 	// Calculate the Cells per Bbl
		// 	var cellsPerBbl = parseFloat(cellsPerMilReq) * 31.5 * 3800 * parseFloat(batchVolume);
		// 	cellsPerBbl = setNumber(parseFloat(cellsPerBbl), 2);
		// 	nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_bbl', cellsPerBbl);

		// 	// Calculate the % Pitch Required
		// 	// var perPitch = (parseFloat(cellsPerGa) / parseFloat(cellsPerBbl)) * 100;
		// 	// nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_per_pitch_req', perPitch.toFixed(0));

		// } 

		if (additionalYeast && preHarvestQty) {
		
			// Calculate the Quantity of Yeast to Add
			var quantityToAdd = parseFloat(additionalYeast) - parseFloat(preHarvestQty);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity_add', quantityToAdd);

		} else if (additionalYeast && !preHarvestQty) {

			// Set the Quantity to Add
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity_add', additionalYeast);

		}

	}

	if (name == 'custpage_sl_per_pitch_req') {

		var liveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_live');
		var preHarvestLiveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_live');
		var preHarvestQty = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_qty');
		var additionalYeast = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity');
		
		if (liveCellCount && preHarvestLiveCellCount && targetOG) {
			
			// Calculate the Average Live Cells from the 1st and 2nd readings
			var avgLiveCells = (parseFloat(liveCellCount) + parseFloat(preHarvestLiveCellCount)) / 2;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_avg_live', avgLiveCells);

			// Calculate the Cells Per Millileter
			var cellsPerMil = parseFloat(avgLiveCells) * 5000000;
			cellsPerMil = setNumber(parseFloat(cellsPerMil), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil', cellsPerMil);

			// Calculate the Cells per Gallon
			var cellsPerGa = parseFloat(cellsPerMil) * 3800 * parseFloat(additionalYeast);
			cellsPerGa = setNumber(parseFloat(cellsPerGa), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_gal', cellsPerGa);

			// Calculate the Cells per Millileter Required
			var cellsPerMilReq = parseFloat(targetOG) * 1400000;
			cellsPerMilReq = setNumber(parseFloat(cellsPerMilReq), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil_req', cellsPerMilReq);

			// Calculate the Cells per Bbl
			var cellsPerBbl = parseFloat(cellsPerMilReq) * 31.5 * 3800 * parseFloat(batchVolume);
			cellsPerBbl = setNumber(parseFloat(cellsPerBbl), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_bbl', cellsPerBbl);

			// Calculate the % Pitch Required
			// var perPitch = (parseFloat(cellsPerGa) / parseFloat(cellsPerBbl)) * 100;
			// nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_per_pitch_req', perPitch.toFixed(0));

		} 

		if (additionalYeast && preHarvestQty) {
		
			// Calculate the Quantity of Yeast to Add
			var quantityToAdd = parseFloat(additionalYeast) - parseFloat(preHarvestQty);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity_add', quantityToAdd);

		} else if (additionalYeast && !preHarvestQty) {

			// Set the Quantity to Add
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity_add', additionalYeast);

		}

	}

	if (name == 'custpage_sl_live') {

		var liveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_live');
		var preHarvestLiveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_live');
		var preHarvestQty = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_qty');
		var additionalYeast = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity');
		var pitchPercent = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_per_pitch_req');
		
		if (liveCellCount && preHarvestLiveCellCount && targetOG) {
			
			// Calculate the Average Live Cells from the 1st and 2nd readings
			var avgLiveCells = (parseFloat(liveCellCount) + parseFloat(preHarvestLiveCellCount)) / 2;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_avg_live', avgLiveCells);

			// Calculate the Cells Per Millileter
			var cellsPerMil = parseFloat(avgLiveCells) * 5000000;
			cellsPerMil = setNumber(parseFloat(cellsPerMil), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil', cellsPerMil);

			if (!additionalYeast) {
			
				// Calculate the Cells per Gallon
				var cellsPerGa = parseFloat(cellsPerMil) * 3800 * parseFloat(preHarvestQty);
				cellsPerGa = setNumber(parseFloat(cellsPerGa), 2);
				nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_gal', cellsPerGa);

			} else {

				// Calculate the Cells per Gallon
				var cellsPerGa = parseFloat(cellsPerMil) * 3800 * parseFloat(additionalYeast);
				cellsPerGa = setNumber(parseFloat(cellsPerGa), 2);
				nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_gal', cellsPerGa);

			}

			// Calculate the Cells per Millileter Required
			var cellsPerMilReq = parseFloat(targetOG) * 1400000;
			cellsPerMilReq = setNumber(parseFloat(cellsPerMilReq), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil_req', cellsPerMilReq);

			// Calculate the Cells per Bbl
			var cellsPerBbl = parseFloat(cellsPerMilReq) * 31.5 * 3800 * parseFloat(batchVolume);
			cellsPerBbl = setNumber(parseFloat(cellsPerBbl), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_bbl', cellsPerBbl);

			// Calculate the % Pitch Required
			// var perPitch = (parseFloat(cellsPerGa) / parseFloat(cellsPerBbl)) * 100;
			// nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_per_pitch_req', perPitch.toFixed(0));

			// Calculate the Quantity Needed
			var qtyNeeded = (((parseFloat(targetOG) * 1400000 * 31.5 * 3800) / (parseFloat(avgLiveCells) * 5000000 * 31.5 *3800)) * 31.5) * pitchPercent;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity', qtyNeeded.toFixed(2));

		}

	}

	if (name == 'custpage_sl_total') {

		var totalCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_total');
		var preHarvestTotalCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_total');

		if (totalCellCount && preHarvestTotalCellCount) {

			var avgTotalCells = (parseFloat(totalCellCount) + parseFloat(preHarvestTotalCellCount)) / 2;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_avg_total', avgTotalCells);

		}

	}

	if (name == 'custpage_sl_per_pitch_req') {

		var liveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_live');
		var preHarvestLiveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_live');
		var preHarvestQty = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_qty');
		var additionalYeast = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity');
		var pitchPercent = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_per_pitch_req');

		if (pitchPercent) {

			// Calculate the Quantity Needed
			var qtyNeeded = (((parseFloat(targetOG) * 1400000 * 31.5 * 3800) / (parseFloat(avgLiveCells) * 5000000 * 31.5 *3800)) * 31.5) * pitchPercent;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity', qtyNeeded.toFixed(2));

		}

	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @returns {Void}
 */
function clientPostSourcing(type, name) {
   
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Void}
 */
function clientLineInit(type) {
     
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Boolean} True to save line item, false to abort save
 */
function clientValidateLine(type) {

	var productionBatch = nlapiGetFieldValue('custpage_prodbatchto');
	
	if (productionBatch) { 

		var recipe = searchBatchInfo(productionBatch);
		var targetOG = recipe[0].getValue('custitem_h_berp_targetog_p', 'item', 'GROUP');
		var batchVolume = recipe[0].getValue('quantity', null, 'SUM');
		recipe = recipe[0].getValue('item', null, 'GROUP');

	}
	
	if (!productionBatch) {

		alert('Please select a Production Batch before committing the line.');

	} else {

		var recipe = searchBatchInfo(productionBatch);
		var targetOG = recipe[0].getValue('custitem_h_berp_targetog_p', 'item', 'GROUP');

		var liveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_live');
		var preHarvestLiveCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_live');
		var preHarvestQty = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_pre_harvest_qty');
		var additionalYeast = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity');
	
		if (liveCellCount && preHarvestLiveCellCount && targetOG) {
			
			// Calculate the Average Live Cells from the 1st and 2nd readings
			var avgLiveCells = (parseFloat(liveCellCount) + parseFloat(preHarvestLiveCellCount)) / 2;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_avg_live', avgLiveCells);

			// Calculate the Cells Per Millileter
			var cellsPerMil = parseFloat(avgLiveCells) * 5000000;
			cellsPerMil = setNumber(parseFloat(cellsPerMil), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil', cellsPerMil);

			if (!additionalYeast) {
			
				// Calculate the Cells per Gallon
				var cellsPerGa = parseFloat(cellsPerMil) * 3800 * parseFloat(preHarvestQty);
				cellsPerGa = setNumber(parseFloat(cellsPerGa), 2);
				nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_gal', cellsPerGa);

			} else {

				// Calculate the Cells per Gallon
				var cellsPerGa = parseFloat(cellsPerMil) * 3800 * parseFloat(additionalYeast);
				cellsPerGa = setNumber(parseFloat(cellsPerGa), 2);
				nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_gal', cellsPerGa);

			}

			// Calculate the Cells per Millileter Required
			var cellsPerMilReq = parseFloat(targetOG) * 1400000;
			cellsPerMilReq = setNumber(parseFloat(cellsPerMilReq), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_mil_req', cellsPerMilReq);

			// Calculate the Cells per Bbl
			var cellsPerBbl = parseFloat(cellsPerMilReq) * 31.5 * 3800 * parseFloat(batchVolume);
			cellsPerBbl = setNumber(parseFloat(cellsPerBbl), 2);
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_cells_per_bbl', cellsPerBbl);

			// Calculate the % Pitch Required
			var perPitch = (parseFloat(cellsPerGa) / parseFloat(cellsPerBbl)) * 100;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_per_pitch_req', perPitch.toFixed(0));

		}

		return true;

	}

	if (name == 'custpage_sl_total') {

		var totalCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_total');
		var preHarvestTotalCellCount = nlapiGetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_reading_total');

		if (totalCellCount && preHarvestTotalCellCount) {

			var avgTotalCells = (parseFloat(totalCellCount) + parseFloat(preHarvestTotalCellCount)) / 2;
			nlapiSetCurrentLineItemValue('custpage_harvestyeast', 'custpage_sl_avg_total', avgTotalCells);

		}

		return true;

	}

}


/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Void}
 */
function clientRecalc(type) {
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Boolean} True to continue line item insert, false to abort insert
 */
function clientValidateInsert(type) {
  
    return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Boolean} True to continue line item delete, false to abort delete
 */
function clientValidateDelete(type) {
   
    return true;
}

function searchBatchInfo(recordid) {

	nlapiLogExecution('DEBUG', 'Search Batch Info Record Id', recordid);
	var Filter = [], Column = [], Result;
	Filter.push(new nlobjSearchFilter('custbody_h_berp_prod_batch', null, 'anyof', recordid));
	Filter.push(new nlobjSearchFilter('type', null, 'anyof', 'WorkOrd'));
	Filter.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	Column.push(new nlobjSearchColumn('custitem_h_berp_targetog_p', 'item', 'GROUP'));
	Column.push(new nlobjSearchColumn('item', null, 'GROUP'));
	Column.push(new nlobjSearchColumn('quantity', null, 'SUM'));
	Column.push(new nlobjSearchColumn('custbody_h_berp_license', null, 'GROUP'));
	Result = nlapiSearchRecord('transaction', null, Filter, Column);
	if (Result) { return Result; }

}

function searchBatchOperationTasks(batchid) {

	var Filter = [], Column = [], Result;
	Filter.push(new nlobjSearchFilter('custbody_h_berp_prod_batch', null, 'anyof', batchid));
	Filter.push(new nlobjSearchFilter('type', null, 'anyof', 'WorkOrd'));
	Filter.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	Filter.push(new nlobjSearchFilter('manufacturingworkcenter', 'manufacturingoperationtask', 'noneof', '@NONE@'));
	Column[0] = new nlobjSearchColumn('manufacturingworkcenter', 'manufacturingOperationTask', 'GROUP');
	Column[0].setSort();
	Result = nlapiSearchRecord('transaction', null, Filter, Column);
	if (Result) { return Result; }

}

function searchAllFermentationTanks(location) {

	var Filter = [], Column = [], Result;

	Filter.push(new nlobjSearchFilter('ismanufacturingworkcenter', null, 'is', 'T'));
	Filter.push(new nlobjSearchFilter('custentity_h_berp_equipment_type', null, 'anyof', 6));
	Filter.push(new nlobjSearchFilter('custentity_h_berp_grouplocation', null, 'anyof', location));
	Column[0] = new nlobjSearchColumn('custentity_h_berp_live_tank_view_seq');
	Column[1] = new nlobjSearchColumn('custentity_h_berp_tank_name');
	Column[2] = new nlobjSearchColumn('custentity_h_berp_capacity');
	Column[3] = new nlobjSearchColumn('internalid');
	Column[0].setSort();

	Result = nlapiSearchRecord('entitygroup', null, Filter, Column);

	return Result;

}

function setNumber(number, decPlaces) {

	var str, suffix = '';

	decPlaces = decPlaces || 0;
	number = +number;

	var factor = Math.pow(10, decPlaces);

	if (number < 1000) {

	    str = number;

	} else if (number < 1000000) {

	    str = Math.floor(number / (1000 / factor)) / factor;
	    suffix = 'K';

	} else if (number < 1000000000) {

	    str = Math.floor(number / (1000000 / factor)) / factor;
	    suffix = 'M';

	} else if (number < 1000000000000) {

	    str = Math.floor(number / (1000000000 / factor)) / factor;
	    suffix = 'B';

	} else if (number < 1000000000000000) {

	    str = Math.floor(number / (1000000000000 / factor)) / factor;
	    suffix = 'T';

	}

	return str + suffix;

}
