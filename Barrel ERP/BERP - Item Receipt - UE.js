/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	var recordId = nlapiGetRecordId();
	var hopsInvDetail = searchHopsInvDetail(recordId);
	nlapiLogExecution('DEBUG', 'Hops Inventory Detail', JSON.stringify(hopsInvDetail));

	if (hopsInvDetail) {

		for (var i = 0; i < hopsInvDetail.length; i++) {
					
			nlapiSubmitField('inventorynumber', hopsInvDetail[i].getValue('internalid', 'itemNumber'), 'custitemnumber_h_berp_alpha_acid', hopsInvDetail[i].getValue('custcol_h_berp_alpha_acid'));

		}

	}

}