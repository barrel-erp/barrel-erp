/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        03 Dec 2015     jbrox
 * 1.01		  08 Nov 2018	  sstreule		   Added a new string to the lotReportSuiteletURL to show the Items vertically, not horizontally 
 *											   on the screen.  New parameter is '&unlayerd=T'.
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	if (type != 'delete') {

		var ordStatus = nlapiGetFieldValue('orderstatus');
		var location = nlapiGetFieldValue('location');
		var prodBatch = nlapiGetFieldValue('custbody_h_berp_prod_batch');
		if (nlapiGetFieldValue('assemblyitem')) { var itemType = nlapiLookupField('item', nlapiGetFieldValue('assemblyitem'), 'custitem_h_berp_itemtype'); }
		var recordType = nlapiGetRecordType();
		
		var recid = nlapiGetRecordId();

		// Add Button for Unlock and Relock Operations
		if (ordStatus == 'A') { var ordStatusText = 'Relock Operations'; } else { var ordStatusText = 'Unlock Operations'; }
		var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_unlock_relock_operat','customdeploy_h_berp_unlock_relock_operat');
		suiteletURL += '&custparam_custom_id=' + recid;
		suiteletURL += '&custparam_orderstatus=' + ordStatus;
		form.setScript('customscript_h_berp_button_click');
		if (ordStatus != 'E' && type == 'view') { form.addButton('custpage_unlockrelockoperations', ordStatusText, 'unlockRelockOperations(\'' + suiteletURL + '\');'); }

		// Add Button for Lot Report

		if (itemType == 4) {

			var lotReportSuiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_lot_report_suitelet','customdeploy_h_berp_lot_report_suitelet');
			lotReportSuiteletURL += '&custparam_custom_id=' + recid;
			lotReportSuiteletURL += '&custparam_orderstatus=' + ordStatus;
			lotReportSuiteletURL += '&custparam_recordtype=' + recordType;
			lotReportSuiteletURL += '&custparam_location=' + location;
			lotReportSuiteletURL += '&unlayered=T';
			form.setScript('customscript_h_berp_button_click');
			if (type == 'view') { form.addButton('custpage_lotreport', 'Lot Report', 'displayLotReports(\'' + lotReportSuiteletURL + '\');'); }

			// Add Button for Generate Batch
			var batchSuiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_generate_batch','customdeploy_h_berp_generate_batch');
			batchSuiteletURL += '&custparam_recordid=' + recid;
			batchSuiteletURL += '&custparam_orderstatus=' + ordStatus;
			batchSuiteletURL += '&custparam_recordtype=' + recordType;
			batchSuiteletURL += '&custparam_location=' + location;
			//batchSuiteletURL += '&unlayered=T';
			form.setScript('customscript_h_berp_button_click');
			if (type == 'view' && ordStatus == 'B' && !prodBatch) { form.addButton('custpage_generatebatch', 'Generate Batch', 'generateBatch(\'' + batchSuiteletURL + '\');'); }

		}

	}
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {
	
	if (type != 'delete') {

		var currentContext = nlapiGetContext();
		var roundingFactor = currentContext.getSetting('SCRIPT', 'custscript_h_berp_pro_rounding_factor');
		nlapiLogExecution('AUDIT', 'Context', currentContext.getExecutionContext());
		if (currentContext.getExecutionContext() == 'userinterface' || currentContext.getExecutionContext() == 'userevent') {
		
			nlapiLogExecution('AUDIT', 'Before Submit | Order Status', nlapiGetFieldValue('orderstatus'));
			var productionOrder = nlapiGetRecordId();
			var hopBillOverride = nlapiGetFieldValue('custbody_h_berp_hopbill_override');
			var productionOrderStatus = nlapiGetFieldValue('orderstatus');
			var location = nlapiGetFieldValue('location');
			var lineCount = nlapiGetLineItemCount('item');
			var recipe = nlapiGetFieldValue('assemblyitem');
			var fermDays = nlapiLookupField('item', recipe, 'custitem_h_berp_total_days_in_ferm');
			var quantity = nlapiGetFieldValue('quantity');
			var quantityGallons = quantity * 31;
			var origGravity = nlapiGetFieldValue('custbody_h_berp_targetog_sg');
			var lineId = 0;
			var potentialYield = 0;
			var totalIBU = 0;
			var lengthOfBoil = 0;
			var hopBills = searchHopBills(recipe);

			if (hopBills) {
				var numOfHopBills = hopBills.length;
				nlapiLogExecution('DEBUG', 'Number of Hop Bills', numOfHopBills);
			}

			var currHopBill = 0;

			lineId = 0;

			for (var i = 0; i < lineCount; i++) {

				lineId = i + 1;
				nlapiSelectLineItem('item', lineId);
				var lineQty = parseFloat(nlapiGetCurrentLineItemValue('item', 'quantity'));
				var grainPoints = nlapiGetCurrentLineItemValue('item', 'custcol_h_berp_grainpoints');
				var alphaPercent = nlapiGetCurrentLineItemValue('item', 'custcol_h_berp_alpha');
				lengthOfBoil = nlapiGetCurrentLineItemValue('item', 'custcol_h_berp_lob');

				if (type == 'create') {

					nlapiSetCurrentLineItemValue('item', 'quantity', lineQty.toFixed(roundingFactor));

				} else if (type != 'create' && type != 'delete') {

					var inventoryDetailSub = nlapiViewLineItemSubrecord('item', 'inventorydetail', lineId);
					var checkIssuesCompletions = searchIssuesCompletions(productionOrder);
					nlapiLogExecution('DEBUG', 'Issue/Completion Check', JSON.stringify(checkIssuesCompletions));

					if (!inventoryDetailSub && !checkIssuesCompletions) {

						nlapiLogExecution('DEBUG', 'Setting Line Qty Line Num', lineId); 
						nlapiSetCurrentLineItemValue('item', 'quantity', lineQty.toFixed(roundingFactor)); 

					}

				}
			
				if (grainPoints) {
			
					var lineQuantity = nlapiGetCurrentLineItemValue('item', 'quantity');
					var grainPoints = nlapiGetCurrentLineItemValue('item', 'custcol_h_berp_grainpoints');
					potentialPoints = (lineQuantity * grainPoints) / (quantity * 31);
					potentialYield = parseFloat(potentialYield) + parseFloat(potentialPoints);
					nlapiSetCurrentLineItemValue('item', 'custcol_h_berp_potpoints', potentialPoints.toFixed(0));
					
				} else {

					if (currHopBill < numOfHopBills && hopBillOverride == 'F') {

						if (hopBills[currHopBill].getValue('custrecord_h_berp_hoptype') == nlapiGetCurrentLineItemValue('item', 'item')) {

							nlapiLogExecution('DEBUG', 'Current Hop Bill Index', currHopBill);
							nlapiLogExecution('DEBUG', 'Current Hop Bill Sequence', hopBills[currHopBill].getValue('custrecord_h_berp_sequence'));
							lengthOfBoil = hopBills[currHopBill].getValue('custrecord_h_berp_duration');
							nlapiSetCurrentLineItemValue('item', 'custcol_h_berp_lob', hopBills[currHopBill].getValue('custrecord_h_berp_duration'));
							nlapiSetCurrentLineItemValue('item', 'custcol_h_berp_sequ_num', hopBills[currHopBill].getValue('custrecord_h_berp_sequence'));
							nlapiSetCurrentLineItemValue('item', 'custcol_h_berp_addition_type', hopBills[currHopBill].getValue('custrecord_h_berp_additiontype'));
							currHopBill++;

						}

					}

					var lineQuantity = nlapiGetCurrentLineItemValue('item', 'quantity') * 16;
					var additionType = nlapiGetCurrentLineItemValue('item', 'custcol_h_berp_addition_type');
					var aau = lineQuantity * alphaPercent;
					var utilization = 1.65 * Math.pow(0.000125, (origGravity - 1)) * ((1 - Math.exp(-0.04 * lengthOfBoil)) / 4.15);

					if (utilization && additionType == 1) {

						nlapiLogExecution('AUDIT', 'Bitter Calc Variables', 'lineQuantity: ' + lineQuantity + ' alphaPercent: ' + alphaPercent + ' lengthOfBoil: ' + lengthOfBoil + ' utilization: ' + utilization);
						var bitteringUnits = (aau * 75 * utilization) / quantityGallons;
						totalIBU = parseFloat(totalIBU) + parseFloat(bitteringUnits);
						nlapiLogExecution('AUDIT', 'Bittering Units', bitteringUnits);
					
						nlapiSetCurrentLineItemValue('item', 'custcol_h_berp_util_per', utilization.toFixed(3));
						nlapiSetCurrentLineItemValue('item', 'custcol_h_berp_aau', aau);
						nlapiSetCurrentLineItemValue('item', 'custcol_h_berp_bittering_units', bitteringUnits.toFixed(0));

					}

				}

				nlapiCommitLineItem('item');
				nlapiSetFieldValue('custbody_h_berp_potyield', potentialYield.toFixed(0));
				nlapiSetFieldValue('custbody_h_berp_ibu', totalIBU.toFixed(0));

			}

			if (type != 'create' && type != 'delete') {

				var manufacturingOperationTasks = searchmanufacturingOperationTasks(productionOrder);
				nlapiLogExecution('DEBUG', 'Manufacturing Operation Tasks', JSON.stringify(manufacturingOperationTasks));

				if (manufacturingOperationTasks) {
				
					for (var m = 0; m < manufacturingOperationTasks.length; m++) {

						var workCenterFields = ['custentity_h_berp_equipment_type', 'custentity_h_berp_additional_mins'];
						var workCenter = nlapiLookupField('entitygroup', manufacturingOperationTasks[m].getValue('manufacturingworkcenter'), workCenterFields);
						var equipmentType = workCenter.custentity_h_berp_equipment_type;
						var workCenterAddMins = workCenter.custentity_h_berp_additional_mins;
						var startDate = manufacturingOperationTasks[m].getValue('startdate');
						var additionalTimeMinutes = manufacturingOperationTasks[m].getValue('custevent_h_berp_additionaltime');
						startDate = nlapiStringToDate(startDate);
						nlapiLogExecution('DEBUG', 'Start Date', startDate);
						var dueDate = manufacturingOperationTasks[m].getValue('startdate');
						dueDate = nlapiStringToDate(dueDate);

						var productionTaskTemplates = searchProductionTaskTemplates(equipmentType, location, recipe);

						if (productionTaskTemplates) {

							for (var p = 0; p < productionTaskTemplates.length; p++) {

								var productionTaskExists = searchCurrentProductionTasks(productionTaskTemplates[p].getValue('internalid'), manufacturingOperationTasks[m].getValue('manufacturingworkcenter'), productionOrder);

								var startDate = manufacturingOperationTasks[m].getValue('startdate');
								startDate = nlapiStringToDate(startDate);
								startDate.setDate(startDate.getDate() + parseInt(productionTaskTemplates[p].getValue('custrecord_h_berp_due_date')));

								var dueDate = manufacturingOperationTasks[m].getValue('startdate');
								dueDate = nlapiStringToDate(dueDate);
								dueDate.setDate(startDate.getDate() + parseInt(productionTaskTemplates[p].getValue('custrecord_h_berp_days_to_complete')));
								dueDate = nlapiDateToString(dueDate);
								startDate = nlapiDateToString(startDate);

								if (!productionTaskExists && nlapiGetFieldValue('orderstatus') == 'D' && nlapiGetFieldValue('custbody_h_berp_prod_tasks_created') == 'F') {
								
									var productionTaskRecord = nlapiCreateRecord('task');
									productionTaskRecord.setFieldValue('title', productionTaskTemplates[p].getValue('name'));
									productionTaskRecord.setFieldValue('assigned', productionTaskTemplates[p].getValue('custrecord_h_berp_assigned_to'));
									productionTaskRecord.setFieldValue('message', productionTaskTemplates[p].getValue('custrecord_h_berp_message'));
									productionTaskRecord.setFieldValue('startdate', startDate);
									productionTaskRecord.setFieldValue('duedate', dueDate);
									productionTaskRecord.setFieldValue('custevent_h_berp_operation_sequence', manufacturingOperationTasks[m].getValue('sequence'));
									productionTaskRecord.setFieldValue('custevent_h_berp_operation_name', manufacturingOperationTasks[m].getValue('name'));
									productionTaskRecord.setFieldValue('custevent_h_berp_equipment_type', productionTaskTemplates[p].getValue('custrecord_h_berp_equipment_type'));
									productionTaskRecord.setFieldValue('custevent_h_berp_manufact_work_center', manufacturingOperationTasks[m].getValue('manufacturingworkcenter'));
									productionTaskRecord.setFieldValue('custevent_h_berp_prod_task_template', productionTaskTemplates[p].getValue('internalid'));
									productionTaskRecord.setFieldValue('custevent_h_berp_prod_order', productionOrder);
									var id = nlapiSubmitRecord(productionTaskRecord, true);

								} else if (productionTaskExists) {

									for (var t = 0; t < productionTaskExists.length; t++) {

										var productionTaskRecord = nlapiLoadRecord('task', productionTaskExists[t].getValue('internalid'));
										productionTaskRecord.setFieldValue('duedate', dueDate);
										productionTaskRecord.setFieldValue('startdate', startDate);
										var taskId = nlapiSubmitRecord(productionTaskRecord, true);

									}

								}

							}

						}

						nlapiLogExecution('DEBUG', 'Variables', equipmentType + ' | ' + workCenterAddMins + ' | ' + additionalTimeMinutes + ' | ' + fermDays);
						if (workCenterAddMins && additionalTimeMinutes && fermDays && nlapiGetFieldValue('orderstatus') != 'D' && nlapiGetFieldValue('orderstatus') != 'G') {

							// Calculate Run Rate
							var fermDaysInMinutes = (fermDays * 24 * 60) + parseFloat(workCenterAddMins) + parseFloat(additionalTimeMinutes);
							nlapiLogExecution('DEBUG', 'Ferm Days Calculations', fermDays + ' | ' + workCenterAddMins);
							var runRate = fermDaysInMinutes / quantity;
							nlapiLogExecution('DEBUG', 'Run Rate Calculations', fermDaysInMinutes + ' | ' + quantity);

							var fermTankOperationTask = nlapiLoadRecord('manufacturingoperationtask', manufacturingOperationTasks[m].getValue('internalid'));
							fermTankOperationTask.setFieldValue('runrate', runRate.toFixed(0));
							var fermTankId = nlapiSubmitRecord(fermTankOperationTask, true);

						}

					}

				}

				if (nlapiGetFieldValue('orderstatus') == 'D' && nlapiGetFieldValue('custbody_h_berp_prod_tasks_created') == 'F') {

					nlapiSetFieldValue('custbody_h_berp_prod_tasks_created', 'T');

				}

			}

		} else if (currentContext.getExecutionContext() == 'suitelet') {

			var productionOrder = nlapiGetRecordId();
			var productionOrderStatus = nlapiGetFieldValue('orderstatus');
			nlapiLogExecution('AUDIT', 'Production Order Status', productionOrderStatus);

			try {
			
				var lineCount = nlapiGetLineItemCount('item');
				var lineId = 0;	

				for (var l = 0; l < lineCount; l++) {

					lineId = l + 1;
					nlapiSelectLineItem('item', lineId);
					var lineQty = parseFloat(nlapiGetCurrentLineItemValue('item', 'quantity'));
					nlapiLogExecution('DEBUG', 'Line Quantity', lineQty);
					
					if (type == 'create') {

						nlapiSetCurrentLineItemValue('item', 'quantity', lineQty.toFixed(roundingFactor));

					} else if (type != 'create' && type != 'delete') {
					
						var inventoryDetailSub = nlapiViewLineItemSubrecord('item', 'inventorydetail', lineId);
						var checkIssuesCompletions = searchIssuesCompletions(productionOrder);
						nlapiLogExecution('DEBUG', 'Issue/Completion Check', JSON.stringify(checkIssuesCompletions));

						if (!inventoryDetailSub && !checkIssuesCompletions) {

							nlapiLogExecution('DEBUG', 'Setting Line Qty Line Num', lineId); 
							nlapiSetCurrentLineItemValue('item', 'quantity', lineQty.toFixed(roundingFactor));
							nlapiLogExecution('DEBUG', 'Set Quantity', lineQty.toFixed(roundingFactor));

						}

					}

					nlapiCommitLineItem('item');

				}

			} catch(e) {

				nlapiLogExecution('ERROR', 'Line Quantities Not Updated', e.getCode() + ' | ' + e.getDetails());

			}

		}

	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	// var currentContext = nlapiGetContext();
	// if (currentContext.getExecutionContext() == 'suitelet') {

	// 	var productionOrder = nlapiGetRecordId();
	// 	var productionBatch = nlapiGetFieldValue('custbody_h_berp_prod_batch');
	// 	if (productionBatch) {

	// 		var origProductionOrder = nlapiLookupField('customrecord_h_berp_prod_batch', productionBatch, 'custrecord_h_berp_originating_prodord');
	// 		var origProOrderWorkCenters = searchProOrderWorkCenters(origProductionOrder);

	// 		for (var i = 0; i < origProOrderWorkCenters.length; i++) {

	// 			if (nlapiLookupField('entitygroup', origProOrderWorkCenters[i].getValue('manufacturingworkcenter', 'manufacturingOperationTask'), 'custentity_h_berp_equipment_type') == '6') {

	// 				var origFermTank = origProOrderWorkCenters[i].getValue('manufacturingworkcenter', 'manufacturingOperationTask');

	// 			}

	// 		}

	// 		var currentProOrderWorkCenters = searchProOrderWorkCenters(productionOrder);

	// 		for (var b = 0; b < currentProOrderWorkCenters.length; b++) {

	// 			if (nlapiLookupField('entitygroup', currentProOrderWorkCenters[b].getValue('manufacturingworkcenter', 'manufacturingOperationTask'), 'custentity_h_berp_equipment_type') == '6') {

	// 				var productionOrderStatus = nlapiLookupField('workorder', productionOrder, 'status');
	// 				nlapiLogExecution('AUDIT', 'Production Order Status', productionOrderStatus);

	// 				if (productionOrderStatus == 'pendingApproval') {
					
	// 					nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', origFermTank);

	// 				} else {

	// 					nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'A');
	// 					var productionOrderStatus = nlapiLookupField('workorder', productionOrder, 'status');
	// 					nlapiLogExecution('AUDIT', 'Production Order Status After Unlock', productionOrderStatus);
	// 					nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', origFermTank);

	// 				}

	// 			}

	// 		}

	// 	}

	// }
  
}