function processSubmissionResults(request, response) {

	if (request.getMethod() == 'GET') {

		// Get Parameters
		var fromdate = request.getParameter('custparam_fromdate');
		var todate = request.getParameter('custparam_todate');
		nlapiLogExecution('AUDIT', 'Param Dates: ', 'From: ' + fromdate + ' To: ' + todate);

		// Set Various Variables
		var defaultdate = new Date(); defaultdate = nlapiDateToString(defaultdate);
		if (!fromdate) { fromdate = defaultdate; }
		if (!todate) { todate = defaultdate; }

		// Create Submission Status Page
		var status = nlapiCreateForm('Process Submission Status');
		status.setScript('customscript_h_berp_process_sub_cli');
		var filters = status.addFieldGroup('filters', 'Filters');
		status.addField('custpage_fromdate', 'date', 'From', null, 'filters').setDefaultValue(fromdate);
		status.addField('custpage_todate', 'date', 'To', null, 'filters').setDefaultValue(todate);

		// Create Sublist
		var statussublist = status.addSubList('custpage_processedstatus', 'staticlist', 'Submission Status');
		statussublist.addField('custpage_submissionid', 'integer', 'Submission ID');
		statussublist.addField('custpage_type', 'text', 'Type');
		statussublist.addField('custpage_invadj', 'text', 'Inventory Adjustment');
		statussublist.addField('custpage_status', 'text', 'Status');
		statussublist.addField('custpage_error', 'text', 'Error Details');
		statussublist.addField('custpage_datecreated', 'text', 'Date Created');
		statussublist.addField('custpage_createdby', 'select', 'Created By', '-4').setDisplayType('inline');

		// Load Sublist
		var submissionResults = searchProcessSubmissions(fromdate, todate);

		if (submissionResults) {

			for (var i = 0; i < submissionResults.length; i++) {

				line = i + 1;
				nlapiLogExecution('AUDIT', 'Status: ', submissionResults[i].getText('custrecord_h_berp_sub_process_status'));

				// Set Sublist Values
				statussublist.setLineItemValue('custpage_submissionid', line, submissionResults[i].getValue('internalid'));
				statussublist.setLineItemValue('custpage_type', line, submissionResults[i].getText('custrecord_h_berp_sub_process_type'));
				statussublist.setLineItemValue('custpage_status', line, submissionResults[i].getText('custrecord_h_berp_sub_process_status'));
				statussublist.setLineItemValue('custpage_error', line, submissionResults[i].getValue('custrecord_h_berp_sub_process_error'));
				var invAdjLink = '<a target="_blank" class="dottedlink" href="' + nlapiResolveURL('RECORD', 'inventoryadjustment', submissionResults[i].getValue('custrecord_h_berp_sub_inv_adjustment')) + '&custparam_submissionid=' + submissionResults[i].getValue('internalid') + '">' + submissionResults[i].getText('custrecord_h_berp_sub_inv_adjustment') + '</a>';
				statussublist.setLineItemValue('custpage_invadj', line, invAdjLink);
				statussublist.setLineItemValue('custpage_datecreated', line, submissionResults[i].getValue('created'));
				statussublist.setLineItemValue('custpage_createdby', line, submissionResults[i].getValue('owner'));

			}

		}

		response.writePage(status);

	}

}