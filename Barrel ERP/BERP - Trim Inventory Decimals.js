function trimDecimals(request, response) {

	var context = nlapiGetContext();
	var accountId = context.getSetting('SCRIPT', 'custscript_h_berp_ns_account_id');
	var activeAccounts = accountActiveCheck();
	nlapiLogExecution('DEBUG', 'Active Accounts', JSON.stringify(activeAccounts));
	nlapiLogExecution('DEBUG', 'Index', activeAccounts.indexOf(accountId));

    if (activeAccounts.indexOf(accountId) >= 0) {

		if (request.getMethod() == 'GET') {

			// OneWorld Check
			var isOneWorld;
			var companyInfo = nlapiLoadConfiguration('userpreferences'); //gets user preferences
			var acctSubsidiary = companyInfo.getFieldValue('subsidiary'); //gets subsidiary from user preferences
			if (acctSubsidiary != null) { // if subsidiary is not null
			   isOneWorld = true; //account is a OneWorld account
			} else {
			   isOneWorld = false; //account is NOT a OneWorld account
			}

			// Set Various Variables
			var date = '';
			var adjAccount = '';
			var subsidiary = '';
			var location = '';
			var department = '';
			var classRec = '';
			var licenseRec = '';
			var itemType = '';
			var isLot = '';

			var adjAccount = nlapiGetContext().getSetting('SCRIPT', 'custscript_h_berp_dec_trim_adj_acct');

			if (request.getParameter('custparam_date')) { date = request.getParameter('custparam_date'); }
			if (request.getParameter('custparam_adjaccount')) { adjAccount = request.getParameter('custparam_adjaccount'); }
			if (request.getParameter('custparam_subsidiary')) { subsidiary = request.getParameter('custparam_subsidiary'); }
			if (request.getParameter('custparam_location')) { location = request.getParameter('custparam_location'); }
			if (request.getParameter('custparam_department')) { department = request.getParameter('custparam_department'); }
			if (request.getParameter('custparam_class')) { classRec = request.getParameter('custparam_class'); }
			if (request.getParameter('custparam_license')) { licenseRec = request.getParameter('custparam_license'); }
			if (request.getParameter('custparam_itemtype')) { itemType = request.getParameter('custparam_itemtype'); }
			if (request.getParameter('custparam_islot') == 'T') { isLot = request.getParameter('custparam_islot'); }
			
			// Set various variables
			var defaultdate = new Date(); defaultdate = nlapiDateToString(defaultdate);
			var userSubsidiary = nlapiGetContext().getSubsidiary();
			nlapiLogExecution('DEBUG', 'Users Subsidiary', userSubsidiary);
			var line = 0;

			// Create the form and add fields to it 
			var form = nlapiCreateForm('Trim Inventory Decimals');
			form.setScript('customscript_h_berp_decimal_trim_suitel');

			// Create the Field Groups
			var primary = form.addFieldGroup('primary', 'Primary Information');
			var classification = form.addFieldGroup('classification', 'Classification');
			var itemOptions = form.addFieldGroup('itemfilters', 'Item Filters');
			form.addField('custpage_date', 'date', 'Date', null, 'primary').setDefaultValue(defaultdate);
			form.addField('custpage_adjaccount', 'select', 'Adjustment Account', '-112', 'primary').setMandatory(true).setDefaultValue(adjAccount);
			form.addField('custpage_location', 'select', 'Location', '-103', 'classification').setMandatory(true).setDefaultValue(location);
			form.addField('custpage_department', 'select', 'Department', '-102', 'classification').setDefaultValue(department);
			form.addField('custpage_class', 'select', 'Class', '-101', 'classification').setDefaultValue(classRec);
			form.addField('custpage_license', 'select', 'License', 'customrecord_h_berp_license', 'classification').setMandatory(true).setDefaultValue(licenseRec);
			if (isOneWorld == true) {

				form.addField('custpage_subsidiary', 'select', 'Subsidiary', '-117', 'classification').setMandatory(true).setDefaultValue(subsidiary);

			}

			form.addField('custpage_itemtype', 'select', 'Item Type', 'customlist_h_berp_itemtype', 'itemfilters').setDefaultValue(itemType);
			form.addField('custpage_islot', 'checkbox', 'Is Lot Numbered?', null, 'itemfilters').setDefaultValue(isLot);
			
			// Create Sublist
			var sublist = form.addSubList('custpage_decimaltrimsubmission', 'list', 'Inventory Items');
		    sublist.addMarkAllButtons();
	        sublist.addField('custpage_trim', 'checkbox', 'Trim').setDefaultValue('T');
	        sublist.addField('custpage_item', 'select', 'Item', '-10').setDisplayType('inline');
	        sublist.addField('custpage_type', 'text', 'Type').setDisplayType('inline');
	        sublist.addField('custpage_itemtype', 'text', 'Item Type').setDisplayType('inline');
	        sublist.addField('custpage_lot', 'checkbox', 'Lot Numbered').setDisplayType('inline');
	        sublist.addField('custpage_itemlot', 'text', 'Lot Number').setDisplayType('inline');
	        sublist.addField('custpage_itemlotinternal', 'text', 'Lot Number').setDisplayType('hidden');
	        sublist.addField('custpage_expirationdate', 'date', 'Expiration Date').setDisplayType('inline');
	        sublist.addField('custpage_location', 'select', 'Location', '-103').setDisplayType('inline');
	        sublist.addField('custpage_onhand', 'float', 'On Hand').setDisplayType('inline');
	        sublist.addField('custpage_available', 'float', 'Available').setDisplayType('inline');
	        sublist.addField('custpage_adjustqtyby', 'float', 'Adjust Qty. By').setDisplayType('inline');
	        sublist.addField('custpage_units', 'text', 'Base Unit Internal ID').setDisplayType('hidden');

	        nlapiLogExecution('DEBUG', 'Inventory Search Begin');
	        var lotNumberItemsWithDecimals = nlapiLoadSearch('item', 'customsearch_h_berp_decim_trim');
			var filters = lotNumberItemsWithDecimals.getFilters();
			var columns = lotNumberItemsWithDecimals.getColumns();
			lotNumberItemsWithDecimals = nlapiCreateSearch('item', filters, columns);
			nlapiLogExecution('DEBUG', 'Inventory Search', lotNumberItemsWithDecimals);

			if (location) { lotNumberItemsWithDecimals.addFilter(new nlobjSearchFilter('internalid', 'inventoryLocation', 'anyof', location)); }
			if (itemType) { lotNumberItemsWithDecimals.addFilter(new nlobjSearchFilter('custitem_h_berp_itemtype', null, 'anyof', itemType)); }
			if (isLot) { lotNumberItemsWithDecimals.addFilter(new nlobjSearchFilter('islotitem', null, 'is', isLot)); }

			lotNumberItemsWithDecimals = lotNumberItemsWithDecimals.runSearch();
			nlapiLogExecution('DEBUG', 'Inventory Search', lotNumberItemsWithDecimals);

	        var lineNum = 0;

	      //   if (lotNumberItemsWithDecimals) {

	      //   	lotNumberItemsWithDecimals.forEachResult(function(searchResult) { 

		     //    	lineNum++;
			    //     sublist.setLineItemValue('custpage_item', lineNum, searchResult.getValue('item'));
			    //     sublist.setLineItemValue('custpage_itemtype', lineNum, searchResult.getText('type', 'item'));
			    //    	sublist.setLineItemValue('custpage_lot', lineNum, searchResult.getValue('islotitem', 'item'));
			    //     sublist.setLineItemValue('custpage_itemlot', lineNum, searchResult.getValue('inventorynumber'));
			    //     sublist.setLineItemValue('custpage_itemlotinternal', lineNum, searchResult.getValue('internalid'));
			    //     sublist.setLineItemValue('custpage_expirationdate', lineNum, searchResult.getValue('expirationdate'));
			    //     sublist.setLineItemValue('custpage_location', lineNum, searchResult.getValue('location'));
			    //     sublist.setLineItemValue('custpage_onhand', lineNum, searchResult.getValue('quantityonhand'));
			    //     sublist.setLineItemValue('custpage_available', lineNum, searchResult.getValue('quantityavailable'));
			    //     sublist.setLineItemValue('custpage_adjustqtyby', lineNum, searchResult.getValue('formulanumeric'));
			    //     sublist.setLineItemValue('custpage_units', lineNum, searchResult.getValue('formulatext'));

			    //     return true;

			    // });

	      //   }

	        if (lotNumberItemsWithDecimals) {

	        	lotNumberItemsWithDecimals.forEachResult(function(searchResult) { 

		        	lineNum++;
			        sublist.setLineItemValue('custpage_item', lineNum, searchResult.getValue('internalid'));
			        sublist.setLineItemValue('custpage_type', lineNum, searchResult.getText('type'));
			        sublist.setLineItemValue('custpage_itemtype', lineNum, searchResult.getText('custitem_h_berp_itemtype'));
			       	sublist.setLineItemValue('custpage_lot', lineNum, searchResult.getValue('islotitem'));
			        sublist.setLineItemValue('custpage_itemlot', lineNum, searchResult.getValue('inventorynumber', 'inventoryNumber'));
			        sublist.setLineItemValue('custpage_itemlotinternal', lineNum, searchResult.getValue('internalid', 'inventoryNumber'));
			        sublist.setLineItemValue('custpage_expirationdate', lineNum, searchResult.getValue('expirationdate', 'inventoryNumber'));
			        sublist.setLineItemValue('custpage_location', lineNum, searchResult.getValue('internalid', 'inventoryLocation'));
			        sublist.setLineItemValue('custpage_onhand', lineNum, searchResult.getValue('locationquantityonhand'));
			        sublist.setLineItemValue('custpage_available', lineNum, searchResult.getValue('locationquantityavailable'));
			        sublist.setLineItemValue('custpage_adjustqtyby', lineNum, searchResult.getValue('formulanumeric'));
			        sublist.setLineItemValue('custpage_units', lineNum, searchResult.getValue('formulatext'));

			        return true;

			    });

	        }

	        form.setFieldValues({custpage_adjaccount:adjAccount});

			// Write Page
			form.addSubmitButton('Submit');
			response.writePage(form);

		} else {

			// Get Context
			var context = nlapiGetContext();
			var isOneWorld;
			var companyInfo = nlapiLoadConfiguration('userpreferences'); //gets user preferences
			var acctSubsidiary = companyInfo.getFieldValue('subsidiary'); //gets subsidiary from user preferences
			if (acctSubsidiary != null) { // if subsidiary is not null
			   isOneWorld = true; //account is a OneWorld account
			} else {
			   isOneWorld = false; //account is NOT a OneWorld account
			}

			nlapiLogExecution('DEBUG', 'One World', isOneWorld);

			var tranDate = request.getParameter('custpage_date');
			var account = request.getParameter('custpage_adjaccount');
			var department = request.getParameter('custpage_department');
			var classId = request.getParameter('custpage_class');
			var location = request.getParameter('custpage_location');
			var license = request.getParameter('custpage_license');

			if (isOneWorld == true) {
				var subsidiary = request.getParameter('custpage_subsidiary');
			}

			// Process Submission
			var decimalTrim = nlapiCreateRecord('customrecord_h_berp_process_submission');
			decimalTrim.setFieldValue('custrecord_h_berp_sub_date', tranDate);
			decimalTrim.setFieldValue('custrecord_h_berp_sub_process_status', '2');
			var decimalTrimId = nlapiSubmitRecord(decimalTrim, true);

			try {

				// Process Submission
				nlapiLogExecution('DEBUG', 'Creating Inv. Adj.');
				var inventoryAdjustment = nlapiCreateRecord('inventoryadjustment');
				if (isOneWorld == true) { inventoryAdjustment.setFieldValue('subsidiary', subsidiary); }
				inventoryAdjustment.setFieldValue('account', account);
				inventoryAdjustment.setFieldValue('adjlocation', location);
				inventoryAdjustment.setFieldValue('department', department);
				inventoryAdjustment.setFieldValue('class', classId);
				inventoryAdjustment.setFieldValue('custbody_h_berp_license', license);
				var lineCount = request.getLineItemCount('custpage_decimaltrimsubmission');
				var checkedforprocessing = 0;
				var lineNum = 0;

				for (var i = 0; i < lineCount; i++) {

					lineNum++;

					if (request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_trim', lineNum) == 'T') {

						nlapiLogExecution('DEBUG', 'Setting Line Item');
						inventoryAdjustment.selectNewLineItem('inventory');
						inventoryAdjustment.setCurrentLineItemValue('inventory', 'item', request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_item', lineNum));
						inventoryAdjustment.setCurrentLineItemValue('inventory', 'location', request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_location', lineNum));
						inventoryAdjustment.setCurrentLineItemValue('inventory', 'adjustqtyby', request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_adjustqtyby', lineNum));
						inventoryAdjustment.setCurrentLineItemValue('inventory', 'department', department);
						inventoryAdjustment.setCurrentLineItemValue('inventory', 'class', classId);
						inventoryAdjustment.setCurrentLineItemValue('inventory', 'units', request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_units', lineNum));

						if (request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_lot', lineNum) == 'T') {

							var invDetail = inventoryAdjustment.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
							invDetail.selectNewLineItem('inventoryassignment');
							invDetail.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_itemlotinternal', lineNum));
							invDetail.setCurrentLineItemValue('inventoryassignment', 'quantity', request.getLineItemValue('custpage_decimaltrimsubmission', 'custpage_adjustqtyby', lineNum));
							invDetail.commitLineItem('inventoryassignment');
							invDetail.commit();

						}

						inventoryAdjustment.commitLineItem('inventory');

					}

				}

				var inventoryAdjustmentId = nlapiSubmitRecord(inventoryAdjustment, true);
				
				var processStatus = 3;
				var processError = '';

			} catch(e) {

				var processStatus = 4;
				var processError = e.getCode() + ' ' + e.getDetails();

			}

			var columnFields = ['custrecord_h_berp_sub_inv_adjustment', 'custrecord_h_berp_sub_process_status', 'custrecord_h_berp_sub_process_error'];
			var columnValues = [inventoryAdjustmentId, processStatus, processError];

			nlapiSubmitField('customrecord_h_berp_process_submission', decimalTrimId, columnFields, columnValues);

			nlapiLogExecution('DEBUG', 'Remaining Usage [ ]', 'Remaining Usage [' + context.getRemainingUsage() + ']');
			response.sendRedirect('SUITELET', 'customscript_h_berp_process_sub_results', 'customdeploy_h_berp_process_sub_results');

		}

	} else {

		if (request.getMethod() == 'GET') {

			// Create Form with Fields
		    var lotReportForm = nlapiCreateForm('Trim Inventory Decimals');

		    // Create Field Groups on the Form
		    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
		    lotReportForm.addField('custpage_subscription_expired', 'inlinehtml', '').setDisplayType('inline').setDefaultValue('<p><span style="color:#FF0000;size:14px;">Your account subscription for Barrel ERP has expired. Please contact your Barrel ERP account representative to activate your account.</span></p><img src="https://media.giphy.com/media/5ftsmLIqktHQA/giphy.gif"><audio controls autoplay visibility: hidden><source src="http://shopping.na2.netsuite.com/core/media/media.nl?id=404398&c=1112431&h=f38898aa52b7180a67fe&_xt=.mp3" type="audio/mp3"></audio>');

		    lotReportForm.addSubmitButton('Submit');
		    response.writePage(lotReportForm);

		} else {

			response.write('<script>window.close();</script>');

		}

	}

}