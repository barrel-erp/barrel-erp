/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 2.0        14 Nov 2016     jbrox
 *
 */

function drainBrite(request, response) {

	var context = nlapiGetContext();
	var accountId = context.getSetting('SCRIPT', 'custscript_h_berp_ns_account_id');
	var activeAccounts = accountActiveCheck();
	nlapiLogExecution('DEBUG', 'Active Accounts', JSON.stringify(activeAccounts));
	nlapiLogExecution('DEBUG', 'Index', activeAccounts.indexOf(accountId));

    if (activeAccounts.indexOf(accountId) >= 0) {

		if (request.getMethod() == 'GET') {

			var context = nlapiGetContext();

			// OneWorld Check
			var isOneWorld;
			var companyInfo = nlapiLoadConfiguration('userpreferences'); //gets user preferences
			var acctSubsidiary = companyInfo.getFieldValue('subsidiary'); //gets subsidiary from user preferences
			if (acctSubsidiary != null) { // if subsidiary is not null
			   isOneWorld = true; //account is a OneWorld account
			} else {
			   isOneWorld = false; //account is NOT a OneWorld account
			}

			// Get Parameters
			var prodBatchId = request.getParameter('custparam_prodbatchid');
			var prodBatchLocation = request.getParameter('custparam_prodbatchlocation');
			var prodBatchLicense = '';
			var prodBatchSubsidiary = '';
			var origProdOrder = nlapiLookupField('customrecord_h_berp_prod_batch', prodBatchId, 'custrecord_h_berp_originating_prodord');
			if (origProdOrder) { var prodBatchLicense = nlapiLookupField('workorder', origProdOrder, 'custbody_h_berp_license'); }
			if (origProdOrder && isOneWorld == true) { var prodBatchSubsidiary = nlapiLookupField('workorder', origProdOrder, 'subsidiary'); }
			var adjAccount = context.getSetting('SCRIPT', 'custscript_h_berp_drain_adj_acct');

			// Set Various Variables
			var date = '';
			var location = '';
			var department = '';
			var classRec = '';

			if (request.getParameter('custparam_date')) { date = request.getParameter('custparam_date'); }
			if (request.getParameter('custparam_adjaccount')) { adjAccount = request.getParameter('custparam_adjaccount'); }
			if (request.getParameter('custparam_subsidiary')) { prodBatchSubsidiary = request.getParameter('custparam_subsidiary'); }
			if (request.getParameter('custparam_location')) { location = request.getParameter('custparam_location'); }
			if (request.getParameter('custparam_department')) { department = request.getParameter('custparam_department'); }
			if (request.getParameter('custparam_class')) { classRec = request.getParameter('custparam_class'); }
			if (request.getParameter('custparam_license')) { prodBatchLicense = request.getParameter('custparam_license'); }
			
			// Set various variables
			var defaultdate = new Date(); defaultdate = nlapiDateToString(defaultdate);
			var userSubsidiary = nlapiGetContext().getSubsidiary();
			nlapiLogExecution('DEBUG', 'Users Subsidiary', userSubsidiary);
			var line = 0;


			// Create the form and add fields to it 
			var form = nlapiCreateForm('Drain Brite Tank');

			// Create the Field Groups
			var primary = form.addFieldGroup('primary', 'Primary Information');
			var classification = form.addFieldGroup('classification', 'Classification');
			var compliance = form.addFieldGroup('compliance', 'Compliance');
			var itemOptions = form.addFieldGroup('itemfilters', 'Item Filters');
			form.addField('custpage_date', 'date', 'Date', null, 'primary').setDefaultValue(defaultdate);
			form.addField('custpage_adjaccount', 'select', 'Adjustment Account', '-112', 'primary').setMandatory(true).setDefaultValue(adjAccount);
			form.addField('custpage_prodbatch', 'select', 'Production Batch', 'customrecord_h_berp_prod_batch', 'primary').setDisplayType('inline').setDefaultValue(prodBatchId);
			form.addField('custpage_location', 'select', 'Location', '-103', 'classification').setDisplayType('inline').setMandatory(true).setDefaultValue(location);
			if (isOneWorld == true) {

				form.addField('custpage_subsidiary', 'select', 'Subsidiary', '-117', 'classification').setDisplayType('inline').setMandatory(true).setDefaultValue(prodBatchSubsidiary);

			}
			form.addField('custpage_department', 'select', 'Department', '-102', 'classification').setDefaultValue(department);
			form.addField('custpage_class', 'select', 'Class', '-101', 'classification').setDefaultValue(classRec);
			form.addField('custpage_license', 'select', 'License', 'customrecord_h_berp_license', 'compliance').setDisplayType('inline').setMandatory(true).setDefaultValue(prodBatchLicense);
			
			// Create Sublist
			var sublist = form.addSubList('custpage_drainbrite', 'list', 'Brite Beer');
			sublist.addField('custpage_location', 'select', 'Location', '-103').setDisplayType('inline');
	        sublist.addField('custpage_item', 'select', 'Brite Beer', '-10').setDisplayType('inline');
	        // sublist.addField('custpage_type', 'text', 'Type').setDisplayType('inline');
	        // sublist.addField('custpage_itemtype', 'text', 'Item Type').setDisplayType('inline');
	        sublist.addField('custpage_itemlot', 'text', 'Lot Number').setDisplayType('inline');
	        sublist.addField('custpage_itemlotinternal', 'text', 'Lot Number').setDisplayType('hidden');
	        // sublist.addField('custpage_expirationdate', 'date', 'Expiration Date').setDisplayType('inline');
	        // sublist.addField('custpage_onhand', 'float', 'On Hand').setDisplayType('inline');
	        sublist.addField('custpage_available', 'float', 'Available').setDisplayType('inline');
	        // sublist.addField('custpage_adjustqtyby', 'float', 'Adjust Qty. By').setDisplayType('inline');
	        // sublist.addField('custpage_units', 'text', 'Base Unit Internal ID').setDisplayType('hidden');

	        nlapiLogExecution('DEBUG', 'Inventory Search Begin');
	        var briteBeer = searchBriteBeerToDrain(prodBatchId);

			if (briteBeer) {

				var lineNum = 0;

				for (var i = 0; i < briteBeer.length; i++) {

					lineNum++;
			        sublist.setLineItemValue('custpage_item', lineNum, briteBeer[i].getValue('item', null, 'GROUP'));
			        // sublist.setLineItemValue('custpage_itemtype', lineNum, briteBeer[i].getText('type', 'item'));
			       	// sublist.setLineItemValue('custpage_lot', lineNum, briteBeer[i].getValue('islotitem', 'item'));
			        sublist.setLineItemValue('custpage_itemlot', lineNum, briteBeer[i].getValue('inventorynumber', 'itemNumber', 'GROUP'));
			        sublist.setLineItemValue('custpage_itemlotinternal', lineNum, briteBeer[i].getValue('internalid', 'itemNumber', 'GROUP'));
			        // sublist.setLineItemValue('custpage_expirationdate', lineNum, briteBeer[i].getValue('expirationdate'));
			        sublist.setLineItemValue('custpage_location', lineNum, briteBeer[i].getValue('location', 'itemNumber', 'GROUP'));
			        // sublist.setLineItemValue('custpage_onhand', lineNum, briteBeer[i].getValue('quantityonhand'));
			        sublist.setLineItemValue('custpage_available', lineNum, briteBeer[i].getValue('quantityavailable', 'itemNumber', 'MAX') * -1);
			        // sublist.setLineItemValue('custpage_adjustqtyby', lineNum, briteBeer[i].getValue('formulanumeric'));
			        // sublist.setLineItemValue('custpage_units', lineNum, briteBeer[i].getValue('formulatext'));

				}

			}

			form.setFieldValues({custpage_prodbatch: prodBatchId, custpage_location: prodBatchLocation});

			// Write Page
			form.addSubmitButton('Submit');
			response.writePage(form);

		} else {

			// Get Context
			var context = nlapiGetContext();
			var isOneWorld;
			var companyInfo = nlapiLoadConfiguration('userpreferences'); //gets user preferences
			var acctSubsidiary = companyInfo.getFieldValue('subsidiary'); //gets subsidiary from user preferences
			if (acctSubsidiary != null) { // if subsidiary is not null
			   isOneWorld = true; //account is a OneWorld account
			} else {
			   isOneWorld = false; //account is NOT a OneWorld account
			}

			nlapiLogExecution('DEBUG', 'One World', isOneWorld);

			var prodBatchId = request.getParameter('custpage_prodbatch');
			var tranDate = request.getParameter('custpage_date');
			var account = request.getParameter('custpage_adjaccount');
			var department = request.getParameter('custpage_department');
			var classId = request.getParameter('custpage_class');
			var location = request.getParameter('custpage_location');
			var license = request.getParameter('custpage_license');

			if (isOneWorld == true) {
				var subsidiary = request.getParameter('custpage_subsidiary');
			}

			// Process Submission
			var drainBriteSubmission = nlapiCreateRecord('customrecord_h_berp_process_submission');
			drainBriteSubmission.setFieldValue('custrecord_h_berp_sub_date', tranDate);
			drainBriteSubmission.setFieldValue('custrecord_h_berp_sub_process_status', 2);
			drainBriteSubmission.setFieldValue('custrecord_h_berp_sub_process_type', 2);
			var drainBriteSubmissionId = nlapiSubmitRecord(drainBriteSubmission, true);

			try {

				// Process Submission
				nlapiLogExecution('DEBUG', 'Creating Inv. Adj.');
				var inventoryAdjustment = nlapiCreateRecord('inventoryadjustment');
				if (isOneWorld == true) { inventoryAdjustment.setFieldValue('subsidiary', subsidiary); }
				inventoryAdjustment.setFieldValue('account', account);
				inventoryAdjustment.setFieldValue('trandate', tranDate);
				inventoryAdjustment.setFieldValue('adjlocation', location);
				inventoryAdjustment.setFieldValue('department', department);
				inventoryAdjustment.setFieldValue('class', classId);
				inventoryAdjustment.setFieldValue('custbody_h_berp_license', license);
				inventoryAdjustment.setFieldValue('custbody_h_berp_prod_batch', prodBatchId);
				var lineCount = request.getLineItemCount('custpage_drainbrite');
				var checkedforprocessing = 0;
				var lineNum = 0;

				for (var i = 0; i < lineCount; i++) {

					lineNum++;

					nlapiLogExecution('DEBUG', 'Setting Line Item');
					inventoryAdjustment.selectNewLineItem('inventory');
					inventoryAdjustment.setCurrentLineItemValue('inventory', 'item', request.getLineItemValue('custpage_drainbrite', 'custpage_item', lineNum));
					inventoryAdjustment.setCurrentLineItemValue('inventory', 'location', request.getLineItemValue('custpage_drainbrite', 'custpage_location', lineNum));
					inventoryAdjustment.setCurrentLineItemValue('inventory', 'adjustqtyby', request.getLineItemValue('custpage_drainbrite', 'custpage_available', lineNum));
					inventoryAdjustment.setCurrentLineItemValue('inventory', 'department', department);
					inventoryAdjustment.setCurrentLineItemValue('inventory', 'class', classId);
					// inventoryAdjustment.setCurrentLineItemValue('inventory', 'units', request.getLineItemValue('custpage_drainbrite', 'custpage_units', lineNum));

					var invDetail = inventoryAdjustment.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
					invDetail.selectNewLineItem('inventoryassignment');
					invDetail.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', request.getLineItemValue('custpage_drainbrite', 'custpage_itemlotinternal', lineNum));
					invDetail.setCurrentLineItemValue('inventoryassignment', 'quantity', request.getLineItemValue('custpage_drainbrite', 'custpage_available', lineNum));
					invDetail.commitLineItem('inventoryassignment');
					invDetail.commit();

					inventoryAdjustment.commitLineItem('inventory');

				}

				var inventoryAdjustmentId = nlapiSubmitRecord(inventoryAdjustment, true);
				
				nlapiSubmitField('customrecord_h_berp_prod_batch', prodBatchId, 'custrecord_h_berp_prod_drain_brite_comp', 'T');
				var processStatus = 3;
				var processError = '';

			} catch(e) {

				var processStatus = 4;
				var processError = e.getCode() + ' ' + e.getDetails();

			}

			var columnFields = ['custrecord_h_berp_sub_inv_adjustment', 'custrecord_h_berp_sub_process_status', 'custrecord_h_berp_sub_process_error'];
			var columnValues = [inventoryAdjustmentId, processStatus, processError];

			nlapiSubmitField('customrecord_h_berp_process_submission', drainBriteSubmissionId, columnFields, columnValues);

			nlapiLogExecution('DEBUG', 'Remaining Usage [ ]', 'Remaining Usage [' + context.getRemainingUsage() + ']');
			// response.sendRedirect('SUITELET', 'customscript_h_berp_process_sub_results', 'customdeploy_h_berp_process_sub_results');
			response.write('<script>window.close();</script>');

		}

	} else {

		if (request.getMethod() == 'GET') {

			// Create Form with Fields
		    var lotReportForm = nlapiCreateForm('Drain Brite Tank');

		    // Create Field Groups on the Form
		    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
		    lotReportForm.addField('custpage_subscription_expired', 'inlinehtml', '').setDisplayType('inline').setDefaultValue('<p><span style="color:#FF0000;size:14px;">Your account subscription for Barrel ERP has expired. Please contact your Barrel ERP account representative to activate your account.</span></p><img src="https://media.giphy.com/media/5ftsmLIqktHQA/giphy.gif"><audio controls autoplay visibility: hidden><source src="http://shopping.na2.netsuite.com/core/media/media.nl?id=404398&c=1112431&h=f38898aa52b7180a67fe&_xt=.mp3" type="audio/mp3"></audio>');

		    lotReportForm.addSubmitButton('Submit');
		    response.writePage(lotReportForm);

		} else {

			response.write('<script>window.close();</script>');

		}

	}

}