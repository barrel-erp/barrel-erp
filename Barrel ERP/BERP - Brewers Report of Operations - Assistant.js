function brewersReport(request, response) {

	// Get Context and Settings
	var context = nlapiGetContext();
	var companyInfo = nlapiLoadConfiguration('companyinformation');
	var companyEIN = companyInfo.getFieldValue('employerid');
	var companyName = companyInfo.getFieldValue('legalname');
	nlapiLogExecution('DEBUG', 'EIN', companyEIN);
	nlapiLogExecution('DEBUG', 'getSessionObject', JSON.stringify(context.getSessionObject));
	
	nlapiLogExecution('DEBUG', '5f', (context.getSessionObject('5f')));
	
	//Adding New search mapping

	var box2b, box6c, box6e, box7d, box7f, box9d, box10f, box14d, box14f, box15d, box15f, box21d, box21f, box22b, box23b, box25c, box26e, box27b, box27c, box27d, box27e, box27f, box28b;
	var box28c, box28d, box28e, box28f;

	var mapSearches = nlapiSearchRecord("customrecord_h_berp_bro_mapping",null,
	[
	   ["isinactive","is","F"]
	], 
	[
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_10f",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_14d",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_14f",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_15d",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_15f",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_21d",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_21f",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_22b",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_23b",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_25c",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_26e",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_27b",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_27c",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_27d",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_27e",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_27f",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_28b",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_28c",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_28d",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_28e",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_28f",null,null), 
	   new nlobjSearchColumn("custrecord_h_bro_map_2b",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_6c",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_6e",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_7d",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_7f",null,null), 
	   new nlobjSearchColumn("custrecord_h_berp_bro_map_9d",null,null)
	]
	);
	nlapiLogExecution('DEBUG', 'mapSearches', JSON.stringify(mapSearches));

	if ( mapSearches ) {

		box2b = mapSearches[0].getValue("custrecord_h_bro_map_2b");
		box6c = mapSearches[0].getValue("custrecord_h_berp_bro_map_6c");
		box6e = mapSearches[0].getValue("custrecord_h_berp_bro_map_6e");
		box7d = mapSearches[0].getValue("custrecord_h_berp_bro_map_7d");
		box7f = mapSearches[0].getValue("custrecord_h_berp_bro_map_7f");
		box9d = mapSearches[0].getValue("custrecord_h_berp_bro_map_9d");
		box10f = mapSearches[0].getValue("custrecord_h_berp_bro_map_10f");
		box14d = mapSearches[0].getValue("custrecord_h_berp_bro_map_14d");
		box14f = mapSearches[0].getValue("custrecord_h_berp_bro_map_14f");
		box15d = mapSearches[0].getValue("custrecord_h_berp_bro_map_15d");
		box15f = mapSearches[0].getValue("custrecord_h_berp_bro_map_15f");
		box21d = mapSearches[0].getValue("custrecord_h_berp_bro_map_21d");
		box21f = mapSearches[0].getValue("custrecord_h_berp_bro_map_21f");
		box22b = mapSearches[0].getValue("custrecord_h_berp_bro_map_22b");
		box23b = mapSearches[0].getValue("custrecord_h_berp_bro_map_23b");
		box25c = mapSearches[0].getValue("custrecord_h_berp_bro_map_25c");
		box26e = mapSearches[0].getValue("custrecord_h_berp_bro_map_26e");
		box27b = mapSearches[0].getValue("custrecord_h_berp_bro_map_27b");
		box27c = mapSearches[0].getValue("custrecord_h_berp_bro_map_27c");
		box27d = mapSearches[0].getValue("custrecord_h_berp_bro_map_27d");
		box27e = mapSearches[0].getValue("custrecord_h_berp_bro_map_27e");
		box27f = mapSearches[0].getValue("custrecord_h_berp_bro_map_27f");
		box28b = mapSearches[0].getValue("custrecord_h_berp_bro_map_28b");
		box28c = mapSearches[0].getValue("custrecord_h_berp_bro_map_28c");
		box28d = mapSearches[0].getValue("custrecord_h_berp_bro_map_28d");
		box28e = mapSearches[0].getValue("custrecord_h_berp_bro_map_28e");
		box28f = mapSearches[0].getValue("custrecord_h_berp_bro_map_28f");


	}
nlapiLogExecution('AUDIT', 'BOX 2B', box2b); 
	//end search for mappings

	// Create brewersReportAssistant
	var brewersReportAssistant = nlapiCreateAssistant('Brewer\'s Report of Operations', false);
	brewersReportAssistant.setScript('customscript_h_berp_brewers_report_cli');
	brewersReportAssistant.setOrdered(true);
 	brewersReportAssistant.addStep('breweryinformation', 'Setup Brewery Info').setHelpText("Setup your brewery information in the fields below.");
	brewersReportAssistant.addStep('reportingperiod', 'Select Reporting Period').setHelpText("Select the reporting period for this report.");
	brewersReportAssistant.addStep('partonebeersummary', 'Part 1 - Additions').setHelpText("Additions to beer inventory (round your entries to the nearest second decimal).");
	brewersReportAssistant.addStep('partonebeersummaryremovals', 'Part 1 - Removals').setHelpText("Removals from beer inventory (round your entries to the nearest second decimal.");
	brewersReportAssistant.addStep('priorperiodadj', 'Prior Period Adjustments').setHelpText("Under penalties of perjury I declare that this report is supported by true, complete, and correct records that are available for inspection at my brewery. I have examined this report and to the best of my knowledge and belief it is true, complete, and correct.");
	// brewersReportAssistant.addStep('parttwotax', 'Part 2 - Tax Payments' ).setHelpText("(See Instructions - Part 2).");
	// brewersReportAssistant.addStep('partthreematerials', 'Part 3 - Materials Used' );
	brewersReportAssistant.addStep('partfourcereal', 'Part 2 - Cereal Summary' ).setHelpText("Products that are less than 0.5% alcohol by volume.");
	brewersReportAssistant.addStep('partfiveremarks', 'Part 3 - Remarks').setHelpText("Add remarks below or on a separate piece of paper attached to this form.");
	brewersReportAssistant.addStep('printreport', 'Print TTB Report').setHelpText("Print the TTB Report. To generate the TTB Brewer Report of Operations, first download the .pdf file linked below. Once its downloaded, download the .fdf file. Open the .fdf file with the .pdf file downloaded previously and choose Options > Trust this Document Always. The fields will be populated. ");
 	brewersReportAssistant.addStep('savereport', 'Save Report').setHelpText("Save the Brewer's Report of Operations. Click Finish to save record.");

	if (request.getMethod() == 'GET') {
		
		// Check whether the Assistant is finished
		if (!brewersReportAssistant.isFinished()) {
			
			// If initial step, set the Splash page and set the intial step
			if (brewersReportAssistant.getCurrentStep() == null) {
				
				brewersReportAssistant.setCurrentStep(brewersReportAssistant.getStep('breweryinformation'));
				brewersReportAssistant.setSplash("Welcome to the Brewer's Report of Operations Assistant!", "<b>What you'll be doing</b><br>The Brewer's Report of Operations Assistant will walk you through the process of filing your operations report.", "<b>When you finish</b><br>you will be able to print the report as the exact pdf file from TTB.");
			}
			
			var step = brewersReportAssistant.getCurrentStep();
 
			// Create Brewery Information step
			if (step.getName() == 'breweryinformation')	{

				brewersReportAssistant.addFieldGroup('licenseinfo', 'Select Brewery License');
				
				brewersReportAssistant.addField('brewerylicense', 'select', 'License', 'customrecord_h_berp_license', 'licenseinfo').setMandatory(true);

				// var breweryLocations = searchBreweryLocations();

				// brewersReportAssistant.addFieldGroup('selectbrewery', 'Select Brewery Location');
				// brewersReportAssistant.addField('brewerylocationlabel', 'label', 'Brewery Location', null, 'selectbrewery').setLayoutType('startrow');

				// for (var b = 0; b < breweryLocations.length; b++) {
				
				// 	brewersReportAssistant.addField('brewerylocation', 'radio', breweryLocations[b].getValue('namenohierarchy'), breweryLocations[b].getValue('namenohierarchy'), 'selectbrewery');

				// }

				brewersReportAssistant.addFieldGroup('breweryinfo', 'Brewery Information');
				brewersReportAssistant.addField('breweryein', 'text', 'Our Brewery EIN is', null, 'breweryinfo').setMandatory(true);
				brewersReportAssistant.addField('breweryttbnumber', 'text', 'Our TTB Brewery Number is', null, 'breweryinfo').setMandatory(true);
				brewersReportAssistant.addField('breweryphone', 'phone', 'TTB can reach us by telephone at', null, 'breweryinfo').setMandatory(true);
				brewersReportAssistant.addField('breweryname', 'text', 'What is your brewery\'s name?', null, 'breweryinfo').setMandatory(true);
				brewersReportAssistant.addField('brewerydba', 'text', 'DBA', null, 'breweryinfo').setMandatory(true);

				brewersReportAssistant.addFieldGroup('breweryaddress', 'Brewery Address');
				brewersReportAssistant.addField('breweryaddr1', 'text', 'Number and Street', null, 'breweryaddress').setMandatory(true);
				brewersReportAssistant.addField('brewerycity', 'text', 'City', null, 'breweryaddress').setMandatory(true);
				brewersReportAssistant.addField('brewerycounty', 'text', 'County', null, 'breweryaddress').setMandatory(true);
				brewersReportAssistant.addField('brewerystate', 'text', 'State', null, 'breweryaddress').setMandatory(true);
				brewersReportAssistant.addField('breweryzipcode', 'text', 'ZIP Code', null, 'breweryaddress').setMandatory(true);

				// brewersReportAssistant.addFieldGroup('customerinfo', '3rd Party Customer Information');
				// brewersReportAssistant.addField('customer', 'select', 'Customer', '-2', 'customerinfo');
				
			}
 
			if (step.getName() == 'reportingperiod') {

				var currentYear = nlapiDateToString(new Date());
				currentYear = currentYear.slice((currentYear.length - 4), currentYear.length);
 
				brewersReportAssistant.addFieldGroup('reportperiod', 'Reporting Period');
				brewersReportAssistant.addField('monthlyorquarterly', 'select', 'Report Type', 'customlist_h_berp_report_type', 'reportperiod').setMandatory(true);
				brewersReportAssistant.addField('reportingyear', 'text', 'Reporting Period (Enter Year)', null, 'reportperiod').setMandatory(true).setDefaultValue(currentYear);
				brewersReportAssistant.addField('reportingmonth', 'text', 'Monthly Report for (Enter Month)', null, 'reportperiod').setDefaultValue(getPriorMonth());
				brewersReportAssistant.addField('reportingperiod', 'select', 'Reporting Period', '-105', 'reportperiod');
				brewersReportAssistant.addField('quarterstartdate', 'date', 'Quarter Start Date', null, 'reportperiod');
				brewersReportAssistant.addField('quarterenddate', 'date', 'Quarter End Date', null, 'reportperiod');

				var previouslyFiled = searchPreviouslyFiled(context.getSessionObject('brewerylicense'));
				nlapiLogExecution('DEBUG', 'Previously Filed', context.getSessionObject('brewerylicense'));

				if (previouslyFiled) {

					var previousReport = previouslyFiled[0].getValue('internalid');
					nlapiLogExecution('DEBUG', 'Previous Report', previousReport);

				}

				brewersReportAssistant.addFieldGroup('previouslyfiledreport', 'Previously Filed Report');
				brewersReportAssistant.addField('previousreport', 'select', 'Previous Report', 'customrecord_h_berp_brewers_report', 'previouslyfiledreport').setMandatory(true).setDefaultValue(previousReport);
				
			}
			
			if (step.getName() == 'partonebeersummary') {
				
				var sublist = brewersReportAssistant.addSubList("additionstobeerinv", "inlineeditor", "Additions to Beer Inventory");
 
				sublist.addField("operations", "text", "Operations").setDisplayType('disabled');
				// sublist.addField("viewsearch", "select", "Search Name", "-119").setDisplayType('disabled');
				sublist.addField("cellar", "float", "Cellar (b)");
				sublist.addField("bulk", "float", "Racking - Bulk (c)");
				sublist.addField("keg", "float", "Racking - Keg (d)");
				sublist.addField("bulkb", "float", "Bottling - Bulk (e)");
				sublist.addField("case", "float", "Bottling - Case (f)");
				sublist.addField("totals", "float", "Totals (g)");
 				sublist.setUniqueField("operations");
 				var linenum = 0;

 				var additions = [

 					"1. On hand beginning of this report period", 
 					"2. We produced by fermentation", 
 					"3. We added water and other liquids in cellar operations", 
 					"4. Beer received from racking and bottling", 
 					"5. Beer received in bond from other breweries and pilot brewing plants of same ownership",
 					"6. Beer received from cellars",
 					"7. Beer returned to this brewery after removal from this brewery",
 					"8. Beer returned to this brewery after removal from another brewery of the same ownership",
 					"9. Racked",
 					"10. Bottled",
 					"11. Physical inventory disclosed an overage",
 					"12. ",
 					"13. Total Beer"

 				];

 				var index = [

 				]

 				for (var i = 0; i < additions.length; i++) {

 					linenum = 1 + i;
 					sublist.setLineItemValue('operations', linenum, additions[i]);

 					if (linenum == 1) {

 						var previousReportData = searchPreviouslyFiled(null, context.getSessionObject('previousreport'));
 						nlapiLogExecution('DEBUG', 'Previous Report | Session Object', JSON.stringify(previousReportData) + ' | ' + context.getSessionObject('previousreport'));

 						if (previousReportData) {

 							if (parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_cellar'))) { var cellarQty = parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_cellar')); } else { var cellarQty = 0; }
 							if (parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_rack_b'))) { var bulkQty = parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_rack_b')); } else { var bulkQty = 0; }
 							if (parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_rack_keg'))) { var kegQty = parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_rack_keg')); } else { var kegQty = 0; }
 							if (parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_bottle_b'))) { var bulkBQty = parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_bottle_b')); } else { var bulkBQty = 0; }
 							if (parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_bottle_case'))) { var bottleQty = parseFloat(previousReportData[0].getValue('custrecord_h_berp_bonh_bottle_case')); } else { var bottleQty = 0; }
 							var totalQty = parseFloat(cellarQty) + parseFloat(bulkQty) + parseFloat(kegQty) + parseFloat(bottleQty) + parseFloat(bulkBQty);

 							// sublist.setLineItemValue('viewsearch', linenum, null);
	 						sublist.setLineItemValue('cellar', linenum, cellarQty.toFixed(2));
	 						sublist.setLineItemValue('bulk', linenum, bulkQty.toFixed(2));
	 						sublist.setLineItemValue('keg', linenum, kegQty.toFixed(2));
	 						sublist.setLineItemValue('bulkb', linenum, bulkBQty.toFixed(2));
	 						sublist.setLineItemValue('case', linenum, bottleQty.toFixed(2));
		 					sublist.setLineItemValue('totals', linenum, totalQty.toFixed(2));

 						} else {

 							if (context.getSessionObject('1b')) { var cellarQty = parseFloat(context.getSessionObject('1b')); } else { var cellarQty = 0; }
 							if (context.getSessionObject('1c')) { var bulkQty = parseFloat(context.getSessionObject('1c')); } else { var bulkQty = 0; }
 							if (context.getSessionObject('1d')) { var kegQty = parseFloat(context.getSessionObject('1d')); } else { var kegQty = 0; }
 							if (context.getSessionObject('1e')) { var bulkBQty = parseFloat(context.getSessionObject('1e')); } else { var bulkBQty = 0; }
 							if (context.getSessionObject('1f')) { var bottleQty = parseFloat(context.getSessionObject('1f')); } else { var bottleQty = 0; }
 							var totalQty = parseFloat(cellarQty) + parseFloat(bulkQty) + parseFloat(kegQty) + parseFloat(bottleQty) + parseFloat(bulkBQty);

 							sublist.setLineItemValue('cellar', linenum, cellarQty.toFixed(2));
	 						sublist.setLineItemValue('bulk', linenum, bulkQty.toFixed(2));
	 						sublist.setLineItemValue('keg', linenum, kegQty.toFixed(2));
	 						sublist.setLineItemValue('bulkb', linenum, bulkBQty.toFixed(2));
	 						sublist.setLineItemValue('case', linenum, bottleQty.toFixed(2));
		 					sublist.setLineItemValue('totals', linenum, totalQty.toFixed(2));

 						}

 					}
 					
 					if (linenum == 2) {

 						//var beerProducedByFerm = nlapiLoadSearch('transaction', 'customsearch_h_berp_brewers_report_ferm');
                      nlapiLogExecution('AUDIT', 'BOX 2B', box2b); 
 						var beerProducedByFerm = nlapiLoadSearch('transaction', box2b );
 						var filters = beerProducedByFerm.getFilters();
 						var columns = beerProducedByFerm.getColumns();
 						beerProducedByFerm = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerProducedByFerm.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerProducedByFerm.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerProducedByFerm.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerProducedByFerm.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerProducedByFerm = beerProducedByFerm.runSearch();
 						nlapiLogExecution('DEBUG', '2B Search Length', beerProducedByFerm.length);
 						nlapiLogExecution('DEBUG', '2B Search Results',beerProducedByFerm);
 						var fermenttotal = 0;
 						beerProducedByFerm.forEachResult(function(searchResult) { fermenttotal = parseFloat(fermenttotal) + parseFloat(searchResult.getValue('quantity', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '54');
 						if (!context.getSessionObject('2b')) { sublist.setLineItemValue('cellar', linenum, fermenttotal.toFixed(2)); } else { fermenttotal = parseFloat(context.getSessionObject('2b')); sublist.setLineItemValue('cellar', linenum, fermenttotal.toFixed(2)); }

	 					var fermentationTotal = parseFloat(fermenttotal);
	 					sublist.setLineItemValue('totals', linenum, fermentationTotal.toFixed(2));

 					} else if (linenum == 3) {

	 					if (context.getSessionObject('3b')) { var addedWaterCellar = parseFloat(context.getSessionObject('3b')); sublist.setLineItemValue('cellar', linenum, addedWaterCellar.toFixed(2)); } else { var addedWaterCellar = 0; }

	 					var addedWaterTotal = parseFloat(addedWaterCellar);
	 					sublist.setLineItemValue('totals', linenum, addedWaterTotal.toFixed(2));

 					} else if (linenum == 4) {

	 					if (context.getSessionObject('4b')) { var receivedCellar = parseFloat(context.getSessionObject('4b')); sublist.setLineItemValue('cellar', linenum, receivedCellar.toFixed(2)); } else { var receivedCellar = 0; }

	 					var receivedTotal = parseFloat(receivedCellar);
	 					sublist.setLineItemValue('totals', linenum, receivedTotal.toFixed(2));

 					} else if (linenum == 5) {

	 					if (context.getSessionObject('5b')) { var receivedBondCellar = parseFloat(context.getSessionObject('5b')); sublist.setLineItemValue('cellar', linenum, receivedBondCellar.toFixed(2)); } else { var receivedBondCellar = 0; }
	 					if (context.getSessionObject('5d')) { var receivedBondKeg = parseFloat(context.getSessionObject('5d')); sublist.setLineItemValue('keg', linenum, receivedBondKeg.toFixed(2)); } else { var receivedBondKeg = 0; }
	 					if (context.getSessionObject('5f')) { var receivedBondCase = parseFloat(context.getSessionObject('5f')); sublist.setLineItemValue('case', linenum, receivedBondCase.toFixed(2)); } else { var receivedBondCase = 0; }

	 					var receivedBondTotal = parseFloat(receivedBondCellar) + parseFloat(receivedBondKeg) + parseFloat(receivedBondCase);
	 					sublist.setLineItemValue('totals', linenum, receivedBondTotal.toFixed(2));

 					} else if (linenum == 6) {

 						//var beerReceivedFromCellarsKegLine = nlapiLoadSearch('transaction', 'customsearch_h_berp_brewers_report_cella');
 						var beerReceivedFromCellarsKegLine = nlapiLoadSearch('transaction', box6c);
 						var filters = beerReceivedFromCellarsKegLine.getFilters();
 						var columns = beerReceivedFromCellarsKegLine.getColumns();
 						beerReceivedFromCellarsKegLine = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerReceivedFromCellarsKegLine = beerReceivedFromCellarsKegLine.runSearch();
 						var beerRackBulk = 0;
 						beerReceivedFromCellarsKegLine.forEachResult(function(searchResult) { beerRackBulk = parseFloat(beerRackBulk) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerRackBulk)) { beerRackBulk = 0; }
 						if (!context.getSessionObject('6c')) { sublist.setLineItemValue('bulk', linenum, beerRackBulk.toFixed(2)); } else { beerRackBulk = parseFloat(context.getSessionObject('6c')); sublist.setLineItemValue('bulk', linenum, beerRackBulk.toFixed(2)); }
 						
 						//var beerReceivedFromCellarsBottleLine = nlapiLoadSearch('transaction', 'customsearch_h_berp_brewers_report_bline');
 						var beerReceivedFromCellarsBottleLine = nlapiLoadSearch('transaction', box6e );
 						var filters = beerReceivedFromCellarsBottleLine.getFilters();
 						var columns = beerReceivedFromCellarsBottleLine.getColumns();
 						beerReceivedFromCellarsBottleLine = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') {

 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerReceivedFromCellarsBottleLine = beerReceivedFromCellarsBottleLine.runSearch();
 						var beerBottleBulk = 0;
 						beerReceivedFromCellarsBottleLine.forEachResult(function(searchResult) { beerBottleBulk = parseFloat(beerBottleBulk) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerBottleBulk)) { beerBottleBulk = 0; }
 						if (!context.getSessionObject('6e')) { sublist.setLineItemValue('bulkb', linenum, beerBottleBulk.toFixed(2)); } else { beerBottleBulk = parseFloat(context.getSessionObject('6e')); sublist.setLineItemValue('bulkb', linenum, beerBottleBulk.toFixed(2)); }

 						if (context.getSessionObject('6b')) { var receivedCellarCellar = parseFloat(context.getSessionObject('6b')); sublist.setLineItemValue('cellar', linenum, receivedCellarCellar.toFixed(2)); } else { var receivedCellarCellar = 0; }

	 					var beerReceivedTotal = parseFloat(beerRackBulk) + parseFloat(beerBottleBulk) + parseFloat(receivedCellarCellar);
	 					sublist.setLineItemValue('totals', linenum, beerReceivedTotal.toFixed(2));

 					} else if (linenum == 7) {

 						//var beerReturnedToBreweryKegLine = nlapiLoadSearch('transaction', 'customsearch_h_berp_br_beer_return');
 						var beerReturnedToBreweryKegLine = nlapiLoadSearch('transaction', box7d);
 						var filters = beerReturnedToBreweryKegLine.getFilters();
 						var columns = beerReturnedToBreweryKegLine.getColumns();
 						beerReturnedToBreweryKegLine = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerReturnedToBreweryKegLine.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerReturnedToBreweryKegLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerReturnedToBreweryKegLine.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerReturnedToBreweryKegLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerReturnedToBreweryKegLine = beerReturnedToBreweryKegLine.runSearch();
 						var beerReturnedKeg = 0;
 						beerReturnedToBreweryKegLine.forEachResult(function(searchResult) { beerReturnedKeg = parseFloat(beerReturnedKeg) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); nlapiLogExecution('AUDIT', 'Formula Numeric', searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerReturnedKeg)) { beerReturnedKeg = 0; }
 						if (!context.getSessionObject('7d')) { sublist.setLineItemValue('keg', linenum, beerReturnedKeg.toFixed(2)); } else { beerReturnedKeg = parseFloat(context.getSessionObject('7d')); sublist.setLineItemValue('keg', linenum, beerReturnedKeg.toFixed(2)); }
 						
 						//var beerReturnedToBreweryBottleCase = nlapiLoadSearch('transaction', 'customsearch_h_berp_br_beer_return_ca');
 						var beerReturnedToBreweryBottleCase = nlapiLoadSearch('transaction', box7f);
 						var filters = beerReturnedToBreweryBottleCase.getFilters();
 						var columns = beerReturnedToBreweryBottleCase.getColumns();
 						beerReturnedToBreweryBottleCase = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerReturnedToBreweryBottleCase.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerReturnedToBreweryBottleCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerReturnedToBreweryBottleCase.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerReturnedToBreweryBottleCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerReturnedToBreweryBottleCase = beerReturnedToBreweryBottleCase.runSearch();
 						var beerReturnedCase = 0;
 						beerReturnedToBreweryBottleCase.forEachResult(function(searchResult) { beerReturnedCase = parseFloat(beerReturnedCase) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerReturnedCase)) { beerReturnedCase = 0; }
 						if (!context.getSessionObject('7f')) { sublist.setLineItemValue('case', linenum, beerReturnedCase.toFixed(2)); } else { beerReturnedCase = parseFloat(context.getSessionObject('7f')); sublist.setLineItemValue('case', linenum, beerReturnedCase.toFixed(2)); }

 						if (context.getSessionObject('7b')) { var returnedCellar = parseFloat(context.getSessionObject('7b')); sublist.setLineItemValue('cellar', linenum, returnedCellar.toFixed(2)); } else { var returnedCellar = 0; }

	 					var beerReturnedTotal = parseFloat(beerReturnedKeg) + parseFloat(beerReturnedCase) + parseFloat(returnedCellar);
	 					sublist.setLineItemValue('totals', linenum, beerReturnedTotal.toFixed(2));

 					} else if (linenum == 8) {

 						if (context.getSessionObject('8b')) { var returnedAnotherCellar = parseFloat(context.getSessionObject('8b')); sublist.setLineItemValue('cellar', linenum, returnedAnotherCellar.toFixed(2)); } else { var returnedAnotherCellar = 0; }
	 					if (context.getSessionObject('8d')) { var returnedAnotherKeg = parseFloat(context.getSessionObject('8d')); sublist.setLineItemValue('keg', linenum, returnedAnotherKeg.toFixed(2)); } else { var returnedAnotherKeg = 0; }
	 					if (context.getSessionObject('8f')) { var returnedAnotherCase = parseFloat(context.getSessionObject('8f')); sublist.setLineItemValue('case', linenum, returnedAnotherCase.toFixed(2)); } else { var returnedAnotherCase = 0; }

	 					var returnedAnotherTotal = parseFloat(returnedAnotherCellar) + parseFloat(returnedAnotherKeg) + parseFloat(returnedAnotherCase);
	 					sublist.setLineItemValue('totals', linenum, returnedAnotherTotal.toFixed(2));

 					} else if (linenum == 9) {

 						//var beerRacked = nlapiLoadSearch('transaction', 'customsearch_h_berp_brewers_report_rack');
 						var beerRacked = nlapiLoadSearch('transaction', box9d);
 						var filters = beerRacked.getFilters();
 						var columns = beerRacked.getColumns();
 						beerRacked = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerRacked.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerRacked.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerRacked.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerRacked.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerRacked = beerRacked.runSearch();
 						var beerRackedQty = 0;
 						beerRacked.forEachResult(function(searchResult) { beerRackedQty = parseFloat(beerRackedQty) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerRackedQty)) { beerRackedQty = 0; }
 						if (!context.getSessionObject('9d')) { sublist.setLineItemValue('keg', linenum, beerRackedQty.toFixed(2)); } else { beerRackedQty = parseFloat(context.getSessionObject('9d')); sublist.setLineItemValue('keg', linenum, beerRackedQty.toFixed(2)); }

	 					var beerRackedTotal = parseFloat(beerRackedQty);
	 					sublist.setLineItemValue('totals', linenum, beerRackedTotal.toFixed(2));

 					} else if (linenum == 10) {

 						//var beerBottled = nlapiLoadSearch('transaction', 'customsearch_h_berp_brewers_report_bot');
 						var beerBottled = nlapiLoadSearch('transaction', box10f );
 						var filters = beerBottled.getFilters();
 						var columns = beerBottled.getColumns();
 						beerBottled = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerBottled.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerBottled.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerBottled.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerBottled.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerBottled = beerBottled.runSearch();
 						var beerBottledQty = 0;
 						beerBottled.forEachResult(function(searchResult) { beerBottledQty = parseFloat(beerBottledQty) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerBottledQty)) { beerBottledQty = 0; }
 						if (!context.getSessionObject('10f')) { sublist.setLineItemValue('case', linenum, beerBottledQty.toFixed(2)); } else { beerBottledQty = parseFloat(context.getSessionObject('10f')); sublist.setLineItemValue('case', linenum, beerBottledQty.toFixed(2)); }

	 					var beerBottledTotal = parseFloat(beerBottledQty);
	 					sublist.setLineItemValue('totals', linenum, beerBottledTotal.toFixed(2));

 					} else if (linenum == 11) {

 						if (context.getSessionObject('11b')) { var overageCellar = parseFloat(context.getSessionObject('11b')); sublist.setLineItemValue('cellar', linenum, overageCellar.toFixed(2)); } else { var overageCellar = 0; }
	 					if (context.getSessionObject('11c')) { var overageBulk = parseFloat(context.getSessionObject('11c')); sublist.setLineItemValue('bulk', linenum, overageBulk.toFixed(2)); } else { var overageBulk = 0; }
	 					if (context.getSessionObject('11d')) { var overageKeg = parseFloat(context.getSessionObject('11d')); sublist.setLineItemValue('keg', linenum, overageKeg.toFixed(2)); } else { var overageKeg = 0; }
	 					if (context.getSessionObject('11e')) { var overageBulkB = parseFloat(context.getSessionObject('11e')); sublist.setLineItemValue('bulkb', linenum, overageBulkB.toFixed(2)); } else { var overageBulkB = 0; }
	 					if (context.getSessionObject('11f')) { var overageCase = parseFloat(context.getSessionObject('11f')); sublist.setLineItemValue('case', linenum, overageCase.toFixed(2)); } else { var overageCase = 0; }

	 					var overageTotal = parseFloat(overageCellar) + parseFloat(overageBulk) + parseFloat(overageKeg) + parseFloat(overageBulkB) + parseFloat(overageCase);
	 					sublist.setLineItemValue('totals', linenum, overageTotal.toFixed(2));

 					} else if (linenum == 12) {

 						if (context.getSessionObject('12b')) { var blankCellar = parseFloat(context.getSessionObject('12b')); sublist.setLineItemValue('cellar', linenum, blankCellar.toFixed(2)); } else { var blankCellar = 0; }
	 					if (context.getSessionObject('12c')) { var blankBulk = parseFloat(context.getSessionObject('12c')); sublist.setLineItemValue('bulk', linenum, blankBulk.toFixed(2)); } else { var blankBulk = 0; }
	 					if (context.getSessionObject('12d')) { var blankKeg = parseFloat(context.getSessionObject('12d')); sublist.setLineItemValue('keg', linenum, blankKeg.toFixed(2)); } else { var blankKeg = 0; }
	 					if (context.getSessionObject('12e')) { var blankBulkB = parseFloat(context.getSessionObject('12e')); sublist.setLineItemValue('bulkb', linenum, blankBulkB.toFixed(2)); } else { var blankBulkB = 0; }
	 					if (context.getSessionObject('12f')) { var blankCase = parseFloat(context.getSessionObject('12f')); sublist.setLineItemValue('case', linenum, blankCase.toFixed(2)); } else { var blankCase = 0; }

	 					var overageTotal = parseFloat(blankCellar) + parseFloat(blankBulk) + parseFloat(blankKeg) + parseFloat(blankBulkB) + parseFloat(blankCase);
	 					sublist.setLineItemValue('totals', linenum, overageTotal.toFixed(2));

 					} else if (linenum == 13) {

 						var cellarTotal = parseFloat(cellarQty) + parseFloat(fermenttotal) + parseFloat(addedWaterCellar) + parseFloat(receivedCellar) + parseFloat(receivedBondCellar) + parseFloat(receivedCellarCellar) + parseFloat(returnedCellar) + parseFloat(returnedAnotherCellar) + parseFloat(overageCellar) + parseFloat(blankCellar);
 						sublist.setLineItemValue('cellar', linenum, cellarTotal.toFixed(2));
 						var bulkTotal = parseFloat(bulkQty) + parseFloat(beerRackBulk) + parseFloat(overageBulk) + parseFloat(blankBulk);
	 					sublist.setLineItemValue('bulk', linenum, bulkTotal.toFixed(2));
	 					var kegTotal = parseFloat(kegQty) + parseFloat(receivedBondKeg) + parseFloat(beerReturnedKeg) + parseFloat(returnedAnotherKeg) + parseFloat(beerRackedQty) + parseFloat(overageKeg) + parseFloat(blankKeg);
	 					sublist.setLineItemValue('keg', linenum, kegTotal.toFixed(2));
 						var bulkBTotal = parseFloat(bulkBQty) + parseFloat(beerBottleBulk) + parseFloat(overageBulkB) + parseFloat(blankBulkB);
	 					sublist.setLineItemValue('bulkb', linenum, bulkBTotal.toFixed(2));
 						var caseTotal = parseFloat(bottleQty) + parseFloat(receivedBondCase) + parseFloat(beerReturnedCase) + parseFloat(returnedAnotherCase) + parseFloat(beerBottledQty) + parseFloat(overageCase) + parseFloat(blankCase);
	 					sublist.setLineItemValue('case', linenum, caseTotal.toFixed(2));
 						var totalsTotal = parseFloat(cellarTotal) + parseFloat(bulkTotal) + parseFloat(kegTotal) + parseFloat(bulkBTotal) + parseFloat(caseTotal);
	 					sublist.setLineItemValue('totals', linenum, totalsTotal.toFixed(2));

 					}
 					
 				}

			}
 
			if (step.getName() == "partonebeersummaryremovals") {
				
				var sublist = brewersReportAssistant.addSubList("removalsfrombeerinv", "inlineeditor", "Removals from Beer Inventory");
 
				sublist.addField("operations", "text", "Operations").setDisplayType('disabled');
				// sublist.addField("viewsearch", "select", "Search Name", "-119").setDisplayType('disabled');
				sublist.addField("cellar", "float", "Cellar (b)");
				sublist.addField("bulk", "float", "Racking - Bulk (c)");
				sublist.addField("keg", "float", "Racking - Keg (d)");
				sublist.addField("bulkb", "float", "Bottling - Bulk (e)");
				sublist.addField("case", "float", "Bottling - Case (f)");
				sublist.addField("totals", "float", "Totals (g)");
 				sublist.setUniqueField("operations");
 				var linenum = 0;

 				var removals = [

 					"14. Removed for consumption or sale", 
 					"15. Removed tax-determined for use at tavern on brewery premises", 
 					"16. Removed without payment of tax for export", 
 					"17. Removed without payment of tax for use as supplies (vessels/aircraft)", 
 					"18. Removed without payment of tax for use in research, development, or testing",
 					"19. Removed without payment of tax to other breweries and pilot brewing plants of same ownership",
 					"20. Beer unfit for sale removed for use in manufacturing",
 					"21. Beer consumed on premises",
 					"22. Beer transferred for racking",
 					"23. Beer transferred for bottling",
 					"24. Beer returned to cellars",
 					"25. Beer racked",
 					"26. Beer bottled",
 					"27. Laboratory samples",
 					"28. Beer destroyed at brewery",
 					"29. Beer transferred to a distilled spirits plant",
 					"30. Recorded losses, including theft",
 					"31. Physical inventory disclosed a shortage",
 					"32. ",
 					"33. On hand end of period",
 					"34. Total Beer"

 				];

 				for (var r = 0; r < removals.length; r++) {

 					linenum = 1 + r;
 					sublist.setLineItemValue('operations', linenum, removals[r]);

 					if (linenum == 1) {
 					
 						//var beerRemovedForConsumptionKeg = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_removed');
 						var beerRemovedForConsumptionKeg = nlapiLoadSearch('transaction', box14d );
 						var filters = beerRemovedForConsumptionKeg.getFilters();
 						var columns = beerRemovedForConsumptionKeg.getColumns();
 						beerRemovedForConsumptionKeg = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerRemovedForConsumptionKeg.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerRemovedForConsumptionKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerRemovedForConsumptionKeg.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerRemovedForConsumptionKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerRemovedForConsumptionKeg = beerRemovedForConsumptionKeg.runSearch();
 						var beerRemovedKeg = 0;
 						beerRemovedForConsumptionKeg.forEachResult(function(searchResult) { beerRemovedKeg = parseFloat(beerRemovedKeg) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerRemovedKeg)) { beerRemovedKeg = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (!context.getSessionObject('14d')) { sublist.setLineItemValue('keg', linenum, beerRemovedKeg.toFixed(2)); } else { beerRemovedKeg = parseFloat(context.getSessionObject('14d')); sublist.setLineItemValue('keg', linenum, beerRemovedKeg.toFixed(2)); }
 						
 						//var beerRemovedForConsumptionCase = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_removed_case');
 						var beerRemovedForConsumptionCase = nlapiLoadSearch('transaction', box14f );
 						var filters = beerRemovedForConsumptionCase.getFilters();
 						var columns = beerRemovedForConsumptionCase.getColumns();
 						beerRemovedForConsumptionCase = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerRemovedForConsumptionCase.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerRemovedForConsumptionCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerRemovedForConsumptionCase.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerRemovedForConsumptionCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerRemovedForConsumptionCase = beerRemovedForConsumptionCase.runSearch();
 						var beerRemovedCase = 0;
 						beerRemovedForConsumptionCase.forEachResult(function(searchResult) { beerRemovedCase = parseFloat(beerRemovedCase) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerRemovedCase)) { beerRemovedCase = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (!context.getSessionObject('14f')) { sublist.setLineItemValue('case', linenum, beerRemovedCase.toFixed(2)); } else { beerRemovedCase = parseFloat(context.getSessionObject('14f')); sublist.setLineItemValue('case', linenum, beerRemovedCase.toFixed(2)); }

 						var beerRemovedTotal = parseFloat(beerRemovedKeg) + parseFloat(beerRemovedCase);
	 					sublist.setLineItemValue('totals', linenum, beerRemovedTotal.toFixed(2));

	 				} else if (linenum == 2) {

	 					//var beerRemovedTaxDeterminedKeg = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_removed_tax_k');
	 					var beerRemovedTaxDeterminedKeg = nlapiLoadSearch('transaction', box15d );
 						var filters = beerRemovedTaxDeterminedKeg.getFilters();
 						var columns = beerRemovedTaxDeterminedKeg.getColumns();
 						beerRemovedTaxDeterminedKeg = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerRemovedTaxDeterminedKeg.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerRemovedTaxDeterminedKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerRemovedTaxDeterminedKeg.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerRemovedTaxDeterminedKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerRemovedTaxDeterminedKeg = beerRemovedTaxDeterminedKeg.runSearch();
 						var beerRemovedTDKeg = 0;
 						beerRemovedTaxDeterminedKeg.forEachResult(function(searchResult) { beerRemovedTDKeg = parseFloat(beerRemovedTDKeg) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerRemovedTDKeg)) { beerRemovedTDKeg = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (!context.getSessionObject('15d')) { sublist.setLineItemValue('keg', linenum, beerRemovedTDKeg.toFixed(2)); } else { beerRemovedTDKeg = parseFloat(context.getSessionObject('15d')); sublist.setLineItemValue('keg', linenum, beerRemovedTDKeg.toFixed(2)); }
 						
 						//var beerRemovedTaxDeterminedCase = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_removed_tax_c');
 						var beerRemovedTaxDeterminedCase = nlapiLoadSearch('transaction', box15f );
 						var filters = beerRemovedTaxDeterminedCase.getFilters();
 						var columns = beerRemovedTaxDeterminedCase.getColumns();
 						beerRemovedTaxDeterminedCase = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerRemovedTaxDeterminedCase.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerRemovedTaxDeterminedCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerRemovedTaxDeterminedCase.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerRemovedTaxDeterminedCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerRemovedTaxDeterminedCase = beerRemovedTaxDeterminedCase.runSearch();
 						var beerRemovedTDCase = 0;
 						beerRemovedTaxDeterminedCase.forEachResult(function(searchResult) { beerRemovedTDCase = parseFloat(beerRemovedTDCase) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerRemovedTDCase)) { beerRemovedTDCase = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (!context.getSessionObject('15f')) { sublist.setLineItemValue('case', linenum, beerRemovedTDCase.toFixed(2)); } else { beerRemovedTDCase = parseFloat(context.getSessionObject('15f')); sublist.setLineItemValue('case', linenum, beerRemovedTDCase.toFixed(2)); }

	 					if (context.getSessionObject('15b')) { var removedTDCellar = parseFloat(context.getSessionObject('15b')); sublist.setLineItemValue('cellar', linenum, removedTDCellar.toFixed(2)); } else { var removedTDCellar = 0; }

 						var beerRemovedTDTotal = parseFloat(removedTDCellar) + parseFloat(beerRemovedTDKeg) + parseFloat(beerRemovedTDCase);
	 					sublist.setLineItemValue('totals', linenum, beerRemovedTDTotal.toFixed(2));

	 				} else if (linenum == 3) {

	 					if (context.getSessionObject('16b')) { var removedExportCellar = parseFloat(context.getSessionObject('16b')); sublist.setLineItemValue('cellar', linenum, removedExportCellar.toFixed(2)); } else { var removedExportCellar = 0; }
	 					if (context.getSessionObject('16d')) { var removedExportKeg = parseFloat(context.getSessionObject('16d')); sublist.setLineItemValue('keg', linenum, removedExportKeg.toFixed(2)); } else { var removedExportKeg = 0; }
	 					if (context.getSessionObject('16f')) { var removedExportCase = parseFloat(context.getSessionObject('16f')); sublist.setLineItemValue('case', linenum, removedExportCase.toFixed(2)); } else { var removedExportCase = 0; }

	 					var removedExportTotal = parseFloat(removedExportCellar) + parseFloat(removedExportKeg) + parseFloat(removedExportCase);
	 					sublist.setLineItemValue('totals', linenum, removedExportTotal.toFixed(2));

	 				} else if (linenum == 4) {

	 					if (context.getSessionObject('17d')) { var removedSuppliesKeg = parseFloat(context.getSessionObject('17d')); sublist.setLineItemValue('keg', linenum, removedSuppliesKeg.toFixed(2)); } else { var removedSuppliesKeg = 0; }
	 					if (context.getSessionObject('17f')) { var removedSuppliesCase = parseFloat(context.getSessionObject('17f')); sublist.setLineItemValue('case', linenum, removedSuppliesCase.toFixed(2)); } else { var removedSuppliesCase = 0; }

	 					var removedSuppliesTotal = parseFloat(removedSuppliesKeg) + parseFloat(removedSuppliesCase);
	 					sublist.setLineItemValue('totals', linenum, removedSuppliesTotal.toFixed(2));

	 				} else if (linenum == 5) {

	 					if (context.getSessionObject('18b')) { var removedRDCellar = parseFloat(context.getSessionObject('18b')); sublist.setLineItemValue('cellar', linenum, removedRDCellar.toFixed(2)); } else { var removedRDCellar = 0; }
	 					if (context.getSessionObject('18d')) { var removedRDKeg = parseFloat(context.getSessionObject('18d')); sublist.setLineItemValue('keg', linenum, removedRDKeg.toFixed(2)); } else { var removedRDKeg = 0; }
	 					if (context.getSessionObject('18f')) { var removedRDCase = parseFloat(context.getSessionObject('18f')); sublist.setLineItemValue('case', linenum, removedRDCase.toFixed(2)); } else { var removedRDCase = 0; }

	 					var removedRDTotal = parseFloat(removedRDCellar) + parseFloat(removedRDKeg) + parseFloat(removedRDCase);
	 					sublist.setLineItemValue('totals', linenum, removedRDTotal.toFixed(2));

	 				} else if (linenum == 6) {

	 					if (context.getSessionObject('19b')) { var removedBreweriesCellar = parseFloat(context.getSessionObject('19b')); sublist.setLineItemValue('cellar', linenum, removedBreweriesCellar.toFixed(2)); } else { var removedBreweriesCellar = 0; }
	 					if (context.getSessionObject('19d')) { var removedBreweriesKeg = parseFloat(context.getSessionObject('19d')); sublist.setLineItemValue('keg', linenum, removedBreweriesKeg.toFixed(2)); } else { var removedBreweriesKeg = 0; }
	 					if (context.getSessionObject('19f')) { var removedBreweriesCase = parseFloat(context.getSessionObject('19f')); sublist.setLineItemValue('case', linenum, removedBreweriesCase.toFixed(2)); } else { var removedBreweriesCase = 0; }

	 					var removedBreweriesTotal = parseFloat(removedBreweriesCellar) + parseFloat(removedBreweriesKeg) + parseFloat(removedBreweriesCase);
	 					sublist.setLineItemValue('totals', linenum, removedBreweriesTotal.toFixed(2));

	 				} else if (linenum == 7) {

	 					if (context.getSessionObject('20b')) { var beerUnfitCellar = parseFloat(context.getSessionObject('20b')); sublist.setLineItemValue('cellar', linenum, beerUnfitCellar.toFixed(2)); } else { var beerUnfitCellar = 0; }

	 					var beerUnfitTotal = parseFloat(beerUnfitCellar);
	 					sublist.setLineItemValue('totals', linenum, beerUnfitTotal.toFixed(2));

	 				} else if (linenum == 8) {

	 					//var beerConsumedOnPremiseKeg = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_beer_consumed_k');
	 					var beerConsumedOnPremiseKeg = nlapiLoadSearch('transaction', box21d );
 						var filters = beerConsumedOnPremiseKeg.getFilters();
 						var columns = beerConsumedOnPremiseKeg.getColumns();
 						beerConsumedOnPremiseKeg = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerConsumedOnPremiseKeg.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerConsumedOnPremiseKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerConsumedOnPremiseKeg.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerConsumedOnPremiseKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerConsumedOnPremiseKeg = beerConsumedOnPremiseKeg.runSearch();
 						var beerConsumedKeg = 0;
 						beerConsumedOnPremiseKeg.forEachResult(function(searchResult) { beerConsumedKeg = parseFloat(beerConsumedKeg) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerConsumedKeg)) { beerConsumedKeg = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (!context.getSessionObject('21d')) { sublist.setLineItemValue('keg', linenum, beerConsumedKeg.toFixed(2)); } else { beerConsumedKeg = parseFloat(context.getSessionObject('21d')); sublist.setLineItemValue('keg', linenum, beerConsumedKeg.toFixed(2)); }
 						
 						//var beerConsumedOnPremiseCase = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_beer_consumed_c');
 						var beerConsumedOnPremiseCase = nlapiLoadSearch('transaction', box21f );
 						var filters = beerConsumedOnPremiseCase.getFilters();
 						var columns = beerConsumedOnPremiseCase.getColumns();
 						beerConsumedOnPremiseCase = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerConsumedOnPremiseCase.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerConsumedOnPremiseCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerConsumedOnPremiseCase.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerConsumedOnPremiseCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerConsumedOnPremiseCase = beerConsumedOnPremiseCase.runSearch();
 						var beerConsumedCase = 0;
 						beerConsumedOnPremiseCase.forEachResult(function(searchResult) { beerConsumedCase = parseFloat(beerConsumedCase) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerConsumedCase)) { beerConsumedCase = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (!context.getSessionObject('21f')) { sublist.setLineItemValue('case', linenum, beerConsumedCase.toFixed(2)); } else { beerConsumedCase = parseFloat(context.getSessionObject('21f')); sublist.setLineItemValue('case', linenum, beerConsumedCase.toFixed(2)); }

 						if (context.getSessionObject('21b')) { var beerConsumedCellar = parseFloat(context.getSessionObject('21b')); sublist.setLineItemValue('cellar', linenum, beerConsumedCellar.toFixed(2)); } else { var beerConsumedCellar = 0; }

 						var beerConsumedTotal = parseFloat(beerConsumedKeg) + parseFloat(beerConsumedCase) + parseFloat(beerConsumedCellar);
	 					sublist.setLineItemValue('totals', linenum, beerConsumedTotal.toFixed(2));

	 				} else if (linenum == 9) {

	 					//var beerReceivedFromCellarsKegLine = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_22b');
 						var beerReceivedFromCellarsKegLine = nlapiLoadSearch('transaction', box22b );
 						var filters = beerReceivedFromCellarsKegLine.getFilters();
 						var columns = beerReceivedFromCellarsKegLine.getColumns();
 						beerReceivedFromCellarsKegLine = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerReceivedFromCellarsKegLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerReceivedFromCellarsKegLine = beerReceivedFromCellarsKegLine.runSearch();
 						var beerRackBulk = 0;
 						beerReceivedFromCellarsKegLine.forEachResult(function(searchResult) { beerRackBulk = parseFloat(beerRackBulk) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerRackBulk)) { beerRackBulk = 0; }
 						if (!context.getSessionObject('6c')) { sublist.setLineItemValue('cellar', linenum, beerRackBulk.toFixed(2)); } else { beerRackBulk = parseFloat(context.getSessionObject('6c')); sublist.setLineItemValue('cellar', linenum, beerRackBulk.toFixed(2)); }

	 					var beerRackTotal = parseFloat(beerRackBulk);
	 					sublist.setLineItemValue('totals', linenum, beerRackTotal.toFixed(2));

	 				} else if (linenum == 10) {

	 					//var beerReceivedFromCellarsBottleLine = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_23b');
	 					var beerReceivedFromCellarsBottleLine = nlapiLoadSearch('transaction', box23b );
 						var filters = beerReceivedFromCellarsBottleLine.getFilters();
 						var columns = beerReceivedFromCellarsBottleLine.getColumns();
 						beerReceivedFromCellarsBottleLine = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerReceivedFromCellarsBottleLine.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerReceivedFromCellarsBottleLine = beerReceivedFromCellarsBottleLine.runSearch();
 						var beerBottleBulk = 0;
 						beerReceivedFromCellarsBottleLine.forEachResult(function(searchResult) { beerBottleBulk = parseFloat(beerBottleBulk) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerBottleBulk)) { beerBottleBulk = 0; }
 						if (!context.getSessionObject('6e')) { sublist.setLineItemValue('cellar', linenum, beerBottleBulk.toFixed(2)); } else { beerBottleBulk = parseFloat(context.getSessionObject('6e')); sublist.setLineItemValue('cellar', linenum, beerBottleBulk.toFixed(2)); }

	 					var beerBottleTotal = parseFloat(beerBottleBulk);
	 					sublist.setLineItemValue('totals', linenum, beerBottleTotal.toFixed(2));

	 				} else if (linenum == 11) {

	 					if (context.getSessionObject('4b')) { var returnedCellarsBulk = parseFloat(context.getSessionObject('4b')); sublist.setLineItemValue('bulk', linenum, returnedCellarsBulk.toFixed(2)); } else { var returnedCellarsBulk = 0; }
	 					if (context.getSessionObject('4b')) { var returnedCellarsBulkB = parseFloat(context.getSessionObject('4b')); sublist.setLineItemValue('bulkb', linenum, returnedCellarsBulkB.toFixed(2)); } else { var returnedCellarsBulkB = 0; }

	 					var returnedCellarsTotal = parseFloat(returnedCellarsBulk) + parseFloat(returnedCellarsBulkB);
	 					sublist.setLineItemValue('totals', linenum, returnedCellarsTotal.toFixed(2));

	 				} else if (linenum == 12) {

						//var beerRacked = nlapiLoadSearch('transaction', 'customsearch_h_berp_bro_25c');
 						var beerRacked = nlapiLoadSearch('transaction', box25c );
 						var filters = beerRacked.getFilters();
 						var columns = beerRacked.getColumns();
 						beerRacked = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerRacked.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerRacked.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerRacked.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerRacked.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerRacked = beerRacked.runSearch();
 						var beerRackedQty = 0;
 						beerRacked.forEachResult(function(searchResult) { beerRackedQty = parseFloat(beerRackedQty) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerRackedQty)) { beerRackedQty = 0; }
 						
						if (!context.getSessionObject('9d')) { sublist.setLineItemValue('bulk', linenum, beerRackedQty.toFixed(2)); } else { beerRackedQty = parseFloat(context.getSessionObject('9d')); sublist.setLineItemValue('bulk', linenum, beerRackedQty.toFixed(2)); }

	 					var beerRackTotal = parseFloat(beerRackedQty);
	 					sublist.setLineItemValue('totals', linenum, beerRackTotal.toFixed(2));

	 				} else if (linenum == 13) {

	 					//var beerBottled = nlapiLoadSearch('transaction', 'customsearch_h_berp_26e');
	 					var beerBottled = nlapiLoadSearch('transaction', box26e );
 						var filters = beerBottled.getFilters();
 						var columns = beerBottled.getColumns();
 						beerBottled = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerBottled.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerBottled.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerBottled.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerBottled.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerBottled = beerBottled.runSearch();
 						var beerBottledQty = 0;
 						beerBottled.forEachResult(function(searchResult) { beerBottledQty = parseFloat(beerBottledQty) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						if (isNaN(beerBottledQty)) { beerBottledQty = 0; }
 						if (!context.getSessionObject('10f')) { sublist.setLineItemValue('bulkb', linenum, beerBottledQty.toFixed(2)); } else { beerBottledQty = parseFloat(context.getSessionObject('10f')); sublist.setLineItemValue('bulkb', linenum, beerBottledQty.toFixed(2)); }

	 					var beerBottledTotal = parseFloat(beerBottledQty);
	 					sublist.setLineItemValue('totals', linenum, beerBottledTotal.toFixed(2));

	 				} else if (linenum == 14) {

	 					//var searchLabSamplesCellar = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_lab_cellar');
	 					var searchLabSamplesCellar = nlapiLoadSearch('transaction', box27b );
 						var filters = searchLabSamplesCellar.getFilters();
 						var columns = searchLabSamplesCellar.getColumns();
 						searchLabSamplesCellar = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							searchLabSamplesCellar.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							searchLabSamplesCellar.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							searchLabSamplesCellar.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							searchLabSamplesCellar.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						searchLabSamplesCellar = searchLabSamplesCellar.runSearch();
 						var labSamplesCellar = 0;
 						searchLabSamplesCellar.forEachResult(function(searchResult) { labSamplesCellar = parseFloat(labSamplesCellar) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(labSamplesCellar)) { labSamplesCellar = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						sublist.setLineItemValue('cellar', linenum, labSamplesCellar.toFixed(2));

 						//var searchLabSamplesBulk = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_lab_bulk');
 						var searchLabSamplesBulk = nlapiLoadSearch('transaction', box27c );
 						var filters = searchLabSamplesBulk.getFilters();
 						var columns = searchLabSamplesBulk.getColumns();
 						searchLabSamplesBulk = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							searchLabSamplesBulk.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							searchLabSamplesBulk.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							searchLabSamplesBulk.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							searchLabSamplesBulk.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						searchLabSamplesBulk = searchLabSamplesBulk.runSearch();
 						var labSamplesBulk = 0;
 						searchLabSamplesBulk.forEachResult(function(searchResult) { labSamplesBulk = parseFloat(labSamplesBulk) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(labSamplesBulk)) { labSamplesBulk = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						sublist.setLineItemValue('bulk', linenum, labSamplesBulk.toFixed(2));

 						//var searchLabSamplesKeg = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_lab_keg');
 						var searchLabSamplesKeg = nlapiLoadSearch('transaction', box27d );
 						var filters = searchLabSamplesKeg.getFilters();
 						var columns = searchLabSamplesKeg.getColumns();
 						searchLabSamplesKeg = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							searchLabSamplesKeg.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							searchLabSamplesKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							searchLabSamplesKeg.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							searchLabSamplesKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						searchLabSamplesKeg = searchLabSamplesKeg.runSearch();
 						var labSamplesKeg = 0;
 						searchLabSamplesKeg.forEachResult(function(searchResult) { labSamplesKeg = parseFloat(labSamplesKeg) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(labSamplesKeg)) { labSamplesKeg = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						sublist.setLineItemValue('keg', linenum, labSamplesKeg.toFixed(2));
 						
 						//var searchLabSamplesBottleBulk = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_lab_bbulk');
 						var searchLabSamplesBottleBulk = nlapiLoadSearch('transaction', box27e );
 						var filters = searchLabSamplesBottleBulk.getFilters();
 						var columns = searchLabSamplesBottleBulk.getColumns();
 						searchLabSamplesBottleBulk = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							searchLabSamplesBottleBulk.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							searchLabSamplesBottleBulk.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							searchLabSamplesBottleBulk.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							searchLabSamplesBottleBulk.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						searchLabSamplesBottleBulk = searchLabSamplesBottleBulk.runSearch();
 						var labSamplesBottleBulk = 0;
 						searchLabSamplesBottleBulk.forEachResult(function(searchResult) { labSamplesBottleBulk = parseFloat(labSamplesBottleBulk) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(labSamplesBottleBulk)) { labSamplesBottleBulk = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						sublist.setLineItemValue('bulkb', linenum, labSamplesBottleBulk.toFixed(2));

 						//var searchLabSamplesCase = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_lab_case');
 						var searchLabSamplesCase = nlapiLoadSearch('transaction', box27f );
 						var filters = searchLabSamplesCase.getFilters();
 						var columns = searchLabSamplesCase.getColumns();
 						searchLabSamplesCase = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							searchLabSamplesCase.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							searchLabSamplesCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							searchLabSamplesCase.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							searchLabSamplesCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						searchLabSamplesCase = searchLabSamplesCase.runSearch();
 						var labSamplesCase = 0;
 						searchLabSamplesCase.forEachResult(function(searchResult) { labSamplesCase = parseFloat(labSamplesCase) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(labSamplesCase)) { labSamplesCase = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						sublist.setLineItemValue('case', linenum, labSamplesCase.toFixed(2));
 						
	 					if (context.getSessionObject('27b')) { var labSamplesCellar = parseFloat(context.getSessionObject('27b')); sublist.setLineItemValue('cellar', linenum, labSamplesCellar.toFixed(2)); } else { var labSamplesCellar = 0; }
	 					if (context.getSessionObject('27c')) { var labSamplesBulk = parseFloat(context.getSessionObject('27c')); sublist.setLineItemValue('bulk', linenum, labSamplesBulk.toFixed(2)); } else { var labSamplesBulk = 0; }
	 					if (context.getSessionObject('27d')) { var labSamplesKeg = parseFloat(context.getSessionObject('27d')); sublist.setLineItemValue('keg', linenum, labSamplesKeg.toFixed(2)); } else { var labSamplesKeg = 0; }
	 					if (context.getSessionObject('27e')) { var labSamplesBottleBulk = parseFloat(context.getSessionObject('27e')); sublist.setLineItemValue('bulkb', linenum, labSamplesBottleBulk.toFixed(2)); } else { var labSamplesBottleBulk = 0; }
	 					if (context.getSessionObject('27f')) { var labSamplesCase = parseFloat(context.getSessionObject('27f')); sublist.setLineItemValue('case', linenum, labSamplesCase.toFixed(2)); } else { var labSamplesCase = 0; }

	 					var labSamplesTotal = parseFloat(labSamplesCellar) + parseFloat(labSamplesBulk) + parseFloat(labSamplesKeg) + parseFloat(labSamplesBottleBulk) + parseFloat(labSamplesCase);
	 					sublist.setLineItemValue('totals', linenum, labSamplesTotal.toFixed(2));

	 				} else if (linenum == 15) {

	 					//var beerDestroyedAtBreweryCellar = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_dest_cellar');
	 					var beerDestroyedAtBreweryCellar = nlapiLoadSearch('transaction', box28b );
 						var filters = beerDestroyedAtBreweryCellar.getFilters();
 						var columns = beerDestroyedAtBreweryCellar.getColumns();
 						beerDestroyedAtBreweryCellar = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerDestroyedAtBreweryCellar.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerDestroyedAtBreweryCellar.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerDestroyedAtBreweryCellar.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerDestroyedAtBreweryCellar.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerDestroyedAtBreweryCellar = beerDestroyedAtBreweryCellar.runSearch();
 						var beerDestroyedCellar = 0;
 						beerDestroyedAtBreweryCellar.forEachResult(function(searchResult) { beerDestroyedCellar = parseFloat(beerDestroyedCellar) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerDestroyedCellar)) { beerDestroyedCellar = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						// sublist.setLineItemValue('cellar', linenum, beerDestroyedCellar.toFixed(2));

 						//var beerDestroyedAtBreweryBulk = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_dest_bulk');
 						var beerDestroyedAtBreweryBulk = nlapiLoadSearch('transaction', box28c );
 						var filters = beerDestroyedAtBreweryBulk.getFilters();
 						var columns = beerDestroyedAtBreweryBulk.getColumns();
 						beerDestroyedAtBreweryBulk = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerDestroyedAtBreweryBulk.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerDestroyedAtBreweryBulk.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerDestroyedAtBreweryBulk.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerDestroyedAtBreweryBulk.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerDestroyedAtBreweryBulk = beerDestroyedAtBreweryBulk.runSearch();
 						var beerDestroyedBulk = 0;
 						beerDestroyedAtBreweryBulk.forEachResult(function(searchResult) { beerDestroyedBulk = parseFloat(beerDestroyedBulk) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerDestroyedBulk)) { beerDestroyedBulk = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						// sublist.setLineItemValue('bulk', linenum, beerDestroyedBulk.toFixed(2));

	 					//var beerDestroyedAtBreweryKeg = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_beer_destroy_ke');
 						var beerDestroyedAtBreweryKeg = nlapiLoadSearch('transaction', box28d );
 						var filters = beerDestroyedAtBreweryKeg.getFilters();
 						var columns = beerDestroyedAtBreweryKeg.getColumns();
 						beerDestroyedAtBreweryKeg = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerDestroyedAtBreweryKeg.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerDestroyedAtBreweryKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerDestroyedAtBreweryKeg.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerDestroyedAtBreweryKeg.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerDestroyedAtBreweryKeg = beerDestroyedAtBreweryKeg.runSearch();
 						var beerDestroyedKeg = 0;
 						beerDestroyedAtBreweryKeg.forEachResult(function(searchResult) { beerDestroyedKeg = parseFloat(beerDestroyedKeg) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerDestroyedKeg)) { beerDestroyedKeg = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						// sublist.setLineItemValue('keg', linenum, beerDestroyedKeg.toFixed(2));

 						//var beerDestroyedAtBreweryBulkB = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_dest_bulkb');
 						var beerDestroyedAtBreweryBulkB = nlapiLoadSearch('transaction', box28e );
 						var filters = beerDestroyedAtBreweryBulkB.getFilters();
 						var columns = beerDestroyedAtBreweryBulkB.getColumns();
 						beerDestroyedAtBreweryBulkB = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerDestroyedAtBreweryBulkB.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerDestroyedAtBreweryBulkB.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerDestroyedAtBreweryBulkB.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerDestroyedAtBreweryBulkB.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerDestroyedAtBreweryBulkB = beerDestroyedAtBreweryBulkB.runSearch();
 						var beerDestroyedBulkB = 0;
 						beerDestroyedAtBreweryBulkB.forEachResult(function(searchResult) { beerDestroyedBulkB = parseFloat(beerDestroyedBulkB) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerDestroyedBulkB)) { beerDestroyedBulkB = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						// sublist.setLineItemValue('bulkb', linenum, beerDestroyedBulkB.toFixed(2));
 						
 						//var beerDestroyedAtBreweryCase = nlapiLoadSearch('transaction', 'customsearch_h_berp_broo_beer_destroy_c');
 						var beerDestroyedAtBreweryCase = nlapiLoadSearch('transaction', box28f );
 						var filters = beerDestroyedAtBreweryCase.getFilters();
 						var columns = beerDestroyedAtBreweryCase.getColumns();
 						beerDestroyedAtBreweryCase = nlapiCreateSearch('transaction', filters, columns);
 						nlapiLogExecution('DEBUG', 'Monthly or Quarterly', context.getSessionObject('monthlyorquarterly'));
 						nlapiLogExecution('DEBUG', 'Month', context.getSessionObject('reportingmonth'));
 						var quarterStartDate = context.getSessionObject('quarterstartdate');
 						var quarterEndDate = context.getSessionObject('quarterenddate');
 						var breweryLicense = context.getSessionObject('brewerylicense');
 						nlapiLogExecution('DEBUG', 'Quarter Dates', quarterStartDate + ' ' + quarterEndDate);
 						if (context.getSessionObject('monthlyorquarterly') == '2') { 

 							beerDestroyedAtBreweryCase.addFilter(new nlobjSearchFilter('trandate', null, 'within', [quarterStartDate, quarterEndDate]));
 							beerDestroyedAtBreweryCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						} else { 

 							beerDestroyedAtBreweryCase.addFilter(new nlobjSearchFilter('postingperiod', null, 'abs', context.getSessionObject('reportingperiod')));
 							beerDestroyedAtBreweryCase.addFilter(new nlobjSearchFilter('custbody_h_berp_license', null, 'anyof', breweryLicense));  

 						}

 						beerDestroyedAtBreweryCase = beerDestroyedAtBreweryCase.runSearch();
 						var beerDestroyedCase = 0;
 						beerDestroyedAtBreweryCase.forEachResult(function(searchResult) { beerDestroyedCase = parseFloat(beerDestroyedCase) + parseFloat(searchResult.getValue('formulanumeric', null, 'SUM')); return true });
 						if (isNaN(beerDestroyedCase)) { beerDestroyedCase = 0; }
 						// sublist.setLineItemValue('viewsearch', linenum, '56');
 						
 						if (!context.getSessionObject('28b')) { sublist.setLineItemValue('cellar', linenum, beerDestroyedCellar.toFixed(2)); } else { beerDestroyedCellar = parseFloat(context.getSessionObject('28b')); sublist.setLineItemValue('cellar', linenum, beerDestroyedCellar.toFixed(2)); }
 						if (!context.getSessionObject('28c')) { sublist.setLineItemValue('bulk', linenum, beerDestroyedBulk.toFixed(2)); } else { beerDestroyedBulk = parseFloat(context.getSessionObject('28c')); sublist.setLineItemValue('bulk', linenum, beerDestroyedBulk.toFixed(2)); }
 						if (!context.getSessionObject('28d')) { sublist.setLineItemValue('keg', linenum, beerDestroyedKeg.toFixed(2)); } else { beerDestroyedKeg = parseFloat(context.getSessionObject('28d')); sublist.setLineItemValue('keg', linenum, beerDestroyedKeg.toFixed(2)); }
 						if (!context.getSessionObject('28e')) { sublist.setLineItemValue('bulkb', linenum, beerDestroyedBulkB.toFixed(2)); } else { beerDestroyedBulkB = parseFloat(context.getSessionObject('28e')); sublist.setLineItemValue('bulkb', linenum, beerDestroyedBulkB.toFixed(2)); }
 						if (!context.getSessionObject('28f')) { sublist.setLineItemValue('case', linenum, beerDestroyedCase.toFixed(2)); } else { beerDestroyedCase = parseFloat(context.getSessionObject('28f')); sublist.setLineItemValue('case', linenum, beerDestroyedCase.toFixed(2)); }

	 					var beerDestroyedTotal = parseFloat(beerDestroyedBulk) + parseFloat(beerDestroyedCellar) + parseFloat(beerDestroyedKeg) + parseFloat(beerDestroyedBulkB) + parseFloat(beerDestroyedCase);
	 					sublist.setLineItemValue('totals', linenum, beerDestroyedTotal.toFixed(2));

	 				} else if (linenum == 16) {

	 					if (context.getSessionObject('29b')) { var beerTransferredCellar = parseFloat(context.getSessionObject('29b')); sublist.setLineItemValue('cellar', linenum, beerTransferredCellar.toFixed(2)); } else { var beerTransferredCellar = 0; }
	 					if (context.getSessionObject('29c')) { var beerTransferredBulk = parseFloat(context.getSessionObject('29c')); sublist.setLineItemValue('bulk', linenum, beerTransferredBulk.toFixed(2)); } else { var beerTransferredBulk = 0; }
	 					if (context.getSessionObject('29d')) { var beerTransferredKeg = parseFloat(context.getSessionObject('29d')); sublist.setLineItemValue('keg', linenum, beerTransferredKeg.toFixed(2)); } else { var beerTransferredKeg = 0; }
	 					if (context.getSessionObject('29e')) { var beerTransferredBulkB = parseFloat(context.getSessionObject('29e')); sublist.setLineItemValue('bulkb', linenum, beerTransferredBulkB.toFixed(2)); } else { var beerTransferredBulkB = 0; }
	 					if (context.getSessionObject('29f')) { var beerTransferredCase = parseFloat(context.getSessionObject('29f')); sublist.setLineItemValue('case', linenum, beerTransferredCase.toFixed(2)); } else { var beerTransferredCase = 0; }

	 					var beerTransferredTotal = parseFloat(beerTransferredCellar) + parseFloat(beerTransferredBulk) + parseFloat(beerTransferredKeg) + parseFloat(beerTransferredBulkB) + parseFloat(beerTransferredCase);
	 					sublist.setLineItemValue('totals', linenum, beerTransferredTotal.toFixed(2));

	 				} else if (linenum == 17) {

	 					if (!context.getSessionObject('30b')) { var cellarLoss = 0; } else { var cellarLoss = parseFloat(context.getSessionObject('30b')); sublist.setLineItemValue('cellar', linenum, cellarLoss.toFixed(2)); }
	 					if (!context.getSessionObject('30c')) { var bulkRackLoss = 0; } else { var bulkRackLoss = parseFloat(context.getSessionObject('30c')); sublist.setLineItemValue('bulk', linenum, bulkRackLoss.toFixed(2)); }
	 					if (!context.getSessionObject('30d')) { var kegLoss = 0; } else { var kegLoss = parseFloat(context.getSessionObject('30d')); sublist.setLineItemValue('keg', linenum, kegLoss.toFixed(2)); }
	 					if (!context.getSessionObject('30e')) { var bulkBottleLoss = 0; } else { var bulkBottleLoss = parseFloat(context.getSessionObject('30e')); sublist.setLineItemValue('bulkb', linenum, bulkBottleLoss.toFixed(2)); }
	 					if (!context.getSessionObject('30f')) { var caseLoss = 0; } else { var caseLoss = parseFloat(context.getSessionObject('30f')); sublist.setLineItemValue('case', linenum, caseLoss.toFixed(2)); }

	 					var lossTotal = parseFloat(cellarLoss) + parseFloat(bulkRackLoss) + parseFloat(kegLoss) + parseFloat(bulkBottleLoss) + parseFloat(caseLoss);
	 					sublist.setLineItemValue('totals', linenum, lossTotal.toFixed(2));

	 				} else if (linenum == 18) {

	 					if (context.getSessionObject('31b')) { var shortageCellar = parseFloat(context.getSessionObject('31b')); sublist.setLineItemValue('cellar', linenum, shortageCellar.toFixed(2)); } else { var shortageCellar = 0; }
	 					if (context.getSessionObject('31c')) { var shortageBulk = parseFloat(context.getSessionObject('31c')); sublist.setLineItemValue('bulk', linenum, shortageBulk.toFixed(2)); } else { var shortageBulk = 0; }
	 					if (context.getSessionObject('31d')) { var shortageKeg = parseFloat(context.getSessionObject('31d')); sublist.setLineItemValue('keg', linenum, shortageKeg.toFixed(2)); } else { var shortageKeg = 0; }
	 					if (context.getSessionObject('31e')) { var shortageBulkB = parseFloat(context.getSessionObject('31e')); sublist.setLineItemValue('bulkb', linenum, shortageBulkB.toFixed(2)); } else { var shortageBulkB = 0; }
	 					if (context.getSessionObject('31f')) { var shortageCase = parseFloat(context.getSessionObject('31f')); sublist.setLineItemValue('case', linenum, shortageCase.toFixed(2)); } else { var shortageCase = 0; }

	 					var shortageTotal = parseFloat(shortageCellar) + parseFloat(shortageBulk) + parseFloat(shortageKeg) + parseFloat(shortageBulkB) + parseFloat(shortageCase);
	 					sublist.setLineItemValue('totals', linenum, shortageTotal.toFixed(2));

	 				} else if (linenum == 19) {

	 					if (context.getSessionObject('32b')) { var blankRemovalsCellar = parseFloat(context.getSessionObject('32b')); sublist.setLineItemValue('cellar', linenum, blankRemovalsCellar.toFixed(2)); } else { var blankRemovalsCellar = 0; }
	 					if (context.getSessionObject('32c')) { var blankRemovalsBulk = parseFloat(context.getSessionObject('32c')); sublist.setLineItemValue('bulk', linenum, blankRemovalsBulk.toFixed(2)); } else { var blankRemovalsBulk = 0; }
	 					if (context.getSessionObject('32d')) { var blankRemovalsKeg = parseFloat(context.getSessionObject('32d')); sublist.setLineItemValue('keg', linenum, blankRemovalsKeg.toFixed(2)); } else { var blankRemovalsKeg = 0; }
	 					if (context.getSessionObject('32e')) { var blankRemovalsBulkB = parseFloat(context.getSessionObject('32e')); sublist.setLineItemValue('bulkb', linenum, blankRemovalsBulkB.toFixed(2)); } else { var blankRemovalsBulkB = 0; }
	 					if (context.getSessionObject('32f')) { var blankRemovalsCase = parseFloat(context.getSessionObject('32f')); sublist.setLineItemValue('case', linenum, blankRemovalsCase.toFixed(2)); } else { var blankRemovalsCase = 0; }

	 					var blankRemovalsTotal = parseFloat(blankRemovalsCellar) + parseFloat(blankRemovalsBulk) + parseFloat(blankRemovalsKeg) + parseFloat(blankRemovalsBulkB) + parseFloat(blankRemovalsCase);
	 					sublist.setLineItemValue('totals', linenum, blankRemovalsTotal.toFixed(2));

	 				} else if (linenum == 21) {

	 					if (context.getSessionObject('1b')) { var onHandCellar = context.getSessionObject('1b'); } else { var onHandCellar = 0; }
	 					if (context.getSessionObject('2b')) { var beerProducedCellar = context.getSessionObject('2b'); } else { var beerProducedCellar = 0; }
	 					if (context.getSessionObject('3b')) { var additionWaterCellar = context.getSessionObject('3b'); } else { var additionWaterCellar = 0; }
	 					if (context.getSessionObject('4b')) { var receivedFromCellar = context.getSessionObject('4b'); } else { var receivedFromCellar = 0; }
	 					if (context.getSessionObject('5b')) { var receivedBondCellar = context.getSessionObject('5b'); } else { var receivedBondCellar = 0; }
	 					if (context.getSessionObject('7b')) { var returnedBreweryCellar = context.getSessionObject('7b'); } else { var returnedBreweryCellar = 0; }
	 					if (context.getSessionObject('8b')) { var returnedOwnershipCellar = context.getSessionObject('8b'); } else { var returnedOwnershipCellar = 0; }
	 					if (context.getSessionObject('11b')) { var overageCellar = context.getSessionObject('11b'); } else { var overageCellar = 0; }
	 					if (context.getSessionObject('12b')) { var blankCellar = context.getSessionObject('12b'); } else { var blankCellar = 0; }
	 					var cellarTotalLine = parseFloat(onHandCellar) + parseFloat(beerProducedCellar) + parseFloat(additionWaterCellar) + parseFloat(receivedFromCellar) + parseFloat(receivedBondCellar) + parseFloat(returnedBreweryCellar) + parseFloat(returnedOwnershipCellar) + parseFloat(overageCellar) + parseFloat(blankCellar);
 						sublist.setLineItemValue('cellar', linenum, cellarTotalLine.toFixed(2));

 						if (context.getSessionObject('1c')) { var onHandBulk = context.getSessionObject('1c'); } else { var onHandBulk = 0; }
	 					if (context.getSessionObject('6c')) { var receivedCellarBulk = context.getSessionObject('6c'); } else { var receivedCellarBulk = 0; }
	 					if (context.getSessionObject('11c')) { var overageBulk = context.getSessionObject('11c'); } else { var overageBulk = 0; }
	 					if (context.getSessionObject('12c')) { var blankBulk = context.getSessionObject('12c'); } else { var blankBulk = 0; }
	 					var bulkTotalLine = parseFloat(onHandBulk) + parseFloat(receivedCellarBulk) + parseFloat(overageBulk) + parseFloat(blankBulk);
	 					sublist.setLineItemValue('bulk', linenum, bulkTotalLine.toFixed(2));

	 					if (context.getSessionObject('1d')) { var onHandKeg = context.getSessionObject('1d'); } else { var onHandKeg = 0; }
	 					if (context.getSessionObject('5d')) { var receivedBondKeg = context.getSessionObject('5d'); } else { var receivedBondKeg = 0; }
	 					if (context.getSessionObject('7d')) { var returnedBreweryKeg = context.getSessionObject('7d'); } else { var returnedBreweryKeg = 0; }
	 					if (context.getSessionObject('8d')) { var returnedOwnershipKeg = context.getSessionObject('8d'); } else { var returnedOwnershipKeg = 0; }
	 					if (context.getSessionObject('9d')) { var rackedKeg = context.getSessionObject('9d'); } else { var rackedKeg = 0; }
	 					if (context.getSessionObject('11d')) { var overageKeg = context.getSessionObject('11d'); } else { var overageKeg = 0; }
	 					if (context.getSessionObject('12d')) { var blankKeg = context.getSessionObject('12d'); } else { var blankKeg = 0; }
	 					var kegTotalLine = parseFloat(onHandKeg) + parseFloat(receivedBondKeg) + parseFloat(returnedBreweryKeg) + parseFloat(returnedOwnershipKeg) + parseFloat(rackedKeg) + parseFloat(overageKeg) + parseFloat(blankKeg);
	 					sublist.setLineItemValue('keg', linenum, kegTotalLine.toFixed(2));

	 					if (context.getSessionObject('1e')) { var onHandBulkB = context.getSessionObject('1e'); } else { var onHandBulkB = 0; }
						if (context.getSessionObject('6e')) { var receivedCellarBulkB = context.getSessionObject('6e'); } else { var receivedCellarBulkB = 0; }
	 					if (context.getSessionObject('7e')) { var returnedBreweryBulkB = context.getSessionObject('7e'); } else { var returnedBreweryBulkB = 0; }
	 					if (context.getSessionObject('8e')) { var returnedOwnershipBulkB = context.getSessionObject('8e'); } else { var returnedOwnershipBulkB = 0; }
	 					if (context.getSessionObject('11e')) { var overageBulkB = context.getSessionObject('11e'); } else { var overageBulkB = 0; }
	 					if (context.getSessionObject('12e')) { var blankBulkB = context.getSessionObject('12e'); } else { var blankBulkB = 0; }
	 					var bulkBTotalLine = parseFloat(onHandBulkB) + parseFloat(receivedCellarBulkB) + parseFloat(returnedBreweryBulkB) + parseFloat(returnedOwnershipBulkB) + parseFloat(overageBulkB) + parseFloat(blankBulkB);
	 					sublist.setLineItemValue('bulkb', linenum, bulkBTotalLine.toFixed(2));

	 					if (context.getSessionObject('1f')) { var onHandCase = context.getSessionObject('1f'); } else { var onHandCase = 0; }
	 					if (context.getSessionObject('5f')) { var receivedBondCase = context.getSessionObject('5f'); } else { var receivedBondCase = 0; }
	 					if (context.getSessionObject('7f')) { var returnedBreweryCase = context.getSessionObject('7f'); } else { var returnedBreweryCase = 0; }
	 					if (context.getSessionObject('8f')) { var returnedOwnershipCase = context.getSessionObject('8f'); } else { var returnedOwnershipCase = 0; }
	 					if (context.getSessionObject('10f')) { var bottledCase = context.getSessionObject('10f'); } else { var bottledCase = 0; }
	 					if (context.getSessionObject('11f')) { var overageCase = context.getSessionObject('11f'); } else { var overageCase = 0; }
	 					if (context.getSessionObject('12f')) { var blankCase = context.getSessionObject('12f'); } else { var blankCase = 0; }
	 					var caseTotalLine = parseFloat(onHandCase) + parseFloat(receivedBondCase) + parseFloat(returnedBreweryCase) + parseFloat(returnedOwnershipCase) + parseFloat(bottledCase) + parseFloat(overageCase) + parseFloat(blankCase);
	 					sublist.setLineItemValue('case', linenum, caseTotalLine.toFixed(2));

	 					var totalBeer = parseFloat(cellarTotalLine) + parseFloat(bulkTotalLine) + parseFloat(kegTotalLine) + parseFloat(bulkBTotalLine) + parseFloat(caseTotalLine); 
	 					sublist.setLineItemValue('totals', linenum, totalBeer.toFixed(2));

 					} else if (linenum == 20) {

 						if (context.getSessionObject('1b')) { var onHandCellar = context.getSessionObject('1b'); } else { var onHandCellar = 0; }
	 					if (context.getSessionObject('2b')) { var beerProducedCellar = context.getSessionObject('2b'); } else { var beerProducedCellar = 0; }
	 					if (context.getSessionObject('3b')) { var additionWaterCellar = context.getSessionObject('3b'); } else { var additionWaterCellar = 0; }
	 					if (context.getSessionObject('4b')) { var receivedFromCellar = context.getSessionObject('4b'); } else { var receivedFromCellar = 0; }
	 					if (context.getSessionObject('5b')) { var receivedBondCellar = context.getSessionObject('5b'); } else { var receivedBondCellar = 0; }
	 					if (context.getSessionObject('7b')) { var returnedBreweryCellar = context.getSessionObject('7b'); } else { var returnedBreweryCellar = 0; }
	 					if (context.getSessionObject('8b')) { var returnedOwnershipCellar = context.getSessionObject('8b'); } else { var returnedOwnershipCellar = 0; }
	 					if (context.getSessionObject('11b')) { var overageCellar = context.getSessionObject('11b'); } else { var overageCellar = 0; }
	 					if (context.getSessionObject('12b')) { var blankCellar = context.getSessionObject('12b'); } else { var blankCellar = 0; }
	 					var cellarTotalLine = parseFloat(onHandCellar) + parseFloat(beerProducedCellar) + parseFloat(additionWaterCellar) + parseFloat(receivedFromCellar) + parseFloat(receivedBondCellar) + parseFloat(returnedBreweryCellar) + parseFloat(returnedOwnershipCellar) + parseFloat(overageCellar) + parseFloat(blankCellar);

 						if (context.getSessionObject('1c')) { var onHandBulk = context.getSessionObject('1c'); } else { var onHandBulk = 0; }
	 					if (context.getSessionObject('6c')) { var receivedCellarBulk = context.getSessionObject('6c'); } else { var receivedCellarBulk = 0; }
	 					if (context.getSessionObject('11c')) { var overageBulk = context.getSessionObject('11c'); } else { var overageBulk = 0; }
	 					if (context.getSessionObject('12c')) { var blankBulk = context.getSessionObject('12c'); } else { var blankBulk = 0; }
	 					var bulkTotalLine = parseFloat(onHandBulk) + parseFloat(receivedCellarBulk) + parseFloat(overageBulk) + parseFloat(blankBulk);

	 					if (context.getSessionObject('1d')) { var onHandKeg = context.getSessionObject('1d'); } else { var onHandKeg = 0; }
	 					if (context.getSessionObject('5d')) { var receivedBondKeg = context.getSessionObject('5d'); } else { var receivedBondKeg = 0; }
	 					if (context.getSessionObject('7d')) { var returnedBreweryKeg = context.getSessionObject('7d'); } else { var returnedBreweryKeg = 0; }
	 					if (context.getSessionObject('8d')) { var returnedOwnershipKeg = context.getSessionObject('8d'); } else { var returnedOwnershipKeg = 0; }
	 					if (context.getSessionObject('9d')) { var rackedKeg = context.getSessionObject('9d'); } else { var rackedKeg = 0; }
	 					if (context.getSessionObject('11d')) { var overageKeg = context.getSessionObject('11d'); } else { var overageKeg = 0; }
	 					if (context.getSessionObject('12d')) { var blankKeg = context.getSessionObject('12d'); } else { var blankKeg = 0; }
	 					var kegTotalLine = parseFloat(onHandKeg) + parseFloat(receivedBondKeg) + parseFloat(returnedBreweryKeg) + parseFloat(returnedOwnershipKeg) + parseFloat(rackedKeg) + parseFloat(overageKeg) + parseFloat(blankKeg);

	 					if (context.getSessionObject('1e')) { var onHandBulkB = context.getSessionObject('1e'); } else { var onHandBulkB = 0; }
						if (context.getSessionObject('6e')) { var receivedCellarBulkB = context.getSessionObject('6e'); } else { var receivedCellarBulkB = 0; }
	 					if (context.getSessionObject('7e')) { var returnedBreweryBulkB = context.getSessionObject('7e'); } else { var returnedBreweryBulkB = 0; }
	 					if (context.getSessionObject('8e')) { var returnedOwnershipBulkB = context.getSessionObject('8e'); } else { var returnedOwnershipBulkB = 0; }
	 					if (context.getSessionObject('11e')) { var overageBulkB = context.getSessionObject('11e'); } else { var overageBulkB = 0; }
	 					if (context.getSessionObject('12e')) { var blankBulkB = context.getSessionObject('12e'); } else { var blankBulkB = 0; }
	 					var bulkBTotalLine = parseFloat(onHandBulkB) + parseFloat(receivedCellarBulkB) + parseFloat(returnedBreweryBulkB) + parseFloat(returnedOwnershipBulkB) + parseFloat(overageBulkB) + parseFloat(blankBulkB);

	 					if (context.getSessionObject('1f')) { var onHandCase = context.getSessionObject('1f'); } else { var onHandCase = 0; }
	 					if (context.getSessionObject('5f')) { var receivedBondCase = context.getSessionObject('5f'); } else { var receivedBondCase = 0; }
	 					if (context.getSessionObject('7f')) { var returnedBreweryCase = context.getSessionObject('7f'); } else { var returnedBreweryCase = 0; }
	 					if (context.getSessionObject('8f')) { var returnedOwnershipCase = context.getSessionObject('8f'); } else { var returnedOwnershipCase = 0; }
	 					if (context.getSessionObject('10f')) { var bottledCase = context.getSessionObject('10f'); } else { var bottledCase = 0; }
	 					if (context.getSessionObject('11f')) { var overageCase = context.getSessionObject('11f'); } else { var overageCase = 0; }
	 					if (context.getSessionObject('12f')) { var blankCase = context.getSessionObject('12f'); } else { var blankCase = 0; }
	 					var caseTotalLine = parseFloat(onHandCase) + parseFloat(receivedBondCase) + parseFloat(returnedBreweryCase) + parseFloat(returnedOwnershipCase) + parseFloat(bottledCase) + parseFloat(overageCase) + parseFloat(blankCase);

 						var cellarOnHand = parseFloat(cellarTotalLine) - (parseFloat(removedTDCellar) + parseFloat(removedExportCellar) + parseFloat(removedRDCellar) + parseFloat(removedBreweriesCellar) + parseFloat(beerUnfitCellar) + parseFloat(beerConsumedCellar) + parseFloat(beerRackBulk) + parseFloat(beerBottleBulk) + parseFloat(labSamplesCellar) + parseFloat(beerDestroyedCellar) + parseFloat(beerTransferredCellar) + parseFloat(cellarLoss) + parseFloat(shortageCellar) + parseFloat(blankRemovalsCellar));
	 					nlapiLogExecution('AUDIT', 'Cellar On Hand End', context.getSessionObject('13b') + ' - (' + removedTDCellar + ' + ' + removedExportCellar + ' + ' + removedRDCellar + ' + ' + removedBreweriesCellar + ' + ' + beerUnfitCellar + ' + ' + beerConsumedCellar + ' + ' + beerRackBulk + ' + ' + beerBottleBulk + ' + ' + labSamplesCellar + ' + ' + beerDestroyedCellar + ' + ' + beerTransferredCellar + ' + ' + cellarLoss + ' + ' + shortageCellar + ' + ' + blankRemovalsCellar + ')');
	 					sublist.setLineItemValue('cellar', linenum, cellarOnHand.toFixed(2));

	 					var bulkOnHand = parseFloat(bulkTotalLine) - (parseFloat(returnedCellarsBulk) + parseFloat(beerRackedQty) + parseFloat(labSamplesBulk) + parseFloat(beerDestroyedBulk) + parseFloat(beerTransferredBulk) + parseFloat(bulkRackLoss) + parseFloat(shortageBulk) + parseFloat(blankRemovalsBulk));
	 					nlapiLogExecution('AUDIT', 'Bulk On Hand End', context.getSessionObject('13c') + ' - (' + returnedCellarsBulk + ' + ' + beerRackedQty + ' + ' + bulkRackLoss + ')');
	 					sublist.setLineItemValue('bulk', linenum, bulkOnHand.toFixed(2));

	 					var kegOnHand = parseFloat(kegTotalLine) - (parseFloat(beerRemovedKeg) + parseFloat(beerRemovedTDKeg) + parseFloat(removedExportKeg) + parseFloat(removedSuppliesKeg) + parseFloat(removedRDKeg) + parseFloat(removedBreweriesKeg) + parseFloat(beerConsumedKeg) + parseFloat(labSamplesKeg) + parseFloat(beerDestroyedKeg) + parseFloat(beerTransferredKeg) + parseFloat(kegLoss) + parseFloat(shortageKeg) + parseFloat(blankRemovalsKeg));
	 					sublist.setLineItemValue('keg', linenum, kegOnHand.toFixed(2));

	 					var bulkBOnHand = parseFloat(bulkBTotalLine) - (parseFloat(returnedCellarsBulkB) + parseFloat(beerBottledQty) + parseFloat(labSamplesBottleBulk) + parseFloat(beerDestroyedBulkB) + parseFloat(beerTransferredBulkB) + parseFloat(bulkBottleLoss) + parseFloat(shortageBulkB) + parseFloat(blankRemovalsBulkB));
	 					sublist.setLineItemValue('bulkb', linenum, bulkBOnHand.toFixed(2));
	 					nlapiLogExecution('AUDIT', 'Bulk Bottle On Hand End', context.getSessionObject('13e') + ' - (' + ' + ' + returnedCellarsBulkB + ' + ' + beerBottledQty + ' + ' + labSamplesBottleBulk + ' + ' + beerDestroyedBulkB + ' + ' + beerTransferredBulkB + ' + ' + bulkBottleLoss + ' + ' + shortageBulkB + ' + ' + blankRemovalsBulkB);

	 					var caseOnHand = parseFloat(caseTotalLine) - (parseFloat(beerRemovedCase) + parseFloat(beerRemovedTDCase) + parseFloat(removedExportCase) + parseFloat(removedSuppliesCase) + parseFloat(removedRDCase) + parseFloat(removedBreweriesCase) + parseFloat(beerConsumedCase) + parseFloat(labSamplesCase) + parseFloat(beerDestroyedCase) + parseFloat(beerTransferredCase) + parseFloat(caseLoss) + parseFloat(shortageCase) + parseFloat(blankRemovalsCase));
	 					sublist.setLineItemValue('case', linenum, caseOnHand.toFixed(2));

	 					var onHandTotals = parseFloat(cellarOnHand) + parseFloat(bulkOnHand) + parseFloat(kegOnHand) + parseFloat(bulkBOnHand) + parseFloat(caseOnHand);
	 					sublist.setLineItemValue('totals', linenum, onHandTotals.toFixed(2));

 					}

 				}

			}
 
			else if (step.getName() == "priorperiodadj") {
				
				var sublist = brewersReportAssistant.addSubList("priorperiodadj", "inlineeditor", "Prior Period Adjustments");
 
				sublist.addField("operations", "text", "Operations").setDisplayType('disabled');
				// sublist.addField("viewsearch", "select", "Search Name", "-119").setDisplayType('disabled');
				sublist.addField("priorname", "text", "Prior Period Adjustment");
				sublist.addField("plus", "float", "(+)");
				sublist.addField("minus", "float", "(-)");
 				sublist.setUniqueField("operations");
 				var linenum = 0;

 				var priorperiodadj = [

 					"35. Additions to beer inventory", 
 					"36. Removals from beer inventory"

 				];

 				for (var p = 0; p < priorperiodadj.length; p++) {

 					linenum = 1 + p;
 					sublist.setLineItemValue('operations', linenum, priorperiodadj[p]);
 					if (linenum == 1) { 

 						// sublist.setLineItemValue('viewsearch', linenum, '60');
 						sublist.setLineItemValue('priorname', linenum, context.getSessionObject('35ad')); 
 						sublist.setLineItemValue('plus', linenum, context.getSessionObject('35a')); 
 						sublist.setLineItemValue('minus', linenum, context.getSessionObject('35b')); 

 					} else if (linenum == 2) {
 					
 						// sublist.setLineItemValue('viewsearch', linenum, '55');
 						sublist.setLineItemValue('priorname', linenum, context.getSessionObject('36re'));
 						sublist.setLineItemValue('plus', linenum, context.getSessionObject('36a')); 
 						sublist.setLineItemValue('minus', linenum, context.getSessionObject('36b'));

 					}

 				}

 				brewersReportAssistant.addFieldGroup("priorperiod", "Prior Period Adjustments");
 				brewersReportAssistant.addField("signature", "text", "Signature", null, "priorperiod").setMandatory( false ).setLayoutType('startrow');
 				brewersReportAssistant.addField("title", "text", "Title", null, "priorperiod").setMandatory( false ).setLayoutType('midrow');
 				brewersReportAssistant.addField("date", "date", "Date", null, "priorperiod").setMandatory( false ).setLayoutType('midrow').setDefaultValue(nlapiDateToString(new Date()));

 			}

 		// 	else if (step.getName() == "parttwotax") {
				
			// 	var sublist = brewersReportAssistant.addSubList("parttwotax", "inlineeditor", "Part 2 - Report Period Tax Payments");
 
			// 	sublist.addField("returnserialnumber", "text", "Return Serial Number");
			// 	sublist.addField("datefiled", "text", "Date Filed");
			// 	sublist.addField("taxliability", "currency", "Tax Liability");
			// 	sublist.addField("adjustments", "currency", "Adjustments");
			// 	sublist.addField("amountpaid", "currency", "Amount Paid");
 		// 		sublist.setUniqueField("returnserialnumber");
 		// 		var linenum = 0;

			// }

			// else if (step.getName() == "partthreematerials") {
				
			// 	var sublist = brewersReportAssistant.addSubList("partthreematerials", "inlineeditor", "Part 3 - Summary of Materials Used and Wort Produced");
 
			// 	sublist.addField("item", "text", "Item").setDisplayType('disabled');
			// 	sublist.addField("hopsa", "float", "Hops (pounds) (a)");
			// 	sublist.addField("hopsb", "float", "Hops Extract (pounds) (b)");
			// 	sublist.addField("hopsc", "float", "Hops Extract Equivalent (c)");
			// 	sublist.addField("wortd", "float", "Wort (barrels) (d)");
			// 	sublist.addField("specifye", "float", "Specify (e)");
			// 	sublist.addField("specifyf", "float", "Specify (f)");
			// 	sublist.addField("specifyg", "float", "Specify (g)");
			// 	sublist.addField("specifyh", "float", "Specify (h)");
 		// 		sublist.setUniqueField("item");
 		// 		var linenum = 0;

 		// 		var partthreematerials = [

 		// 			"1. Material for beer & cereal beverage", 
 		// 			"2. Wort received and used",
 		// 			"3. Wort removed",
 		// 			"4. "

 		// 		];

 		// 		for (var m = 0; m < partthreematerials.length; m++) {

 		// 			linenum = 1 + m;
 		// 			sublist.setLineItemValue('item', linenum, partthreematerials[m]);

 		// 		}
			// }
 
			else if (step.getName() == "partfourcereal")	{
				
				var sublist = brewersReportAssistant.addSubList("partfourcereal", "inlineeditor", "Part 4 - Cereal Beverage Summary");
 
				sublist.addField("operations", "text", "Operations").setDisplayType('disabled');
				// sublist.addField("viewsearch", "select", "Search Name", "-119").setDisplayType('disabled');
				sublist.addField("cerealbev", "text", "Cereal Beverage");
				sublist.addField("bbls", "float", "Bbls.");
 				sublist.setUniqueField("operations");
 				var linenum = 0;

 				var partfourcereal = [

 					"1. Produced", 
 					"2. Removed",
 					"3. Received",
 					"4. Loss and wastage",
 					"5. ",
 					"6. Total on hand end of period",

 				];

 				for (var c = 0; c < partfourcereal.length; c++) {

 					linenum = 1 + c;
 					sublist.setLineItemValue('operations', linenum, partfourcereal[c]);

 				}

 					sublist.setLineItemValue('cerealbev', 1, context.getSessionObject('produced'));
 					sublist.setLineItemValue('bbls', 1, context.getSessionObject('producedbbl'));
 					sublist.setLineItemValue('cerealbev', 2, context.getSessionObject('removed'));
 					sublist.setLineItemValue('bbls', 2, context.getSessionObject('removedbbl'));
 					sublist.setLineItemValue('cerealbev', 3, context.getSessionObject('received'));
 					sublist.setLineItemValue('bbls', 3, context.getSessionObject('receivedbbl'));
 					sublist.setLineItemValue('cerealbev', 4, context.getSessionObject('wastage'));
 					sublist.setLineItemValue('bbls', 4, context.getSessionObject('wastagebbl'));
 					sublist.setLineItemValue('cerealbev', 5, context.getSessionObject('5'));
 					sublist.setLineItemValue('bbls', 5, context.getSessionObject('5bbl'));

 					var producedBbl = parseFloat(context.getSessionObject('producedbbl'));
 					if (isNaN(producedBbl)) { producedBbl = 0; } else { producedBbl.toFixed(2); }
 					var receivedBbl = parseFloat(context.getSessionObject('receivedbbl'));
 					if (isNaN(receivedBbl)) { receivedBbl = 0; } else { receivedBbl.toFixed(2); }
 					var removedBbl = parseFloat(context.getSessionObject('removedbbl'));
 					if (isNaN(removedBbl)) { removedBbl = 0; } else { removedBbl.toFixed(2); }
 					var wasteBbl = parseFloat(context.getSessionObject('wastebbl'));
 					if (isNaN(wasteBbl)) { wasteBbl = 0; } else { wasteBbl.toFixed(2); }
 					var fiveBbl = parseFloat(context.getSessionObject('5bbl'));
 					if (isNaN(fiveBbl)) { fiveBbl = 0; } else { fiveBbl.toFixed(2); }

 					var totalBbls = producedBbl + receivedBbl + removedBbl + wasteBbl + fiveBbl;
 					if (isNaN(totalBbls)) { totalBbls = ''; } else { totalBbls.toFixed(2); }
 					sublist.setLineItemValue('bbls', 6, totalBbls);

			}
 
			else if (step.getName() == "partfiveremarks") {
 
				brewersReportAssistant.addFieldGroup("partfiveremarks", "Remarks");
				brewersReportAssistant.addField('remarks','textarea', null, null, 'partfiveremarks');				
			
			}

			else if (step.getName() == "printreport") {

				nlapiLogExecution('DEBUG', 'Brewery EIN: ', context.getSessionObject('breweryein'));
				var fdfcontents = "%FDF-1.2\n";
				fdfcontents += "%âãÏÓ\n";
				fdfcontents += "1 0 obj\n";
				fdfcontents += "<</FDF<</F(BrewersReportofOperations.Blank.pdf)/Fields[<</T(1b)/V(";
				if (context.getSessionObject('1b') != null) { var val1b = context.getSessionObject('1b'); } else { var val1b = ''; }
				fdfcontents += val1b;
				fdfcontents += ")>><</T(1c)/V(";
				if (context.getSessionObject('1c') != null) { var val1c = context.getSessionObject('1c'); } else { var val1c = ''; }
				fdfcontents += val1c;
				fdfcontents += ")>><</T(1d)/V(";
				if (context.getSessionObject('1d') != null) { var val1d = context.getSessionObject('1d'); } else { var val1d = ''; }
				fdfcontents += val1d;
				fdfcontents += ")>><</T(1e)/V(";
				if (context.getSessionObject('1e') != null) { var val1e = context.getSessionObject('1e'); } else { var val1e = ''; }
				fdfcontents += val1e;
				fdfcontents += ")>><</T(1f)/V(";
				if (context.getSessionObject('1f') != null) { var val1f = context.getSessionObject('1f'); } else { var val1f = ''; }
				fdfcontents += val1f;
				fdfcontents += ")>><</T(1g)/V(";
				if (context.getSessionObject('1g') != null) { var val1g = context.getSessionObject('1g'); } else { var val1g = ''; }
				fdfcontents += val1g;
				fdfcontents += ")>><</T(2b)/V(";
				if (context.getSessionObject('2b') != null) { var val2b = context.getSessionObject('2b'); } else { var val2b = ''; }
				fdfcontents += val2b;
				fdfcontents += ")>><</T(2g)/V(";
				if (context.getSessionObject('2g') != null) { var val2g = context.getSessionObject('2g'); } else { var val2g = ''; }
				fdfcontents += val2g;
				fdfcontents += ")>><</T(3b)/V(";
				if (context.getSessionObject('3b') != null) { var val3b = context.getSessionObject('3b'); } else { var val3b = ''; }
				fdfcontents += val3b;
				fdfcontents += ")>><</T(3g)/V(";
				if (context.getSessionObject('3g') != null) { var val3g = context.getSessionObject('3g'); } else { var val3g = ''; }
				fdfcontents += val3g;
				fdfcontents += ")>><</T(4b)/V(";
				if (context.getSessionObject('4b') != null) { var val4b = context.getSessionObject('4b'); } else { var val4b = ''; }
				fdfcontents += val4b;
				fdfcontents += ")>><</T(4g)/V(";
				if (context.getSessionObject('4g') != null) { var val4g = context.getSessionObject('4g'); } else { var val4g = ''; }
				fdfcontents += val4g;
				fdfcontents += ")>><</T(5b)/V(";
				if (context.getSessionObject('5b') != null) { var val5b = context.getSessionObject('5b'); } else { var val5b = ''; }
				fdfcontents += val5b;
				fdfcontents += ")>><</T(5d)/V(";
				if (context.getSessionObject('5d') != null) { var val5d = context.getSessionObject('5d'); } else { var val5d = ''; }
				fdfcontents += val5d;
				fdfcontents += ")>><</T(5f)/V(";
				if (context.getSessionObject('5f') != null) { var val5f = context.getSessionObject('5f'); } else { var val5f = ''; }
				fdfcontents += val5f;
				fdfcontents += ")>><</T(5g)/V(";
				if (context.getSessionObject('5g') != null) { var val5g = context.getSessionObject('5g'); } else { var val5g = ''; }
				fdfcontents += val5g;
				fdfcontents += ")>><</T(6b)/V(";
				if (context.getSessionObject('6b') != null) { var val6b = context.getSessionObject('6b'); } else { var val6b = ''; }
				fdfcontents += val6b;
				fdfcontents += ")>><</T(6c)/V(";
				if (context.getSessionObject('6c') != null) { var val6c = context.getSessionObject('6c'); } else { var val6c = ''; }
				fdfcontents += val6c;
				fdfcontents += ")>><</T(6e)/V(";
				if (context.getSessionObject('6e') != null) { var val6e = context.getSessionObject('6e'); } else { var val6e = ''; }
				fdfcontents += val6e;
				fdfcontents += ")>><</T(6g)/V(";
				if (context.getSessionObject('6g') != null) { var val6g = context.getSessionObject('6g'); } else { var val6g = ''; }
				fdfcontents += val6g;
				fdfcontents += ")>><</T(7b)/V(";
				if (context.getSessionObject('7b') != null) { var val7b = context.getSessionObject('7b'); } else { var val7b = ''; }
				fdfcontents += val7b;
				fdfcontents += ")>><</T(7d)/V(";
				if (context.getSessionObject('7d') != null) { var val7d = context.getSessionObject('7d'); } else { var val7d = ''; }
				fdfcontents += val7d;
				fdfcontents += ")>><</T(7f)/V(";
				if (context.getSessionObject('7f') != null) { var val7f = context.getSessionObject('7f'); } else { var val7f = ''; }
				fdfcontents += val7f;
				fdfcontents += ")>><</T(7g)/V(";
				if (context.getSessionObject('7g') != null) { var val7g = context.getSessionObject('7g'); } else { var val7g = ''; }
				fdfcontents += val7g;
				fdfcontents += ")>><</T(8b)/V(";
				if (context.getSessionObject('8b') != null) { var val8b = context.getSessionObject('8b'); } else { var val8b = ''; }
				fdfcontents += val8b;
				fdfcontents += ")>><</T(8d)/V(";
				if (context.getSessionObject('8d') != null) { var val8d = context.getSessionObject('8d'); } else { var val8d = ''; }
				fdfcontents += val8d;
				fdfcontents += ")>><</T(8f)/V(";
				if (context.getSessionObject('8f') != null) { var val8f = context.getSessionObject('8f'); } else { var val8f = ''; }
				fdfcontents += val8f;
				fdfcontents += ")>><</T(8g)/V(";
				if (context.getSessionObject('8g') != null) { var val8g = context.getSessionObject('8g'); } else { var val8g = ''; }
				fdfcontents += val8g;
				fdfcontents += ")>><</T(9d)/V(";
				if (context.getSessionObject('9d') != null) { var val9d = context.getSessionObject('9d'); } else { var val9d = ''; }
				fdfcontents += val9d;
				fdfcontents += ")>><</T(9g)/V(";
				if (context.getSessionObject('9g') != null) { var val9g = context.getSessionObject('9g'); } else { var val9g = ''; }
				fdfcontents += val9g;
				fdfcontents += ")>><</T(10f)/V(";
				if (context.getSessionObject('10f') != null) { var val10f = context.getSessionObject('10f'); } else { var val10f = ''; }
				fdfcontents += val10f;
				fdfcontents += ")>><</T(10g)/V(";
				if (context.getSessionObject('10g') != null) { var val10g = context.getSessionObject('10g'); } else { var val10g = ''; }
				fdfcontents += val10g;
				fdfcontents += ")>><</T(11b)/V(";
				if (context.getSessionObject('11b') != null) { var val11b = context.getSessionObject('11b'); } else { var val11b = ''; }
				fdfcontents += val11b;
				fdfcontents += ")>><</T(11c)/V(";
				if (context.getSessionObject('11c') != null) { var val11c = context.getSessionObject('11c'); } else { var val11c = ''; }
				fdfcontents += val11c;
				fdfcontents += ")>><</T(11d)/V(";
				if (context.getSessionObject('11d') != null) { var val11d = context.getSessionObject('11d'); } else { var val11d = ''; }
				fdfcontents += val11d;
				fdfcontents += ")>><</T(11e)/V(";
				if (context.getSessionObject('11e') != null) { var val11e = context.getSessionObject('11e'); } else { var val11e = ''; }
				fdfcontents += val11e;
				fdfcontents += ")>><</T(11f)/V(";
				if (context.getSessionObject('11f') != null) { var val11f = context.getSessionObject('11f'); } else { var val11f = ''; }
				fdfcontents += val11f;
				fdfcontents += ")>><</T(11g)/V(";
				if (context.getSessionObject('11g') != null) { var val11g = context.getSessionObject('11g'); } else { var val11g = ''; }
				fdfcontents += val11g;
				fdfcontents += ")>><</T(13b)/V(";
				if (context.getSessionObject('13b') != null) { var val13b = context.getSessionObject('13b'); } else { var val13b = ''; }
				fdfcontents += val13b;
				fdfcontents += ")>><</T(13c)/V(";
				if (context.getSessionObject('13c') != null) { var val13c = context.getSessionObject('13c'); } else { var val13c = ''; }
				fdfcontents += val13c;
				fdfcontents += ")>><</T(13d)/V(";
				if (context.getSessionObject('13d') != null) { var val13d = context.getSessionObject('13d'); } else { var val13d = ''; }
				fdfcontents += val13d;
				fdfcontents += ")>><</T(13e)/V(";
				if (context.getSessionObject('13e') != null) { var val13e = context.getSessionObject('13e'); } else { var val13e = ''; }
				fdfcontents += val13e;
				fdfcontents += ")>><</T(13f)/V(";
				if (context.getSessionObject('13f') != null) { var val13f = context.getSessionObject('13f'); } else { var val13f = ''; }
				fdfcontents += val13f;
				fdfcontents += ")>><</T(13g)/V(";
				if (context.getSessionObject('13g') != null) { var val13g = context.getSessionObject('13g'); } else { var val13g = ''; }
				fdfcontents += val13g;
				fdfcontents += ")>><</T(14b)/V(";
				if (context.getSessionObject('14b') != null) { var val14b = context.getSessionObject('14b'); } else { var val14b = ''; }
				fdfcontents += val14b;
				fdfcontents += ")>><</T(14c)/V(";
				if (context.getSessionObject('14c') != null) { var val14c = context.getSessionObject('14c'); } else { var val14c = ''; }
				fdfcontents += val14c;
				fdfcontents += ")>><</T(14d)/V(";
				if (context.getSessionObject('14d') != null) { var val14d = context.getSessionObject('14d'); } else { var val14d = ''; }
				fdfcontents += val14d;
				fdfcontents += ")>><</T(14e)/V(";
				if (context.getSessionObject('14e') != null) { var val14e = context.getSessionObject('14e'); } else { var val14e = ''; }
				fdfcontents += val14e;
				fdfcontents += ")>><</T(14f)/V(";
				if (context.getSessionObject('14f') != null) { var val14f = context.getSessionObject('14f'); } else { var val14f = ''; }
				fdfcontents += val14f;
				fdfcontents += ")>><</T(14g)/V(";
				if (context.getSessionObject('14g') != null) { var val14g = context.getSessionObject('14g'); } else { var val14g = ''; }
				fdfcontents += val14g;
				fdfcontents += ")>><</T(15b)/V(";
				if (context.getSessionObject('15b') != null) { var val15b = context.getSessionObject('15b'); } else { var val15b = ''; }
				fdfcontents += val15b;
				fdfcontents += ")>><</T(15c)/V(";
				if (context.getSessionObject('15c') != null) { var val15c = context.getSessionObject('15c'); } else { var val15c = ''; }
				fdfcontents += val15c;
				fdfcontents += ")>><</T(15d)/V(";
				if (context.getSessionObject('15d') != null) { var val15d = context.getSessionObject('15d'); } else { var val15d = ''; }
				fdfcontents += val15d;
				fdfcontents += ")>><</T(15e)/V(";
				if (context.getSessionObject('15e') != null) { var val15e = context.getSessionObject('15e'); } else { var val15e = ''; }
				fdfcontents += val15e;
				fdfcontents += ")>><</T(15f)/V(";
				if (context.getSessionObject('15f') != null) { var val15f = context.getSessionObject('15f'); } else { var val15f = ''; }
				fdfcontents += val15f;
				fdfcontents += ")>><</T(15g)/V(";
				if (context.getSessionObject('15g') != null) { var val15g = context.getSessionObject('15g'); } else { var val15g = ''; }
				fdfcontents += val15g;
				fdfcontents += ")>><</T(16b)/V(";
				if (context.getSessionObject('16b') != null) { var val16b = context.getSessionObject('16b'); } else { var val16b = ''; }
				fdfcontents += val16b;
				fdfcontents += ")>><</T(16c)/V(";
				if (context.getSessionObject('16c') != null) { var val16c = context.getSessionObject('16c'); } else { var val16c = ''; }
				fdfcontents += val16c;
				fdfcontents += ")>><</T(16d)/V(";
				if (context.getSessionObject('16d') != null) { var val16d = context.getSessionObject('16d'); } else { var val16d = ''; }
				fdfcontents += val16d;
				fdfcontents += ")>><</T(16e)/V(";
				if (context.getSessionObject('16e') != null) { var val16e = context.getSessionObject('16e'); } else { var val16e = ''; }
				fdfcontents += val16e;
				fdfcontents += ")>><</T(16f)/V(";
				if (context.getSessionObject('16f') != null) { var val16f = context.getSessionObject('16f'); } else { var val16f = ''; }
				fdfcontents += val16f;
				fdfcontents += ")>><</T(16g)/V(";
				if (context.getSessionObject('16g') != null) { var val16g = context.getSessionObject('16g'); } else { var val16g = ''; }
				fdfcontents += val16g;
				fdfcontents += ")>><</T(17b)/V(";
				if (context.getSessionObject('17b') != null) { var val17b = context.getSessionObject('17b'); } else { var val17b = ''; }
				fdfcontents += val17b;
				fdfcontents += ")>><</T(17c)/V(";
				if (context.getSessionObject('17c') != null) { var val17c = context.getSessionObject('17c'); } else { var val17c = ''; }
				fdfcontents += val17c;
				fdfcontents += ")>><</T(17d)/V(";
				if (context.getSessionObject('17d') != null) { var val17d = context.getSessionObject('17d'); } else { var val17d = ''; }
				fdfcontents += val17d;
				fdfcontents += ")>><</T(17e)/V(";
				if (context.getSessionObject('17e') != null) { var val17e = context.getSessionObject('17e'); } else { var val17e = ''; }
				fdfcontents += val17e;
				fdfcontents += ")>><</T(17f)/V(";
				if (context.getSessionObject('17f') != null) { var val17f = context.getSessionObject('17f'); } else { var val17f = ''; }
				fdfcontents += val17f;
				fdfcontents += ")>><</T(17g)/V(";
				if (context.getSessionObject('17g') != null) { var val17g = context.getSessionObject('17g'); } else { var val17g = ''; }
				fdfcontents += val17g;
				fdfcontents += ")>><</T(18b)/V(";
				if (context.getSessionObject('18b') != null) { var val18b = context.getSessionObject('18b'); } else { var val18b = ''; }
				fdfcontents += val18b;
				fdfcontents += ")>><</T(18c)/V(";
				if (context.getSessionObject('18c') != null) { var val18c = context.getSessionObject('18c'); } else { var val18c = ''; }
				fdfcontents += val18c;
				fdfcontents += ")>><</T(18d)/V(";
				if (context.getSessionObject('18d') != null) { var val18d = context.getSessionObject('18d'); } else { var val18d = ''; }
				fdfcontents += val18d;
				fdfcontents += ")>><</T(18e)/V(";
				if (context.getSessionObject('18e') != null) { var val18e = context.getSessionObject('18e'); } else { var val18e = ''; }
				fdfcontents += val18e;
				fdfcontents += ")>><</T(18f)/V(";
				if (context.getSessionObject('18f') != null) { var val18f = context.getSessionObject('18f'); } else { var val18f = ''; }
				fdfcontents += val18f;
				fdfcontents += ")>><</T(18g)/V(";
				if (context.getSessionObject('18g') != null) { var val18g = context.getSessionObject('18g'); } else { var val18g = ''; }
				fdfcontents += val18g;
				fdfcontents += ")>><</T(19b)/V(";
				if (context.getSessionObject('19b') != null) { var val19b = context.getSessionObject('19b'); } else { var val19b = ''; }
				fdfcontents += val19b;
				fdfcontents += ")>><</T(19c)/V(";
				if (context.getSessionObject('19c') != null) { var val19c = context.getSessionObject('19c'); } else { var val19c = ''; }
				fdfcontents += val19c;
				fdfcontents += ")>><</T(19d)/V(";
				if (context.getSessionObject('19d') != null) { var val19d = context.getSessionObject('19d'); } else { var val19d = ''; }
				fdfcontents += val19d;
				fdfcontents += ")>><</T(19e)/V(";
				if (context.getSessionObject('19e') != null) { var val19e = context.getSessionObject('19e'); } else { var val19e = ''; }
				fdfcontents += val19e;
				fdfcontents += ")>><</T(19f)/V(";
				if (context.getSessionObject('19f') != null) { var val19f = context.getSessionObject('19f'); } else { var val19f = ''; }
				fdfcontents += val19f;
				fdfcontents += ")>><</T(19g)/V(";
				if (context.getSessionObject('19g') != null) { var val19g = context.getSessionObject('19g'); } else { var val19g = ''; }
				fdfcontents += val19g;
				fdfcontents += ")>><</T(20b)/V(";
				if (context.getSessionObject('20b') != null) { var val20b = context.getSessionObject('20b'); } else { var val20b = ''; }
				fdfcontents += val20b;
				fdfcontents += ")>><</T(20c)/V(";
				if (context.getSessionObject('20c') != null) { var val20c = context.getSessionObject('20c'); } else { var val20c = ''; }
				fdfcontents += val20c;
				fdfcontents += ")>><</T(20d)/V(";
				if (context.getSessionObject('20d') != null) { var val20d = context.getSessionObject('20d'); } else { var val20d = ''; }
				fdfcontents += val20d;
				fdfcontents += ")>><</T(20e)/V(";
				if (context.getSessionObject('20e') != null) { var val20e = context.getSessionObject('20e'); } else { var val20e = ''; }
				fdfcontents += val20e;
				fdfcontents += ")>><</T(20f)/V(";
				if (context.getSessionObject('20f') != null) { var val20f = context.getSessionObject('20f'); } else { var val20f = ''; }
				fdfcontents += val20f;
				fdfcontents += ")>><</T(20g)/V(";
				if (context.getSessionObject('20g') != null) { var val20g = context.getSessionObject('20g'); } else { var val20g = ''; }
				fdfcontents += val20g;
				fdfcontents += ")>><</T(21b)/V(";
				if (context.getSessionObject('21b') != null) { var val21b = context.getSessionObject('21b'); } else { var val21b = ''; }
				fdfcontents += val21b;
				fdfcontents += ")>><</T(21c)/V(";
				if (context.getSessionObject('21c') != null) { var val21c = context.getSessionObject('21c'); } else { var val21c = ''; }
				fdfcontents += val21c;
				fdfcontents += ")>><</T(21d)/V(";
				if (context.getSessionObject('21d') != null) { var val21d = context.getSessionObject('21d'); } else { var val21d = ''; }
				fdfcontents += val21d;
				fdfcontents += ")>><</T(21e)/V(";
				if (context.getSessionObject('21e') != null) { var val21e = context.getSessionObject('21e'); } else { var val21e = ''; }
				fdfcontents += val21e;
				fdfcontents += ")>><</T(21f)/V(";
				if (context.getSessionObject('21f') != null) { var val21f = context.getSessionObject('21f'); } else { var val21f = ''; }
				fdfcontents += val21f;
				fdfcontents += ")>><</T(21g)/V(";
				if (context.getSessionObject('21g') != null) { var val21g = context.getSessionObject('21g'); } else { var val21g = ''; }
				fdfcontents += val21g;
				fdfcontents += ")>><</T(22b)/V(";
				if (context.getSessionObject('22b') != null) { var val22b = context.getSessionObject('22b'); } else { var val22b = ''; }
				fdfcontents += val22b;
				fdfcontents += ")>><</T(22c)/V(";
				if (context.getSessionObject('22c') != null) { var val22c = context.getSessionObject('22c'); } else { var val22c = ''; }
				fdfcontents += val22c;
				fdfcontents += ")>><</T(22d)/V(";
				if (context.getSessionObject('22d') != null) { var val22d = context.getSessionObject('22d'); } else { var val22d = ''; }
				fdfcontents += val22d;
				fdfcontents += ")>><</T(22e)/V(";
				if (context.getSessionObject('22e') != null) { var val22e = context.getSessionObject('22e'); } else { var val22e = ''; }
				fdfcontents += val22e;
				fdfcontents += ")>><</T(22f)/V(";
				if (context.getSessionObject('22f') != null) { var val22f = context.getSessionObject('22f'); } else { var val22f = ''; }
				fdfcontents += val22f;
				fdfcontents += ")>><</T(22g)/V(";
				if (context.getSessionObject('22g') != null) { var val22g = context.getSessionObject('22g'); } else { var val22g = ''; }
				fdfcontents += val22g;
				fdfcontents += ")>><</T(23b)/V(";
				if (context.getSessionObject('23b') != null) { var val23b = context.getSessionObject('23b'); } else { var val23b = ''; }
				fdfcontents += val23b;
				fdfcontents += ")>><</T(23c)/V(";
				if (context.getSessionObject('23c') != null) { var val23c = context.getSessionObject('23c'); } else { var val23c = ''; }
				fdfcontents += val23c;
				fdfcontents += ")>><</T(23d)/V(";
				if (context.getSessionObject('23d') != null) { var val23d = context.getSessionObject('23d'); } else { var val23d = ''; }
				fdfcontents += val23d;
				fdfcontents += ")>><</T(23e)/V(";
				if (context.getSessionObject('23e') != null) { var val23e = context.getSessionObject('23e'); } else { var val23e = ''; }
				fdfcontents += val23e;
				fdfcontents += ")>><</T(23f)/V(";
				if (context.getSessionObject('23f') != null) { var val23f = context.getSessionObject('23f'); } else { var val23f = ''; }
				fdfcontents += val23f;
				fdfcontents += ")>><</T(23g)/V(";
				if (context.getSessionObject('23g') != null) { var val23g = context.getSessionObject('23g'); } else { var val23g = ''; }
				fdfcontents += val23g;
				fdfcontents += ")>><</T(24b)/V(";
				if (context.getSessionObject('24b') != null) { var val24b = context.getSessionObject('24b'); } else { var val24b = ''; }
				fdfcontents += val24b;
				fdfcontents += ")>><</T(24c)/V(";
				if (context.getSessionObject('24c') != null) { var val24c = context.getSessionObject('24c'); } else { var val24c = ''; }
				fdfcontents += val24c;
				fdfcontents += ")>><</T(24d)/V(";
				if (context.getSessionObject('24d') != null) { var val24d = context.getSessionObject('24d'); } else { var val24d = ''; }
				fdfcontents += val24d;
				fdfcontents += ")>><</T(24e)/V(";
				if (context.getSessionObject('24e') != null) { var val24e = context.getSessionObject('24e'); } else { var val24e = ''; }
				fdfcontents += val24e;
				fdfcontents += ")>><</T(24f)/V(";
				if (context.getSessionObject('24f') != null) { var val24f = context.getSessionObject('24f'); } else { var val24f = ''; }
				fdfcontents += val24f;
				fdfcontents += ")>><</T(24g)/V(";
				if (context.getSessionObject('24g') != null) { var val24g = context.getSessionObject('24g'); } else { var val24g = ''; }
				fdfcontents += val24g;
				fdfcontents += ")>><</T(25b)/V(";
				if (context.getSessionObject('25b') != null) { var val25b = context.getSessionObject('25b'); } else { var val25b = ''; }
				fdfcontents += val25b;
				fdfcontents += ")>><</T(25c)/V(";
				if (context.getSessionObject('25c') != null) { var val25c = context.getSessionObject('25c'); } else { var val25c = ''; }
				fdfcontents += val25c;
				fdfcontents += ")>><</T(25d)/V(";
				if (context.getSessionObject('25d') != null) { var val25d = context.getSessionObject('25d'); } else { var val25d = ''; }
				fdfcontents += val25d;
				fdfcontents += ")>><</T(25e)/V(";
				if (context.getSessionObject('25e') != null) { var val25e = context.getSessionObject('25e'); } else { var val25e = ''; }
				fdfcontents += val25e;
				fdfcontents += ")>><</T(25f)/V(";
				if (context.getSessionObject('25f') != null) { var val25f = context.getSessionObject('25f'); } else { var val25f = ''; }
				fdfcontents += val25f;
				fdfcontents += ")>><</T(25g)/V(";
				if (context.getSessionObject('25g') != null) { var val25g = context.getSessionObject('25g'); } else { var val25g = ''; }
				fdfcontents += val25g;
				fdfcontents += ")>><</T(26b)/V(";
				if (context.getSessionObject('26b') != null) { var val26b = context.getSessionObject('26b'); } else { var val26b = ''; }
				fdfcontents += val26b;
				fdfcontents += ")>><</T(26c)/V(";
				if (context.getSessionObject('26c') != null) { var val26c = context.getSessionObject('26c'); } else { var val26c = ''; }
				fdfcontents += val26c;
				fdfcontents += ")>><</T(26d)/V(";
				if (context.getSessionObject('26d') != null) { var val26d = context.getSessionObject('26d'); } else { var val26d = ''; }
				fdfcontents += val26d;
				fdfcontents += ")>><</T(26e)/V(";
				if (context.getSessionObject('26e') != null) { var val26e = context.getSessionObject('26e'); } else { var val26e = ''; }
				fdfcontents += val26e;
				fdfcontents += ")>><</T(26f)/V(";
				if (context.getSessionObject('26f') != null) { var val26f = context.getSessionObject('26f'); } else { var val26f = ''; }
				fdfcontents += val26f;
				fdfcontents += ")>><</T(26g)/V(";
				if (context.getSessionObject('26g') != null) { var val26g = context.getSessionObject('26g'); } else { var val26g = ''; }
				fdfcontents += val26g;
				fdfcontents += ")>><</T(27b)/V(";
				if (context.getSessionObject('27b') != null) { var val27b = context.getSessionObject('27b'); } else { var val27b = ''; }
				fdfcontents += val27b;
				fdfcontents += ")>><</T(27c)/V(";
				if (context.getSessionObject('27c') != null) { var val27c = context.getSessionObject('27c'); } else { var val27c = ''; }
				fdfcontents += val27c;
				fdfcontents += ")>><</T(27d)/V(";
				if (context.getSessionObject('27d') != null) { var val27d = context.getSessionObject('27d'); } else { var val27d = ''; }
				fdfcontents += val27d;
				fdfcontents += ")>><</T(27e)/V(";
				if (context.getSessionObject('27e') != null) { var val27e = context.getSessionObject('27e'); } else { var val27e = ''; }
				fdfcontents += val27e;
				fdfcontents += ")>><</T(27f)/V(";
				if (context.getSessionObject('27f') != null) { var val27f = context.getSessionObject('27f'); } else { var val27f = ''; }
				fdfcontents += val27f;
				fdfcontents += ")>><</T(27g)/V(";
				if (context.getSessionObject('27g') != null) { var val27g = context.getSessionObject('27g'); } else { var val27g = ''; }
				fdfcontents += val27g;
				fdfcontents += ")>><</T(28b)/V(";
				if (context.getSessionObject('28b') != null) { var val28b = context.getSessionObject('28b'); } else { var val28b = ''; }
				fdfcontents += val28b;
				fdfcontents += ")>><</T(28c)/V(";
				if (context.getSessionObject('28c') != null) { var val28c = context.getSessionObject('28c'); } else { var val28c = ''; }
				fdfcontents += val28c;
				fdfcontents += ")>><</T(28d)/V(";
				if (context.getSessionObject('28d') != null) { var val28d = context.getSessionObject('28d'); } else { var val28d = ''; }
				fdfcontents += val28d;
				fdfcontents += ")>><</T(28e)/V(";
				if (context.getSessionObject('28e') != null) { var val28e = context.getSessionObject('28e'); } else { var val28e = ''; }
				fdfcontents += val28e;
				fdfcontents += ")>><</T(28f)/V(";
				if (context.getSessionObject('28f') != null) { var val28f = context.getSessionObject('28f'); } else { var val28f = ''; }
				fdfcontents += val28f;
				fdfcontents += ")>><</T(28g)/V(";
				if (context.getSessionObject('28g') != null) { var val28g = context.getSessionObject('28g'); } else { var val28g = ''; }
				fdfcontents += val28g;
				fdfcontents += ")>><</T(29b)/V(";
				if (context.getSessionObject('29b') != null) { var val29b = context.getSessionObject('29b'); } else { var val29b = ''; }
				fdfcontents += val29b;
				fdfcontents += ")>><</T(29c)/V(";
				if (context.getSessionObject('29c') != null) { var val29c = context.getSessionObject('29c'); } else { var val29c = ''; }
				fdfcontents += val29c;
				fdfcontents += ")>><</T(29d)/V(";
				if (context.getSessionObject('29d') != null) { var val29d = context.getSessionObject('29d'); } else { var val29d = ''; }
				fdfcontents += val29d;
				fdfcontents += ")>><</T(29e)/V(";
				if (context.getSessionObject('29e') != null) { var val29e = context.getSessionObject('29e'); } else { var val29e = ''; }
				fdfcontents += val29e;
				fdfcontents += ")>><</T(29f)/V(";
				if (context.getSessionObject('29f') != null) { var val29f = context.getSessionObject('29f'); } else { var val29f = ''; }
				fdfcontents += val29f;
				fdfcontents += ")>><</T(29g)/V(";
				if (context.getSessionObject('29g') != null) { var val29g = context.getSessionObject('29g'); } else { var val29g = ''; }
				fdfcontents += val29g;
				fdfcontents += ")>><</T(30b)/V(";
				if (context.getSessionObject('30b') != null) { var val30b = context.getSessionObject('30b'); } else { var val30b = ''; }
				fdfcontents += val30b;
				fdfcontents += ")>><</T(30c)/V(";
				if (context.getSessionObject('30c') != null) { var val30c = context.getSessionObject('30c'); } else { var val30c = ''; }
				fdfcontents += val30c;
				fdfcontents += ")>><</T(30d)/V(";
				if (context.getSessionObject('30d') != null) { var val30d = context.getSessionObject('30d'); } else { var val30d = ''; }
				fdfcontents += val30d;
				fdfcontents += ")>><</T(30e)/V(";
				if (context.getSessionObject('30e') != null) { var val30e = context.getSessionObject('30e'); } else { var val30e = ''; }
				fdfcontents += val30e;
				fdfcontents += ")>><</T(30f)/V(";
				if (context.getSessionObject('30f') != null) { var val30f = context.getSessionObject('30f'); } else { var val30f = ''; }
				fdfcontents += val30f;
				fdfcontents += ")>><</T(30g)/V(";
				if (context.getSessionObject('30g') != null) { var val30g = context.getSessionObject('30g'); } else { var val30g = ''; }
				fdfcontents += val30g;
				fdfcontents += ")>><</T(31b)/V(";
				if (context.getSessionObject('31b') != null) { var val31b = context.getSessionObject('31b'); } else { var val31b = ''; }
				fdfcontents += val31b;
				fdfcontents += ")>><</T(31c)/V(";
				if (context.getSessionObject('31c') != null) { var val31c = context.getSessionObject('31c'); } else { var val31c = ''; }
				fdfcontents += val31c;
				fdfcontents += ")>><</T(31d)/V(";
				if (context.getSessionObject('31d') != null) { var val31d = context.getSessionObject('31d'); } else { var val31d = ''; }
				fdfcontents += val31d;
				fdfcontents += ")>><</T(31e)/V(";
				if (context.getSessionObject('31e') != null) { var val31e = context.getSessionObject('31e'); } else { var val31e = ''; }
				fdfcontents += val31e;
				fdfcontents += ")>><</T(31f)/V(";
				if (context.getSessionObject('31f') != null) { var val31f = context.getSessionObject('31f'); } else { var val31f = ''; }
				fdfcontents += val31f;
				fdfcontents += ")>><</T(31g)/V(";
				if (context.getSessionObject('31g') != null) { var val31g = context.getSessionObject('31g'); } else { var val31g = ''; }
				fdfcontents += val31g;
				fdfcontents += ")>><</T(32b)/V(";
				if (context.getSessionObject('32b') != null) { var val32b = context.getSessionObject('32b'); } else { var val32b = ''; }
				fdfcontents += val32b;
				fdfcontents += ")>><</T(32c)/V(";
				if (context.getSessionObject('32c') != null) { var val32c = context.getSessionObject('32c'); } else { var val32c = ''; }
				fdfcontents += val32c;
				fdfcontents += ")>><</T(32d)/V(";
				if (context.getSessionObject('32d') != null) { var val32d = context.getSessionObject('32d'); } else { var val32d = ''; }
				fdfcontents += val32d;
				fdfcontents += ")>><</T(32e)/V(";
				if (context.getSessionObject('32e') != null) { var val32e = context.getSessionObject('32e'); } else { var val32e = ''; }
				fdfcontents += val32e;
				fdfcontents += ")>><</T(32f)/V(";
				if (context.getSessionObject('32f') != null) { var val32f = context.getSessionObject('32f'); } else { var val32f = ''; }
				fdfcontents += val32f;
				fdfcontents += ")>><</T(32g)/V(";
				if (context.getSessionObject('32g') != null) { var val32g = context.getSessionObject('32g'); } else { var val32g = ''; }
				fdfcontents += val32g;
				fdfcontents += ")>><</T(33b)/V(";
				if (context.getSessionObject('33b') != null) { var val33b = context.getSessionObject('33b'); } else { var val33b = ''; }
				fdfcontents += val33b;
				fdfcontents += ")>><</T(33c)/V(";
				if (context.getSessionObject('33c') != null) { var val33c = context.getSessionObject('33c'); } else { var val33c = ''; }
				fdfcontents += val33c;
				fdfcontents += ")>><</T(33d)/V(";
				if (context.getSessionObject('33d') != null) { var val33d = context.getSessionObject('33d'); } else { var val33d = ''; }
				fdfcontents += val33d;
				fdfcontents += ")>><</T(33e)/V(";
				if (context.getSessionObject('33e') != null) { var val33e = context.getSessionObject('33e'); } else { var val33e = ''; }
				fdfcontents += val33e;
				fdfcontents += ")>><</T(33f)/V(";
				if (context.getSessionObject('33f') != null) { var val33f = context.getSessionObject('33f'); } else { var val33f = ''; }
				fdfcontents += val33f;
				fdfcontents += ")>><</T(33g)/V(";
				if (context.getSessionObject('33g') != null) { var val33g = context.getSessionObject('33g'); } else { var val33g = ''; }
				fdfcontents += val33g;
				fdfcontents += ")>><</T(34b)/V(";
				if (context.getSessionObject('34b') != null) { var val34b = context.getSessionObject('34b'); } else { var val34b = ''; }
				fdfcontents += val34b;
				fdfcontents += ")>><</T(34c)/V(";
				if (context.getSessionObject('34c') != null) { var val34c = context.getSessionObject('34c'); } else { var val34c = ''; }
				fdfcontents += val34c;
				fdfcontents += ")>><</T(34d)/V(";
				if (context.getSessionObject('34d') != null) { var val34d = context.getSessionObject('34d'); } else { var val34d = ''; }
				fdfcontents += val34d;
				fdfcontents += ")>><</T(34e)/V(";
				if (context.getSessionObject('34e') != null) { var val34e = context.getSessionObject('34e'); } else { var val34e = ''; }
				fdfcontents += val34e;
				fdfcontents += ")>><</T(34f)/V(";
				if (context.getSessionObject('34f') != null) { var val34f = context.getSessionObject('34f'); } else { var val34f = ''; }
				fdfcontents += val34f;
				fdfcontents += ")>><</T(34g)/V(";
				if (context.getSessionObject('34g') != null) { var val34g = context.getSessionObject('34g'); } else { var val34g = ''; }
				fdfcontents += val34g;
				fdfcontents += ")>><</T(35a)/V(";
				if (context.getSessionObject('35a') != null) { var val35a = context.getSessionObject('35a'); } else { var val35a = ''; }
				fdfcontents += val35a;
				fdfcontents += ")>><</T(35b)/V(";
				if (context.getSessionObject('35b') != null) { var val35b = context.getSessionObject('35b'); } else { var val35b = ''; }
				fdfcontents += val35b;
				fdfcontents += ")>><</T(36a)/V(";
				if (context.getSessionObject('36a') != null) { var val36a = context.getSessionObject('36a'); } else { var val36a = ''; }
				fdfcontents += val36a;
				fdfcontents += ")>><</T(36b)/V(";
				if (context.getSessionObject('36b') != null) { var val36b = context.getSessionObject('36b'); } else { var val36b = ''; }
				fdfcontents += val36b;
				fdfcontents += ")>><</T(35ad)/V(";
				if (context.getSessionObject('35ad') != null) { var val35ad = context.getSessionObject('35ad'); } else { var val35ad = ''; }
				fdfcontents += val35ad;
				fdfcontents += ")>><</T(36re)/V(";
				if (context.getSessionObject('36re') != null) { var val36re = context.getSessionObject('36re'); } else { var val36re = ''; }
				fdfcontents += val36re;
				fdfcontents += ")>><</T(produced)/V(";
				if (context.getSessionObject('produced') != null) { var valproduced = context.getSessionObject('produced'); } else { var valproduced = ''; }
				fdfcontents += valproduced;
				fdfcontents += ")>><</T(received)/V(";
				if (context.getSessionObject('received') != null) { var valreceived = context.getSessionObject('received'); } else { var valreceived = ''; }
				fdfcontents += valreceived;
				fdfcontents += ")>><</T(removed)/V(";
				if (context.getSessionObject('removed') != null) { var valremoved = context.getSessionObject('removed'); } else { var valremoved = ''; }
				fdfcontents += valremoved;
				fdfcontents += ")>><</T(wastage)/V(";
				if (context.getSessionObject('wastage') != null) { var valwastage = context.getSessionObject('wastage'); } else { var valwastage = ''; }
				fdfcontents += valwastage;
				fdfcontents += ")>><</T(5)/V(";
				if (context.getSessionObject('5') != null) { var val5 = context.getSessionObject('5'); } else { var val5 = ''; }
				fdfcontents += val5;
				fdfcontents += ")>><</T(producedbbl)/V(";
				if (context.getSessionObject('producedbbl') != null) { var valproducedbbl = context.getSessionObject('producedbbl'); } else { var valproducedbbl = ''; }
				fdfcontents += valproducedbbl;
				fdfcontents += ")>><</T(receivedbbl)/V(";
				if (context.getSessionObject('receivedbbl') != null) { var valreceivedbbl = context.getSessionObject('receivedbbl'); } else { var valreceivedbbl = ''; }
				fdfcontents += valreceivedbbl;
				fdfcontents += ")>><</T(removedbbl)/V(";
				if (context.getSessionObject('removedbbl') != null) { var valremovedbbl = context.getSessionObject('removedbbl'); } else { var valremovedbbl = ''; }
				fdfcontents += valremovedbbl;
				fdfcontents += ")>><</T(wastagebbl)/V(";
				if (context.getSessionObject('wastagebbl') != null) { var valwastagebbl = context.getSessionObject('wastagebbl'); } else { var valwastagebbl = ''; }
				fdfcontents += valwastagebbl;
				fdfcontents += ")>><</T(5bbl)/V(";
				if (context.getSessionObject('5bbl') != null) { var val5bbl = context.getSessionObject('5bbl'); } else { var val5bbl = ''; }
				fdfcontents += val5bbl;
				fdfcontents += ")>><</T(totalbbl)/V(";
				if (context.getSessionObject('totalbbl') != null) { var valtotalbbl = context.getSessionObject('totalbbl'); } else { var valtotalbbl = ''; }
				fdfcontents += valtotalbbl;
				fdfcontents += ")>><</T(breweryaddr1)/V(";
				fdfcontents += context.getSessionObject('breweryaddr1');
				fdfcontents += ")>><</T(breweryein)/V(";
				fdfcontents += context.getSessionObject('breweryein');
				fdfcontents += ")>><</T(breweryname)/V(";
				fdfcontents += context.getSessionObject('breweryname');
				fdfcontents += ")>><</T(brewerydba)/V(";
				fdfcontents += context.getSessionObject('brewerydba');
				fdfcontents += ")>><</T(brewerycity)/V(";
				fdfcontents += context.getSessionObject('brewerycity');
				fdfcontents += ")>><</T(brewerycounty)/V(";
				fdfcontents += context.getSessionObject('brewerycounty');
				fdfcontents += ")>><</T(date)/V(";
				fdfcontents += context.getSessionObject('date');
				fdfcontents += ")>><</T(breweryphone)/V(";
				fdfcontents += context.getSessionObject('breweryphone');
				fdfcontents += ")>><</T(q1)/V/";
				if (context.getSessionObject('quarterselection') == 'q1') { fdfcontents += 'Yes'; } else { fdfcontents += 'Off'; } nlapiLogExecution('AUDIT', 'Q1', context.getSessionObject('quarterselection'));
				fdfcontents += ">><</T(q2)/V/";
				if (context.getSessionObject('quarterselection') == 'q2') { fdfcontents += 'Yes'; } else { fdfcontents += 'Off'; } nlapiLogExecution('AUDIT', 'Q2', context.getSessionObject('quarterselection'));
				fdfcontents += ">><</T(q3)/V/";
				if (context.getSessionObject('quarterselection') == 'q3') { fdfcontents += 'Yes'; } else { fdfcontents += 'Off'; } nlapiLogExecution('AUDIT', 'Q3', context.getSessionObject('quarterselection'));
				fdfcontents += ">><</T(q4)/V/";
				if (context.getSessionObject('quarterselection') == 'q4') { fdfcontents += 'Yes'; } else { fdfcontents += 'Off'; } nlapiLogExecution('AUDIT', 'Q4', context.getSessionObject('quarterselection'));
				fdfcontents += ">><</T(remarks)/V(";
				fdfcontents += context.getSessionObject('remarks');
				fdfcontents += ")>><</T(reportingmonth)/V(";
				fdfcontents += context.getSessionObject('reportingmonth');
				fdfcontents += ")>><</T(reportingyear)/V(";
				fdfcontents += context.getSessionObject('reportingyear');
				fdfcontents += ")>><</T(brewerystate)/V(";
				fdfcontents += context.getSessionObject('brewerystate');
				fdfcontents += ")>><</T(title)/V(";
				fdfcontents += context.getSessionObject('title');
				fdfcontents += ")>><</T(breweryttbnumber)/V(";
				fdfcontents += context.getSessionObject('breweryttbnumber');
				fdfcontents += ")>><</T(breweryzipcode)/V(";
				fdfcontents += context.getSessionObject('breweryzipcode');
				fdfcontents += ")>>]/ID[<6F5A9E7166151F8C705D013FF82F7C6E><4F89438ECF1F954391C5F02BA2A51DFF>]/UF(BrewersReportofOperations.pdf)>>/Type/Catalog>>\n";
				fdfcontents += "endobj\n";
				fdfcontents += "trailer\n"
				fdfcontents += "<</Root 1 0 R>>\n";
				fdfcontents += "%%EOF";

				var textfile = nlapiCreateFile('TTB-Report-' + new Date() + '.fdf', 'PLAINTEXT', fdfcontents);
				textfile.setFolder(context.getSetting('SCRIPT', 'custscript_h_berp_brewers_report_folder'));
				var id = nlapiSubmitFile(textfile);

				var file = nlapiLoadFile(id);
				nlapiLogExecution('DEBUG', 'FDF URL: ', file.getURL());

				response.setContentType('PLAINTEXT', 'BrewersReportOfOperationsAssistant.fdf', 'attachment');
				response.write(file.getValue());
 				
 				//var fileURL = 'https://system.na1.netsuite.com';  //mgs removed case 2768 no need to add the domain
 				//fileURL += file.getURL();  //mgs removed case 2768
 				var fileURL = file.getURL();
 				nlapiLogExecution('DEBUG', 'URL: ', fileURL);
				var pdffile = fileURL.replace("xt=.txt", "xd=T&_xt=.pdf");
				var pdfdownloadurl = nlapiLoadFile(context.getSetting('SCRIPT', 'custscript_h_berp_brewers_report'));
				var pdfFileURL = pdfdownloadurl.getURL();
				brewersReportAssistant.addFieldGroup("downloadpdf", "Download the .PDF Template File");
				brewersReportAssistant.addField("downloadpdffile", "url", "", null, "downloadpdf" ).setDisplayType( "inline" ).setLinkText( "Download PDF Template").setDefaultValue(pdfFileURL);
				brewersReportAssistant.addFieldGroup("downloadfdf", "Download the .FDF File");
				brewersReportAssistant.addField("downloadfdffile", "url", "", null, "downloadfdf" ).setDisplayType( "inline" ).setLinkText( "Download FDF File").setDefaultValue(pdffile);	
				brewersReportAssistant.addField("fdffile", "integer", 'File Internal ID', null, "downloadfdf" ).setDisplayType('hidden').setDefaultValue(id);

			} else if (step.getName() == "savereport") {



			}
			
		}

		response.writePage(brewersReportAssistant);
	
	} else {
 
		brewersReportAssistant.setError(null);
 
		if (brewersReportAssistant.getLastAction() == "finish") {
			
			brewersReportAssistant.setFinished("You have completed the Brewer's Report of Operations Assistant.");

			//var selectedBreweryLocation = searchBreweryLocations(context.getSessionObject('brewerylocation'));  //mgs removed case 2768
			//nlapiLogExecution('DEBUG', 'Brewery Location', selectedBreweryLocation[0].getValue('internalid'));  //mgs removed case 2768

			// var selectedreportingyear = getAccountingPeriod(context.getSessionObject('reportingyear'), context.getSessionObject('reportingmonth'));

			brewersReportAssistant.sendRedirect(response);

			// nlapiLogExecution('DEBUG', 'ID: ', context.getSetting('SCRIPT', 'custscript_berp_broo_internal'));
			// nlapiSetFieldValue('custscript_berp_broo_internal', 'TEST');
			// nlapiLogExecution('DEBUG', 'INTERNAL: ', context.getSetting('SESSION', 'sessionobject'));
			// var newsessionobject = context.getSetting('SESSION', 'sessionobject');
			// nlapiLogExecution('DEBUG', 'Session Object in Finish', newsessionobject);

			var brewersReportRecord = nlapiCreateRecord('customrecord_h_berp_brewers_report');
			brewersReportRecord.setFieldValue('name', context.getSessionObject('reportingmonth') + ' ' + context.getSessionObject('reportingyear'));
			//brewersReportRecord.setFieldValue('custrecord_h_berp_brewery_location', selectedBreweryLocation[0].getValue('internalid'));  //mgs removed case 2768
			brewersReportRecord.setFieldValue('custrecord_h_berp_brew_license', context.getSessionObject('brewerylicense'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_reporting_period', context.getSessionObject('reportingperiod'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_quarter', context.getSessionObject('quarterselection'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_report_type', context.getSessionObject('monthlyorquarterly'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_brewers_report_pdf', context.getSessionObject('pdffile'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_bonh_cellar', context.getSessionObject('33b'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_bonh_rack_b', context.getSessionObject('33c'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_bonh_rack_keg', context.getSessionObject('33d'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_bonh_bottle_b', context.getSessionObject('33e'));
			brewersReportRecord.setFieldValue('custrecord_h_berp_bonh_bottle_case', context.getSessionObject('33f'));
			var brewersRecordId = nlapiSubmitRecord(brewersReportRecord, true);

		} else if (brewersReportAssistant.getLastAction() == "cancel") {

			nlapiSetRedirectURL('tasklink', "CARD_-10");

		} else {
 
			if (brewersReportAssistant.getLastStep().getName() == "breweryinformation" && brewersReportAssistant.getLastAction() == "next" ) {
				
				nlapiLogExecution('DEBUG', 'EIN: ', request.getParameter('breweryein'));
              nlapiLogExecution('DEBUG', 'Brewery License: ', request.getParameter('brewerylicense'));
            
				
				// var sessionobject = {

				// 	breweryein:request.getParameter('breweryein'), 
				// 	ttbnumber:request.getParameter('ttbnumber'), 
				// 	phone:request.getParameter('phone')

				// };
				context.setSessionObject('brewerylicense', request.getParameter('brewerylicense'));
				//context.setSessionObject('brewerylocation', request.getParameter('brewerylocation')); //mgs removed case 2768
				context.setSessionObject('breweryein', request.getParameter('breweryein'));
				context.setSessionObject('breweryttbnumber', request.getParameter('breweryttbnumber'));
				context.setSessionObject('breweryphone', request.getParameter('breweryphone'));
				context.setSessionObject('breweryname', request.getParameter('breweryname'));
				context.setSessionObject('brewerydba', request.getParameter('brewerydba'));
				context.setSessionObject('breweryaddr1', request.getParameter('breweryaddr1'));
				context.setSessionObject('brewerycity', request.getParameter('brewerycity'));
				context.setSessionObject('brewerystate', request.getParameter('brewerystate'));
				context.setSessionObject('brewerycounty', request.getParameter('brewerycounty'));
				context.setSessionObject('breweryzipcode', request.getParameter('breweryzipcode'));
				// context.setSessionObject('customer', request.getParameter('customer'));
				// context.setSetting('SESSION', 'sessionobject', sessionobject);
				// nlapiLogExecution('DEBUG', 'Session Object: ', context.getSetting('SESSION', 'sessionobject'));

			}
 
			if (brewersReportAssistant.getLastStep().getName() == "reportingperiod" && brewersReportAssistant.getLastAction() == "next" ) {
				
				context.setSessionObject('monthlyorquarterly', request.getParameter('monthlyorquarterly'));
				context.setSessionObject('quarterselection', request.getParameter('quarterselection'));
				context.setSessionObject('reportingyear', request.getParameter('reportingyear'));
				context.setSessionObject('reportingmonth', request.getParameter('reportingmonth'));
				context.setSessionObject('reportingperiod', request.getParameter('reportingperiod'));
				context.setSessionObject('quarterstartdate', request.getParameter('quarterstartdate'));
				context.setSessionObject('quarterenddate', request.getParameter('quarterenddate'));
				context.setSessionObject('previousreport', request.getParameter('previousreport'));

			}

			if (brewersReportAssistant.getLastStep().getName() == "partonebeersummary" && brewersReportAssistant.getLastAction() == "next" ) {

				context.setSessionObject('1b', request.getLineItemValue('additionstobeerinv', 'cellar', 1));
				context.setSessionObject('1c', request.getLineItemValue('additionstobeerinv', 'bulk', 1));
				context.setSessionObject('1d', request.getLineItemValue('additionstobeerinv', 'keg', 1));
				context.setSessionObject('1e', request.getLineItemValue('additionstobeerinv', 'bulkb', 1));
				context.setSessionObject('1f', request.getLineItemValue('additionstobeerinv', 'case', 1));
				context.setSessionObject('1g', request.getLineItemValue('additionstobeerinv', 'totals', 1));

				context.setSessionObject('2b', request.getLineItemValue('additionstobeerinv', 'cellar', 2));
				context.setSessionObject('2c', request.getLineItemValue('additionstobeerinv', 'bulk', 2));
				context.setSessionObject('2d', request.getLineItemValue('additionstobeerinv', 'keg', 2));
				context.setSessionObject('2e', request.getLineItemValue('additionstobeerinv', 'bulkb', 2));
				context.setSessionObject('2f', request.getLineItemValue('additionstobeerinv', 'case', 2));
				context.setSessionObject('2g', request.getLineItemValue('additionstobeerinv', 'totals', 2));

				context.setSessionObject('3b', request.getLineItemValue('additionstobeerinv', 'cellar', 3));
				context.setSessionObject('3c', request.getLineItemValue('additionstobeerinv', 'bulk', 3));
				context.setSessionObject('3d', request.getLineItemValue('additionstobeerinv', 'keg', 3));
				context.setSessionObject('3e', request.getLineItemValue('additionstobeerinv', 'bulkb', 3));
				context.setSessionObject('3f', request.getLineItemValue('additionstobeerinv', 'case', 3));
				context.setSessionObject('3g', request.getLineItemValue('additionstobeerinv', 'totals', 3));

				context.setSessionObject('4b', request.getLineItemValue('additionstobeerinv', 'cellar', 4));
				context.setSessionObject('4c', request.getLineItemValue('additionstobeerinv', 'bulk', 4));
				context.setSessionObject('4d', request.getLineItemValue('additionstobeerinv', 'keg', 4));
				context.setSessionObject('4e', request.getLineItemValue('additionstobeerinv', 'bulkb', 4));
				context.setSessionObject('4f', request.getLineItemValue('additionstobeerinv', 'case', 4));
				context.setSessionObject('4g', request.getLineItemValue('additionstobeerinv', 'totals', 4));

				context.setSessionObject('5b', request.getLineItemValue('additionstobeerinv', 'cellar', 5));
				context.setSessionObject('5c', request.getLineItemValue('additionstobeerinv', 'bulk', 5));
				context.setSessionObject('5d', request.getLineItemValue('additionstobeerinv', 'keg', 5));
				context.setSessionObject('5e', request.getLineItemValue('additionstobeerinv', 'bulkb', 5));
				context.setSessionObject('5f', request.getLineItemValue('additionstobeerinv', 'case', 5));
				context.setSessionObject('5g', request.getLineItemValue('additionstobeerinv', 'totals', 5));

				context.setSessionObject('6b', request.getLineItemValue('additionstobeerinv', 'cellar', 6));
				context.setSessionObject('6c', request.getLineItemValue('additionstobeerinv', 'bulk', 6));
				context.setSessionObject('6d', request.getLineItemValue('additionstobeerinv', 'keg', 6));
				context.setSessionObject('6e', request.getLineItemValue('additionstobeerinv', 'bulkb', 6));
				context.setSessionObject('6f', request.getLineItemValue('additionstobeerinv', 'case', 6));
				context.setSessionObject('6g', request.getLineItemValue('additionstobeerinv', 'totals', 6));

				context.setSessionObject('7b', request.getLineItemValue('additionstobeerinv', 'cellar', 7));
				context.setSessionObject('7c', request.getLineItemValue('additionstobeerinv', 'bulk', 7));
				context.setSessionObject('7d', request.getLineItemValue('additionstobeerinv', 'keg', 7));
				context.setSessionObject('7e', request.getLineItemValue('additionstobeerinv', 'bulkb', 7));
				context.setSessionObject('7f', request.getLineItemValue('additionstobeerinv', 'case', 7));
				context.setSessionObject('7g', request.getLineItemValue('additionstobeerinv', 'totals', 7));

				context.setSessionObject('8b', request.getLineItemValue('additionstobeerinv', 'cellar', 8));
				context.setSessionObject('8c', request.getLineItemValue('additionstobeerinv', 'bulk', 8));
				context.setSessionObject('8d', request.getLineItemValue('additionstobeerinv', 'keg', 8));
				context.setSessionObject('8e', request.getLineItemValue('additionstobeerinv', 'bulkb', 8));
				context.setSessionObject('8f', request.getLineItemValue('additionstobeerinv', 'case', 8));
				context.setSessionObject('8g', request.getLineItemValue('additionstobeerinv', 'totals', 8));

				context.setSessionObject('9b', request.getLineItemValue('additionstobeerinv', 'cellar', 9));
				context.setSessionObject('9c', request.getLineItemValue('additionstobeerinv', 'bulk', 9));
				context.setSessionObject('9d', request.getLineItemValue('additionstobeerinv', 'keg', 9));
				context.setSessionObject('9e', request.getLineItemValue('additionstobeerinv', 'bulkb', 9));
				context.setSessionObject('9f', request.getLineItemValue('additionstobeerinv', 'case', 9));
				context.setSessionObject('9g', request.getLineItemValue('additionstobeerinv', 'totals', 9));

				context.setSessionObject('10b', request.getLineItemValue('additionstobeerinv', 'cellar', 10));
				context.setSessionObject('10c', request.getLineItemValue('additionstobeerinv', 'bulk', 10));
				context.setSessionObject('10d', request.getLineItemValue('additionstobeerinv', 'keg', 10));
				context.setSessionObject('10e', request.getLineItemValue('additionstobeerinv', 'bulkb', 10));
				context.setSessionObject('10f', request.getLineItemValue('additionstobeerinv', 'case', 10));
				context.setSessionObject('10g', request.getLineItemValue('additionstobeerinv', 'totals', 10));

				context.setSessionObject('11b', request.getLineItemValue('additionstobeerinv', 'cellar', 11));
				context.setSessionObject('11c', request.getLineItemValue('additionstobeerinv', 'bulk', 11));
				context.setSessionObject('11d', request.getLineItemValue('additionstobeerinv', 'keg', 11));
				context.setSessionObject('11e', request.getLineItemValue('additionstobeerinv', 'bulkb', 11));
				context.setSessionObject('11f', request.getLineItemValue('additionstobeerinv', 'case', 11));
				context.setSessionObject('11g', request.getLineItemValue('additionstobeerinv', 'totals', 11));

				context.setSessionObject('12b', request.getLineItemValue('additionstobeerinv', 'cellar', 12));
				context.setSessionObject('12c', request.getLineItemValue('additionstobeerinv', 'bulk', 12));
				context.setSessionObject('12d', request.getLineItemValue('additionstobeerinv', 'keg', 12));
				context.setSessionObject('12e', request.getLineItemValue('additionstobeerinv', 'bulkb', 12));
				context.setSessionObject('12f', request.getLineItemValue('additionstobeerinv', 'case', 12));
				context.setSessionObject('12g', request.getLineItemValue('additionstobeerinv', 'totals', 12));

				context.setSessionObject('13b', request.getLineItemValue('additionstobeerinv', 'cellar', 13));
				context.setSessionObject('13c', request.getLineItemValue('additionstobeerinv', 'bulk', 13));
				context.setSessionObject('13d', request.getLineItemValue('additionstobeerinv', 'keg', 13));
				context.setSessionObject('13e', request.getLineItemValue('additionstobeerinv', 'bulkb', 13));
				context.setSessionObject('13f', request.getLineItemValue('additionstobeerinv', 'case', 13));
				context.setSessionObject('13g', request.getLineItemValue('additionstobeerinv', 'totals', 13));

			}

			if (brewersReportAssistant.getLastStep().getName() == "partonebeersummaryremovals" && brewersReportAssistant.getLastAction() == "next" ) {

				context.setSessionObject('14b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 1));
				context.setSessionObject('14c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 1));
				context.setSessionObject('14d', request.getLineItemValue('removalsfrombeerinv', 'keg', 1));
				context.setSessionObject('14e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 1));
				context.setSessionObject('14f', request.getLineItemValue('removalsfrombeerinv', 'case', 1));
				context.setSessionObject('14g', request.getLineItemValue('removalsfrombeerinv', 'totals', 1));

				context.setSessionObject('15b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 2));
				context.setSessionObject('15c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 2));
				context.setSessionObject('15d', request.getLineItemValue('removalsfrombeerinv', 'keg', 2));
				context.setSessionObject('15e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 2));
				context.setSessionObject('15f', request.getLineItemValue('removalsfrombeerinv', 'case', 2));
				context.setSessionObject('15g', request.getLineItemValue('removalsfrombeerinv', 'totals', 2));

				context.setSessionObject('16b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 3));
				context.setSessionObject('16c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 3));
				context.setSessionObject('16d', request.getLineItemValue('removalsfrombeerinv', 'keg', 3));
				context.setSessionObject('16e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 3));
				context.setSessionObject('16f', request.getLineItemValue('removalsfrombeerinv', 'case', 3));
				context.setSessionObject('16g', request.getLineItemValue('removalsfrombeerinv', 'totals', 3));

				context.setSessionObject('17b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 4));
				context.setSessionObject('17c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 4));
				context.setSessionObject('17d', request.getLineItemValue('removalsfrombeerinv', 'keg', 4));
				context.setSessionObject('17e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 4));
				context.setSessionObject('17f', request.getLineItemValue('removalsfrombeerinv', 'case', 4));
				context.setSessionObject('17g', request.getLineItemValue('removalsfrombeerinv', 'totals', 4));

				context.setSessionObject('18b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 5));
				context.setSessionObject('18c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 5));
				context.setSessionObject('18d', request.getLineItemValue('removalsfrombeerinv', 'keg', 5));
				context.setSessionObject('18e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 5));
				context.setSessionObject('18f', request.getLineItemValue('removalsfrombeerinv', 'case', 5));
				context.setSessionObject('18g', request.getLineItemValue('removalsfrombeerinv', 'totals', 5));

				context.setSessionObject('19b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 6));
				context.setSessionObject('19c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 6));
				context.setSessionObject('19d', request.getLineItemValue('removalsfrombeerinv', 'keg', 6));
				context.setSessionObject('19e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 6));
				context.setSessionObject('19f', request.getLineItemValue('removalsfrombeerinv', 'case', 6));
				context.setSessionObject('19g', request.getLineItemValue('removalsfrombeerinv', 'totals', 6));

				context.setSessionObject('20b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 7));
				context.setSessionObject('20c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 7));
				context.setSessionObject('20d', request.getLineItemValue('removalsfrombeerinv', 'keg', 7));
				context.setSessionObject('20e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 7));
				context.setSessionObject('20f', request.getLineItemValue('removalsfrombeerinv', 'case', 7));
				context.setSessionObject('20g', request.getLineItemValue('removalsfrombeerinv', 'totals', 7));

				context.setSessionObject('21b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 8));
				context.setSessionObject('21c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 8));
				context.setSessionObject('21d', request.getLineItemValue('removalsfrombeerinv', 'keg', 8));
				context.setSessionObject('21e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 8));
				context.setSessionObject('21f', request.getLineItemValue('removalsfrombeerinv', 'case', 8));
				context.setSessionObject('21g', request.getLineItemValue('removalsfrombeerinv', 'totals', 8));

				context.setSessionObject('22b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 9));
				context.setSessionObject('22c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 9));
				context.setSessionObject('22d', request.getLineItemValue('removalsfrombeerinv', 'keg', 9));
				context.setSessionObject('22e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 9));
				context.setSessionObject('22f', request.getLineItemValue('removalsfrombeerinv', 'case', 9));
				context.setSessionObject('22g', request.getLineItemValue('removalsfrombeerinv', 'totals', 9));

				context.setSessionObject('23b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 10));
				context.setSessionObject('23c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 10));
				context.setSessionObject('23d', request.getLineItemValue('removalsfrombeerinv', 'keg', 10));
				context.setSessionObject('23e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 10));
				context.setSessionObject('23f', request.getLineItemValue('removalsfrombeerinv', 'case', 10));
				context.setSessionObject('23g', request.getLineItemValue('removalsfrombeerinv', 'totals', 10));

				context.setSessionObject('24b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 11));
				context.setSessionObject('24c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 11));
				context.setSessionObject('24d', request.getLineItemValue('removalsfrombeerinv', 'keg', 11));
				context.setSessionObject('24e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 11));
				context.setSessionObject('24f', request.getLineItemValue('removalsfrombeerinv', 'case', 11));
				context.setSessionObject('24g', request.getLineItemValue('removalsfrombeerinv', 'totals', 11));

				context.setSessionObject('25b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 12));
				context.setSessionObject('25c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 12));
				context.setSessionObject('25d', request.getLineItemValue('removalsfrombeerinv', 'keg', 12));
				context.setSessionObject('25e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 12));
				context.setSessionObject('25f', request.getLineItemValue('removalsfrombeerinv', 'case', 12));
				context.setSessionObject('25g', request.getLineItemValue('removalsfrombeerinv', 'totals', 12));

				context.setSessionObject('26b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 13));
				context.setSessionObject('26c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 13));
				context.setSessionObject('26d', request.getLineItemValue('removalsfrombeerinv', 'keg', 13));
				context.setSessionObject('26e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 13));
				context.setSessionObject('26f', request.getLineItemValue('removalsfrombeerinv', 'case', 13));
				context.setSessionObject('26g', request.getLineItemValue('removalsfrombeerinv', 'totals', 13));

				context.setSessionObject('27b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 14));
				context.setSessionObject('27c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 14));
				context.setSessionObject('27d', request.getLineItemValue('removalsfrombeerinv', 'keg', 14));
				context.setSessionObject('27e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 14));
				context.setSessionObject('27f', request.getLineItemValue('removalsfrombeerinv', 'case', 14));
				context.setSessionObject('27g', request.getLineItemValue('removalsfrombeerinv', 'totals', 14));

				context.setSessionObject('28b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 15));
				context.setSessionObject('28c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 15));
				context.setSessionObject('28d', request.getLineItemValue('removalsfrombeerinv', 'keg', 15));
				context.setSessionObject('28e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 15));
				context.setSessionObject('28f', request.getLineItemValue('removalsfrombeerinv', 'case', 15));
				context.setSessionObject('28g', request.getLineItemValue('removalsfrombeerinv', 'totals', 15));

				context.setSessionObject('29b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 16));
				context.setSessionObject('29c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 16));
				context.setSessionObject('29d', request.getLineItemValue('removalsfrombeerinv', 'keg', 16));
				context.setSessionObject('29e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 16));
				context.setSessionObject('29f', request.getLineItemValue('removalsfrombeerinv', 'case', 16));
				context.setSessionObject('29g', request.getLineItemValue('removalsfrombeerinv', 'totals', 16));

				context.setSessionObject('30b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 17));
				context.setSessionObject('30c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 17));
				context.setSessionObject('30d', request.getLineItemValue('removalsfrombeerinv', 'keg', 17));
				context.setSessionObject('30e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 17));
				context.setSessionObject('30f', request.getLineItemValue('removalsfrombeerinv', 'case', 17));
				context.setSessionObject('30g', request.getLineItemValue('removalsfrombeerinv', 'totals', 17));

				context.setSessionObject('31b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 18));
				context.setSessionObject('31c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 18));
				context.setSessionObject('31d', request.getLineItemValue('removalsfrombeerinv', 'keg', 18));
				context.setSessionObject('31e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 18));
				context.setSessionObject('31f', request.getLineItemValue('removalsfrombeerinv', 'case', 18));
				context.setSessionObject('31g', request.getLineItemValue('removalsfrombeerinv', 'totals', 18));

				context.setSessionObject('32b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 19));
				context.setSessionObject('32c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 19));
				context.setSessionObject('32d', request.getLineItemValue('removalsfrombeerinv', 'keg', 19));
				context.setSessionObject('32e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 19));
				context.setSessionObject('32f', request.getLineItemValue('removalsfrombeerinv', 'case', 19));
				context.setSessionObject('32g', request.getLineItemValue('removalsfrombeerinv', 'totals', 19));

				context.setSessionObject('33b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 20));
				context.setSessionObject('33c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 20));
				context.setSessionObject('33d', request.getLineItemValue('removalsfrombeerinv', 'keg', 20));
				context.setSessionObject('33e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 20));
				context.setSessionObject('33f', request.getLineItemValue('removalsfrombeerinv', 'case', 20));
				context.setSessionObject('33g', request.getLineItemValue('removalsfrombeerinv', 'totals', 20));

				context.setSessionObject('34b', request.getLineItemValue('removalsfrombeerinv', 'cellar', 21));
				context.setSessionObject('34c', request.getLineItemValue('removalsfrombeerinv', 'bulk', 21));
				context.setSessionObject('34d', request.getLineItemValue('removalsfrombeerinv', 'keg', 21));
				context.setSessionObject('34e', request.getLineItemValue('removalsfrombeerinv', 'bulkb', 21));
				context.setSessionObject('34f', request.getLineItemValue('removalsfrombeerinv', 'case', 21));
				context.setSessionObject('34g', request.getLineItemValue('removalsfrombeerinv', 'totals', 21));

			}

			if (brewersReportAssistant.getLastStep().getName() == "priorperiodadj" && brewersReportAssistant.getLastAction() == "next" ) {
				
				context.setSessionObject('signature', request.getParameter('signature'));
				context.setSessionObject('title', request.getParameter('title'));
				context.setSessionObject('date', request.getParameter('date'));

				context.setSessionObject('35ad', request.getLineItemValue('priorperiodadj', 'priorname', 1));
				context.setSessionObject('36re', request.getLineItemValue('priorperiodadj', 'priorname', 2));
				context.setSessionObject('35a', request.getLineItemValue('priorperiodadj', 'plus', 1));
				context.setSessionObject('35b', request.getLineItemValue('priorperiodadj', 'minus', 1));
				context.setSessionObject('36a', request.getLineItemValue('priorperiodadj', 'plus', 2));
				context.setSessionObject('36b', request.getLineItemValue('priorperiodadj', 'minus', 2));
				
			}

			if (brewersReportAssistant.getLastStep().getName() == "partfourcereal" && brewersReportAssistant.getLastAction() == "next" ) {
				
				context.setSessionObject('produced', request.getLineItemValue('partfourcereal', 'cerealbev', 1));
				context.setSessionObject('removed', request.getLineItemValue('partfourcereal', 'cerealbev', 2));
				context.setSessionObject('received', request.getLineItemValue('partfourcereal', 'cerealbev', 3));
				context.setSessionObject('wastage', request.getLineItemValue('partfourcereal', 'cerealbev', 4));
				context.setSessionObject('5', request.getLineItemValue('partfourcereal', 'cerealbev', 5));
				context.setSessionObject('producedbbl', request.getLineItemValue('partfourcereal', 'bbls', 1));
				context.setSessionObject('removedbbl', request.getLineItemValue('partfourcereal', 'bbls', 2));
				context.setSessionObject('receivedbbl', request.getLineItemValue('partfourcereal', 'bbls', 3));
				context.setSessionObject('wastagebbl', request.getLineItemValue('partfourcereal', 'bbls', 4));
				context.setSessionObject('5bbl', request.getLineItemValue('partfourcereal', 'bbls', 5));
				context.setSessionObject('totalbbl', request.getLineItemValue('partfourcereal', 'bbls', 6));

			}

			if (brewersReportAssistant.getLastStep().getName() == "partfiveremarks" && brewersReportAssistant.getLastAction() == "next" ) {
				
				context.setSessionObject('remarks', request.getParameter('remarks'));

			}

			if (brewersReportAssistant.getLastStep().getName() == 'printreport' && brewersReportAssistant.getLastAction() == "next") {

				context.setSessionObject('pdffile', request.getParameter('fdffile'));

			}
			
			if (!brewersReportAssistant.hasError()) {
				
				brewersReportAssistant.setCurrentStep( brewersReportAssistant.getNextStep());

			}
 
			brewersReportAssistant.sendRedirect(response);
 
		}
	}
}
 
function getLinkoutURL(redirect, type) {
	
	var url = redirect;
 
	if (type == "record") {
		
		url = nlapiResolveURL('record', redirect);

	}
 
	url += url.indexOf('?') == -1 ? '?' : '&';
 
	var context = nlapiGetContext();
	url += 'customwhence='+ escape(nlapiResolveURL('suitelet',context.getScriptId(), context.getDeploymentId()))
 
	return url; 
 
} 