/**
 * Module Description
 *  2019_1Beta - Live Tank View for Fermentation Tanks
 *
 *  Adds location designation to Portlet Title
 *  Based on BERP - Live Tank Views.js  Version 1.0 21 Dec 2015 jbrox
 *  References a second search library for location based searches.
 *
 * Version    Date            Author           Remarks
 * 0.9.0      04 Apr 2019     candrews         initial script with location label
 * 0.9.1	  17 Apr 2019     candrews         removed BERP from portlet titles
 *                                             added Barrel ERP(TM) to portlet titles
 *                                             added bold/underline to tank titles
 *                                             changed remaining to remain for day label
 *
 */

function showLiveTankViews(portlet, column) {

	// Get Context & Script Parameters
	var context = nlapiGetContext();
	var accountId = context.getSetting('SCRIPT', 'custscript_h_berp_ns_account_id');
	var activeAccounts = accountActiveCheck();
	nlapiLogExecution('DEBUG', 'Active Accounts', JSON.stringify(activeAccounts));
	nlapiLogExecution('DEBUG', 'Index', activeAccounts.indexOf(accountId));
    if (activeAccounts.indexOf(accountId) >= 0) {
    //if (1 >= 0) {

		// Modified for location test script parameter
		var locationParam = context.getSetting('SCRIPT', 'custscript_h_berp_location');
		// Modified for location test script parameter
		var tankRowSize = context.getSetting('SCRIPT', 'custscript_h_berp_number_of_tanks');
		nlapiLogExecution('DEBUG', 'Location Parameter', locationParam);
		portlet.setTitle('Barrel ERP\u2122  |  Fermentation Tanks');
		var fld = portlet.addField('custpage_htmltext', 'inlinehtml', 'HTML Content', null);

		if (locationParam) {
			// Get location name 
			var locationName = getLocationName(locationParam);
			// Add location to portlet title. 
			portlet.setTitle('Barrel ERP\u2122  |  ' + locationName + ' Fermentation Tanks');
			
			
			var allFermTanks = searchAllFermentationTanks(locationParam);

			if (allFermTanks) {
			
				nlapiLogExecution('DEBUG', 'Fermentation Tanks', JSON.stringify(allFermTanks));
				var inProgressFermTanks = searchFermentationTanksinProgress();
				var tankinternalids = [];
				var tankcapacities = [];

				if (inProgressFermTanks) {

					for (var u = 0; u < inProgressFermTanks.length; u++) {
						tankinternalids.push(inProgressFermTanks[u].getValue('manufacturingworkcenter', null, 'GROUP'));
					}

				}

				nlapiLogExecution('DEBUG', 'Tanks: ', tankinternalids);
				for (var f = 0; f < allFermTanks.length; f++) {
					tankcapacities.push(allFermTanks[f].getValue('custentity_h_berp_capacity'));
				}

				var tankcapacity = 0;
				var tankindex = 0;
				var gallons = 0;
				var barrels = 0;
				var percentoftank = 0;
				var tankname = '';
				var item = '';
				var productionorder = '';
				var productionorderid = '';
				var tdwidth = (1 / allFermTanks.length)*100;
				var conecolor = '#ADAEB0';
				var tanksize = 350;
				var newtanksize = 350;
				var tanksizepercent = 100;
				var tankmaxsize = getMaxOfArray(tankcapacities);
				// nlapiLogExecution('DEBUG', 'Max Tank Size', tankmaxsize);

				// Generate Content Style
			    var content = "<!DOCTYPE html><html><head><title>JQuery Tooltip</title><script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js\"></script>";
			    content += "<script>function simple_tooltip(target_items, name){$(target_items).each(function(i){$(\"body\").append(\"<div class='\"+name+\"' id='\"+name+i+\"'><p>\"+$(this).attr('title')+\"</p></div>\");";
				content += "var my_tooltip = $(\"#\"+name+i);$(this).removeAttr(\"title\").mouseover(function(){my_tooltip.css({opacity:0.8, display:\"none\"}).fadeIn(400);}).mousemove(function(kmouse){my_tooltip.css({left:kmouse.pageX+15, top:kmouse.pageY+15});";		
				content += "}).mouseout(function(){my_tooltip.fadeOut(0);});});}$(document).ready(function(){simple_tooltip(\".progress\",\"tooltip\");});</script>";
				content += "<style>.progress{max-width: 150px;min-width: 150px;position: relative;margin-top:10px;overflow:hidden;height:350px;background-color:#ccc;border-top-left-radius:36px;border-top-right-radius:36px;-webkit-border-top-left-radius:36px;-webkit-border-top-right-radius:36px;-moz-border-top-left-radius:36px;-moz-border-top-right-radius:36px;box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);}";
			    content += ".notprogress{text-decoration:none;max-width: 150px;min-width: 150px;position: relative;margin-top:10px;overflow:hidden;height:350px;background-color:#ccc;border-top-left-radius:36px;border-top-right-radius:36px;-webkit-border-top-left-radius:36px;-webkit-border-top-right-radius:36px;-moz-border-top-left-radius:36px;-moz-border-top-right-radius:36px;box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);-moz-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);-webkit-box-shadow:inset 0 1px 2px rgba(0,0,0,0.1);}";
			    content += ".notprogress a{text-decoration:none;}";
			    content += "a:hover, a:visited, a:link, a:active{text-decoration: none;}";
			    content += ".percent { font-size:16px; padding-top:10px; text-shadow: 1px 1px 1px #000;}";
			    content += ".percentmiddle {line-height:10px;color:#78A22F;text-align:center;text-decoration: none; font-size:16px; padding-top:50px; text-shadow: 1px 1px 1px #000;}";
			    content += ".progress-bar{position: absolute;bottom: 0;float:left;width:100%;height:0;font-size:14px;line-height:10px;color:#78A22F;text-align:center;text-decoration: none;box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-webkit-box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-moz-box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-webkit-transition:width .6s ease;transition:width .6s ease;-moz-transition:width .6s ease;}";
			    content += ".notprogress-bar{position: absolute;bottom: 0;float:left;width:100%;height:0;font-size:18px;line-height:20px;color:#78A22F;text-align:center;background-color:#E2E477;box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-webkit-box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-moz-box-shadow:inset 0 -1px 0 rgba(0,0,0,0.15);-webkit-transition:width .6s ease;transition:width .6s ease;-moz-transition:width .6s ease;}";
			    content += ".progress-bottom{position:relative;overflow:hidden;width:0;height:0;border-left: 75px solid transparent;border-right: 75px solid transparent;border-top: 125px solid #ADAEB0;}";
			    content += ".notprogress-bottom{position:relative;overflow:hidden;width:0;height:0;border-left: 75px solid transparent;border-right: 75px solid transparent;border-top: 125px solid #ADAEB0;}";
			    content += ".tank-container{text-align:center;max-width: 150px;min-width:150px;}";
			    content += ".tank{height:0;font-size:14px;line-height:20px;color:#005984;text-decoration: none;}";
			    content += ".tooltip {position:absolute;z-index:999;left:-9999px;background-color:#00A0AF;padding:5px;border:1px solid #fff;width:300px;}";
			    content += ".tooltip p {margin:0;padding:0;color:#fff;background-color:#005984;padding:2px 7px;}</style></head><body>";

			    // Begin Table
			    content += "<table width=\"100%\"><tr>";

			    // Create Rows and Columns for All Fermentation Tanks
			    for (var a = 0; a < allFermTanks.length; a++) {


			    	// nlapiLogExecution('DEBUG', 'For Loop Index: ', a);
			    	var tankInt = isInt((a + 1) / parseInt(tankRowSize));
			    	// nlapiLogExecution('DEBUG', 'For Loop Div 6: ', tankInt);
			    	tankcapacity = allFermTanks[a].getValue('custentity_h_berp_capacity');
			    	tankname = allFermTanks[a].getValue('custentity_h_berp_tank_name');

			    	newtanksize = 150;
			    	tanksizepercent = tankcapacity / tankmaxsize;
			    	newtanksize = tanksize * tanksizepercent;
			    	// nlapiLogExecution('DEBUG', 'Tank Size: ', tanksize);

			    	// nlapiLogExecution('DEBUG', 'Tank Internal ID | Tank Size | Tank Max Size | Tank Size Percent | New Tank Size (px)', allFermTanks[a].getValue('internalid') + ' | ' + tankcapacity + ' | ' + tankmaxsize + ' | ' + tanksizepercent + ' | ' + tanksize);
			    	var tankInProgress = 'F';
			    	var fermentationDays = 0;
			    	var daysInProgress = 0;
			    	var tankPercentComplete = 0;
			    	var daysRemaining = 0;

			    		if (tankinternalids.indexOf(allFermTanks[a].getValue('internalid')) >= 0) {

			    			tankInProgress = 'T';
			    			nlapiLogExecution('DEBUG', 'All Ferm Tank Interal ID: ', allFermTanks[a].getValue('internalid'));
			    			tankindex = tankinternalids.indexOf(allFermTanks[a].getValue('internalid'));
			    			item = inProgressFermTanks[tankindex].getText('item', 'workOrder', 'GROUP');
			    			var itemLookupFields = ['custitem_h_berp_fanciful']
			    			var itemLookup = nlapiLookupField('item', inProgressFermTanks[tankindex].getValue('item', 'workOrder', 'GROUP'), itemLookupFields);
			    			var fancifulLookupFields = ['name', 'custrecord_h_berp_live_tank_hex'];
			    			if (itemLookup.custitem_h_berp_fanciful) { 

			    				var itemFanciful = nlapiLookupField('customrecord_h_berp_fanciful', itemLookup.custitem_h_berp_fanciful, fancifulLookupFields);
			    				var itemName = itemFanciful.name;
			    				var itemHexColor = itemFanciful.custrecord_h_berp_live_tank_hex;

			    			} else {

			    				var itemName = item;
			    				var itemHexColor = '#E2E477';

			    			}

			    			if (inProgressFermTanks[tankindex].getValue('custbody_h_berp_prod_batch', 'workOrder', 'GROUP')) { 
			    				var prodBatchName = nlapiLookupField('customrecord_h_berp_prod_batch', inProgressFermTanks[tankindex].getValue('custbody_h_berp_prod_batch', 'workOrder', 'GROUP'), 'altname');
			    			} else {
			    				var prodBatchName = '- NONE -';
			    			}

			    			// Calculate Tank % Complete
			    			fermentationDays = daysBetweenDates(nlapiStringToDate(inProgressFermTanks[tankindex].getValue('enddate', null, 'MAX')), nlapiStringToDate(inProgressFermTanks[tankindex].getValue('startdate', null, 'MAX')));
			    			nlapiLogExecution('DEBUG', '# of Fermentation Days', fermentationDays);
			    			daysInProgress = daysBetweenDates(nlapiStringToDate(inProgressFermTanks[tankindex].getValue('startdate', null, 'MAX')), new Date());
			    			nlapiLogExecution('DEBUG', '# of Days in Progress', daysInProgress);
			    			if (daysInProgress == 0 || fermentationDays == 0) { 

			    				tankPercentComplete = 0;

			    			} else {

			    				tankPercentComplete = (daysInProgress / fermentationDays) * 100;

			    			}
			    			
			    			nlapiLogExecution('DEBUG', '% Complete', tankPercentComplete);
			    			daysRemaining = fermentationDays - daysInProgress;

			    			productionorder = '<html><p><strong>';
			    			productionorder += 'Production Batch: ';
			    			productionorder += inProgressFermTanks[tankindex].getText('custbody_h_berp_prod_batch', 'workOrder', 'GROUP');
			    			productionorder += '</strong></p><p>';
			    			productionorder += 'Batch Name: ';
			    			productionorder += prodBatchName;
			    			productionorder += '</p>';
			    			productionorder += '<p>Recipe: ';
							productionorder += itemName;
			    			productionorder += '</p>';
			    			productionorder += '<p>Start Date: ';
			    			productionorder += inProgressFermTanks[tankindex].getValue('startdatetime', null, 'MAX');
			    			productionorder += '</p><p>';
			    			productionorder += 'End Date: ';
			    			productionorder += inProgressFermTanks[tankindex].getValue('enddatetime', null, 'MAX');
			    			productionorder += '</p>';
			    			productionorder += '<p>';
			    			productionorder += 'Fill Quantity: ';
			    			productionorder += inProgressFermTanks[tankindex].getValue('completedquantity', 'predecessor', 'SUM');
                            // Label to Bbl 
			    			productionorder += ' Bbl</p></html>';
			    			productionorderid = inProgressFermTanks[tankindex].getValue('custbody_h_berp_prod_batch', 'workOrder', 'GROUP');
					    	barrels = inProgressFermTanks[tankindex].getValue('inputquantity', null, 'SUM');
					    	percentoftank = (barrels / tankcapacity)*100*.70;
					    	conecolor = itemHexColor;

					    } else {

					    	tankInProgress = 'F';
					    	item = '';
					    	barrels = 0;
					    	percentoftank = 0;
					    	conecolor = '#ADAEB0';
					    	productionorder = '';
					    	productionorderid = '';

					    }

				    // Add Column
				    
				    if (tankInProgress == 'T') {

				    	content += "<td width=\"";
					    content += tdwidth;
					    content += "%\"; style=\"padding-left:35px\"><a href=\"";
					    content += nlapiResolveURL('RECORD', 'customrecord_h_berp_prod_batch');
					    content += "&id=";
					    content += productionorderid;
					    content += "&whence=\" target=\"_blank\">";
					    content += "<div title=\"";
					    content += productionorder;
					    content += "\" class=\"progress\" style=\"height:";
					    content += '175';
					    content += "px\"><div style=\"margin-left:50px;margin-top:5px\"><p class=\"tank\"><b><u>";
					    content += tankname;
					    content += "</u></b></br>";
					    content += tankcapacity;
                        //Label to Bbl
					    content += " Bbl</p></div>";

					} else {

						content += "<td width=\"";
					    content += tdwidth;
					    content += "%\"; style=\"padding-left:35px\">";
						content += "<div class=\"notprogress\" style=\"height:";
					    content += '175';
					    content += "px\"><div style=\"margin-left:50px;margin-top:5px\"><p class=\"tank\"><b><u>";
					    content += tankname;
					    content += "</b></u></br>";
					    content += tankcapacity;
                        //Label from Bbl
					    content += " Bbl</p></div>";

					}

					if (percentoftank <= 50 && percentoftank != 0) {

					    content += "<p class=\"percentmiddle\"><span style=\"color:";
					    if (tankPercentComplete < 25) { content += "#FF0000;\">"; } else if (tankPercentComplete >= 25 && tankPercentComplete < 50) { content += "#FFFF66;\">"; } else if (tankPercentComplete >= 50 && tankPercentComplete < 75) { content += "#CCFF66;\">"; } else if (tankPercentComplete >= 75) { content += "#00FF00;\">"; }
					    content += tankPercentComplete.toFixed(0);
					    content += "%</span></br></br><span style=\"color:";
			   		    if (tankPercentComplete < 25) { content += "#FF0000;\">"; } else if (tankPercentComplete >= 25 && tankPercentComplete < 50) { content += "#FFFF66;\">"; } else if (tankPercentComplete >= 50 && tankPercentComplete < 75) { content += "#CCFF66;\">"; } else if (tankPercentComplete >= 75) { content += "#00FF00;\">"; }
					    content += daysRemaining;
					    content += " Days Remain</span></p>";

					}

					content += "<div class=\"progress-bar\" style=\"height:";
				    content += percentoftank;
				    content += "%;background-color:";
				    content += itemHexColor;
				    content += ";\">"

					if (percentoftank > 50) {


					    content += "<p class=\"percent\"><span style=\"color:";
					    if (tankPercentComplete < 25) { content += "#FF0000;\">"; } else if (tankPercentComplete >= 25 && tankPercentComplete < 50) { content += "#FFFF66;\">"; } else if (tankPercentComplete >= 50 && tankPercentComplete < 75) { content += "#CCFF66;\">"; } else if (tankPercentComplete >= 75) { content += "#00FF00;\">"; }
					    content += tankPercentComplete.toFixed(0);
					    content += "%</span></br></br><span style=\"color:";
			   		    if (tankPercentComplete < 25) { content += "#FF0000;\">"; } else if (tankPercentComplete >= 25 && tankPercentComplete < 50) { content += "#FFFF66;\">"; } else if (tankPercentComplete >= 50 && tankPercentComplete < 75) { content += "#CCFF66;\">"; } else if (tankPercentComplete >= 75) { content += "#00FF00;\">"; }
					    content += daysRemaining;
					    content += " Days Remain</span></p>";

					}

					content += "</div></div></a>";
				    content += "<div class=\"progress-bottom\" style=\"border-top: 125px solid ";
				    content += conecolor;
				    content += ";\"><div class=\"progress-bar\" style=\"height:100%\"></div></div>";
				    if (tankInt == true) { content += "</tr><tr>"; }

				}

			    // End Row
			    content += "</tr></tr></table></body></html>";

			} else {

				var content = '<!DOCTYPE html><html><head><title>BERP Live Tank View</title></head><body><p style=\"font-size:12pt\">Please setup the Fermentation Tanks for the location selected by selecting the Equipment Type field on the Tank Info tab of the Fermentation Tank (Employee Group) record.</p></body></html>';

			}

		} else {

			var content = '<!DOCTYPE html><html><head><title>BERP Live Tank View</title></head><body><p style=\"font-size:12pt\">Please select a location in the Portlet settings.</p></body></html>';

		}

		fld.setDefaultValue(content);

	} else {

		portlet.setTitle('Barrel ERP\u2122  |  Fermentation Tanks');
		var fld = portlet.addField('custpage_htmltext', 'inlinehtml', 'HTML Content', null);

		var content = '<!DOCTYPE html><html><head><title>BERP Live Tank View</title></head><body><p style="font-size:16px"><span style="color:#FF0000">Your account subscription for Barrel ERP is not currently active. Please contact your Barrel ERP account representative to activate your account.</span></p></body></html>';

		fld.setDefaultValue(content);

	}

}
