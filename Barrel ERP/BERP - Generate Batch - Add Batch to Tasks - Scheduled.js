function addBatch(){
	 //only execute ondemand
	  if ( type != 'ondemand' ) 
	  {
	     nlapiLogExecution('AUDIT','Scheduled Script is not ondemand.', type)
	    return; 
	  }

 	var context = nlapiGetContext();
	var batchId = context.getSetting('SCRIPT', 'custscript_h_berp_batchId');
	var allRelatedProdOrders = searchProdOrdersFromBatch(batchId);
	if(isNotNull(allRelatedProdOrders)){
		for(var a = 0; a < allRelatedProdOrders.length; a++){
			var prodOrdId = allRelatedProdOrders[a].getValue('internalid');
			var relatedManTasks = searchMOTasks(prodOrdId);
			if(isNotNull(relatedManTasks)){
				for(var b = 0; b < relatedManTasks.length; b++){
					var motId = relatedManTasks[b].getValue('internalid');
					
					var productionOrderStatus = nlapiLookupField('workorder', prodOrdId, 'status');
					if(isNotNull(motId)){
						if (productionOrderStatus == 'pendingApproval' || productionOrderStatus == 'Released' || productionOrderStatus == 'pendingBuild') {
							try{
								var motRec = nlapiLoadRecord('manufacturingoperationtask', motId);
								var workCenter = motRec.getFieldValue('manufacturingworkcenter');
								var equipmentType = nlapiLookupField('entitygroup', workCenter, 'custentity_h_berp_equipment_type');
								motRec.setFieldValue('custevent_h_berp_mot_equipment_type', equipmentType);
								motRec.setFieldValue('custevent_h_berp_mot_production_batch', batchId);
								nlapiSubmitRecord(motRec);
							}
							catch(e){
								nlapiLogExecution('ERROR', 'error', e.getDetails())
							}
						} else {
							try{

								nlapiSubmitField('workorder', prodOrdId, 'status', 'A');
								var motRec = nlapiLoadRecord('manufacturingoperationtask', motId);
								var workCenter = motRec.getFieldValue('manufacturingworkcenter');
								var equipmentType = nlapiLookupField('entitygroup', workCenter, 'custentity_h_berp_equipment_type');
								motRec.setFieldValue('custevent_h_berp_mot_equipment_type', equipmentType);
								motRec.setFieldValue('custevent_h_berp_mot_production_batch', batchId);
								nlapiSubmitRecord(motRec);
								nlapiSubmitField('workorder', prodOrdId, 'status', productionOrderStatus);
							}
							catch(e){
								nlapiLogExecution('ERROR', 'error', e.getDetails())
							}
						}
					}
				}
			}
		}
	}

}