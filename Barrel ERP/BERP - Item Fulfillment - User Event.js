/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        03 Dec 2015     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	var recordType = nlapiGetRecordType();
	var recordId = nlapiGetRecordId();
	nlapiLogExecution('DEBUG', 'Record ID', recordId);
	var recid = nlapiGetNewRecord();
	nlapiLogExecution('DEBUG', 'Record ID', recid);
	var id = recid.getId();
	nlapiLogExecution('DEBUG', 'ID', id);


	// Add Button for Lot Report
	var lotReportSuiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_lot_report_it','customdeploy_h_berp_lot_report_it');
	lotReportSuiteletURL += '&custparam_recordtype=' + recordType;
	form.setScript('customscript_h_berp_button_click');
	form.addButton('custpage_lotreport', 'Lot Report', 'displayLotReports(\'' + lotReportSuiteletURL + '\');');
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
  
}