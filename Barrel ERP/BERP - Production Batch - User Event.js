/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Dec 2015     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	nlapiLogExecution('DEBUG', 'Script Execution Context', nlapiGetContext().getExecutionContext());

	if (nlapiGetContext().getExecutionContext() != 'suitelet') {
	
		var recordId = nlapiGetRecordId();
		var recordType = nlapiGetRecordType();
		var recipe = nlapiGetFieldValue('custrecord_h_berp_prb_recipe');
		var origProdOrder = nlapiGetFieldValue('custrecord_h_berp_originating_prodord');
		var recipeIncludesDryHop = searchDryHopLines(recordId);
		if (recipeIncludesDryHop) { recipeIncludesDryHop = true; }
		var location = nlapiGetFieldValue('custrecord_h_berp_prb_location');
		var yeastItem = nlapiGetFieldValue('custrecord_h_berp_yeast');
		var alreadyCommitted = nlapiGetFieldValue('custrecord_h_berp_yp_commit_prod_batch');
		var batchComplete = nlapiGetFieldValue('custrecord_h_berp_batch_complete');
		var dryHopComplete = nlapiGetFieldValue('custrecord_h_berp_prb_dry_hop');
		var isNotInBrite = searchNotCompletedBatch_dryHopValidation(recordId);
      	//nlapiLogExecution('DEBUG', 'dry hop', dryHopComplete);

		// Add Button for Harvest Yeast
		var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_harvest_yeast','customdeploy_h_berp_harvest_yeast');
		suiteletURL += '&custparam_recordid=' + recordId;
		suiteletURL += '&custparam_location=' + location;
		form.setScript('customscript_h_berp_button_click');
		if (type == 'view') { form.addButton('custpage_harvestyeast', 'Harvest Yeast', 'harvestYeast(\'' + suiteletURL + '\');'); }

		//if (batchComplete == 'F') {

			//if (recipeIncludesDryHop) {
				
				

					// Add Button for Dry Hop
					var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_dry_hop','customdeploy_h_berp_dry_hop');
					suiteletURL += '&custparam_recordid=' + recordId;
					suiteletURL += '&custparam_location=' + location;
					form.setScript('customscript_h_berp_button_click');
					if (type == 'view') { form.addButton('custpage_dryhop', 'Dry Hop', 'dryHop(\'' + suiteletURL + '\');'); //}
//			}
      
      			// Add Button for Complete Batch
			var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_complete_batch','customdeploy_h_berp_complete_batch');
			suiteletURL += '&custparam_recordid=' + recordId;
			suiteletURL += '&custparam_location=' + location;
			suiteletURL += '&custparam_origprod=' + origProdOrder;
			form.setScript('customscript_h_berp_button_click');
			if (type == 'view') { form.addButton('custpage_completebatch', 'Complete Batch', 'completeBatch(\'' + suiteletURL + '\');'); }

        }
		// Add Button for Drain Brite Tank
		var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_drain_brite','customdeploy_h_berp_drain_brite');
		suiteletURL += '&custparam_prodbatchid=' + recordId;
		suiteletURL += '&custparam_prodbatchlocation=' + location;
		form.setScript('customscript_h_berp_button_click');
		if (type == 'view') { form.addButton('custpage_drainbrite', 'Drain Brite Tank', 'drainBrite(\'' + suiteletURL + '\');'); }
      
		// Add Button for Moving Brite Tanks
        var batchId = nlapiGetRecordId();
       //nlapiLogExecution('DEBUG', 'id', batchId);
        var results = searchBatchProductionOrders(batchId);
      //nlapiLogExecution('DEBUG', 'results', results);
        if(results){
          for(var o= 0; o < results.length; o++){
            nlapiLogExecution('DEBUG' , 'status', results[o].getValue('statusref'));
          }
			var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_move_tanks', 'customdeploy_move_tanks');
			suiteletURL += '&custparam_prodbatchid=' + recordId;
			suiteletURL += '&custparam_prodbatchlocation=' + location;
			form.setScript('customscript_h_berp_button_click');
			if (type == 'view') { form.addButton('custpage_movetank', 'Move Tank', 'moveTanks(\'' + suiteletURL + '\');'); }
        }

	}
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
  
}
