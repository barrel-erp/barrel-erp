function fieldChange(type, name) {

	if (name == 'custpage_fromdate') {

		var fromDate = nlapiGetFieldValue('custpage_fromdate');
		var toDate = nlapiGetFieldValue('custpage_todate');
		var url = nlapiResolveURL('SUITELET', 'customscript_h_berp_process_sub_results', 'customdeploy_h_berp_process_sub_results') + '&custparam_fromdate=' + fromDate + '&custparam_todate=' + toDate;
		window.onbeforeunload = null;
		document.location = url;

	}

	if (name == 'custpage_todate') {

		var fromDate = nlapiGetFieldValue('custpage_fromdate');
		var toDate = nlapiGetFieldValue('custpage_todate');
		var url = nlapiResolveURL('SUITELET', 'customscript_h_berp_process_sub_results', 'customdeploy_h_berp_process_sub_results') + '&custparam_fromdate=' + fromDate + '&custparam_todate=' + toDate;
		window.onbeforeunload = null;
		document.location = url;

	}

}