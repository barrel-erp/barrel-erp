function copyRouting()
{
	var copyFromID = nlapiGetRecordId();
	var copyFromName = nlapiLookupField('manufacturingrouting',copyFromID, 'name');
	var newRouting = nlapiCopyRecord('manufacturingrouting', copyFromID);
	newRouting.setFieldValue('name','Copy of ' + copyFromName);
	newRouting.setFieldValue('isdefault','F');
	var newRoutingID = nlapiSubmitRecord(newRouting);
    
	nlapiSetRedirectURL('RECORD', 'manufacturingrouting', newRoutingID, true, null)

}