/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Dec 2015     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	// nlapiLogExecution('DEBUG', 'Script Execution Context', nlapiGetContext().getExecutionContext());

	if (nlapiGetContext().getExecutionContext() != 'suitelet' && nlapiGetContext().getExecutionContext() != 'scheduled') {
	
		var recordId = nlapiGetRecordId();
		var recordType = nlapiGetRecordType();
		var prodOrder = nlapiGetFieldValue('createdfrom');
		
		// Add Button for Complete Batch
		form.setScript('customscript_h_berp_button_click');
		if (type != 'view') { form.addButton('custpage_fillcompletiontimes', 'Auto Fill Completion Times', 'fillCompletionTimes(\'' + prodOrder + '\');'); }

	}
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
  
}
