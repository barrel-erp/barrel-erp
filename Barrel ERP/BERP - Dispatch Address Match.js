function lookUpRltdStop(type){
	var oldSalesRec = nlapiGetOldRecord();
	var oldAddress = oldSalesRec.getFieldText("shipaddresslist");
	nlapiLogExecution('DEBUG', 'old address', oldAddress);

	var recordId = nlapiGetRecordId();
	var recordType = nlapiGetRecordType();

	var salesRec = nlapiLoadRecord(recordType, recordId);
	var addressTxt = salesRec.getFieldText("shipaddresslist");
	nlapiLogExecution('DEBUG', 'new address', addressTxt);

	if(addressTxt != oldAddress){
		var customer = salesRec.getFieldValue("entity");
		//nlapiLogExecution('DEBUG', 'customer address text', customer + ', ' + addressTxt);

		var routeAddrss = nlapiSearchRecord("customrecord_h_berp_route_stop",null,
		[
		   ["custrecord_h_berp_rt_stp_customer","anyof", customer], 
		   "AND", 
		   ["custrecord_h_berp_rt_stp_shipto_addr","contains", addressTxt ]
		], 
		[
		   new nlobjSearchColumn("name",null,null).setSort(false), 
		   new nlobjSearchColumn("custrecord_h_berp_rt_stp_cust_cat",null,null), 
		   new nlobjSearchColumn("custrecord_h_berp_rt_stp_phone",null,null), 
		   new nlobjSearchColumn("custrecord_h_berp_rt_stp_delivery_area",null,null),
		   new nlobjSearchColumn("internalid",null,null)
		]
		);
	
		var newRouteStop = routeAddrss[0].getValue("internalid");
		salesRec.setFieldValue("custbody_h_berp_rt_stp", newRouteStop);
		salesRec.setFieldValue("custbody_h_berp_dispatch_event", '');
		nlapiSubmitRecord(salesRec);
	}
}