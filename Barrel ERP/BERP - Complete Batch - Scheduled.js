/**
 * Module Description
 * 
 * Version    Date             Author           Remarks
 * 1.0        26 April 2016    jbrox
 * 2.0		  8 November 2016  jbrox            Add Scheduled Script to increase Script Usage points.
 * 2.0 ^ 2    13 June 2017     amillen	        Add additional features to complete all manufacturing operation tasks on completion of batch/work orders, set scrap quantity on manufacturing operation task, and link batch to lot number record
 */

function completeBatch(type) {

  //@param {String} type Context Types: scheduled, ondemand, userinterface,aborted, skipped

    nlapiLogExecution('debug','Scheduled Script TYPE:', type)
	 //only execute ondemand
  if ( type != 'ondemand' ) 
  {
     nlapiLogExecution('AUDIT','Scheduled Script is not ondemand.', type)
    return; 
  }
  
	var context = nlapiGetContext();
	var transactionId = context.getSetting('SCRIPT', 'custscript_h_berp_transaction');

	// Get Parameters
	var location = context.getSetting('SCRIPT', 'custscript_h_berp_comp_batch_location');
	var batchLotNumber = context.getSetting('SCRIPT', 'custscript_h_berp_comp_batch_lot');
	var recordId = context.getSetting('SCRIPT', 'custscript_h_berp_comp_batch_probatch');
	var briteTank = context.getSetting('SCRIPT', 'custscript_h_berp_comp_batch_brite_tank');
	var completedQuantity = context.getSetting('SCRIPT', 'custscript_h_berp_comp_batch_completed');
	var prodBatchQuantity = context.getSetting('SCRIPT', 'custscript_h_berp_comp_batch_probatch_qt');
	var completionDate = context.getSetting('SCRIPT', 'custscript_h_berp_comp_batch_comp_date');
	var brightBeerExpirationDays = context.getSetting('SCRIPT', 'custscript_h_berp_bright_beer_exp');
	var expirationDate = new Date();
	expirationDate = nlapiAddDays(expirationDate, brightBeerExpirationDays);
	expirationDate = nlapiDateToString(expirationDate);

	// Process Production Completions
	var productionOrdersToComplete = searchProductionOrdersToComplete(recordId);

	if (productionOrdersToComplete) {

		var completionQuantity = completedQuantity / productionOrdersToComplete.length;
		
		if (parseFloat(completedQuantity) <= parseFloat(prodBatchQuantity)) { 

			var scrapQuantity = (parseFloat(prodBatchQuantity) - parseFloat(completedQuantity)) / productionOrdersToComplete.length; 
			// nlapiLogExecution('DEBUG', 'Scrap Quantity', parseFloat(prodBatchQuantity) + ' - ' + parseFloat(completedQuantity) + ' / ' + productionOrdersToComplete.length + ' = ' + scrapQuantity.toFixed(2));

		} else {

			var scrapQuantity = 0;

		}

		for (var i = 0; i < productionOrdersToComplete.length; i++) {
			nlapiLogExecution('DEBUG', 'Production Order Length', productionOrdersToComplete.length);

			var productionOrder = productionOrdersToComplete[i].getValue('internalid');

			//set scrap quantity on manufacturing operation task and change status to complete on manufacturing operation task
			// nlapiLogExecution('DEBUG', 'Production Order', productionOrder);

			// var origProductionOrder = nlapiLookupField('customrecord_h_berp_prod_batch', recordId, 'custrecord_h_berp_originating_prodord');
			// var origProOrderWorkCenters = searchProOrderWorkCenters(origProductionOrder);

			var currentProOrderWorkCenters = searchProOrderWorkCenters(productionOrder);
			// nlapiLogExecution('DEBUG', 'Production Order Work Centers', JSON.stringify(currentProOrderWorkCenters));

			for (var b = 0; b < currentProOrderWorkCenters.length; b++) {

				if (nlapiLookupField('entitygroup', currentProOrderWorkCenters[b].getValue('manufacturingworkcenter', 'manufacturingOperationTask'), 'custentity_h_berp_equipment_type') == '8') {

					var productionOrderStatus = nlapiLookupField('workorder', productionOrder, 'status');
					// nlapiLogExecution('AUDIT', 'Production Order Status', productionOrder + ' ' + productionOrderStatus);

					if (productionOrderStatus == 'pendingApproval' || productionOrderStatus == 'Released' || productionOrderStatus == 'pendingBuild') {
					
						nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', briteTank);
						nlapiLogExecution('AUDIT', 'Setting Work Center', 'Manufacturing Operation Task = ' + currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask') + ' | ' + 'Orig Brite Tank = ' + briteTank);

					} else {

						nlapiLogExecution('AUDIT', 'Production Order Status Before Unlock', productionOrder + ' ' + productionOrderStatus);
						nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'A');
						// var productionOrderStatus = nlapiLookupField('workorder', productionOrder, 'status');
						nlapiLogExecution('AUDIT', 'Production Order Status After Unlock', productionOrder + ' ' + productionOrderStatus);
						nlapiLogExecution('DEBUG', 'Manufacturing Operation | Orig Ferm Tank', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask') + ' | ' + briteTank);
						try { nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', briteTank); } catch(e) { nlapiLogExecution('ERROR', e.getCode(), e.getDetails()); }
						nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'D');

					}

				}

			}
			
			var productionOrderQuantity = productionOrdersToComplete[i].getValue('quantity');
			var fermTanks = [];
			var manufacturingWorkCenters = searchAllFermentationTanks(location);
			if (manufacturingWorkCenters) { 

				for (var f = 0; f < manufacturingWorkCenters.length; f++) {

					fermTanks.push(manufacturingWorkCenters[f].getValue('internalid'));

				}

			}
			var productionOrderOperations = searchproductionOrderOperations(productionOrder, fermTanks);
			productionOrderOperations = productionOrderOperations[0].getValue('internalid', 'manufacturingOperationTask');
			var productionOrderOperationMax = searchproductionOrderOperationMax(productionOrder);
			nlapiLogExecution('DEBUG', 'Production Order Operation Max Search', JSON.stringify(productionOrderOperationMax));
			productionOrderOperationMax = productionOrderOperationMax[0].getValue('internalid', 'manufacturingOperationTask', 'GROUP');

			var productionCompletion = nlapiTransformRecord('workorder', productionOrder, 'workordercompletion', {recordmode: 'dynamic'});
			nlapiLogExecution('DEBUG', 'Starting Operation', productionOrderOperations);
			productionCompletion.setFieldValue('startoperation', productionOrderOperations);
			nlapiLogExecution('DEBUG', 'Ending Operation', productionOrderOperationMax);
			productionCompletion.setFieldValue('endoperation', productionOrderOperationMax);
			productionCompletion.setFieldValue('quantity', productionOrderQuantity);
            productionCompletion.setFieldValue('trandate',completionDate);
			nlapiLogExecution('DEBUG', 'Production Order Completion Quantity', completionQuantity.toFixed(2));
			nlapiLogExecution('DEBUG', 'Production Order Completion Build Quantity', productionOrderQuantity);
			productionCompletion.setFieldValue('completedquantity', completionQuantity.toFixed(2));
			productionCompletion.setFieldValue('scrapquantity', scrapQuantity.toFixed(2));
			var invDetail = productionCompletion.createSubrecord('inventorydetail');
			invDetail.setFieldValue('custitemnumber_h_berp_invdetail_prod_batch', recordId);
			invDetail.selectNewLineItem('inventoryassignment');
			invDetail.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchLotNumber);
			invDetail.setCurrentLineItemValue('inventoryassignment', 'expirationdate', expirationDate);
			invDetail.setCurrentLineItemValue('inventoryassignment', 'quantity', completionQuantity.toFixed(2));
			invDetail.commitLineItem('inventoryassignment');
			invDetail.commit();
			var productionCompletionId = nlapiSubmitRecord(productionCompletion, true);

			if (productionCompletionId) { nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'G'); }

			var lotResults = searchLotNumber(batchLotNumber);
			if(isNotNull(lotResults)){
				for(var h = 0; h < lotResults.length; h++){
					var lotId = lotResults[h].getValue('internalid');
					nlapiSubmitField('inventorynumber', lotId, 'custitemnumber_h_berp_invdetail_prod_batch', recordId);
				}
			}

			var manOpTaskResults = searchMOTasks(productionOrder);
			var fields = [];
			var values = [];
			fields[0] = 'custevent_h_berp_mot_output_delta';
			fields[1] = 'status';
			values[0] = scrapQuantity;
			values[1] = 'COMPLETE';

			var prodStatus = nlapiLookupField('workorder', productionOrder, 'status');
			nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'A');
			for(var m = 0; m < manOpTaskResults.length; m++){

				nlapiLogExecution('DEBUG', 'man task id', manOpTaskResults[m].getValue('internalid'));
				var opTaskId = manOpTaskResults[m].getValue('internalid');
				var opStatus = nlapiLookupField('manufacturingoperationtask', opTaskId, 'status');
				nlapiLogExecution('DEBUG', 'man op task status', opStatus);	

				var manRec = nlapiLoadRecord('manufacturingoperationtask', opTaskId);
				manRec.setFieldValue('custevent_h_berp_mot_output_delta', scrapQuantity);
				manRec.setFieldValue('status', 'COMPLETE');
				nlapiSubmitRecord(manRec);
				//nlapiSubmitField('manufacturingoperationtask', opTaskId, fields, values);
			}
			nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'G');


			nlapiLogExecution('AUDIT', 'Remaining Usage', productionOrder + ' ' + nlapiGetContext().getRemainingUsage());

		}

	}

	if (productionCompletionId) { 

		nlapiSubmitField('customrecord_h_berp_prod_batch', recordId, 'custrecord_h_berp_batch_complete', 'T');

	}

	nlapiLogExecution('DEBUG', 'Remaining Usage', nlapiGetContext().getRemainingUsage());

}