function createBlend(request, response){


	if (request.getMethod() == 'GET') {

			var context = nlapiGetContext();

			// Get Parameters
			var blendId = request.getParameter('custparam_recordid');
			var baSub = nlapiLookupField('customrecord_h_berp_blending', blendId, 'custrecord_h_berp_bl_sub');
			var baLoc = nlapiLookupField('customrecord_h_berp_blending', blendId, 'custrecord_h_berp_bl_loc');
			var date = nlapiLookupField('customrecord_h_berp_blending', blendId, 'custrecord_h_berp_bl_date');
  			var commercialItem = nlapiLookupField('customrecord_h_berp_blending', blendId,'custrecord_h_berp_bl_comm_item');
  			var onHand = 'T';
  			nlapiLogExecution('DEBUG', 'blend', blendId + ', ' + baSub + ' , ' + baLoc + ', ' + commercialItem);

			var searchBAitems = nlapiSearchRecord("customrecord_h_berp_barrels",null,
					[
					   ["custrecord_h_berp_blending","anyof",blendId]
					], 
					[
					   new nlobjSearchColumn("internalid",null,null), 
					   new nlobjSearchColumn("custrecord_h_berp_ba_item",null,null),
					   new nlobjSearchColumn("custrecord_h_berp_qty", null, null),
					   new nlobjSearchColumn("custrecord_h_berp_item_lot", null,  null)
					]
					);


			 // Create Form with Fields
		    var createBlend = nlapiCreateForm('Create Commercial Blend');

		    // Create Field Groups on the Form
		    var primary = createBlend.addFieldGroup('primary', 'Blending Information');
		   	createBlend.addField('custpage_recordid', 'select', 'Blending Record', 'customrecord_h_berp_blending', 'primary').setDisplayType('disabled');
		    createBlend.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('disabled');
		   	createBlend.addField('custpage_sub', 'select', 'Subsidiary', '-117', 'primary').setDisplayType('disabled');
			createBlend.addField('custpage_date', 'date', 'Date', null, 'primary');
			createBlend.addField('custpage_commercial', 'select', 'Item', '-10').setDisplayType('disabled').setDefaultValue(commercialItem)
			createBlend.addField('custpage_qty', 'integer', 'Quantity on Production Order', null, 'primary');

			//var itmArr = [];
			for(var i = 0; i < searchBAitems.length; i++){

				var ba_racked = searchBAitems[i].getValue('custrecord_h_berp_ba_item');
				//itmArr.push(ba_racked);
				var sublistName = 'custpage_lotitemdetail' + searchBAitems[i].getValue('custrecord_h_berp_ba_item');
				var ba_rackedTxt = searchBAitems[i].getText('custrecord_h_berp_ba_item');
				var ba_Items = createBlend.addSubList(sublistName, 'inlineeditor', ba_rackedTxt);	
			
				ba_Items.addField('custpage_sl_location', 'select', 'Location', '-103').setDisplayType('disabled');
			    ba_Items.addField('custpage_sl_lotnumber', 'text', 'Lot Number').setDisplayType('disabled');
			    ba_Items.addField('custpage_sl_lotid', 'integer', 'ID').setDisplayType('hidden');
			    ba_Items.addField('custpage_available', 'float', 'Available QTY').setDisplayType('disabled');
			    ba_Items.addField('custpage_selectlot', 'checkbox', 'Select Lot').setMandatory(true);


				var inventoryNumbers = searchInventoryNumbers(ba_racked, onHand, baLoc);
				nlapiLogExecution('DEBUG' ,'inventory', inventoryNumbers.length);
				for(var j = 0; j < inventoryNumbers.length; j++){
				 	var lineNum = j + 1;
					
					var lineLoc = ba_Items.setLineItemValue('custpage_sl_location', lineNum, baLoc);
					var lotTxt = ba_Items.setLineItemValue('custpage_sl_lotnumber', lineNum, inventoryNumbers[j].getValue('inventorynumber'));
					var lotId = ba_Items.setLineItemValue('custpage_sl_lotid', lineNum, inventoryNumbers[j].getValue('internalid'));
					var onHandqty = ba_Items.setLineItemValue('custpage_available', lineNum, inventoryNumbers[j].getValue('quantityavailable'));
				}
			}
			createBlend.setFieldValues({custpage_recordid: blendId, custpage_location: baLoc, custpage_sub: baSub, custpage_date: date});

			createBlend.addSubmitButton('Submit');

	    	response.writePage(createBlend);
	}
	else {
		//custscript_overhead_labor
		//non labor item to add = 531

		// Get Context & Script Paramaters
		var context = nlapiGetContext();
		var laborCost = context.getSetting('SCRIPT', 'custscript_overhead_labor');
		var setLabor = false;
		// Get Parameters
		var location = request.getParameter('custpage_location');
		var subsidiary = request.getParameter('custpage_sub');
		var blendId = request.getParameter('custpage_recordid');
		var prodQTY = request.getParameter('custpage_qty');
		var selectLot = request.getParameter('custpage_selectlot');
		var date = nlapiLookupField('customrecord_h_berp_blending', blendId, 'custrecord_h_berp_bl_date');
		var commercialItem = nlapiLookupField('customrecord_h_berp_blending', blendId,'custrecord_h_berp_bl_comm_item');

		var prodOrd = nlapiCreateRecord('workorder', {recordmode: 'dynamic'});

		prodOrd.setFieldValue('subsidiary', subsidiary);
		prodOrd.setFieldValue('assemblyitem', commercialItem);
		prodOrd.setFieldValue('trandate', date);
		prodOrd.setFieldValue('quantity', prodQTY);
		//prodOrd.setFieldValue('iswip', 'T');


		//customrecord_h_berp_license
		var licenseSearch = nlapiSearchRecord("customrecord_h_berp_license", null,
			[
			["custrecord_not_customer_owned", "is", "T" ]
			],
			[
				new nlobjSearchColumn("internalid", null, null)
			]
			);



		var searchBAitems = nlapiSearchRecord("customrecord_h_berp_barrels",null,
			[
			   ["custrecord_h_berp_blending","anyof",blendId]
			], 
			[
			   new nlobjSearchColumn("internalid",null,null), 
			   new nlobjSearchColumn("custrecord_h_berp_ba_item",null,null),
			   new nlobjSearchColumn("custrecord_h_berp_qty", null, null),
			   new nlobjSearchColumn("custrecord_h_berp_item_lot", null,  null)
			]
			);


		var setLicense = licenseSearch[0].getValue('internalid');
		nlapiLogExecution('DEBUG', 'set license', setLicense);

		prodOrd.setFieldValue('custbody_h_berp_license', 2);

		var workOrdID = nlapiSubmitRecord(prodOrd);
		// try{
		// 	var productionIssue = nlapiTransformRecord('workorder', workOrdID, 'workorderissue');
		// 	productionIssue.setFieldValue('trandate', date);
		// 	var productionIssueId = nlapiSubmitRecord(productionIssue);
		// }
		// catch(E){
		// 	nlapiLogExecution('DEBUG', 'error 1', E);
		// }
 

		// try{		
	
		//nlapiLogExecution('DEBUG', 'line count', lineCnt);

		var searchBAitems = nlapiSearchRecord("customrecord_h_berp_barrels",null,
		[
		   ["custrecord_h_berp_blending","anyof",blendId]
		], 
		[
		   new nlobjSearchColumn("internalid",null,null), 
		   new nlobjSearchColumn("custrecord_h_berp_ba_item",null,null),
		   new nlobjSearchColumn("custrecord_h_berp_qty", null, null),
		   new nlobjSearchColumn("custrecord_h_berp_item_lot", null,  null)
		]
		);
		var itemDictionary = { itemInternal: [], itemLot: [], QTY: [] };
		for(var i = 0; i < searchBAitems.length; i++){
			var blendItm = searchBAitems[i].getValue('custrecord_h_berp_ba_item');
			var lotNum = searchBAitems[i].getValue('custrecord_h_berp_item_lot');
			var blendQTY = searchBAitems[i].getValue('custrecord_h_berp_qty');

			var prodOrd = nlapiLoadRecord('workorder', workOrdID);
			var lineCnt = prodOrd.getLineItemCount('item');

			nlapiLogExecution('DEBUG', 'prod ord line count', lineCnt);

			for(var j = 1; j <= lineCnt; j++){
				
				var prodItem = prodOrd.getLineItemValue('item', 'item', j);
				var proditmQTY = prodOrd.getLineItemValue('item', 'quantity', j);
				itemDictionary.itemInternal.push(prodItem);
				itemDictionary.itemLot.push(lotNum);
				itemDictionary.QTY.push(proditmQTY);

				nlapiLogExecution('DEBUG', 'prod vals', prodItem + ' ' + proditmQTY + ' ' + lotNum);

				if(blendItm == prodItem){

					prodOrd.selectLineItem('item', j);
					var subrecord = prodOrd.createCurrentLineItemSubrecord('item', 'inventorydetail');
						subrecord.selectNewLineItem('inventoryassignment');
						subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotNum);
						subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', proditmQTY);
						subrecord.commitLineItem('inventoryassignment');
						subrecord.commit();
						prodOrd.commitLineItem('item');
				}
			}

			var workOrdID = nlapiSubmitRecord(prodOrd);
		// 	var prodIssue = nlapiLoadRecord('workorderissue', productionIssue);

		// 	var prodIssueLineCnt = nlapiGetLineItemCount('components');

		// 	for(var k = 1; k <= prodIssueLineCnt; k ++){
		// 		var issueItem = prodIssue.getLineItemValue('components', 'item', k);
		// 		prodIssue.selectLineItem('components', k);
		// 		var inventoryDetailExists = prodIssue.viewCurrentLineItemSubrecord('components', 'componentinventorydetail');
		// 		for(var l = 0; l < itemDictionary.itemInternal.length; l++){
		// 			nlapiLogExecution('DEBUG', 'item dictionary', itemDictionary.itemInternal[l]);

		// 			var prodOrdItem = itemDictionary.itemInternal[l];

		// 			if(prodOrdItem == issueItem){
		// 				if (inventoryDetailExists) { var subrecord2 = prodIssue.removeCurrentLineItemSubrecord('components', 'componentinventorydetail'); prodIssue.commitLineItem('item'); var recid = nlapiSubmitRecord(prodIssue, true); var prodIssue = nlapiLoadRecord('workorder', productionIssueId, {recordmode: 'dynamic'}); prodIssue.selectLineItem('components', k);  } 

		// 				var subrecord2 = prodIssue.createCurrentLineItemSubrecord('components', 'componentinventorydetail');
		// 				prodIssue.setCurrentLineItemValue('components', 'quantity', itemDictionary.QTY[l]);
		// 				subrecord2.selectNewLineItem('componentinventorydetail');
		// 				subrecord2.setCurrentLineItemValue('componentinventorydetail', 'issueinventorynumber', itemDictionary.itemLot[l]);
		// 				subrecord2.setCurrentLineItemValue('componentinventorydetail', 'quantity', itemDictionary.QTY[l]);
		// 				subrecord2.commitLineItem('componentinventorydetail');
		// 				subrecord2.commit();
		// 				prodIssue.commitLineItem('components');
		// 			}
		// 		}
		// 	}
		 }
		// var prodIssueCommit = nlapiSubmitRecord(prodIssue, true);
	// }
	// catch(E){
	// 	nlapiLogExecution('DEBUG', 'error 2', E);
	// }

		if(setLabor == false){
			setLabor = true;
			prodOrd.selectNewLineItem('item');
			prodOrd.setCurrentLineItemValue('item', 'item', 531);
			prodOrd.setCurrentLineItemValue('item', 'description', laborCost);
			prodOrd.commitLineItem('item');
		}	
				
		nlapiSubmitField('customrecord_h_berp_blending', blendId, 'custrecord_prod_ord_blendie', workOrdID);
		nlapiSetRedirectURL('RECORD', 'workorder', workOrdID);
	}
}

function checkBarrel(item_name){
	//3968 oz in 1 barrel of beer.
	//117347.7658 mL in 1 barrel of beer
	
	var isBrrl = new RegExp("Barrel");
	var isbrrl = new RegExp("barrel");

	var res = isBrrl.test(item_name);
	var res2 = isbrrl.test(item_name);

	if(res == true || res2 == true){
		return true;
	}else{
		return false;
	}
}