/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Dec 2015     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	var recordId = nlapiGetRecordId();
	var recordType = nlapiGetRecordType();
	var yeastItem = nlapiGetFieldValue('custrecord_h_berp_yeast');
	var alreadyCommitted = nlapiGetFieldValue('custrecord_h_berp_yp_commit_prod_batch');

	var location = nlapiGetFieldValue('custrecord_h_berp_yp_location');

	// Add Button for Commit to Production Batch
	var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_yeast_propagation','customdeploy_h_berp_yeast_propagation');
	suiteletURL += '&custparam_recordid=' + recordId;
	suiteletURL += '&custparam_location=' + location;
	suiteletURL += '&custparam_yeastitem=' + yeastItem;
	form.setScript('customscript_h_berp_button_click');
	if (type == 'view' && alreadyCommitted == 'F') { form.addButton('custpage_yeastpropagation', 'Commit to Production Batch', 'yeastPropagation(\'' + suiteletURL + '\');'); }
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
  
}
