function dispatchScheduled(){
	//nlapiLogExecution('DEBUG', 'start', 'start');
	var todaysDate = new Date();
	var setDate = nlapiDateToString(todaysDate, 'date');
	//nlapiLogExecution('DEBUG', 'date', setDate);

	//item fulfillment packed status only
	//script runs every day and looks at Item Fulfillment in stage 'packed' that has a populated delivery area field
	//script matches item fulfillment ship address 

	var itemfullSearch = nlapiSearchRecord("itemfulfillment",null,
				[
				   ["type","anyof","ItemShip"], 
				   "AND", 
				   ["mainline","is","T"], 
				   "AND", 
				   ["status","anyof", "ItemShip:B"], 
				   "AND", 
				   ["custbody_h_berp_delivery_area","noneof","@NONE@"]
				], 
				[
				   new nlobjSearchColumn("custbody_h_berp_rt_stp",null,null), 
				   new nlobjSearchColumn("internalid",null,null),
				   new nlobjSearchColumn('createdfrom', null, null),
				   new nlobjSearchColumn("shipaddress1", null, null)
				]
				);
	if(isNotNull(itemfullSearch)){
		//nlapiLogExecution('DEBUG', 'search length', itemfullSearch.length);
		for(var i = 0; i < itemfullSearch.length; i++){

			var itmfill_id = itemfullSearch[i].getValue("internalid");
			var so_id = itemfullSearch[i].getValue("createdfrom");
			var shipToAddrss = itemfullSearch[i].getValue('shipaddress1');
			var matchShipAddrss = shipToAddrss.toString();
			nlapiLogExecution('DEBUG', 'ship to address', shipToAddrss);

			var setCompany = nlapiLookupField("itemfulfillment", itmfill_id, "entity", false);
			var routeStopId = nlapiLookupField("itemfulfillment", itmfill_id, "custbody_h_berp_rt_stp");
			var deliveryArea = nlapiLookupField("itemfulfillment", itmfill_id, "custbody_h_berp_delivery_area");
			//nlapiLogExecution('DEBUG', 'item fulfillment vals', setCompany + ', ' + routeStopId + ', ' + deliveryArea);


			if(isNotNull(routeStopId)){
				var routeRecord = nlapiLoadRecord('customrecord_h_berp_route_stop', routeStopId);
				var title = routeRecord.getFieldValue("name");
				var specTruck = routeRecord.getFieldValue("custrecord_h_berp_rt_stp_spectruck");
			}

			//if item fulfillment shipping address matches an event that has delivery type criteria met - copy event.
			try{
			var calendareventSearch = nlapiSearchRecord("calendarevent",null,
					[
					   ["custevent_h_berp_event_shipto_addr","contains", matchShipAddrss]
					], 
					[
					   new nlobjSearchColumn("internalid",null,null), 
					   new nlobjSearchColumn("status",null,null), 
					   new nlobjSearchColumn("custevent_h_berp_event_delb_type",null,null)
					]
					);

					if(isNotNull(calendareventSearch)){

						for(var i = 0; i < calendareventSearch.length; i++){
							var event_id = calendareventSearch[i].getValue('internalid');
							var eventRec = nlapiLoadRecord('calendarevent', event_id);
							var deliveryType = eventRec.getFieldValue('custevent_h_berp_event_delb_type');
							var runDelivery = deliveryType.toString();
							nlapiLogExecution('DEBUG', 'delivery type', deliveryType);
							
							var copyOrCreate = checkStanding(runDelivery);
							nlapiLogExecution('DEBUG', 'copy or create', copyOrCreate);

							if(isNotNull(copyOrCreate)){
								var eventCopyRec = nlapiCopyRecord('calendarevent', event_id);
								eventCopyRec.setFieldValue("accesslevel", 'PUBLIC');
								eventCopyRec.setFieldValue('organizer', 3535);
								eventCopyRec.setFieldValue('transaction', itmfill_id);
								eventCopyRec.setFieldValue("starttime", '12:00 am');
								eventCopyRec.setFieldValue("endtime", '11:59 pm');
								eventCopyRec.setFieldValue("startdate", setDate);
								var copyId = nlapiSubmitRecord(eventCopyRec);
								nlapiLogExecution('DEBUG', 'event created', copyId);
								nlapiSubmitField("salesorder", so_id, 'custbody_h_berp_dispatch_event', copyId);
		          				nlapiSubmitField("itemfulfillment", itmfill_id, "custbody_h_berp_dispatch_event", copyId);
							}
							break;
						}
		   			
				}
				else if(isNull(calendareventSearch)){
					nlapiLogExecution('DEBUG', 'calendar event null', 'is Null bracket');
				    var newEvent = nlapiCreateRecord("calendarevent", {recordmode: 'dynamic'});
					newEvent.setFieldValue("title", title);
					newEvent.setFieldValue("company", setCompany);
					newEvent.setFieldValue("accesslevel", 'PUBLIC');
					newEvent.setFieldValue("customform", '63');
					newEvent.setFieldValue("starttime", '12:00 am');
					newEvent.setFieldValue("endtime", '11:59 pm');
					newEvent.setFieldValue("startdate", setDate);
					newEvent.setFieldValue("status", 'TENTATIVE');
					//newEvent.setFieldValue("custevent_h_berp_event_delb_type", "Delivery");
					newEvent.setFieldValue("organizer", 3535);
					newEvent.setFieldValue("custevent_h_berp_event_shipto_addr", shipToAddrss);
					newEvent.setFieldValue("transaction", itmfill_id);
					newEvent.setFieldValue("custevent_h_berp_rt_stp", routeStopId);
					newEvent.setFieldValue("custevent_h_berp_event_delivery_area", deliveryArea);
					newEvent.setFieldValue("custevent_h_berp_event_spec_truck", specTruck);
		          
		          	var newEventId = nlapiSubmitRecord(newEvent);

		          	nlapiSubmitField("salesorder", so_id, 'custbody_h_berp_dispatch_event', newEventId);
		          	nlapiSubmitField("itemfulfillment", itmfill_id, "custbody_h_berp_dispatch_event", newEventId);
		       } 
          	}
      		catch(E){
      			nlapiLogExecution("ERROR", 'error', E);
      		}
		}
	}
}
function checkStanding(string){
	var isStanding = new RegExp('Standing');

	var returnMatch = string.match(isStanding);

	return returnMatch;

	nlapiLogExecution('DEBUG', 'is standing reg exp', returnMatch);
}
function isNotNull(field_val){
	return (field_val != '' && field_val != null);
}
function isNull(s_string){
	return (s_string == null || s_string == '');
}