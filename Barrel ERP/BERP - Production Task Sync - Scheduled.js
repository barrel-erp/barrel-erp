function syncProductionTasks(type) {

  //@param {String} type Context Types: scheduled, ondemand, userinterface,aborted, skipped

    nlapiLogExecution('debug','Scheduled Script TYPE:', type)
	 //only execute ondemand
  if ( type != 'scheduled' ) 
  {
     nlapiLogExecution('AUDIT','Scheduled Script is not executing from the schedule.', type)
    return; 
  }
  
	var inProcessProductionOrders = searchInProcessProductionOrders();

	if (inProcessProductionOrders) {

		for (var i = 0; i < inProcessProductionOrders.length; i++) {

			var manufacturingWorkCenter = inProcessProductionOrders[i].getValue('manufacturingworkcenter', 'manufacturingOperationTask');
			var productionOrder = inProcessProductionOrders[i].getValue('internalid');
			var startDate = inProcessProductionOrders[i].getValue('startdate', 'manufacturingOperationTask');
			var endDate = inProcessProductionOrders[i].getValue('enddate', 'manufacturingOperationTask');

			var tasksToUpdate = searchTasksToUpdate(manufacturingWorkCenter, productionOrder, startDate, endDate);

			if (tasksToUpdate) {

				for (var t = 0; t < tasksToUpdate.length; t++) {

					var productionTaskTemplate = tasksToUpdate[t].getValue('custevent_h_berp_prod_task_template');
					var expectedStartDate = nlapiStringToDate(startDate);
					expectedStartDate.setDate(expectedStartDate.getDate() + parseInt(nlapiLookupField('customrecord_h_berp_prod_task_template', productionTaskTemplate, 'custrecord_h_berp_due_date')));
					expectedStartDate = nlapiDateToString(expectedStartDate);

					var currentStartDate = tasksToUpdate[t].getValue('startdate');

					var expectedDueDate = nlapiStringToDate(expectedStartDate);
					expectedDueDate.setDate(expectedDueDate.getDate() + parseInt(nlapiLookupField('customrecord_h_berp_prod_task_template', productionTaskTemplate, 'custrecord_h_berp_days_to_complete')));
					expectedDueDate = nlapiDateToString(expectedDueDate);

					var currentStartDate = tasksToUpdate[t].getValue('startdate');
					var currentDueDate = tasksToUpdate[t].getValue('duedate');

					if (expectedStartDate != currentStartDate) {

						var productionTaskRecord = nlapiLoadRecord('task', tasksToUpdate[t].getValue('internalid'));
						productionTaskRecord.setFieldValue('duedate', expectedDueDate);
						productionTaskRecord.setFieldValue('startdate', expectedStartDate);
						var taskId = nlapiSubmitRecord(productionTaskRecord, true);

					}

				}

			}

		}

	}

}