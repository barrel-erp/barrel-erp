function unlockRelockOperations(suiteletURL) {

	window.open(suiteletURL, '_self');
 
    return true;      

}

function harvestYeast(suiteletURL) {

	var w = 1750;
	var h = 1000;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    

}
function blendSuitelet(suiteletURL){
	var w = 1750;
	var h = 1000;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    
}

function storeYeast(suiteletURL) {

	var w = 1750;
	var h = 1000;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    

}

function yeastPropagation(suiteletURL) {

	var w = 1750;
	var h = 1000;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    

}

function displayLotReports(lotReportSuiteletURL) {

	var w = 1000;
	var h = 800;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(lotReportSuiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;

}

function generateBatch(batchSuiteletURL) {

	var w = 1000;
	var h = 800;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(batchSuiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;

}

function completeBatch(suiteletURL) {

	var w = 1000;
	var h = 800;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    

}

function dryHop(suiteletURL) {

	var w = 1000;
	var h = 800;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    

}

function drainBrite(suiteletURL) {

	var w = 1000;
	var h = 800;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    

}

function moveTanks(suiteletURL) {

	var w = 1000;
	var h = 800;
	var left = ((screen.width/2)-(w/2));
	var tops = ((screen.height/2)-(h/2));

	window.open(suiteletURL, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+tops+', left='+left);
 
    return true;    

}

function openPDFinNewTab(strURL) {    
     
    window.open(strURL, '_blank');
 
    return true;         
     
}