/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        03 Dec 2015     jbrox
 *
 */

function displaySalesLotReport(request, response) {

		var context = nlapiGetContext();
		var accountId = context.getSetting('SCRIPT', 'custscript_h_berp_ns_account_id');
		var activeAccounts = accountActiveCheck();
		nlapiLogExecution('DEBUG', 'Active Accounts', JSON.stringify(activeAccounts));
		nlapiLogExecution('DEBUG', 'Index', activeAccounts.indexOf(accountId));
	    if (activeAccounts.indexOf(accountId) >= 0) {

	    	if (request.getMethod() == 'GET') {

				// Get Parameters
				var location = request.getParameter('custparam_location');
				nlapiLogExecution('DEBUG', 'Location', location);

				if (location != 'null') {

					var recordId = request.getParameter('custparam_custom_id');
					var recordType = request.getParameter('custparam_recordtype');
					nlapiLogExecution('DEBUG', 'Before Submit - Record Type | Record ID', recordType + ' | ' + recordId);
					var onHand = 'T';
					var context = nlapiGetContext();
					nlapiLogExecution('DEBUG', 'Record Type', recordType);

					var record = nlapiLoadRecord(recordType, recordId);
					var lineCount = record.getLineItemCount('item');

					// Get Lot Numbered Item Records
				  	var itemRecordsObj = { itemRecords: [], itemInternal: [], itemLineId: [], itemLocation: [], itemRecordQuantities: [], itemInventoryDetail: [] };
				  	var lineNum = 0;
					for (var l = 0; l < lineCount; l++) {

						lineNum = l + 1;

						if (nlapiLookupField('item', record.getLineItemValue('item', 'item', lineNum), 'islotitem') == 'T' && nlapiLookupField('item', record.getLineItemValue('item', 'item', lineNum), 'custitem_h_berp_itemtype') != '3' && record.getLineItemValue('item', 'custcol_h_berp_addition_type', lineNum) != '2') {

							itemRecordsObj.itemRecords.push(record.getLineItemText('item', 'item', lineNum));
							itemRecordsObj.itemInternal.push(record.getLineItemValue('item', 'item', lineNum));
							itemRecordsObj.itemLineId.push(record.getLineItemValue('item', 'line', lineNum));
							itemRecordsObj.itemLocation.push(record.getLineItemValue('item', 'location', lineNum));
							itemRecordsObj.itemRecordQuantities.push(record.getLineItemValue('item', 'quantity', lineNum));
							itemRecordsObj.itemInventoryDetail.push(record.viewLineItemSubrecord('item', 'inventorydetail', lineNum));

						}

					}

					nlapiLogExecution('DEBUG', 'Item Record Object', JSON.stringify(itemRecordsObj));

				    // Create Form with Fields
				    var lotReportForm = nlapiCreateForm('Sales Transaction Lot Report');

				    // Create Field Groups on the Form
				    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
				    // lotReportForm.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('inline').setDefaultValue(location);
				    lotReportForm.addField('custpage_sublists', 'integer', 'Sublists').setDisplayType('hidden');
				    lotReportForm.addField('custpage_recordid', 'integer', 'Record ID').setDisplayType('hidden');
				    lotReportForm.addField('custpage_recordtype', 'text', 'Record Type').setDisplayType('hidden');

				  	var sublistRecs = [];

				  	for (var i = 0; i < itemRecordsObj.itemRecords.length; i++) {
				  		// for (var i = 0; i < 1; i++) {

				  		var sublistName = 'custpage_lotitemdetail' + itemRecordsObj.itemLineId[i];
				  		sublistRecs.push(sublistName);
				  		// nlapiLogExecution('DEBUG', 'Sublist Name', sublistName);
					    var lotItemDetail = lotReportForm.addSubList(sublistName, 'inlineeditor', itemRecordsObj.itemRecords[i] + ' (' + itemRecordsObj.itemRecordQuantities[i] + ')');
					    lotItemDetail.addField('custpage_sublistname', 'text', 'Sublist Name').setDisplayType('hidden');
					    lotItemDetail.addField('custpage_item', 'select', 'Item', '-10').setDisplayType('hidden');
					    lotItemDetail.addField('custpage_lot', 'text', 'Lot Number').setDisplayType('disabled');
					    lotItemDetail.addField('custpage_location', 'select', 'Location', '-103').setDisplayType('disabled');
					    lotItemDetail.addField('custpage_hopalpha', 'percent', 'Alpha Acid %').setDisplayType('disabled');
					    lotItemDetail.addField('custpage_lotid', 'integer', 'Lot ID').setDisplayType('hidden');
					    lotItemDetail.addField('custpage_expirationdate', 'date', 'Expiration Date').setDisplayType('disabled');
					    lotItemDetail.addField('custpage_onhand', 'float', 'On Hand').setDisplayType('disabled');
						lotItemDetail.addField('custpage_available', 'float', 'Available').setDisplayType('disabled');
						lotItemDetail.addField('custpage_isonhand', 'checkbox', 'Is On Hand').setDisplayType('hidden');
						lotItemDetail.addField('custpage_quantity', 'float', 'Quantity');
						// lotItemDetail.addField('custpage_cumulativequantity', 'float', 'Cumulative Quantity');
						lotItemDetail.addField('custpage_selectlot', 'checkbox', 'Select Lot').setMandatory(true);

						// if (itemRecordsObj.itemInternal[i] == 209) {

							var inventoryNumbers = searchInventoryNumbers(itemRecordsObj.itemInternal[i], onHand, itemRecordsObj.itemLocation[i]);
							if (itemRecordsObj.itemInventoryDetail[i]) { var inventoryDetail = searchInventoryDetail(recordId, itemRecordsObj.itemInventoryDetail[i].id, null); }
							var setLineNum = 0;
							var setLotLine = 'T';
							var setQuantity = 0;
							var previousSetQuantity = 0;
							var totalLineQuantity = itemRecordsObj.itemRecordQuantities[i];
							var remainingLineQuantity = itemRecordsObj.itemRecordQuantities[i];
							var cumulativeQuantity = 0;

							if (inventoryNumbers) {
							
								for (var n = 0; n < inventoryNumbers.length; n++) {

									if (!itemRecordsObj.itemInventoryDetail[i]) {

										previousSetQuantity = parseFloat(setQuantity) + parseFloat(previousSetQuantity);
										// if (itemRecordsObj.itemInternal[i] == 209) { nlapiLogExecution('AUDIT', 'Grain 2Row', 'setQuantity: ' + ' | ' + setQuantity + ' previousSetQuantity: ' + ' | ' + previousSetQuantity); }
										setQuantity = findSetQuantity(totalLineQuantity, remainingLineQuantity, inventoryNumbers[n].getValue('quantityavailable'));
										nlapiLogExecution('AUDIT', 'Set Quantity | Remaining Line Quantity', setQuantity + ' | ' + remainingLineQuantity);
										if (n > 0) { 
											// remainingLineQuantity = parseFloat(remainingLineQuantity) - parseFloat(setQuantity); 
											setQuantity = parseFloat(setQuantity) - parseFloat(previousSetQuantity);
											setQuantity = setQuantity.toFixed(2);
											// setQuantity = parseFloat(remainingLineQuantity) - parseFloat(setQuantity);
										}
										nlapiLogExecution('AUDIT', 'Set Quantity | Remaining Line Quantity', setQuantity + ' | ' + remainingLineQuantity);
										if (remainingLineQuantity < 0) { setQuantity = 0; setLotLine = 'F'; } 
										if (setQuantity <= 0) { setQuantity = 0; setLotLine = 'F'; }
										setLineNum = n + 1;

										lotItemDetail.setLineItemValue('custpage_item', setLineNum, itemRecordsObj.itemInternal[i]);
										lotItemDetail.setLineItemValue('custpage_sublistname', setLineNum, sublistName);
										lotItemDetail.setLineItemValue('custpage_lot', setLineNum, inventoryNumbers[n].getValue('inventorynumber'));
										lotItemDetail.setLineItemValue('custpage_location', setLineNum, inventoryNumbers[n].getValue('location'));
										lotItemDetail.setLineItemValue('custpage_hopalpha', setLineNum, inventoryNumbers[n].getValue('custitemnumber_h_berp_alpha_acid'));
										lotItemDetail.setLineItemValue('custpage_lotid', setLineNum, inventoryNumbers[n].getValue('internalid'));
									    lotItemDetail.setLineItemValue('custpage_expirationdate', setLineNum, inventoryNumbers[n].getValue('expirationdate'));
									    lotItemDetail.setLineItemValue('custpage_onhand', setLineNum, inventoryNumbers[n].getValue('quantityonhand'));
										lotItemDetail.setLineItemValue('custpage_available', setLineNum, inventoryNumbers[n].getValue('quantityavailable'));
										lotItemDetail.setLineItemValue('custpage_isonhand', setLineNum, inventoryNumbers[n].getValue('isonhand'));
										lotItemDetail.setLineItemValue('custpage_quantity', setLineNum, setQuantity);
										// lotItemDetail.setLineItemValue('custpage_cumulativequantity', setLineNum, setQuantity);				
										lotItemDetail.setLineItemValue('custpage_selectlot', setLineNum, setLotLine);
										setLotLine = 'T';

									}

								}

							}

						// }

						if (inventoryDetail) {

							// nlapiLogExecution('DEBUG', 'i', i);
							nlapiLogExecution('DEBUG', 'Item', itemRecordsObj.itemRecords[i]);
							var inventoryNumbers = searchInventoryNumbers(itemRecordsObj.itemInternal[i], null, itemRecordsObj.itemLocation[i]);
							nlapiLogExecution('DEBUG', 'Inventory Detail', JSON.stringify(inventoryNumbers));
							
							// if (inventoryNumbers) {
							
								for (var d = 0; d < inventoryNumbers.length; d++) {

									if (itemRecordsObj.itemInventoryDetail[i]) {

										// previousSetQuantity = parseFloat(setQuantity) + parseFloat(previousSetQuantity);
										setQuantity = searchInventoryDetail(recordId, itemRecordsObj.itemInventoryDetail[i].id, inventoryNumbers[d].getValue('internalid'));
										// nlapiLogExecution('DEBUG', 'Set Quantity Search Variables', recordId + ' | ' + itemRecordsObj.itemInventoryDetail[i].id + ' | ' + inventoryNumbers[d].getValue('internalid'));
										// if (setQuantity) { nlapiLogExecution('DEBUG', 'Set Quantity', JSON.stringify(setQuantity)); }
										// if (d > 0) { remainingLineQuantity = parseFloat(remainingLineQuantity) - parseFloat(setQuantity); setQuantity = parseFloat(setQuantity) - parseFloat(previousSetQuantity); }
										// if (setQuantity) { setQuantity = setQuantity[0].getValue('quantity', 'inventoryDetail'); setLotLine = 'T'; } else { setQuantity = ''; setLotLine = 'F'; }
										setLineNum = d + 1;

										lotItemDetail.setLineItemValue('custpage_item', setLineNum, itemRecordsObj.itemInternal[i]);
										lotItemDetail.setLineItemValue('custpage_sublistname', setLineNum, sublistName);
										lotItemDetail.setLineItemValue('custpage_lot', setLineNum, inventoryNumbers[d].getValue('inventorynumber'));
										lotItemDetail.setLineItemValue('custpage_location', setLineNum, inventoryNumbers[d].getValue('location'));
										lotItemDetail.setLineItemValue('custpage_hopalpha', setLineNum, inventoryNumbers[d].getValue('custitemnumber_h_berp_alpha_acid'));
										lotItemDetail.setLineItemValue('custpage_lotid', setLineNum, inventoryNumbers[d].getValue('internalid'));
									    lotItemDetail.setLineItemValue('custpage_expirationdate', setLineNum, inventoryNumbers[d].getValue('expirationdate'));
									    lotItemDetail.setLineItemValue('custpage_onhand', setLineNum, inventoryNumbers[d].getValue('quantityonhand'));
										lotItemDetail.setLineItemValue('custpage_available', setLineNum, inventoryNumbers[d].getValue('quantityavailable'));
										lotItemDetail.setLineItemValue('custpage_isonhand', setLineNum, inventoryNumbers[d].getValue('isonhand'));
										// var isSet = searchInventoryDetail(recordId, null, inventoryNumbers[d].getValue('inventorynumber'));
										// nlapiLogExecution('DEBUG', 'isSet', JSON.stringify(isSet));
										// if (isSet) { 

										// 	lotItemDetail.setLineItemValue('custpage_quantity', setLineNum, isSet[0].getValue('quantity', 'inventoryDetail'));
										// 	lotItemDetail.setLineItemValue('custpage_selectlot', setLineNum, 'T');

										// }

										if (setQuantity) {

											for (var s = 0; s < setQuantity.length; s++) {
												// nlapiLogExecution('DEBUG', 'if', setQuantity[s].getText('inventorynumber', 'inventoryDetail') + ' | ' + inventoryNumbers[d].getValue('inventorynumber'));
												if (setQuantity[s].getText('inventorynumber', 'inventoryDetail') == inventoryNumbers[d].getValue('inventorynumber')) {
												
													lotItemDetail.setLineItemValue('custpage_quantity', setLineNum, setQuantity[s].getValue('quantity', 'inventoryDetail'));
													lotItemDetail.setLineItemValue('custpage_selectlot', setLineNum, 'T');

												}

											}

										}

									}

								}

							// }

						}

					}

					lotReportForm.setFieldValues({custpage_sublists: itemRecordsObj.itemRecords.length, custpage_recordid: recordId, custpage_recordtype: recordType});

					lotReportForm.addSubmitButton('Submit');

				    response.writePage(lotReportForm);

				} else {

					// Create Form with Fields
				    var lotReportForm = nlapiCreateForm('Lot Report');

				    var noLocationError = 'USER ERROR: Please enter a value for Location.';

				    // Create Field Groups on the Form
				    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
				    lotReportForm.addField('custpage_nolocationerror', 'textarea', null, null, 'primary').setDisplayType('inline').setDefaultValue(noLocationError);
				    // lotReportForm.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('inline').setDefaultValue(location);

				     var lotItemDetail = lotReportForm.addSubList('custpage_lotreport', 'inlineeditor', 'Lot Report');
				    lotItemDetail.addField('custpage_sublistname', 'text', 'Sublist Name').setDisplayType('hidden');
				    lotItemDetail.addField('custpage_item', 'select', 'Item', '-10').setDisplayType('hidden');
				    lotItemDetail.addField('custpage_lot', 'text', 'Lot Number').setDisplayType('disabled');
				    lotItemDetail.addField('custpage_lotid', 'integer', 'Lot ID').setDisplayType('hidden');
				    lotItemDetail.addField('custpage_expirationdate', 'date', 'Expiration Date').setDisplayType('disabled');
				    lotItemDetail.addField('custpage_onhand', 'float', 'On Hand').setDisplayType('disabled');
					lotItemDetail.addField('custpage_available', 'float', 'Available').setDisplayType('disabled');
					lotItemDetail.addField('custpage_isonhand', 'checkbox', 'Is On Hand').setDisplayType('hidden');
					lotItemDetail.addField('custpage_quantity', 'float', 'Quantity');
					lotItemDetail.addField('custpage_selectlot', 'checkbox', 'Select Lot').setMandatory(true);

					response.writePage(lotReportForm);

				}

			} else {

				// Get Parameters
				var sublistRecs = request.getParameter('custpage_sublists');
				// var location = request.getParameter('custpage_location');
				var recordId = request.getParameter('custpage_recordid');
				var recordType = request.getParameter('custpage_recordtype');
				nlapiLogExecution('DEBUG', 'After Submit - Record Type | Record ID', recordType + ' | ' + recordId);
				var context = nlapiGetContext();
				var itemLineNum = 0;

				var tranRecord = nlapiLoadRecord(recordType, recordId, {recordmode: 'dynamic'});

				var linecount = tranRecord.getLineItemCount('item');
				var lineNum = 0;
				var itemLine = 0;
				var firstItem = 'F';
				var previousItemSelected = '';

				for (var l = 0; l < linecount; l++) {

					lineNum = l + 1;
					tranRecord.selectLineItem('item', lineNum);

					if (tranRecord.getCurrentLineItemValue('item', 'inventorydetailavail') == 'T') {

						var lineId = tranRecord.getCurrentLineItemValue('item', 'line');
						var sublistName = 'custpage_lotitemdetail';
						sublistName = sublistName + lineId;
						// nlapiLogExecution('AUDIT', 'Sublist Name Line ID', sublistName);
						var itemLineCount = request.getLineItemCount(sublistName);
						// nlapiLogExecution('AUDIT', 'Sublist Line Count', itemLineCount);

						var lotSublistName = request.getLineItemValue(sublistName, 'custpage_sublistname', 1);
						var inventoryDetailExists = tranRecord.viewCurrentLineItemSubrecord('item', 'inventorydetail');
						// nlapiLogExecution('DEBUG', 'Inventory Detail', inventoryDetailExists);

						if (sublistName ==  lotSublistName) {
							
							if (inventoryDetailExists) { var subrecord2 = tranRecord.removeCurrentLineItemSubrecord('item', 'inventorydetail'); tranRecord.commitLineItem('item'); var recid = nlapiSubmitRecord(tranRecord, true); var tranRecord = nlapiLoadRecord(recordType, recordId, {recordmode: 'dynamic'}); tranRecord.selectLineItem('item', lineNum);  } 

							var subrecord2 = tranRecord.createCurrentLineItemSubrecord('item', 'inventorydetail');
			
							// nlapiLogExecution('DEBUG', 'Subrecord', JSON.stringify(subrecord2));
							var lineSelected = 'F';
							
							for (var i = 0; i < itemLineCount; i++) {

								// nlapiLogExecution('DEBUG', 'itemLineCount', itemLineCount);

								itemLineNum = i + 1;
								
								if (request.getLineItemValue(sublistName, 'custpage_selectlot', itemLineNum) == 'T') {

									lineSelected = 'T';
									// nlapiLogExecution('DEBUG', 'sublistName', sublistName);
									// nlapiLogExecution('DEBUG', 'i', i);
									var lotToSet = request.getLineItemValue(sublistName, 'custpage_lotid', itemLineNum);
									// nlapiLogExecution('DEBUG', 'lotToSet', lotToSet);
									var quantityToSet = request.getLineItemValue(sublistName, 'custpage_quantity', itemLineNum);
									// nlapiLogExecution('DEBUG', 'quantityToSet', quantityToSet);

									subrecord2.selectNewLineItem('inventoryassignment');
									subrecord2.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotToSet);
									subrecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', quantityToSet);
									subrecord2.commitLineItem('inventoryassignment');

								}

							}

							if (lineSelected == 'T') { subrecord2.commit(); } else { subrecord2.cancel(); }

						}

					}
						
					tranRecord.commitLineItem('item');

				}

				var recid = nlapiSubmitRecord(tranRecord, true);

			}

			response.write('<script>window.close();</script>');

		} else {

			if (request.getMethod() == 'GET') {

				// Create Form with Fields
			    var lotReportForm = nlapiCreateForm('Sales Transaction Lot Report');

			    // Create Field Groups on the Form
			    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
			    lotReportForm.addField('custpage_subscription_expired', 'inlinehtml', '').setDisplayType('inline').setDefaultValue('<p><span style="color:#FF0000;size:14px;">Your account subscription for Barrel ERP has expired. Please contact your Barrel ERP account representative to activate your account.</span></p><img src="https://media.giphy.com/media/5ftsmLIqktHQA/giphy.gif"><audio controls autoplay visibility: hidden><source src="http://shopping.na2.netsuite.com/core/media/media.nl?id=404398&c=1112431&h=f38898aa52b7180a67fe&_xt=.mp3" type="audio/mp3"></audio>');

			    lotReportForm.addSubmitButton('Submit');
			    response.writePage(lotReportForm);

			} else {

				response.write('<script>window.close();</script>');

			}

		}

}