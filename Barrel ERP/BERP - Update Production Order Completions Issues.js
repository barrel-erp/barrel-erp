/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */

function updateCompletionsIssues(type) {

 //@param {String} type Context Types: scheduled, ondemand, userinterface,aborted, skipped

    nlapiLogExecution('debug','Scheduled Script TYPE:', type)
	 //only execute ondemand
  if ( type != 'ondemand' ) 
  {
     nlapiLogExecution('AUDIT','Scheduled Script is not ondemand.', type)
    return; 
  }
  
	// var toUpdate = searchCompletionsIssues();
	var toUpdate = nlapiLoadSearch('transaction', 'customsearch_h_berp_gl_impact_revalue');
	var filters = toUpdate.getFilters();
	var columns = toUpdate.getColumns();
	toUpdate = nlapiCreateSearch('transaction', filters, columns);
	toUpdate = toUpdate.runSearch();

	var count = 0;

	toUpdate.forEachResult(function(searchResult) {
		if (nlapiGetContext().getRemainingUsage() < 100) { nlapiYieldScript(); }
		count++;
		var recordType = searchResult.getValue('type');

		var upDate = nlapiDateToString(new Date(), 'datetimetz');
		// nlapiLogExecution('DEBUG', 'Time Stamp', upDate);

		if (recordType == 'WOCompl') {

			try {

				nlapiLogExecution('DEBUG', 'Record Type | Internal ID', recordType + ' | ' + searchResult.getValue('internalid'));

				var record = nlapiLoadRecord('workordercompletion', searchResult.getValue('internalid'));
				record.setFieldValue('custbody_h_berp_date_last_gl', upDate);
				var recordId = nlapiSubmitRecord(record, true);

			} catch(e) {

				nlapiLogExecution('ERROR', e.getCode(), e.getDetails() + ' | ' + ' Record ID: ' + searchResult.getValue('internalid'));
				// var unresolvedFailureRecords = searchUnresolvedFailureRecords(searchResult.getValue('internalid'));

				// if (!unresolvedFailureRecords) {

				// 	var failureRec = nlapiCreateRecord('customrecord192');
				// 	failureRec.setFieldValue('custrecord_production_order_completion', searchResult.getValue('internalid'));
				// 	failureRec.setFieldValue('custrecord_failure_description', e.getCode() + ' | ' + e.getDetails());
				// 	var failureRecId = nlapiSubmitRecord(failureRec, true);

				// } else {

				// 	var currentNumberOfAttempts = unresolvedFailureRecords[0].getValue('custrecord_number_of_attempts');
				// 	nlapiLogExecution('DEBUG', 'Current Attempts', currentNumberOfAttempts);
				// 	var newAttempts = parseInt(currentNumberOfAttempts) + 1;
				// 	nlapiLogExecution('DEBUG', 'New Attempts', newAttempts);
				// 	nlapiSubmitField('customrecord192', unresolvedFailureRecords[0].getValue('internalid'), 'custrecord_number_of_attempts', newAttempts);

				// }

			}

		} else if (recordType == 'WOIssue') {

			try {

				// nlapiLogExecution('DEBUG', 'Record Type | Internal ID', recordType + ' | ' + searchResult.getValue('internalid'));

				var record = nlapiLoadRecord('workorderissue', searchResult.getValue('internalid'));
				record.setFieldValue('custbody_h_berp_date_last_gl', upDate);
				var recordId = nlapiSubmitRecord(record, true);

			} catch(e) {


				nlapiLogExecution('ERROR', e.getCode(), e.getDetails() + ' | ' + ' Record ID: ' + searchResult.getValue('internalid'));
			}

		} else if (recordType == 'WorkOrd') {

			try {

				// nlapiLogExecution('DEBUG', 'Record Type | Internal ID', recordType + ' | ' + searchResult.getValue('internalid'));

				var record = nlapiLoadRecord('workorder', searchResult.getValue('internalid'));
				record.setFieldValue('custbody_h_berp_date_last_gl', upDate);
				var recordId = nlapiSubmitRecord(record, true);

			} catch(e) {

				nlapiLogExecution('ERROR', e.getCode(), e.getDetails() + ' | ' + ' Record ID: ' + searchResult.getValue('internalid'));

			}

		}

		return true;

	});

	nlapiLogExecution('DEBUG', 'Count', count);

}

function searchUnresolvedFailureRecords(productionOrderId) {

	var Filter = [], Column = [], Result;
	Filter.push(new nlobjSearchFilter('custrecord_production_order_completion', null, 'anyof', productionOrderId));
	Column.push(new nlobjSearchColumn('internalid'));
	Column.push(new nlobjSearchColumn('custrecord_number_of_attempts'));
	Result = nlapiSearchRecord('customrecord192', null, Filter, Column);
	if (Result) { return Result; }

}