function forecastedOrderSubmissionStatus(request, response) {

	if (request.getMethod() == 'GET') {

		// Get Parameters
		var user = request.getParameter('custparam_owner');
		
		// Set various variables
		var defaultdate = new Date(); defaultdate = nlapiDateToString(defaultdate);
		var line = 0;

		// Create the form and add fields to it 
		var form = nlapiCreateForm('Forecasted Order Submission Status');
		// form.setScript('customscript_ba_vb_processing_client');

		// Create the Field Groups
		var primary = form.addFieldGroup('primary', 'Primary Information');

		// Create Sublist
		var sublist = form.addSubList('custpage_submissionstatus', 'list', 'Forecasted Order Submission Status');
		sublist.addMarkAllButtons();
		sublist.addField('custpage_date', 'text', 'Date').setDisplayType('inline');
		sublist.addField('custpage_customer', 'select', 'Customer', '-2').setDisplayType('inline');
		sublist.addField('custpage_estimate', 'text', 'Estimate').setDisplayType('inline');
		sublist.addField('custpage_amount', 'currency', 'Amount').setDisplayType('inline');

		var estimatesProcess = searchEstimatesCreatedByScript();
		var lineNum = 0;

		if (estimatesProcess) {

			for (var i = 0; i < estimatesProcess.length; i++) {
				
				lineNum++;
				sublist.setLineItemValue('custpage_date', lineNum, estimatesProcess[i].getValue('trandate'));
				sublist.setLineItemValue('custpage_customer', lineNum, estimatesProcess[i].getValue('entity'));
				sublist.setLineItemValue('custpage_estimate', lineNum, estimatesProcess[i].getValue('internalid'));
				var estimatelink = '<a target="_blank" class="dottedlink" href="https://system.na1.netsuite.com' + nlapiResolveURL('RECORD', 'estimate', estimatesProcess[i].getValue('internalid')) + '">' + 'Estimate #' + estimatesProcess[i].getValue('tranid') + '</a>';
				sublist.setLineItemValue('custpage_estimate', lineNum, estimatelink);
				sublist.setLineItemValue('custpage_amount', lineNum, estimatesProcess[i].getValue('amount'));

			}

		}

		// Write Page
		response.writePage(form);

	}

}