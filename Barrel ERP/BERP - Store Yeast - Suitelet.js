/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        03 Dec 2015     jbrox
 *
 */

function storeYeast(request, response) {

	if (request.getMethod() == 'GET') {

		// Get Context
		var context = nlapiGetContext();
		var isOneWorld = context.getSetting('FEATURE', 'SUBSIDIARIES');

		// Get Parameters
		var recordId = request.getParameter('custparam_recordid');
		var location = request.getParameter('custparam_location');
		if (isOneWorld == 'T') { var locationRec = nlapiLoadRecord('location', location); var subsidiaryRec = locationRec.getFieldValue('subsidiary'); nlapiSubmitRecord(locationRec, true); }
		var recordType = request.getParameter('custparam_recordtype');

		// Get Pitch From Fermentation Tank Name
		var manufacturingOperationTasks = searchBatchOperationTasks(recordId);
		var allFermTanks = searchAllFermentationTanks(location);
		var fermTanks = [];

		for (var a = 0; a < allFermTanks.length; a++) { fermTanks.push(allFermTanks[a].getValue('internalid')); }

		if (manufacturingOperationTasks) {

			for (var i = 0; i < manufacturingOperationTasks.length; i++) {

				var currentManufacturingWorkCenter = manufacturingOperationTasks[i].getValue('manufacturingworkcenter', 'manufacturingOperationTask', 'GROUP');

				if (fermTanks.indexOf(currentManufacturingWorkCenter) > -1) {

					var fromTank = allFermTanks[fermTanks.indexOf(currentManufacturingWorkCenter)].getValue('custentity_h_berp_tank_name');

				}

			}

		}

		// Get Time Stamp Information
		var yeastItems = searchYeastItems(recordId);
		var batchInfo = searchBatchInfo(recordId);
		var recipe = batchInfo[0].getValue('item', null, 'GROUP');
		var recipeOG = batchInfo[0].getValue('custitem_h_berp_targetog_p', 'item', 'GROUP');
		var batchQuantity = batchInfo[0].getValue('quantity', null, 'SUM');
		// nlapiLogExecution('DEBUG', 'Yeast Items Search', JSON.stringify(yeastItems) + ' | ' + yeastItems.length);
		var yeastLotNumberPrefix = context.getSetting('SCRIPT', 'custscript_h_berp_yeast_lot_number_prefi');
		var date = new Date();
		var lotTimestamp;
		lotTimestamp = date.getMonth() + 1;
		lotTimestamp += String(date.getYear());
		lotTimestamp += String(date.getHours());
		lotTimestamp += String(date.getSeconds());
		if (yeastItems) { var yeastLotNumber = lotTimestamp + 'G'; }

	    // Create Form with Fields
	    var storeYeastForm = nlapiCreateForm('Store Yeast');
	    // storeYeastForm.setScript('customscript_h_berp_harvest_yeast_cli');

	    // Create Primary Field Groups on the Form
	    storeYeastForm.addFieldGroup('primary', 'Primary Information');
	    storeYeastForm.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('disabled');
	    if (isOneWorld == 'T') { storeYeastForm.addField('custpage_subsidiary', 'select', 'Subsidiary', '-117', 'primary').setDisplayType('disabled'); }
	    storeYeastForm.addField('custpage_recordid', 'integer', 'Record ID', 'primary').setDisplayType('hidden');

	    // Create Store From Field Group on the Form
	    storeYeastForm.addFieldGroup('storefrom', 'Store From Information');
	    storeYeastForm.addField('custpage_prodbatchfrom', 'select', 'Store From', 'customrecord_h_berp_prod_batch', 'storefrom').setDisplayType('disabled');
	    storeYeastForm.addField('custpage_prodbatchfromtank', 'text', 'Store From Tank', null, 'storefrom').setDisplayType('disabled');				

		storeYeastForm.setFieldValues({custpage_prodbatchfrom: recordId, custpage_prodbatchfromtank: fromTank, custpage_location: location, custpage_recordid: recordId });
		if (isOneWorld == 'T') { storeYeastForm.setFieldValues({custpage_subsidiary: subsidiaryRec }); }

		storeYeastForm.addSubmitButton('Submit');
	    response.writePage(storeYeastForm);

	} else {

		// Get Context & Script Paramaters
		var context = nlapiGetContext();
		var isOneWorld = context.getSetting('FEATURE', 'SUBSIDIARIES');
		var inventoryAdjustmentAccount = context.getSetting('SCRIPT', 'custscript_h_berp_harv_yeast_adj');

		// Get Parameters
		var location = request.getParameter('custpage_location');
		var subsidiary = request.getParameter('custpage_subsidiary');
		var recordId = request.getParameter('custpage_recordid');
		var pitchToBatch = request.getParameter('custpage_prodbatchto');
		var previousBrew = request.getParameter('custpage_recipe');
		
		// Create Inventory Adjustment from Harvest Yeast Sublist
		var inventoryAdjustment = nlapiCreateRecord('inventoryadjustment');

		// Set Header Fields on Inventory Adjustment Transaction
		inventoryAdjustment.setFieldValue('account', inventoryAdjustmentAccount);
		inventoryAdjustment.setFieldValue('adjlocation', location);
		if (isOneWorld == 'T') { inventoryAdjustment.setFieldValue('subsidiary', subsidiary); }
		inventoryAdjustment.setFieldValue('custbody_h_berp_prod_batch', pitchToBatch);

		var lineCount = request.getLineItemCount('custpage_harvestyeast');
		var lineNum = 0;

		for (var i = 0; i < lineCount; i++) {

			lineNum = i + 1;
			inventoryAdjustment.selectNewLineItem('inventory');
			inventoryAdjustment.setCurrentLineItemValue('inventory', 'item', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_yeast', lineNum));
			inventoryAdjustment.setCurrentLineItemValue('inventory', 'memo', 'Production Batch - Harvest Yeast');
			inventoryAdjustment.setCurrentLineItemValue('inventory', 'location', location);
			inventoryAdjustment.setCurrentLineItemValue('inventory', 'adjustqtyby', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity_add', lineNum));
			inventoryAdjustment.setCurrentLineItemValue('inventory', 'unitcost', '0.00');

			var generationNumber = nlapiLookupField('customrecord_h_berp_yeast_generation', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_generation', lineNum), 'name');
			var inventoryDetailSub = inventoryAdjustment.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
			inventoryDetailSub.selectNewLineItem('inventoryassignment');
			inventoryDetailSub.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_lotnumber', lineNum) + generationNumber);
			inventoryDetailSub.setCurrentLineItemValue('inventoryassignment', 'quantity', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity_add', lineNum));
			inventoryDetailSub.commitLineItem('inventoryassignment');
			inventoryDetailSub.commit();
			inventoryAdjustment.commitLineItem('inventory');

		}

		var inventoryAdjustmentId = nlapiSubmitRecord(inventoryAdjustment, true);

		// Search Production Orders Tied to the Pitch To Production Batch
		var prodOrdersToUpdate = searchBatchProductionOrders(pitchToBatch);

		// Search Inventory Numbers to Update from the Created Inventory Adjustment
		var inventoryNumbersToUpdate = searchInventoryAdjustment(inventoryAdjustmentId);

		var invLineNum = 0;

		// Loop through each Inventory Number and Update the Fields
		for (var n = 0; n < inventoryNumbersToUpdate.length; n++) {

			invLineNum = n + 1;

			var inventoryNumberRec = nlapiLoadRecord('inventorynumber', inventoryNumbersToUpdate[n].getValue('internalid', 'itemNumber'));

			// If Pre-Harvest Exists, Set the Post-Harvest Fields for Live, Dead, and Total Cell Counts
			if (!inventoryNumberRec.getFieldValue('custitemnumber_h_berp_live_cells')) { inventoryNumberRec.setFieldValue('custitemnumber_h_berp_live_cells', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_live', invLineNum)); } else { inventoryNumberRec.setFieldValue('custitemnumber_h_berp_po_harvest_live_cells', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_live', invLineNum)); }
			if (!inventoryNumberRec.getFieldValue('custitemnumber_h_berp_dead_cells')) { inventoryNumberRec.setFieldValue('custitemnumber_h_berp_dead_cells', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_total', invLineNum) - request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_live', invLineNum)); } else { inventoryNumberRec.setFieldValue('custitemnumber_h_berp_po_harvest_dead_cells', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_total', invLineNum) - request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_live', invLineNum)); }
			if (!inventoryNumberRec.getFieldValue('custitemnumber_h_berp_total_cells')) { inventoryNumberRec.setFieldValue('custitemnumber_h_berp_total_cells', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_total', invLineNum)); } else { inventoryNumberRec.setFieldValue('custitemnumber_h_berp_po_harvest_total_cells', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_total', invLineNum)); }

			// Set Additional Field Values for the Inventory Number
			inventoryNumberRec.setFieldValue('custitemnumber_h_berp_prod_order_line', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_linenumber', invLineNum));
			inventoryNumberRec.setFieldValue('custitemnumber_h_berp_quantity', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity', invLineNum));
			inventoryNumberRec.setFieldValue('custitemnumber_h_berp_yeast_generation', request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_generation', invLineNum));
			inventoryNumberRec.setFieldValue('custitemnumber_h_berp_previous_batch', pitchToBatch);
			inventoryNumberRec.setFieldValue('custitemnumber_h_berp_previous_brew', previousBrew);
			var inventoryNumberRecId = nlapiSubmitRecord(inventoryNumberRec, true);

			// Loop through each Production Order tied to the Production Batch and update each Yeast Item line
			for (var b = 0; b < prodOrdersToUpdate.length; b++) {

				var productionOrder = nlapiLoadRecord('workorder', prodOrdersToUpdate[b].getValue('internalid'));
				var prodLineCount = productionOrder.getLineItemCount('item');
				var preHarvestComplete = nlapiLookupField('customrecord_h_berp_prod_batch', pitchToBatch, 'custrecord_h_berp_pre_harvest_complete');

				var prodLineNum = 0;

				for (var p = 0; p < prodLineCount; p++) {

					prodLineNum = p + 1;

					// Set Production Order Yeast Line Inventory Detail
					var yeastLineQuantity = parseFloat(request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_quantity_add', invLineNum)) / prodOrdersToUpdate.length;
					productionOrder.selectLineItem('item', prodLineNum);
					
					if (productionOrder.getCurrentLineItemValue('item', 'line') == request.getLineItemValue('custpage_harvestyeast', 'custpage_sl_linenumber', invLineNum)) {

						var currentLineQuantity = productionOrder.getCurrentLineItemValue('item', 'quantity');
						nlapiLogExecution('AUDIT', 'Prod = ' + pitchToBatch + ' Current Line Quantity = ' + currentLineQuantity + ' Request Line Quantity = ' + yeastLineQuantity);
						var inventoryDetailExists = productionOrder.viewCurrentLineItemSubrecord('item', 'inventorydetail');
						
						if (!inventoryDetailExists) {

							productionOrder.setCurrentLineItemValue('item', 'quantity', yeastLineQuantity);
							nlapiLogExecution('DEBUG', 'Inventory Detail Doesnt Exist - Setting Line Item Quantity To', yeastLineQuantity);
							var productionOrdInvDetailSub = productionOrder.createCurrentLineItemSubrecord('item', 'inventorydetail');

						} else if (inventoryDetailExists && preHarvestComplete == 'T') {
						
							productionOrder.setCurrentLineItemValue('item', 'quantity', parseFloat(currentLineQuantity) + yeastLineQuantity);
							nlapiLogExecution('DEBUG', 'Inventory Detail Exists - Setting Line Item Quantity To', parseFloat(currentLineQuantity) + yeastLineQuantity);
							var productionOrdInvDetailSub = productionOrder.editCurrentLineItemSubrecord('item', 'inventorydetail');

						} else if (inventoryDetailExists && preHarvestComplete == 'F') {
						
							productionOrder.setCurrentLineItemValue('item', 'quantity', parseFloat(currentLineQuantity) + yeastLineQuantity);
							nlapiLogExecution('DEBUG', 'Inventory Detail Exists No Pre-Harvest - Setting Line Item Quantity To', yeastLineQuantity);
							nlapiLogExecution('AUDIT', 'Else Inventory Exists and Pre Harvest is False', parseFloat(currentLineQuantity) + yeastLineQuantity);
							var productionOrdInvDetailSub = productionOrder.editCurrentLineItemSubrecord('item', 'inventorydetail');

						}

						nlapiLogExecution('DEBUG', 'Prod Inventory Detail', productionOrdInvDetailSub);
						productionOrdInvDetailSub.selectNewLineItem('inventoryassignment');
						productionOrdInvDetailSub.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', inventoryNumberRecId);
						nlapiLogExecution('DEBUG', 'Setting Additional Line Quantity: ', yeastLineQuantity);
						productionOrdInvDetailSub.setCurrentLineItemValue('inventoryassignment', 'quantity', yeastLineQuantity);
						productionOrdInvDetailSub.commitLineItem('inventoryassignment');
						productionOrdInvDetailSub.commit();

					}
				
					productionOrder.commitLineItem('item');

				}

				nlapiSubmitField('customrecord_h_berp_prod_batch', pitchToBatch, 'custrecord_h_berp_pre_harvest_complete', 'T');
				var prodOrderId = nlapiSubmitRecord(productionOrder, true);

			}

		}

    	response.write('<script>window.close();</script>');

	}

}