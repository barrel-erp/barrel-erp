function forecastedOrderSubmission(request, response) {

	if (request.getMethod() == 'GET') {

		// Get Parameters
		var customer = request.getParameter('custparam_customer');
		
		// Set various variables
		var defaultdate = new Date(); defaultdate = nlapiDateToString(defaultdate);
		var line = 0;

		// Create the form and add fields to it 
		var form = nlapiCreateForm('Submit Forecasted Orders');
		form.setScript('customscript_h_berp_forecast_ord_client');

		// Create the Field Groups
		var primary = form.addFieldGroup('custpage_primary', 'Primary Information');
		form.addField('custpage_orderdate', 'date', 'Order Date', null, 'custpage_primary').setDefaultValue(defaultdate);
		form.addField('custpage_customer', 'select', 'Customer', '-2', 'custpage_primary').setDefaultValue(customer);

		// Create Sublist
		var sublist = form.addSubList('custpage_targetinvsubmission', 'list', 'Item Forecast');
		sublist.addMarkAllButtons();
		sublist.addField('custpage_process', 'checkbox', 'Process');
		sublist.addField('custpage_customer', 'select', 'Customer', '-2').setDisplayType('inline');
		sublist.addField('custpage_item', 'select', 'Item', '-10').setDisplayType('inline');
		sublist.addField('custpage_prefinventory', 'integer', 'Preferred Inventory Level').setDisplayType('inline');
		sublist.addField('custpage_customeronhand', 'integer', 'Customer On-Hand Inventory').setDisplayType('inline');
		sublist.addField('custpage_minquantity', 'integer', 'Min Quantity').setDisplayType('inline');
		// sublist.addField('custpage_unitsperpallet', 'integer', 'Units Per Pallet').setDisplayType('inline');
		sublist.addField('custpage_onorderquantity', 'integer', 'On Order').setDisplayType('inline');
		sublist.addField('custpage_orderquantity', 'integer', 'Order Quantity').setDisplayType('inline');
		sublist.addField('custpage_orderprice', 'currency', 'Order Price').setDisplayType('inline');

		// Load Sublist
		var forecastedItems = searchInventoryTargets(customer);
		var line = 0;

		if (forecastedItems) {

			for (var i = 0; i < forecastedItems.length; i++) {

				// Set Variables
				var customer = forecastedItems[i].getValue('custrecordh_berp_it_customer');
				var item = forecastedItems[i].getValue('custrecord_h_berp_it_item');


				// Search Customer On Hand Inventory
				var customerOnHand = searchCustomerInventory(customer, item);
				nlapiLogExecution('DEBUG', 'Customer On Hand JSON', JSON.stringify(customerOnHand));

				var customerOnOrder = searchCustomerOnOrderInventory(customer, item);
				nlapiLogExecution('DEBUG', 'Customer On Order JSON', JSON.stringify(customerOnOrder));

				if (customerOnHand) {

					var customerOnHandQty = Math.floor(customerOnHand[0].getValue('custrecord_h_berp_ci_quantity'));

				} else {

					var customerOnHandQty = 0;

				}

				if (customerOnHand) {

					var customerOnOrderQty = Math.floor(customerOnOrder[0].getValue('formulanumeric', null, 'SUM'));

				} else {

					var customerOnOrderQty = 0;

				}
					var preferredInventoryLevel = forecastedItems[i].getValue('custrecord_h_berp_it_preferredlevel');
					var minimumQty = forecastedItems[i].getValue('custitem_h_berp_forecast_order_min', 'custrecord_h_berp_it_item');
					var unitsPerPallet = forecastedItems[i].getValue('custitem_h_berp_cases_per_pallet', 'custrecord_h_berp_it_item');
					var orderQty = parseInt(preferredInventoryLevel) - parseInt(customerOnHandQty) - parseInt(customerOnOrderQty);
					nlapiLogExecution('DEBUG', 'Order Quantity', orderQty);
					if (orderQty > 0 && orderQty < minimumQty) {

						orderQty = parseInt(minimumQty);

					}

					if (orderQty > 0) {

						line++;

						nlapiLogExecution('DEBUG', 'Line', line);
						// Set Sublist Values
						sublist.setLineItemValue('custpage_process', line, 'T');
						sublist.setLineItemValue('custpage_customer', line, customer);
						sublist.setLineItemValue('custpage_item', line, forecastedItems[i].getValue('custrecord_h_berp_it_item'));
						sublist.setLineItemValue('custpage_prefinventory', line, preferredInventoryLevel);
						sublist.setLineItemValue('custpage_customeronhand', line, customerOnHandQty.toFixed(0));
						sublist.setLineItemValue('custpage_minquantity', line, minimumQty);
						// sublist.setLineItemValue('custpage_unitsperpallet', line, unitsPerPallet);
						sublist.setLineItemValue('custpage_onorderquantity', line, customerOnOrderQty.toFixed(0));
						sublist.setLineItemValue('custpage_orderquantity', line, orderQty.toFixed(0));
						sublist.setLineItemValue('custpage_orderprice', line, forecastedItems[i].getValue('custrecord_h_berp_it_salesprice'));

					}

			}

		}

		// Write Page
		form.addSubmitButton('Submit');
		response.writePage(form);

	} else {

		// Get Context
		var context = nlapiGetContext();

		var trandate = request.getParameter('custpage_orderdate');
		nlapiLogExecution('DEBUG', 'Tran Date', trandate);

		// Deploy Scheduled Script to Create Transactions
		// nlapiScheduleScript('customscript_hein_rys_submission_sch', 'customdeploy_hein_rys_submission_sch', params);
		// nlapiScheduleScript('customscript_hein_vbill_process_schedule', 'customdeploy_hein_vbill_process_schedule', params);
		
		var lineCount = request.getLineItemCount('custpage_targetinvsubmission');
		nlapiLogExecution('DEBUG', 'Line Count', lineCount);
		var lineNum = 0;
		var customerArr = [];
		var itemArr = [];

		for (var l = 0; l < lineCount; l++) {

			lineNum++;

			if (request.getLineItemValue('custpage_targetinvsubmission', 'custpage_process', lineNum) == 'T') {

				var currentCustomer = request.getLineItemValue('custpage_targetinvsubmission', 'custpage_customer', lineNum);
				nlapiLogExecution('DEBUG', 'Current Customer', currentCustomer);
				if (customerArr.indexOf(currentCustomer) == -1) {
					
					customerArr.push(currentCustomer);

				}
					
				itemArr.push(request.getLineItemValue('custpage_targetinvsubmission', 'custpage_item', lineNum));

			}

		}

		for (var e = 0; e < customerArr.length; e++) {

			var estimateRecord = nlapiCreateRecord('estimate');
			nlapiLogExecution('DEBUG', 'Setting Entity', customerArr[e]);
			estimateRecord.setFieldValue('entity', customerArr[e]);
			nlapiLogExecution('DEBUG', 'Setting Tran Date', trandate);
			estimateRecord.setFieldValue('trandate', trandate);

			nlapiLogExecution('DEBUG', 'Search Items to Add', customerArr[e] + ' | ' + itemArr);
			var itemsToAdd = searchItemsToAdd(customerArr[e], itemArr);

			for (var a = 0; a < itemsToAdd.length; a++) {

				var customerOnHand = searchCustomerInventory(customerArr[e], itemsToAdd[a].getValue('custrecord_h_berp_it_item'));
				nlapiLogExecution('DEBUG', 'Customer On Hand JSON', JSON.stringify(customerOnHand));

				if (customerOnHand) {

					var customerOnHandQty = Math.floor(customerOnHand[0].getValue('custrecord_h_berp_ci_quantity'));

				} else {

					var customerOnHandQty = 0;

				}

				var preferredInventoryLevel = itemsToAdd[a].getValue('custrecord_h_berp_it_preferredlevel');
				var minimumQty = itemsToAdd[a].getValue('minimumquantity', 'custrecord_h_berp_it_item');
				var unitsPerPallet = itemsToAdd[a].getValue('custitem_h_berp_cases_per_pallet', 'custrecord_h_berp_it_item');
				var orderQty = parseInt(preferredInventoryLevel) - parseInt(customerOnHandQty);
				if (orderQty < minimumQty) {

					orderQty = parseInt(minimumQty);

				}
			
				estimateRecord.selectNewLineItem('item');
				estimateRecord.setCurrentLineItemValue('item', 'item', itemsToAdd[a].getValue('custrecord_h_berp_it_item'));
				estimateRecord.setCurrentLineItemValue('item', 'quantity', orderQty);
				// estimateRecord.setCurrentLineItemValue('item', 'itemrate', itemsToAdd[a].getValue('custrecord_h_berp_it_salesprice'));
				estimateRecord.commitLineItem('item');

			}

			var estimateId = nlapiSubmitRecord(estimateRecord, true);

		}

		response.sendRedirect('SUITELET', 'customscript_h_berp_forecasted_ord_statu', 'customdeploy_h_berp_forecasted_ord_statu');

	}

}