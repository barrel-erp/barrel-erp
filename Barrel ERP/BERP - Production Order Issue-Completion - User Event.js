/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        03 Dec 2015     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

	nlapiLogExecution('DEBUG', 'Execution Context', nlapiGetContext().getExecutionContext());
	if (nlapiGetContext().getExecutionContext() != 'scheduled') {

		if (nlapiGetFieldValue('item')) {

			var itemType = nlapiLookupField('item', nlapiGetFieldValue('item'), 'custitem_h_berp_itemtype');
			var lineCount = nlapiGetLineItemCount('component');
			nlapiLogExecution('DEBUG', 'Line Count = ', lineCount);

			if (itemType == 5) {

				nlapiLogExecution('DEBUG', 'Item is Packaged', itemType);
				var lineNum = 0;

				for (var i = 0; i < lineCount; i++) {

					lineNum++;
					var inventoryDetailSub = nlapiViewLineItemSubrecord('component', 'componentinventorydetail', lineNum);

					if (inventoryDetailSub) {

						nlapiLogExecution('DEBUG', 'Inventory Detail | Item | Line Quantity', inventoryDetailSub + ' | ' + nlapiGetLineItemValue('component', 'item', lineNum) + ' | ' + nlapiGetLineItemValue('component', 'quantity', lineNum));
						itemType = nlapiLookupField('item', nlapiGetLineItemValue('component', 'item', lineNum), 'custitem_h_berp_itemtype');

						if (itemType == 4) {

							nlapiSetFieldValue('custbody_h_berp_bright_beer_consumed', nlapiGetLineItemValue('component', 'quantity', lineNum));

						}

					}

				}

			} else if (itemType == 4 && type == 'create') {

				nlapiSetFieldValue('custbody_h_berp_exclude_from_bro', nlapiLookupField('manufacturingrouting', nlapiGetFieldValue('manufacturingrouting'), 'custrecord_h_berp_exclude_from_bro'));

			}

		}

	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	// nlapiLogExecution('DEBUG', 'Execution Context', nlapiGetContext().getExecutionContext());
	if (nlapiGetContext().getExecutionContext() != 'scheduled') {

		if (type != 'delete') {
          	var context = nlapiGetContext();
			var createTasks = context.getSetting('SCRIPT', 'custscript_tasks_routing');

			var productionOrderId = nlapiGetFieldValue('createdfrom');
			nlapiLogExecution('DEBUG', 'Production Order', productionOrderId);
			var recipe = nlapiGetFieldValue('item');
			nlapiLogExecution('DEBUG', 'Production Order Recipe', recipe);
			var productionOrder = nlapiLoadRecord('workorder', productionOrderId);
			var orderStatus = productionOrder.getFieldValue('orderstatus');
			var productionTasksCreated = productionOrder.getFieldValue('custbody_h_berp_prod_tasks_created');
			var location = productionOrder.getFieldValue('location');

			if (orderStatus == 'D' && productionTasksCreated == 'F') {
			
				var manufacturingOperationTasks = searchmanufacturingOperationTasks(productionOrderId);
				nlapiLogExecution('DEBUG', 'Manufacturing Operation Tasks', JSON.stringify(manufacturingOperationTasks));

				for (var m = 0; m < manufacturingOperationTasks.length; m++) {

					var equipmentType = nlapiLookupField('entitygroup', manufacturingOperationTasks[m].getValue('manufacturingworkcenter'), 'custentity_h_berp_equipment_type');
					var startDate = manufacturingOperationTasks[m].getValue('startdate');
					startDate = nlapiStringToDate(startDate);
					nlapiLogExecution('DEBUG', 'Start Date', startDate);
					var dueDate = manufacturingOperationTasks[m].getValue('startdate');
					dueDate = nlapiStringToDate(dueDate);

					var productionTaskTemplates = searchProductionTaskTemplates(equipmentType, location, recipe);

					if (productionTaskTemplates) {

						for (var p = 0; p < productionTaskTemplates.length; p++) {

							var productionTaskExists = searchCurrentProductionTasks(productionTaskTemplates[p].getValue('internalid'), manufacturingOperationTasks[m].getValue('manufacturingworkcenter'), productionOrderId);

							var startDate = manufacturingOperationTasks[m].getValue('startdate');
							startDate = nlapiStringToDate(startDate);
							startDate.setDate(startDate.getDate() + parseInt(productionTaskTemplates[p].getValue('custrecord_h_berp_due_date')));

							var dueDate = manufacturingOperationTasks[m].getValue('startdate');
							dueDate = nlapiStringToDate(dueDate);
							dueDate.setDate(startDate.getDate() + parseInt(productionTaskTemplates[p].getValue('custrecord_h_berp_days_to_complete')));
							dueDate = nlapiDateToString(dueDate);
							startDate = nlapiDateToString(startDate);
                          
                            

							if (!productionTaskExists && createTasks == 'F') {
							
								var productionTaskRecord = nlapiCreateRecord('task');
								productionTaskRecord.setFieldValue('title', productionTaskTemplates[p].getValue('name'));
								productionTaskRecord.setFieldValue('assigned', productionTaskTemplates[p].getValue('custrecord_h_berp_assigned_to'));
								productionTaskRecord.setFieldValue('message', productionTaskTemplates[p].getValue('custrecord_h_berp_message'));
								productionTaskRecord.setFieldValue('startdate', startDate);
								productionTaskRecord.setFieldValue('duedate', dueDate);
								productionTaskRecord.setFieldValue('custevent_h_berp_operation_sequence', manufacturingOperationTasks[m].getValue('sequence'));
								productionTaskRecord.setFieldValue('custevent_h_berp_operation_name', manufacturingOperationTasks[m].getValue('name'));
								productionTaskRecord.setFieldValue('custevent_h_berp_equipment_type', productionTaskTemplates[p].getValue('custrecord_h_berp_equipment_type'));
								productionTaskRecord.setFieldValue('custevent_h_berp_manufact_work_center', manufacturingOperationTasks[m].getValue('manufacturingworkcenter'));
								productionTaskRecord.setFieldValue('custevent_h_berp_prod_task_template', productionTaskTemplates[p].getValue('internalid'));
								productionTaskRecord.setFieldValue('custevent_h_berp_prod_order', productionOrderId);
								var id = nlapiSubmitRecord(productionTaskRecord, true);

							} else if (productionTaskExists) {

								for (var t = 0; t < productionTaskExists.length; t++) {

									var productionTaskRecord = nlapiLoadRecord('task', productionTaskExists[t].getValue('internalid'));
									productionTaskRecord.setFieldValue('duedate', dueDate);
									productionTaskRecord.setFieldValue('startdate', startDate);
									var taskId = nlapiSubmitRecord(productionTaskRecord, true);

								}

							}

						}

					}

				}

				productionOrder.setFieldValue('custbody_h_berp_prod_tasks_created', 'T');
				var productionRecord = nlapiSubmitRecord(productionOrder, true);

			}

		}

	}

}