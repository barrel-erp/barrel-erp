/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Dec 2015     jbrox
 * 1.00 ^ 2   19 Jun 2017     amillen          Moved scheduled script keg deposit functionality to before record submit
 * 1.00       03 Jan 2019     sstreule         Added check for roleCenter so that the LOT Report button does not appear for the Customer Center
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	var ordStatus = nlapiGetFieldValue('orderstatus');
	var location = nlapiGetFieldValue('location');
	var recordType = nlapiGetRecordType();
	var recid = nlapiGetRecordId();
	var context = nlapiGetContext();
	var roleCenter = context.getRoleCenter();
	nlapiLogExecution('DEBUG', 'roleCenter = ', roleCenter);

	if (recordType != 'opportunity') {

		// Add Button for Lot Report
		var lotReportSuiteletURL = nlapiResolveURL('SUITELET', 'customscript_h_berp_lot_report_suitelet','customdeploy_h_berp_lot_report_suitelet');
		lotReportSuiteletURL += '&custparam_custom_id=' + recid;
		lotReportSuiteletURL += '&custparam_orderstatus=' + ordStatus;
		lotReportSuiteletURL += '&custparam_recordtype=' + recordType;
		lotReportSuiteletURL += '&custparam_location=' + location;
		lotReportSuiteletURL += '&unlayered=T';
		form.setScript('customscript_h_berp_button_click');
		if ((type == 'view') && (roleCenter != 'CUSTOMER')) { form.addButton('custpage_saleslotreport', 'Lot Report', 'displayLotReports(\'' + lotReportSuiteletURL + '\');'); }

	}

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

	// Get Context & Script Parameters
	var context = nlapiGetContext();
	var setExpShipDate = context.getSetting('SCRIPT', 'custscript_h_berp_set_expected_ship_date');

	var recordType = nlapiGetRecordType();
	var recordId = nlapiGetRecordId();
	var shipDate = nlapiGetFieldValue('shipdate');
	var lineCount = nlapiGetLineItemCount('item');
	var lineNum = 0;
	var palletSum = 0;
	var bblSum = 0;
	var weightSum = 0;
	var invoiceCreated = '';

	if(type != 'delete'){
		for (var i = 0; i < lineCount; i++) {

			lineNum = i + 1;
			if (nlapiGetLineItemValue('item', 'custcol_h_berp_cases_per_pallet', lineNum)) {
				var palletLine = nlapiGetLineItemValue('item', 'quantity', lineNum) / nlapiGetLineItemValue('item', 'custcol_h_berp_cases_per_pallet', lineNum);
				palletLine = palletLine.toFixed(1);
				palletSum = parseFloat(palletSum) + parseFloat(palletLine);
			}		

			var bblLine = nlapiGetLineItemValue('item', 'quantity', lineNum) * nlapiGetLineItemValue('item', 'custcol_h_berp_bbl_per_unit', lineNum);
			bblLine = bblLine.toFixed(1);
			bblSum = parseFloat(bblSum) + parseFloat(bblLine);

			if (nlapiGetLineItemValue('item', 'custcol_h_berp_weight', lineNum)) {
				var weightLine = nlapiGetLineItemValue('item', 'quantity', lineNum) * nlapiGetLineItemValue('item', 'custcol_h_berp_weight', lineNum);
				weightLine = weightLine.toFixed(1);
				weightSum = parseFloat(weightSum) + parseFloat(weightLine);
			}

			if (recordType == 'salesorder' && setExpShipDate == 'T' && type == 'create' && shipDate) {

				nlapiSelectLineItem('item', lineNum);
				var itemType = nlapiGetCurrentLineItemValue('item', 'itemtype');
				var itemSubType = nlapiGetCurrentLineItemValue('item', 'itemsubtype');

				if (itemType == 'InvtPart' || itemType == 'Assembly') {
				
					nlapiSetCurrentLineItemValue('item', 'expectedshipdate', shipDate);
				}
				nlapiCommitLineItem('item');
			}
			
		}
        // Get customer keg exempt status
        var customerid = nlapiGetFieldValue('entity');
        nlapiLogExecution('DEBUG', 'entity is ', customerid);

        var custKegExempt = nlapiLookupField('customer', customerid,'custentity_exempt_keg_deposit');
        nlapiLogExecution('DEBUG', 'custKegExempt is ', custKegExempt);
		//dictionary to hold keg deposit objects
		var kegDepQty = {};
		//array to hold line numbers of keg deposit items
		var depLine = [];

		var itemCount = nlapiGetLineItemCount('item');
		//check if invoice is created to not break the native netsuite link between items 'invoiced' on sales order and invoice.
		if(type != 'create'){
			invoiceCreated = searchInvoices(recordId);
		}
		for(var j = 1; j <= itemCount; j ++){

			//begin new keg deposit logic to add deposit BRS so Invoice reflects accurate pricing in real time
			var addKegDepositLines = context.getSetting('SCRIPT', 'custscript_h_berp_add_keg_deposit_lines');
          	nlapiLogExecution('DEBUG', 'addKegDepositLines is ', addKegDepositLines);

			if ((addKegDepositLines == 'T') && (custKegExempt == 'F')) {
				var item = nlapiGetLineItemValue('item', 'item', j);
				//look up packageType and see if packageType stores a keg deposit item on its package type record
				try{
					var packageType = nlapiLookupField('item', item, 'custitem_h_berp_packagetype');
					var depositItem = nlapiLookupField('customrecord_h_berp_package', packageType, 'custrecord_h_berp_pt_deposititem');
				}
				//if no keg deposit on packagetype than store values as empty strings
				catch(e){
					nlapiLogExecution('DEBUG', 'error', e.getDetails());
					var packageType = '';
					var depositItem = '';
				}
				//if there is a keg deposit on package type and it has not been defined in object dictionary yet then define it in key/value pair - key is deposit item/value is line item quantity
				if(isNotNull(depositItem) && kegDepQty[depositItem] == undefined){
					kegDepQty[depositItem] = nlapiGetLineItemValue('item', 'quantity', j);
					//store line number as deposit item for later look up
					kegDepQty[j] = depositItem;
					//push line number into array for later use
					depLine.push(j);
				}
				//if there is a package type with a keg deposit item and it has already been defined in dictionary
				//then add item quantity to the current keg deposit quantity for later setting keg deposit
				else if(isNotNull(depositItem) && kegDepQty[depositItem] != undefined){
					kegDepQty[depositItem] = ( Number(kegDepQty[depositItem]) + Number(nlapiGetLineItemValue('item', 'quantity', j)) );
				}
				//if the record type is a sales order/estimate and the order has not been invoiced
				//if there is a keg deposit added to the order but no items have package type keg deposits
				//then remove the keg deposit from sales order.
				if(recordType != 'invoice' && isNull(invoiceCreated)){
					var removedKegDeposit = searchAllDepositItems(item);
					if(kegDepQty[item] != undefined || isNotNull(removedKegDeposit)){
						nlapiRemoveLineItem('item', j);
						j = j-1;
					}
				}
			}
		}

		
		var lineTotal = nlapiGetLineItemCount('item');
		//look through array that is used for storing a number to look up keg deposit items
		for(var a = 0; a < depLine.length; a++){
			//set each line num to a variable
			var itemLineNum = depLine[a];
			//look up each keg deposit w/ line number from array
			var kegDepItem = kegDepQty[itemLineNum];

			//if an invoice has not been created for a sales order or if the record is an invoiec
			if(isNotNull(invoiceCreated) || recordType == 'invoice'){
				for(var b = 1; b <= lineTotal; b++){
					var item = nlapiGetLineItemValue('item', 'item', b);
					//if the item is a keg deposit item then compare quantities 
					if(kegDepQty[item] == kegDepQty[kegDepItem]){
						var depositQuantity = nlapiGetLineItemValue('item', 'quantity', b);
						//if old quantity does not equal new quantity of keg deposit 
						//update quantity to new quantity
						if(depositQuantity != kegDepQty[kegDepItem]){
							nlapiSetLineItemValue('item', 'quantity', b, kegDepQty[kegDepItem]);
						}
					}
				}
			}

			//if we removed all keg deposits from a sales order that has not been invoiced
			//here we reset them with the most updated quantity of keg deposits
			if(recordType != 'invoice' && isNull(invoiceCreated)){		
				var counter = 1;
				nlapiInsertLineItem('item', lineTotal+counter);
				nlapiSetLineItemValue('item', 'item', lineTotal+counter, kegDepItem);
				nlapiSetLineItemValue('item', 'quantity', lineTotal+counter, kegDepQty[kegDepItem]);
				counter = counter + 1;
			}
		}
		//if an invoice was created and incorrectly pulled on keg deposits
		//check if any keg deposits were pushed into the array depLine, and if none were
		//remove all keg deposits from invoice
		if((depLine.length == 0 || depLine.length == 1) && recordType == 'invoice'){
			for(var p = 1; p <= lineTotal; p++){
				var item = nlapiGetLineItemValue('item', 'item', p);
				var isKegDeposit = searchAllDepositItems(item);
				if(isNotNull(isKegDeposit) && kegDepQty[item] == undefined){
					nlapiRemoveLineItem('item', p);
					p = p-1;
				}
			}
		}
		
		nlapiSetFieldValue('custbody_h_berp_pallets', palletSum.toFixed(1));
		nlapiLogExecution('DEBUG', 'Pallet Sum', palletSum.toFixed(1));

		nlapiSetFieldValue('custbody_h_berp_bbl_equiv', bblSum.toFixed(1));
		nlapiLogExecution('DEBUG', 'BBL Sum', bblSum.toFixed(1));

		nlapiSetFieldValue('custbody_h_berp_total_weight', weightSum.toFixed(1));
		nlapiLogExecution('DEBUG', 'Weight Sum', weightSum.toFixed(1));
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	// Get Context & Script Parameters
	var context = nlapiGetContext();
	
}