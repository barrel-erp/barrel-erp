function fieldChange(type, name) {

	if (name == 'custpage_location') {

		var date = nlapiGetFieldValue('custpage_date');
		var adjAccount = nlapiGetFieldValue('custpage_adjaccount');
		var subsidiary = nlapiGetFieldValue('custpage_subsidiary');
		var locationId = nlapiGetFieldValue('custpage_location');
		var department = nlapiGetFieldValue('custpage_department');
		var classRec = nlapiGetFieldValue('custpage_class');
		var license = nlapiGetFieldValue('custpage_license');
		var itemType = nlapiGetFieldValue('custpage_itemtype');
		var isLot = nlapiGetFieldValue('custpage_islot');
		var url = nlapiResolveURL('SUITELET', 'customscript_h_berp_trim_inv_deci', 'customdeploy_h_berp_trim_inv_deci') + '&custparam_location=' + locationId + '&custparam_itemtype=' + itemType + '&custparam_date=' + date + '&custparam_adjaccount=' + adjAccount + '&custparam_subsidiary=' + subsidiary + '&custparam_department=' + department + '&custparam_class=' + classRec + '&custparam_license=' + license + '&custparam_islot=' + isLot;
		window.onbeforeunload = null;
		document.location = url;

	}

	if (name == 'custpage_itemtype') {

		var date = nlapiGetFieldValue('custpage_date');
		var adjAccount = nlapiGetFieldValue('custpage_adjaccount');
		var subsidiary = nlapiGetFieldValue('custpage_subsidiary');
		var locationId = nlapiGetFieldValue('custpage_location');
		var department = nlapiGetFieldValue('custpage_department');
		var classRec = nlapiGetFieldValue('custpage_class');
		var license = nlapiGetFieldValue('custpage_license');
		var itemType = nlapiGetFieldValue('custpage_itemtype');
		var isLot = nlapiGetFieldValue('custpage_islot');
		var url = nlapiResolveURL('SUITELET', 'customscript_h_berp_trim_inv_deci', 'customdeploy_h_berp_trim_inv_deci') + '&custparam_location=' + locationId + '&custparam_itemtype=' + itemType + '&custparam_date=' + date + '&custparam_adjaccount=' + adjAccount + '&custparam_subsidiary=' + subsidiary + '&custparam_department=' + department + '&custparam_class=' + classRec + '&custparam_license=' + license + '&custparam_islot=' + isLot;
		window.onbeforeunload = null;
		document.location = url;

	}

	if (name == 'custpage_islot') {

		var date = nlapiGetFieldValue('custpage_date');
		var adjAccount = nlapiGetFieldValue('custpage_adjaccount');
		var subsidiary = nlapiGetFieldValue('custpage_subsidiary');
		var locationId = nlapiGetFieldValue('custpage_location');
		var department = nlapiGetFieldValue('custpage_department');
		var classRec = nlapiGetFieldValue('custpage_class');
		var license = nlapiGetFieldValue('custpage_license');
		var itemType = nlapiGetFieldValue('custpage_itemtype');
		var isLot = nlapiGetFieldValue('custpage_islot');
		var url = nlapiResolveURL('SUITELET', 'customscript_h_berp_trim_inv_deci', 'customdeploy_h_berp_trim_inv_deci') + '&custparam_location=' + locationId + '&custparam_itemtype=' + itemType + '&custparam_date=' + date + '&custparam_adjaccount=' + adjAccount + '&custparam_subsidiary=' + subsidiary + '&custparam_department=' + department + '&custparam_class=' + classRec + '&custparam_license=' + license + '&custparam_islot=' + isLot;
		window.onbeforeunload = null;
		document.location = url;

	}

}