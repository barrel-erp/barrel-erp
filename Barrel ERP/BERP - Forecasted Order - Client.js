function fieldChange(type, name) {

	if (name == 'custpage_customer') {

		var customer = nlapiGetFieldValue('custpage_customer');
		var url = nlapiResolveURL('SUITELET', 'customscript_h_berp_forecast_order_submi', 'customdeploy_h_berp_forecast_order_submi') + '&custparam_customer=' + customer;
		window.onbeforeunload = null;
		document.location = url;

	}

}

function redirectURL() {

	var url = nlapiResolveURL('SUITELET', 'customscript_h_berp_forecast_order_submi', 'customdeploy_h_berp_forecast_order_submi');
	window.onbeforeunload = null;
	document.location = url;

}