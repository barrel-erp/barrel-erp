function pageInit(type, name) {

	var reportType = nlapiGetFieldText('monthlyorquarterly');
	
	if (reportType == 'Monthly') {
	
		nlapiGetField('quarterstartdate').setDisplayType('hidden');
		nlapiGetField('quarterenddate').setDisplayType('hidden');

	} else if (reportType == 'Quarterly') {

		nlapiGetField('reportingperiod').setDisplayType('hidden');
		nlapiGetField('reportingmonth').setDisplayType('hidden');

	}

}

function fieldChange(type, name) {

	if (name == 'brewerylicense') {

		var breweryLicense = searchBreweryLicense(nlapiGetFieldValue('brewerylicense'));

		if (breweryLicense) { 

			nlapiSetFieldValue('breweryein', breweryLicense[0].getValue('custrecord_h_berp_brewery_ein'));
			nlapiSetFieldValue('breweryttbnumber', breweryLicense[0].getValue('custrecord_h_berp_ttb_brew_num'));
			nlapiSetFieldValue('breweryphone', breweryLicense[0].getValue('custrecord_h_berp_brewery_phone'));
			nlapiSetFieldValue('breweryname', breweryLicense[0].getValue('custrecord_h_berp_brewery_name'));
			nlapiSetFieldValue('brewerydba', breweryLicense[0].getValue('custrecord_h_berp_dba_name'));
			nlapiSetFieldValue('breweryaddr1', breweryLicense[0].getValue('custrecord_h_berp_street_address'));
			nlapiSetFieldValue('brewerycity', breweryLicense[0].getValue('custrecord_h_berp_brewery_city'));
			nlapiSetFieldValue('brewerystate', breweryLicense[0].getText('custrecord_h_berp_brewery_state'));
			nlapiSetFieldValue('breweryzipcode', breweryLicense[0].getValue('custrecord_h_berp_zipcode'));
			nlapiSetFieldValue('brewerycounty', breweryLicense[0].getValue('custrecord_h_berp_brewery_county'));

		}

	}

	if (name == 'monthlyorquarterly') {

		var reportType = nlapiGetFieldText('monthlyorquarterly');

		if (reportType == 'Quarterly') {

			nlapiGetField('quarterstartdate').setDisplayType('normal');
			nlapiGetField('quarterenddate').setDisplayType('normal');
			nlapiGetField('reportingperiod').setDisplayType('hidden');
			nlapiGetField('reportingmonth').setDisplayType('hidden');

		} else if (reportType == 'Monthly') {

			nlapiGetField('quarterstartdate').setDisplayType('hidden');
			nlapiGetField('quarterenddate').setDisplayType('hidden');
			nlapiGetField('reportingperiod').setDisplayType('normal');
			nlapiGetField('reportingmonth').setDisplayType('normal');

		}

	}

}

function searchBreweryLicense(breweryLicense) {

	nlapiLogExecution('DEBUG', 'Brewery License Internal ID', breweryLicense);
	var Filter = [], Column = [], Result;
	if (breweryLicense) { Filter.push(new nlobjSearchFilter('internalid', null, 'anyof', breweryLicense)); }
	Column.push(new nlobjSearchColumn('name'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_brewery_ein'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_ttb_brew_num'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_brewery_phone'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_brewery_name'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_dba_name'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_street_address'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_brewery_city'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_brewery_county'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_brewery_state'));
	Column.push(new nlobjSearchColumn('custrecord_h_berp_zipcode'));
	Result = nlapiSearchRecord('customrecord_h_berp_license', null, Filter, Column);
	if (Result) { return Result; }

}