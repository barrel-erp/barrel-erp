function locationAddressSrc(type){
	if(type == 'create'){
		var locationId = nlapiGetFieldValue('custrecord_h_berp_licenselocation');
		var locationRec = nlapiLoadRecord('location', locationId);
		var locationObj = {};
		locationObj.city = locationRec.getFieldValue('city');
		locationObj.state = locationRec.getFieldValue('state');
		locationObj.zip = locationRec.getFieldValue('zip')
		locationObj.address = locationRec.getFieldValue('addr1');
		locationObj.name = locationRec.getFieldValue('name');
		locationObj.phone = locationRec.getFieldValue('phone');
		//nlapiLogExecution('DEBUG', 'address vals', locationObj.city + ', ' + locationObj.state + ', ' +locationObj.zip);
		//nlapiLogExecution('DEBUG', 'phone', locationObj.phone + ', ' + locationObj.address)


		var stateLookUp = locationObj.state;
		//nlapiLogExecution('DEBUG', 'state', stateLookUp);
		var stateId = states[stateLookUp];
		//nlapiLogExecution('DEBUG','internal id state',  stateId);
		try{
		var licenseRec = nlapiLoadRecord('customrecord_h_berp_license', nlapiGetRecordId());
		licenseRec.setFieldValue('custrecord_h_berp_brewery_state', stateId);
		licenseRec.setFieldValue('custrecord_h_berp_brewery_city', locationObj.city);
		licenseRec.setFieldValue('custrecord_h_berp_zipcode', locationObj.zip);
		licenseRec.setFieldValue('custrecord_h_berp_street_address',locationObj.address)
		licenseRec.setFieldValue('custrecord_h_berp_brewery_name', locationObj.name);
		licenseRec.setFieldValue('custrecord_h_berp_brewery_phone', locationObj.phone);
		nlapiSubmitRecord(licenseRec);
		}
		catch(e){
			nlapiLogExecution('ERROR', 'could not source address', e);
		}
	}
}