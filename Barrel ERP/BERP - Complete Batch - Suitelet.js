/**
 * Module Description
 * 
 * Version    Date             Author           Remarks
 * 1.0        26 April 2016    jbrox
 * 2.0		  8 November 2016  jbrox            Add Scheduled Script to increase Script Usage points. Added Brite Tank selection. Added Issue Components TransformRecord.
 * 2.0 ^ 2    13 June 2017     amillen	        Add additional features to complete all manufacturing operation tasks on completion of batch/work orders and to track the scrap quantity on the manufacturing operation task.
 */

function completeBatch(request, response) {

	var context = nlapiGetContext();
	var accountId = context.getSetting('SCRIPT', 'custscript_h_berp_ns_account_id');
	var activeAccounts = accountActiveCheck();
	nlapiLogExecution('DEBUG', 'Active Accounts', JSON.stringify(activeAccounts));
	nlapiLogExecution('DEBUG', 'Index', activeAccounts.indexOf(accountId));

    if (activeAccounts.indexOf(accountId) >= 0) {
	
		if (request.getMethod() == 'GET') {

			var context = nlapiGetContext();

			// Get Parameters
			var location = request.getParameter('custparam_location');
			var recordId = request.getParameter('custparam_recordid');
			var origProdOrder = request.getParameter('custparam_origprod');
          	var dryHopComplete = request.getParameter('custrecord_h_berp_prb_dry_hop');
            if(dryHopComplete == 'F'){
               alert('Configure your dry hop inventory detail before completing this batch.');
            }
			var compDate = nlapiDateToString(new Date());
			var brightBeerExpirationDays = context.getSetting('SCRIPT', 'custscript_h_berp_bright_beer_exp');
			// nlapiLogExecution('DEBUG', 'brightBeerExpirationDays', brightBeerExpirationDays);

			var batchQuantity = searchProductionBatchQuantity(recordId);
			batchQuantity = batchQuantity[0].getValue('quantity', null, 'SUM');
			var batchName = nlapiLookupField('customrecord_h_berp_prod_batch', recordId, 'altname') + nlapiLookupField('customrecord_h_berp_prod_batch', recordId, 'name');
			batchName = batchName.replace("'",'');
			batchName = batchName.split(' ').join('');

		    // Create Form with Fields
		    var completeBatch = nlapiCreateForm('Complete Production Batch');

		    // Create Field Groups on the Form
		    var primary = completeBatch.addFieldGroup('primary', 'Production Batch Information');
		    completeBatch.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('disabled');
		   	completeBatch.addField('custpage_recordid', 'select', 'Production Batch', 'customrecord_h_berp_prod_batch', 'primary').setDisplayType('disabled');
		   	completeBatch.addField('custpage_batchname', 'text', 'Batch Lot Number', null, 'primary').setMandatory(true);
		   	var batch = completeBatch.addFieldGroup('batch', 'Batch Completion Information');
		   	completeBatch.addField('custpage_prodorderquantity', 'float', 'Production Batch Quantity', null, 'batch').setDisplayType('disabled');
		    completeBatch.addField('custpage_completedquantity', 'float', 'Total BBLs Completed (Bright Beer Quantity)', null, 'batch').setMandatory(true);
	   	    completeBatch.addField('custpage_completiondate', 'date', 'Completion Date', null, 'batch').setMandatory(true);

	   	    var briteTankSelection = completeBatch.addField('custpage_britetank','select', 'Brite Tank', null, 'batch').setMandatory(true);
	        briteTankSelection.addSelectOption('','');

	        var briteAvailable = searchAllBrightTanks(location);

	        if (briteAvailable) {

	        	for (var b = 0; b < briteAvailable.length; b++) {

	        		briteTankSelection.addSelectOption(briteAvailable[b].getValue('internalid'), briteAvailable[b].getValue('custentity_h_berp_tank_name'));

	        	}

	        }

	        var currentProOrderWorkCenters = searchProOrderWorkCenters(origProdOrder);
			// nlapiLogExecution('DEBUG', 'Production Order Work Centers', JSON.stringify(currentProOrderWorkCenters));

			for (var t = 0; t < currentProOrderWorkCenters.length; t++) {

				if (nlapiLookupField('entitygroup', currentProOrderWorkCenters[t].getValue('manufacturingworkcenter', 'manufacturingOperationTask'), 'custentity_h_berp_equipment_type') == '8') {
					
					var origBriteTank = currentProOrderWorkCenters[t].getValue('manufacturingworkcenter', 'manufacturingOperationTask');
					nlapiLogExecution('AUDIT', 'Orig Brite Tank', 'Manufacturing Operation Task = ' + origBriteTank);

				}

			}

			completeBatch.setFieldValues({custpage_britetank:origBriteTank, custpage_recordid:recordId, custpage_location:location, custpage_completiondate:compDate, custpage_prodorderquantity: batchQuantity, custpage_completedquantity: batchQuantity, custpage_batchname: batchName });

			completeBatch.addSubmitButton('Submit');

		    response.writePage(completeBatch);

		} else {

			var context = nlapiGetContext();

			// Get Parameters
             var user = nlapiGetUser();
          nlapiLogExecution('DEBUG', 'user', user);
			var location = request.getParameter('custpage_location');
			var batchLotNumber = request.getParameter('custpage_batchname');
			batchLotNumber = batchLotNumber.replace("'",'');
			batchLotNumber = batchLotNumber.split(' ').join('');

			var recordId = request.getParameter('custpage_recordid');
			var briteTank = request.getParameter('custpage_britetank');
			var completedQuantity = request.getParameter('custpage_completedquantity');
			var prodBatchQuantity = request.getParameter('custpage_prodorderquantity');
			var completionDate = request.getParameter('custpage_completiondate');
			var brightBeerExpirationDays = context.getSetting('SCRIPT', 'custscript_h_berp_bright_beer_exp');
			var expirationDate = new Date();
			expirationDate = nlapiAddDays(expirationDate, brightBeerExpirationDays);
			expirationDate = nlapiDateToString(expirationDate);

			var params = {

				custscript_h_berp_comp_batch_location: location,
				custscript_h_berp_comp_batch_lot: batchLotNumber,
				custscript_h_berp_comp_batch_probatch: recordId,
				custscript_h_berp_comp_batch_brite_tank: briteTank,
				custscript_h_berp_comp_batch_completed: completedQuantity,
				custscript_h_berp_comp_batch_probatch_qt: prodBatchQuantity,
				custscript_h_berp_comp_batch_comp_date: completionDate,
                custscript_user_err_msg: user

			}

			// Process Production Completions
			var productionOrdersToComplete = searchProductionOrdersToComplete(recordId);

			if (productionOrdersToComplete) {

				if (productionOrdersToComplete.length < 1) {

					var completionQuantity = completedQuantity / productionOrdersToComplete.length;
					
					if (parseFloat(completedQuantity) <= parseFloat(prodBatchQuantity)) { 

						var scrapQuantity = (parseFloat(prodBatchQuantity) - parseFloat(completedQuantity)) / productionOrdersToComplete.length; 
						// nlapiLogExecution('DEBUG', 'Scrap Quantity', parseFloat(prodBatchQuantity) + ' - ' + parseFloat(completedQuantity) + ' / ' + productionOrdersToComplete.length + ' = ' + scrapQuantity.toFixed(2));

					} else {

						var scrapQuantity = 0;

					}

					for (var i = 0; i < productionOrdersToComplete.length; i++) {
						nlapiLogExecution('DEBUG', 'Production Order Length', productionOrdersToComplete.length + ' prod ord ids ' + productionOrdersToComplete[i].getValue('internalid'));
						var productionOrder = productionOrdersToComplete[i].getValue('internalid');

						//set scrap quantity on manufacturing operation task and change status to complete on manufacturing operation task
								var manOpTaskResults = searchMOTasks(productionOrder);
								// var fields = [];
								// var values = [];
								// fields[0] = 'custevent_h_berp_mot_output_delta';
								// fields[1] = 'status';
								// values[0] = scrapQuantity;
								// values[1] = 'COMPLETE';
								for(var m = 0; m < manOpTaskResults.length; m++){

									nlapiLogExecution('DEBUG', 'man task id', manOpTaskResults[m].getValue('internalid'));
									var opTaskId = manOpTaskResults[m].getValue('internalid');
									var opStatus = nlapiLookupField('manufacturingoperationtask', opTaskId, 'status');
									nlapiLogExecution('DEBUG', 'man op task status', opStatus);
									var prodStatus = nlapiLookupField('workorder', productionOrder, 'status');
									if (productionOrderStatus == 'pendingApproval' || productionOrderStatus == 'Released' || productionOrderStatus == 'pendingBuild'){
										var manRec = nlapiLoadRecord('manufacturingoperationtask', opTaskId);
										manRec.setFieldValue('custevent_h_berp_mot_output_delta', scrapQuantity);
										manRec.setFieldValue('status', 'COMPLETE');
										nlapiSubmitRecord(manRec);
										//nlapiSubmitField('manufacturingoperationtask', opTaskId, fields, values);
									}
									else{
										nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'A');
										
										var manRec = nlapiLoadRecord('manufacturingoperationtask', opTaskId);
										manRec.setFieldValue('custevent_h_berp_mot_output_delta', scrapQuantity);
										manRec.setFieldValue('status', 'COMPLETE');
										nlapiSubmitRecord(manRec);

										nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'D');
									}
								}						// nlapiLogExecution('DEBUG', 'Production Order', productionOrder);

						// var origProductionOrder = nlapiLookupField('customrecord_h_berp_prod_batch', recordId, 'custrecord_h_berp_originating_prodord');
						// var origProOrderWorkCenters = searchProOrderWorkCenters(origProductionOrder);

						var currentProOrderWorkCenters = searchProOrderWorkCenters(productionOrder);
						// nlapiLogExecution('DEBUG', 'Production Order Work Centers', JSON.stringify(currentProOrderWorkCenters));

						for (var b = 0; b < currentProOrderWorkCenters.length; b++) {

							if (nlapiLookupField('entitygroup', currentProOrderWorkCenters[b].getValue('manufacturingworkcenter', 'manufacturingOperationTask'), 'custentity_h_berp_equipment_type') == '8') {

								var productionOrderStatus = nlapiLookupField('workorder', productionOrder, 'status');
								// nlapiLogExecution('AUDIT', 'Production Order Status', productionOrder + ' ' + productionOrderStatus);

								if (productionOrderStatus == 'pendingApproval' || productionOrderStatus == 'Released' || productionOrderStatus == 'pendingBuild') {
								
									nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', briteTank);
									nlapiLogExecution('AUDIT', 'Setting Work Center', 'Manufacturing Operation Task = ' + currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask') + ' | ' + 'Orig Brite Tank = ' + briteTank);

								} else {

									nlapiLogExecution('AUDIT', 'Production Order Status Before Unlock', productionOrder + ' ' + productionOrderStatus);
									nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'A');
									// var productionOrderStatus = nlapiLookupField('workorder', productionOrder, 'status');
									nlapiLogExecution('AUDIT', 'Production Order Status After Unlock', productionOrder + ' ' + productionOrderStatus);
									nlapiLogExecution('DEBUG', 'Manufacturing Operation | Orig Ferm Tank', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask') + ' | ' + briteTank);
									try { nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', briteTank); } catch(e) { nlapiLogExecution('ERROR', e.getCode(), e.getDetails()); }
									nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'D');

								}

							}

						}
						
						var productionOrderQuantity = productionOrdersToComplete[i].getValue('quantity');
						var fermTanks = [];
						var manufacturingWorkCenters = searchAllFermentationTanks(location);
						if (manufacturingWorkCenters) { 

							for (var f = 0; f < manufacturingWorkCenters.length; f++) {

								fermTanks.push(manufacturingWorkCenters[f].getValue('internalid'));

							}

						}
						var productionOrderOperations = searchproductionOrderOperations(productionOrder, fermTanks);
						productionOrderOperations = productionOrderOperations[0].getValue('internalid', 'manufacturingOperationTask');
						var productionOrderOperationMax = searchproductionOrderOperationMax(productionOrder);
						nlapiLogExecution('DEBUG', 'Production Order Operation Max Search', JSON.stringify(productionOrderOperationMax));
						productionOrderOperationMax = productionOrderOperationMax[0].getValue('internalid', 'manufacturingOperationTask', 'GROUP');

						var productionCompletion = nlapiTransformRecord('workorder', productionOrder, 'workordercompletion', {recordmode: 'dynamic'});
						nlapiLogExecution('DEBUG', 'Starting Operation', productionOrderOperations);
						productionCompletion.setFieldValue('startoperation', productionOrderOperations);
						nlapiLogExecution('DEBUG', 'Ending Operation', productionOrderOperationMax);
						productionCompletion.setFieldValue('endoperation', productionOrderOperationMax);
						productionCompletion.setFieldValue('quantity', productionOrderQuantity);
                        productionCompletion.setFieldValue('trandate', completionDate);
						nlapiLogExecution('DEBUG', 'Production Order Completion Quantity', completionQuantity.toFixed(2));
						nlapiLogExecution('DEBUG', 'Production Order Completion Build Quantity', productionOrderQuantity);
						productionCompletion.setFieldValue('completedquantity', completionQuantity.toFixed(2));
						productionCompletion.setFieldValue('scrapquantity', scrapQuantity.toFixed(2));
						var invDetail = productionCompletion.createSubrecord('inventorydetail');
						invDetail.setFieldValue('custitemnumber_h_berp_invdetail_prod_batch', recordId);
						invDetail.selectNewLineItem('inventoryassignment');
						invDetail.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', batchLotNumber);
						invDetail.setCurrentLineItemValue('inventoryassignment', 'expirationdate', expirationDate);
						invDetail.setCurrentLineItemValue('inventoryassignment', 'quantity', completionQuantity.toFixed(2));
						invDetail.commitLineItem('inventoryassignment');
						invDetail.commit();
						var productionCompletionId = nlapiSubmitRecord(productionCompletion, true);

						if (productionCompletionId) { nlapiSubmitField('workorder', productionOrder, 'orderstatus', 'G'); }

						nlapiLogExecution('AUDIT', 'Remaining Usage', productionOrder + ' ' + nlapiGetContext().getRemainingUsage());

					} //end of productin order id loop

				} else {

					nlapiScheduleScript('customscript_h_berp_complete_batch_sch', null, params);

				}

			}

			if (productionCompletionId) {

				nlapiSubmitField('customrecord_h_berp_prod_batch', recordId, 'custrecord_h_berp_batch_complete', 'T');

			}

			nlapiLogExecution('DEBUG', 'Remaining Usage', nlapiGetContext().getRemainingUsage());

		}

		response.write('<script>window.close();</script>');

	} else {

		if (request.getMethod() == 'GET') {

			// Create Form with Fields
		    var lotReportForm = nlapiCreateForm('Complete Production Batch');

		    // Create Field Groups on the Form
		    var primary = lotReportForm.addFieldGroup('primary', 'Primary Information');
		    lotReportForm.addField('custpage_subscription_expired', 'inlinehtml', '').setDisplayType('inline').setDefaultValue('<p><span style="color:#FF0000;size:14px;">Your account subscription for Barrel ERP has expired. Please contact your Barrel ERP account representative to activate your account.</span></p><img src="https://media.giphy.com/media/5ftsmLIqktHQA/giphy.gif"><audio controls autoplay visibility: hidden><source src="http://shopping.na2.netsuite.com/core/media/media.nl?id=404398&c=1112431&h=f38898aa52b7180a67fe&_xt=.mp3" type="audio/mp3"></audio>');

		    lotReportForm.addSubmitButton('Submit');
		    response.writePage(lotReportForm);

		} else {

			response.write('<script>window.close();</script>');

		}

	}

}