function completePrO(type) {

//@param {String} type Context Types: scheduled, ondemand, userinterface,aborted, skipped

    nlapiLogExecution('debug','Scheduled Script TYPE:', type)
	 //only execute ondemand
  if ( type != 'ondemand' ) 
  {
     nlapiLogExecution('AUDIT','Scheduled Script is not ondemand.', type)
    return; 
  }
	var productionCompletion = nlapiTransformRecord('workorder', 14340, 'workordercompletion');
	productionCompletion.setFieldValue('startoperation', 7991);
	productionCompletion.setFieldValue('endoperation', 7993);
	// productionCompletion.setFieldValue('quantity', 15);
	productionCompletion.setFieldValue('completedquantity', 15);
	// productionCompletion.setFieldValue('scrapquantity', scrapQuantity.toFixed(2));
	var invDetail = productionCompletion.createSubrecord('inventorydetail');
	invDetail.selectNewLineItem('inventoryassignment');
	invDetail.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', 'TEST');
	// invDetail.setCurrentLineItemValue('inventoryassignment', 'expirationdate', expirationDate);
	invDetail.setCurrentLineItemValue('inventoryassignment', 'quantity', 15);
	invDetail.commitLineItem('inventoryassignment');
	invDetail.commit();
	var productionCompletionId = nlapiSubmitRecord(productionCompletion, true);

}