/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0       03 Dec 2015     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {
  var context = nlapiGetContext();
  nlapiLogExecution('DEBUG', 'execution context', context.getExecutionContext());
	if(context.getExecutionContext() != 'suitelet' && context.getExecutionContext() != 'scheduled'){

	var additionalTimeMinutes = 0;
	additionalTimeMinutes = nlapiGetFieldValue('custevent_h_berp_additionaltime');
	if (!additionalTimeMinutes) { additionalTimeMinutes = 0; }
	var workCenter = nlapiGetFieldValue('manufacturingworkcenter');
	var workCenterFields = ['custentity_h_berp_additional_mins', 'custentity_h_berp_equipment_type'];
	var workCenterLookup = nlapiLookupField('entitygroup', workCenter, workCenterFields);
	var workCenterAddMinutes = 0;
	workCenterAddMinutes = workCenterLookup.custentity_h_berp_additional_mins;
	var workCenterEquipmentType = workCenterLookup.custentity_h_berp_equipment_type;
	if (!workCenterAddMinutes) { workCenterAddMinutes = 0; }
	var productionOrder = nlapiGetFieldValue('workorder');
	nlapiLogExecution('DEBUG', 'Work Order', productionOrder);
	var productionOrderFields = ['item', 'quantity'];
	var productionOrderLookup = nlapiLookupField('workorder', productionOrder, productionOrderFields);
	nlapiLogExecution('DEBUG', 'Work Order Fields', productionOrderLookup.item + ' | ' + productionOrderLookup.quantity);
	var beerRecipe = productionOrderLookup.item;
	var productionOrderQuantity = productionOrderLookup.quantity;
	var beerRecipeFields = ['custitem_h_berp_total_days_in_ferm'];
	var beerRecipeLookup = nlapiLookupField('item', beerRecipe, beerRecipeFields);
	var totalDaysFermentation = beerRecipeLookup.custitem_h_berp_total_days_in_ferm;
	nlapiLogExecution('AUDIT', 'Calculating Time', workCenterAddMinutes + ' | ' + additionalTimeMinutes + ' | ' + totalDaysFermentation);
	var fermDaysInMinutes = (totalDaysFermentation * 24 * 60) + parseFloat(workCenterAddMinutes) + parseFloat(additionalTimeMinutes);
	var runRate = fermDaysInMinutes / productionOrderQuantity;
	
	if ((additionalTimeMinutes != 0 || workCenterAddMinutes != 0) && workCenterEquipmentType != 6) {
	
		nlapiSetFieldValue('runrate', runRate.toFixed(0));

	} else if (workCenterEquipmentType == 6) {

		nlapiSetFieldValue('runrate', runRate.toFixed(0));
			
	}
  }

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {
     var context = nlapiGetContext();
  if (type != 'create' && context.getExecutionContext() != 'suitelet' && context.getExecutionContext() != 'scheduled' ) {
		
		var manufacturingOperationTask = nlapiGetRecordId();
		var location = nlapiLookupField('workorder', nlapiGetFieldValue('workorder'), 'location');
		var recipe = nlapiLookupField('workorder', nlapiGetFieldValue('workorder'), 'item');
		var manufacturingWorkCenter = nlapiGetFieldValue('manufacturingworkcenter');
		var productionOrder = nlapiGetFieldValue('workorder');
		var startDate = nlapiGetFieldValue('startdate');
		var endDate = nlapiGetFieldValue('enddate');

		var manufacturingOperationTasks = searchmanufacturingOperationTasks(productionOrder);
		nlapiLogExecution('DEBUG', 'Manufacturing Operation Tasks', JSON.stringify(manufacturingOperationTasks));

		for (var m = 0; m < manufacturingOperationTasks.length; m++) {

			var startDate = manufacturingOperationTasks[m].getValue('startdate');
			startDate = nlapiStringToDate(startDate);
			nlapiLogExecution('DEBUG', 'Start Date', startDate);
			var dueDate = manufacturingOperationTasks[m].getValue('startdate');
			dueDate = nlapiStringToDate(dueDate);

			var productionTaskTemplates = searchProductionTaskTemplates(null, location, recipe);

			if (productionTaskTemplates) {

				for (var p = 0; p < productionTaskTemplates.length; p++) {

					var productionTaskExists = searchCurrentProductionTasks(productionTaskTemplates[p].getValue('internalid'), manufacturingOperationTasks[m].getValue('manufacturingworkcenter'), productionOrder);

					var startDate = manufacturingOperationTasks[m].getValue('startdate');
					startDate = nlapiStringToDate(startDate);
					startDate.setDate(startDate.getDate() + parseInt(productionTaskTemplates[p].getValue('custrecord_h_berp_due_date')));

					var dueDate = manufacturingOperationTasks[m].getValue('startdate');
					dueDate = nlapiStringToDate(dueDate);
					dueDate.setDate(startDate.getDate() + parseInt(productionTaskTemplates[p].getValue('custrecord_h_berp_days_to_complete')));
					dueDate = nlapiDateToString(dueDate);
					startDate = nlapiDateToString(startDate);

					if (productionTaskExists) {

						for (var t = 0; t < productionTaskExists.length; t++) {

							var productionTaskRecord = nlapiLoadRecord('task', productionTaskExists[t].getValue('internalid'));
							productionTaskRecord.setFieldValue('duedate', dueDate);
							productionTaskRecord.setFieldValue('startdate', startDate);
							var taskId = nlapiSubmitRecord(productionTaskRecord, true);

						}

					}

				}

			}

		}

	}
  
}