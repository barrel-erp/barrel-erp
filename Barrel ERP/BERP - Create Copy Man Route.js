// function addButtonBeforeLoad(type, form){
// 	if(type == 'view' || type == 'edit'){
// 		var recordId = nlapiGetRecordId();
// 		form.setScript('customscript_create_man_route');
// 		form.addButton('custpage_createCommercial', 'Create Copy', 'createCopy()');
// 	}
// }
function createCopy(type){
	nlapiLogExecution('DEBUG', 'start', 'START');
	var copyFromID = nlapiGetRecordId();
	var copyFromRec = nlapiLoadRecord('manufacturingrouting', copyFromID);
	
	var copyFromName = copyFromRec.getFieldValue('name');
	var copyFromItem = copyFromRec.getFieldValue('item');
	var copyFromLoc = copyFromRec.getFieldValue('location');
	var copyFromSub = copyFromRec.getFieldValue('subsidiary');
	nlapiLogExecution('DEBUG', 'subsidiary', copyFromSub);

	var lineCount = copyFromRec.getLineItemCount('routingstep');
	var stepDictionary = { opSqunc: [], opNm: [], manWorkCnt: [], machRes:[], labRes: [], manCostTmp: [], setTime: [], runRate: [], lineNum: [] }
	for(var i = 1; i <= lineCount; i ++){
		//nlapiLogExecution('DEBUG', 'ROUTING', copyFromRec.getLineItemValue('routingstep', 'operationsequence', i));
		stepDictionary.opSqunc.push(copyFromRec.getLineItemValue('routingstep', 'operationsequence', i));
		stepDictionary.opNm.push(copyFromRec.getLineItemValue('routingstep','operationname', i));
		stepDictionary.manWorkCnt.push(copyFromRec.getLineItemValue('routingstep','manufacturingworkcenter', i));
		stepDictionary.machRes.push(copyFromRec.getLineItemValue('routingstep','machineresources', i));
		stepDictionary.labRes.push(copyFromRec.getLineItemValue('routingstep','laborresources', i));
		stepDictionary.manCostTmp.push(copyFromRec.getLineItemValue('routingstep','manufacturingcosttemplate', i));
		stepDictionary.setTime.push(copyFromRec.getLineItemValue('routingstep','setuptime', i));
		stepDictionary.runRate.push(copyFromRec.getLineItemValue('routingstep','runrate', i));
	}

	var lineCnt = copyFromRec.getLineItemCount('routingcomponent');
	//nlapiLogExecution('DEBUG', 'routing component', lineCnt);
	var componentDictionary = {cmpnt : [], item: [], itmName : [], yield :[], bomQTY : [], qty : [], units : [], opSqunc : []}
	for(var k = 1; k <= lineCnt; k++){
		componentDictionary.cmpnt.push(copyFromRec.getLineItemValue('routingcomponent', 'component', k));
		componentDictionary.item.push(copyFromRec.getLineItemValue('routingcomponent','item', k));
		componentDictionary.itmName.push(copyFromRec.getLineItemValue('routingcomponent','itemname', k));
		componentDictionary.yield.push(copyFromRec.getLineItemValue('routingcomponent','yield', k));
		componentDictionary.bomQTY.push(copyFromRec.getLineItemValue('routingcomponent','bomquantity', k));
		componentDictionary.qty.push(copyFromRec.getLineItemValue('routingcomponent','quantity', k));
		componentDictionary.units.push(copyFromRec.getLineItemValue('routingcomponent','units', k));
		componentDictionary.opSqunc.push(copyFromRec.getLineItemValue('routingcomponent','operationsequencenumber', k));
	}


	var newRouting = nlapiCreateRecord('manufacturingrouting', {recordmode: 'dynamic'});
	newRouting.setFieldValue('subsidiary', copyFromSub);
	newRouting.setFieldValue('item', copyFromItem);
	newRouting.setFieldValue('location', copyFromLoc);
	newRouting.setFieldValue('name','Copy of ' + copyFromName);
	newRouting.setFieldValue('isdefault','F');


	for(var j = 0; j < stepDictionary.opSqunc.length; j++){
		//nlapiLogExecution('DEBUG', 'step dictionary', stepDictionary.opSqunc[j]);
		//nlapiLogExecution('DEBUG', 'step dictionary length', stepDictionary.opSqunc.length);

		newRouting.selectNewLineItem('routingstep');
		newRouting.setCurrentLineItemValue('routingstep', 'manufacturingcosttemplate', stepDictionary.manCostTmp[j]);
		newRouting.setCurrentLineItemValue('routingstep', 'manufacturingworkcenter', stepDictionary.manWorkCnt[j]);
		newRouting.setCurrentLineItemValue('routingstep', 'operationname', stepDictionary.opNm[j]);
		newRouting.setCurrentLineItemValue('routingstep', 'operationsequence', stepDictionary.opSqunc[j]);
		newRouting.setCurrentLineItemValue('routingstep', 'machineresources', stepDictionary.machRes[j]);
		newRouting.setCurrentLineItemValue('routingstep', 'laborresources', stepDictionary.labRes[j]);
		newRouting.setCurrentLineItemValue('routingstep', 'setuptime', stepDictionary.setTime[j]);
		newRouting.setCurrentLineItemValue('routingstep', 'runrate', stepDictionary.runRate[j]);
		newRouting.commitLineItem('routingstep');
	}

	for(var l = 0; l < componentDictionary.cmpnt.length; l++){
		//nlapiLogExecution('DEBUG', ' component dictionary length', componentDictionary.cmpnt[l]);
		// newRouting.selectNewLineItem('routingcomponent');
		// newRouting.setCurrentLineItemValue('routingcomponent', 'component', componentDictionary.cmpnt[l]);
		// newRouting.setCurrentLineItemValue('routingcomponent', 'yield', componentDictionary.yield[l]);
		// newRouting.setCurrentLineItemValue('routingcomponent', 'bomquantity', componentDictionary.bomQTY[l]);
		// newRouting.setCurrentLineItemValue('routingcomponent', 'quantity', componentDictionary.qty[l]);
		// newRouting.setCurrentLineItemValue('routingcomponent', 'units', componentDictionary.units[l]);
		// newRouting.setCurrentLineItemValue('routingcomponent', 'operationsequencenumber', componentDictionary.opSqunc[l]);
		// newRouting.commitLineItem('routingcomponent');
	}
	try{
		var newRoutingID = nlapiSubmitRecord(newRouting);
		nlapiSetRedirectURL('RECORD', 'manufacturingrouting', newRoutingID);
	}
	catch(E){
		nlapiLogExecution('ERROR', 'error', E);
	}

	
}