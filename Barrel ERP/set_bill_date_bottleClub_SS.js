function setBillDate(type){
	var createdFrom = nlapiGetFieldText("createdfrom");
	nlapiLogExecution('DEBUG', 'created from', createdFrom);
	if(type == "create" && isSubscriptionBased(createdFrom)){

		var customerId = nlapiGetFieldValue("entity");
		
		var searchResults = nlapiSearchRecord("customrecord_h_berp_subscript",null,
								[
								   ["custrecord_h_berp_subscript_cancelled","is","F"], 
								   "AND", 
								   ["custrecord_h_berp_subscript_suspend","is","F"],
								       "AND", 
								   ["custrecord_h_berp_subscript_process_date","on","today"],
								   "AND",
								   ["custrecord_h_berp_subscript_cust_name", "anyof", customerId]
								], 
								[
								   new nlobjSearchColumn("id",null,null).setSort(false), 
								   new nlobjSearchColumn("custrecord_h_berp_subscript_process_date",null,null), 
								   new nlobjSearchColumn("custrecord_h_berp_subscript_frequency",null,null), 
								]
							);
		 if(isNotNull(searchResults)){
		 	for(var i = 0; i < searchResults.length; i++){
		 		var subsRecId = searchResults[i].getValue('id');
		 		var frequency = searchResults[i].getValue('custrecord_h_berp_subscript_frequency');
		 		var newSetBillDate;
		 		var d = new Date();
		 		//1 == weekly subscription
				 if(frequency == 1){
		            var dateConfig = nlapiAddDays(d, 7);
		            newSetBillDate = nlapiDateToString(dateConfig);
		            nlapiLogExecution('DEBUG', '1 weekly', dateConfig + ', ' + newSetBillDate);
		        }
		        //2 == monthly subscription
		        if(frequency == 2){
		            var dateConfig = nlapiAddMonths(d, 1);
		            newSetBillDate = nlapiDateToString(dateConfig);
		            nlapiLogExecution('DEBUG', '2 monthly', dateConfig + ', ' + newSetBillDate);
		        }
		        //3 == quarterly subscription
		        if(frequency == 3){
		            var dateConfig = nlapiAddMonths(d, 3);
		            newSetBillDate = nlapiDateToString(dateConfig);
		            nlapiLogExecution('DEBUG', '3 quarterly', dateConfig + ', ' + newSetBillDate);
		        }
		        //4 == semi monthly subscription
		        if(frequency == 4){
		            var dateConfig = nlapiAddDays(d, 14);
		            newSetBillDate = nlapiDateToString(dateConfig);
		            nlapiLogExecution('DEBUG', '4 semi month', dateConfig + ', ' + newSetBillDate);
		        }
		        //5 == semi annual subscription
		        if(frequency == 5){
		             var dateConfig = nlapiAddMonths(d, 6);
		            newSetBillDate = nlapiDateToString(dateConfig);
		            nlapiLogExecution('DEBUG', '5 semi annual', dateConfig + ', ' + newSetBillDate);
		        }
		        //6 == annual subscription
		        if(frequency == 6){
		             var dateConfig = nlapiAddMonths(d, 12);
		            newSetBillDate = nlapiDateToString(dateConfig);
		            nlapiLogExecution('DEBUG', '6 annual', dateConfig + ', ' + newSetBillDate);
		        }
		        nlapiSubmitField('customrecord_h_berp_subscript', subsRecId, 'custrecord_h_berp_subscript_process_date', newSetBillDate);
		 	}
		 }
     }
};
function isSubscriptionBased(field_text){
	var patt = new RegExp('Estimate');
	nlapiLogExecution('DEBUG', 'reg ex', patt.test(field_text));
	return (patt.test(field_text));
}
function isNotNull(field_val){
	return (field_val != '' && field_val != null);
}

