function blendingAdjustment_ss(){
	var context = nlapiGetContext();

	// Get Parameters
	var adjustmentAccnt = context.getSetting('SCRIPT', 'custscript_account_blending');
	var blendId = context.getSetting('SCRIPT', 'custscript_blend_id');
  
  	nlapiLogExecution('DEBUG', 'script start', 'START' + 'blend id' + blendId);
	
	var searchBAitems = nlapiSearchRecord("customrecord_h_berp_barrels",null,
			[
			   ["custrecord_h_berp_blending","anyof",blendId]
			], 
			[
			   new nlobjSearchColumn("internalid",null,null), 
			   new nlobjSearchColumn("custrecord_h_berp_ba_item",null,null),
			   new nlobjSearchColumn("custrecord_h_berp_qty", null, null),
			   new nlobjSearchColumn("custrecord_h_berp_item_lot", null,  null)
			]
			);
		var baSub = nlapiLookupField('customrecord_h_berp_blending', blendId, 'custrecord_h_berp_bl_sub');
		var baLoc = nlapiLookupField('customrecord_h_berp_blending', blendId, 'custrecord_h_berp_bl_loc');
		var date = nlapiLookupField('customrecord_h_berp_blending', blendId, 'custrecord_h_berp_bl_date');
  		nlapiLogExecution('DEBUG', 'blend vals', 'sub=' + baSub + ' loc=' + baLoc + ' date=' + date);
  		try{
		var invAdj = nlapiCreateRecord('inventoryadjustment', {recordmode: 'dynamic'});		
		invAdj.setFieldValue('customform', 115);
		invAdj.setFieldValue('subsidiary', baSub);
		invAdj.setFieldValue('adjlocation', baLoc);
		invAdj.setFieldValue('trandate', date);
		invAdj.setFieldValue('account', adjustmentAccnt);
		}
		catch(E){
			nlapiLogExecution('DEBUG', 'try/catch 1', E);
		}

		for(var i =0; i <searchBAitems.length; i++){
			var ba_recId = searchBAitems[i].getValue('internalid');
			var ba_item = searchBAitems[i].getValue('custrecord_h_berp_ba_item');
			var ba_qty = searchBAitems[i].getValue('custrecord_h_berp_qty');
			var adjsQTY = (ba_qty * -1);
			nlapiLogExecution('DEBUG', 'adjust qty by', adjsQTY);
			var ba_lot = searchBAitems[i].getValue('custrecord_h_berp_item_lot');
   

			try{
			invAdj.selectNewLineItem('inventory');
			invAdj.setCurrentLineItemValue('inventory', 'item', ba_item);
			invAdj.setCurrentLineItemValue('inventory', 'location', baLoc);
			invAdj.setCurrentLineItemValue('inventory', 'adjustqtyby', adjsQTY);

				var subrecord = invAdj.createCurrentLineItemSubrecord('inventory', 'inventorydetail');
				subrecord.selectNewLineItem('inventoryassignment');
					subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', adjsQTY);
				subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', ba_lot);
				subrecord.commitLineItem('inventoryassignment');
				subrecord.commit();
			invAdj.commitLineItem('inventory');
			}
			catch(e){
				nlapiLogExecution('DEBUG', 'try/catch 2', e);
			}
		}
		try{
		var invAdj_ID = nlapiSubmitRecord(invAdj);
		}
		catch(E){
			nlapiLogExecution('DEBUG', 'try/catch 3', E);
		}
		try{
		var blendieRec = nlapiLoadRecord('customrecord_h_berp_blending', blendId);
		blendieRec.setFieldValue('custrecord_inv_adj', invAdj_ID);
		nlapiSubmitRecord(blendieRec);
		}
		catch(E){
			nlapiLogExecution('DEBUG', 'try catch 4', E);
		}

}