/**
* @NScriptType UserEventScript
* @NApiVersion 2.0
* DATE         AUTHOR
* 5.11.17      Amillen
*/

//checking if expiration date changed and updated customer record. 
define(['N/record'], function (record) { 
	function isNotNull(field_val){
		return (field_val != '' && field_val != null);
	}
	return {
		//entryPoint has context objects containint values
		//equivalent in 1.0 is event types and triggers
		afterSubmit : function(context){
			log.debug('context vals', context);
			//nlapiGetNewRecord equivalent = context.newRecord();
			var license = context.newRecord;
			var ourLicense = license.getValue('custrecord_not_customer_owned');
			log.debug('license', ourLicense);
			if(!ourLicense){
				var expDate = license.getValue('custrecord_h_berp_expiration');
				log.debug('exp date', expDate);
				var today = new Date();
				if(expDate > today){
					var customerId = license.getValue('custrecord_h_berp_licenseowner');
					log.debug('customer id', customerId);
					var customer = record.load({
						type: record.Type.CUSTOMER,
						id: customerId
					});

					var overduebalance = customer.getValue('overduebalance');
					var creditLimit = customer.getValue('creditlimit');

					if(isNotNull(creditLimit)){
						if(overduebalance <= creditLimit){
							customer.setValue('creditholdoverride', 'OFF');
							customer.save();
						}
					}
				}
			}
		}
	}

});