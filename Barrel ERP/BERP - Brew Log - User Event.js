/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 2.00.16    08 Dec 2016     jbrox
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {

	if (type == 'create') {

		var brewLogForm = form;
		// nlapiLogExecution('DEBUG', 'Form', brewLogForm);
		var tabs = brewLogForm.getTabs();
		// nlapiLogExecution('DEBUG', 'Tabs', JSON.stringify(tabs));

		var prodOrder = nlapiGetFieldValue('custrecord_h_berp_qcbrew_productionorder');

		if (prodOrder) {
		
			var prodOrderRecord = nlapiLoadRecord('workorder', prodOrder);
			var lineCount = prodOrderRecord.getLineItemCount('item');
			var lineNum = 0;
			var lineId = 0;

			var operations = searchmanufacturingOperationTasks(prodOrder);

			var tabId = 'custpage_tab_components';
			var tabName = 'Components';

			//Add a tab to the form.
			var compTab = brewLogForm.addTab('custpage_tab_components', 'Components');

			if (operations) {

				for (var i = 0; i < operations.length; i++) {

					nlapiLogExecution('DEBUG', 'Operation Sequence Name', operations[i].getValue('sequence'));
					var tabId = 'custpage_tab_' + operations[i].getValue('sequence');
					var tabName = operations[i].getValue('name');
					// nlapiLogExecution('DEBUG', 'Tab ID | Tab Name', tabId + ' | ' + tabName);

					var subListId = 'custpage_sublist_' + operations[i].getValue('sequence');
					var subListName = operations[i].getValue('name');
					// nlapiLogExecution('DEBUG', 'Sublist ID | Sublist Name', subListId + ' | ' + subListName);

					var columnIdOne = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'item';
					var columnNameOne = 'Item';
					// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdOne + ' | ' + columnNameOne);

					var columnIdTwo = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'plannedqty';
					var columnNameTwo = 'Planned Quantity';
					// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdTwo + ' | ' + columnNameTwo);

					var columnIdThree = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'actualqty';
					var columnNameThree = 'Actual Quantity';
					// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdThree + ' | ' + columnNameThree);

					//Add a tab to the form.
					var subTab = brewLogForm.addSubTab(tabId, tabName, 'custpage_tab_components');

					//Add a sublist to the Tab
					var brewLogSublist = brewLogForm.addSubList(subListId, 'inlineeditor', subListName, tabId);
					brewLogSublist.addField(columnIdOne, 'select', columnNameOne, '-10').setDisplayType('disabled');
				    brewLogSublist.addField(columnIdTwo, 'float', columnNameTwo).setDisplayType('disabled');
				    brewLogSublist.addField(columnIdThree, 'float', columnNameThree);

				    for (var l = 0; l < lineCount; l++) {

				    	lineNum++;
				    	// nlapiLogExecution('DEBUG', 'Operation Sequence Item Line', prodOrderRecord.getLineItemValue('item', 'operationsequencenumber', lineNum));
				    	// nlapiLogExecution('DEBUG', 'Operation Sequence Tab', operations[i].getValue('sequence'));
				    	if (prodOrderRecord.getLineItemValue('item', 'operationsequencenumber', lineNum) == operations[i].getValue('sequence')) {

				    		lineId++;
					    	brewLogSublist.setLineItemValue(columnIdOne, lineId, prodOrderRecord.getLineItemValue('item', 'item', lineNum));
							brewLogSublist.setLineItemValue(columnIdTwo, lineId, prodOrderRecord.getLineItemValue('item', 'quantity', lineNum));

						}

				    }

				    lineNum = 0;
				    lineId = 0;

				}

			}

			var prodOrderRecId = nlapiSubmitRecord(prodOrderRecord, true);

		}

	} else if (type != 'create' && type != 'delete') {

		var brewLog = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'Brew Log Get Record ID', brewLog);
		var brewLogForm = form;
		// nlapiLogExecution('DEBUG', 'Form', brewLogForm);
		var tabs = brewLogForm.getTabs();
		// nlapiLogExecution('DEBUG', 'Tabs', JSON.stringify(tabs));

		var prodOrder = nlapiGetFieldValue('custrecord_h_berp_qcbrew_productionorder');
		nlapiLogExecution('DEBUG', 'Prod Order', prodOrder);

		if (prodOrder) {
		
			var brewLogComponents = searchBrewLogComponents(brewLog, prodOrder);
			nlapiLogExecution('DEBUG', 'Brew Log Components Search', JSON.stringify(brewLogComponents));

			if (brewLogComponents) {
			
				var lineCount = brewLogComponents.length;
				var lineNum = 0;
				var lineId = 0;

				var operations = searchmanufacturingOperationTasks(prodOrder);

				var tabId = 'custpage_tab_components';
				var tabName = 'Components';

				//Add a tab to the form.
				var compTab = brewLogForm.addTab('custpage_tab_components', 'Components');

				if (operations) {

					for (var i = 0; i < operations.length; i++) {

						nlapiLogExecution('DEBUG', 'Operation Sequence Name', operations[i].getValue('sequence'));
						var tabId = 'custpage_tab_' + operations[i].getValue('sequence');
						var tabName = operations[i].getValue('name');
						// nlapiLogExecution('DEBUG', 'Tab ID | Tab Name', tabId + ' | ' + tabName);

						var subListId = 'custpage_sublist_' + operations[i].getValue('sequence');
						var subListName = operations[i].getValue('name');
						// nlapiLogExecution('DEBUG', 'Sublist ID | Sublist Name', subListId + ' | ' + subListName);

						var columnIdOne = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'item';
						var columnNameOne = 'Item';
						// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdOne + ' | ' + columnNameOne);

						var columnIdTwo = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'plannedqty';
						var columnNameTwo = 'Planned Quantity';
						// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdTwo + ' | ' + columnNameTwo);

						var columnIdThree = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'actualqty';
						var columnNameThree = 'Actual Quantity';
						// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdThree + ' | ' + columnNameThree);

						//Add a tab to the form.
						var subTab = brewLogForm.addSubTab(tabId, tabName, 'custpage_tab_components');

						//Add a sublist to the Tab
						var brewLogSublist = brewLogForm.addSubList(subListId, 'inlineeditor', subListName, tabId);
						brewLogSublist.addField(columnIdOne, 'select', columnNameOne, '-10').setDisplayType('disabled');
					    brewLogSublist.addField(columnIdTwo, 'float', columnNameTwo).setDisplayType('disabled');
					    brewLogSublist.addField(columnIdThree, 'float', columnNameThree);

					    for (var l = 0; l < lineCount; l++) {

					    	lineNum++;
					    	// nlapiLogExecution('DEBUG', 'Operation Sequence Item Line', brewLogComponents[l].getValue('custrecord_h_berp_brewlog_comp_manu'));
					    	// nlapiLogExecution('DEBUG', 'Operation Sequence Tab', operations[i].getValue('sequence'));
					    	if (brewLogComponents[l].getValue('custrecord_h_berp_brewlog_comp_manu') == operations[i].getValue('sequence')) {

					    		lineId++;
						    	brewLogSublist.setLineItemValue(columnIdOne, lineId, brewLogComponents[l].getValue('custrecord_h_berp_brewlog_comp_item'));
						    	brewLogSublist.setLineItemValue(columnIdTwo, lineId, brewLogComponents[l].getValue('custrecord_h_berp_brewlog_comp_planned'));
								brewLogSublist.setLineItemValue(columnIdThree, lineId, brewLogComponents[l].getValue('custrecord_h_berp_brewlog_comp_actual'));

							}

					    }

					    lineNum = 0;
					    lineId = 0;

					}

				}

			} else {

				var prodOrderRecord = nlapiLoadRecord('workorder', prodOrder);
				var lineCount = prodOrderRecord.getLineItemCount('item');
				var lineNum = 0;
				var lineId = 0;

				var operations = searchmanufacturingOperationTasks(prodOrder);

				var tabId = 'custpage_tab_components';
				var tabName = 'Components';

				//Add a tab to the form.
				var compTab = brewLogForm.addTab('custpage_tab_components', 'Components');

				if (operations) {

					for (var i = 0; i < operations.length; i++) {

						nlapiLogExecution('DEBUG', 'Operation Sequence Name', operations[i].getValue('sequence'));
						var tabId = 'custpage_tab_' + operations[i].getValue('sequence');
						var tabName = operations[i].getValue('name');
						// nlapiLogExecution('DEBUG', 'Tab ID | Tab Name', tabId + ' | ' + tabName);

						var subListId = 'custpage_sublist_' + operations[i].getValue('sequence');
						var subListName = operations[i].getValue('name');
						// nlapiLogExecution('DEBUG', 'Sublist ID | Sublist Name', subListId + ' | ' + subListName);

						var columnIdOne = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'item';
						var columnNameOne = 'Item';
						// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdOne + ' | ' + columnNameOne);

						var columnIdTwo = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'plannedqty';
						var columnNameTwo = 'Planned Quantity';
						// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdTwo + ' | ' + columnNameTwo);

						var columnIdThree = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'actualqty';
						var columnNameThree = 'Actual Quantity';
						// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdThree + ' | ' + columnNameThree);

						//Add a tab to the form.
						var subTab = brewLogForm.addSubTab(tabId, tabName, 'custpage_tab_components');

						//Add a sublist to the Tab
						var brewLogSublist = brewLogForm.addSubList(subListId, 'inlineeditor', subListName, tabId);
						brewLogSublist.addField(columnIdOne, 'select', columnNameOne, '-10').setDisplayType('disabled');
					    brewLogSublist.addField(columnIdTwo, 'float', columnNameTwo).setDisplayType('disabled');
					    brewLogSublist.addField(columnIdThree, 'float', columnNameThree);

					    for (var l = 0; l < lineCount; l++) {

					    	lineNum++;
					    	// nlapiLogExecution('DEBUG', 'Operation Sequence Item Line', prodOrderRecord.getLineItemValue('item', 'operationsequencenumber', lineNum));
					    	// nlapiLogExecution('DEBUG', 'Operation Sequence Tab', operations[i].getValue('sequence'));
					    	if (prodOrderRecord.getLineItemValue('item', 'operationsequencenumber', lineNum) == operations[i].getValue('sequence')) {

					    		lineId++;
						    	brewLogSublist.setLineItemValue(columnIdOne, lineId, prodOrderRecord.getLineItemValue('item', 'item', lineNum));
								brewLogSublist.setLineItemValue(columnIdTwo, lineId, prodOrderRecord.getLineItemValue('item', 'quantity', lineNum));

							}

					    }

					    lineNum = 0;
					    lineId = 0;

					}

				}

				var prodOrderRecId = nlapiSubmitRecord(prodOrderRecord, true);

			}

		}

	}
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {

	

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type) {

	if (type != 'delete') {
	
		var brewLogId = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'Brew Log ID', brewLogId);
		var prodOrder = nlapiGetFieldValue('custrecord_h_berp_qcbrew_productionorder');
		nlapiLogExecution('DEBUG', 'Prod Order', prodOrder);

		if (prodOrder) {
		
			var operations = searchmanufacturingOperationTasks(prodOrder);

			if (operations) {

				for (var i = 0; i < operations.length; i++) {

					var subListId = 'custpage_sublist_' + operations[i].getValue('sequence');
					var subListName = operations[i].getValue('name');
					// nlapiLogExecution('DEBUG', 'Sublist ID | Sublist Name', subListId + ' | ' + subListName);

					var columnIdOne = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'item';
					var columnNameOne = 'Item';
					// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdOne + ' | ' + columnNameOne);

					var columnIdTwo = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'plannedqty';
					var columnNameTwo = 'Planned Quantity';
					// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdTwo + ' | ' + columnNameTwo);

					var columnIdThree = 'custpage_col_' + operations[i].getValue('sequence') + '_' + 'actualqty';
					var columnNameThree = 'Actual Quantity';
					// nlapiLogExecution('DEBUG', 'Column ID | Column Name', columnIdThree + ' | ' + columnNameThree);

					var subListLineCount = nlapiGetLineItemCount(subListId);
					var subListlineId = 0;

					for (var s = 0; s < subListLineCount; s++) {

						subListlineId++;
						// nlapiSelectLineItem(subListId, subListlineId);
						var brewLogComponents = nlapiCreateRecord('customrecord_h_berp_brewlog_components');
						brewLogComponents.setFieldValue('custrecord_h_berp_brewlog_comp_log', brewLogId);
						brewLogComponents.setFieldValue('custrecord_h_berp_brewlog_comp_pro', prodOrder);
						brewLogComponents.setFieldValue('custrecord_h_berp_brewlog_comp_item', nlapiGetLineItemValue(subListId, columnIdOne, subListlineId));
						brewLogComponents.setFieldValue('custrecord_h_berp_brewlog_comp_manu', operations[i].getValue('sequence'));
						brewLogComponents.setFieldValue('custrecord_h_berp_brewlog_comp_planned', nlapiGetLineItemValue(subListId, columnIdTwo, subListlineId));
						brewLogComponents.setFieldValue('custrecord_h_berp_brewlog_comp_actual', nlapiGetLineItemValue(subListId, columnIdThree, subListlineId));
						var brewLogComponentsId = nlapiSubmitRecord(brewLogComponents, true);

					}

				}

			}

		}

	}
  
}
