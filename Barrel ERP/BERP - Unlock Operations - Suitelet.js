function unlockRelockOperations() {

	var recid = request.getParameter('custparam_custom_id');
	var ordStatus = request.getParameter('custparam_orderstatus');
	nlapiLogExecution('DEBUG', 'Order Status', ordStatus);
	nlapiLogExecution('DEBUG', 'Record ID', recid);
	
	try {

		if (ordStatus == 'A') {

			nlapiSubmitField('workorder', recid, 'orderstatus', 'D');

		} else if (ordStatus == 'D') {

			nlapiSubmitField('workorder', recid, 'orderstatus', 'A');

		}

	} catch(e) {

		nlapiLogExecution('ERROR', 'Error Code | Error Details', e.getCode() + ' | ' + e.getDetails);

	}

	nlapiSetRedirectURL('RECORD', 'workorder', recid);

}