/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        09 Jun 2017     amillen          search look-ups to move Bright and Fermentation tank functionality
 * 1.1        02 Jul 2019     sstreule         Updated active account check function
 *
 */
function searchMOTasks(prodOrdId, sequence){
  var results = nlapiSearchRecord("manufacturingoperationtask",null, 
    [
      ["workorder","anyof", prodOrdId], 
      "AND",
      ["sequence", "equalto", sequence],
    ], 
    [
        new nlobjSearchColumn("internalid",null,null).setSort(),
        new nlobjSearchColumn("sequence", null, null),
        new nlobjSearchColumn("manufacturingworkcenter")
    ]
    );

  return results;
    
}
function searchBrightMOTasks(prodOrdId){
  var brightResults = nlapiSearchRecord("entitygroup",null,
    [
      ["isinactive", "anyof", false],
        "AND",
      ["custentity_h_berp_equipment_type", "anyof", 8]
    ],
    [
      new nlobjSearchColumn("internalid",null,null)
    ]
  );

  var array = [];
  for(var i = 0; i < brightResults.length; i++){
    array.push(brightResults[i].getValue('internalid'));
  }
  array.join();

  var results = nlapiSearchRecord("manufacturingoperationtask",null, 
    [
      ["workorder","anyof", prodOrdId], 
        "AND",
      ["manufacturingworkcenter", "anyof", array]
    ], 
    [
      new nlobjSearchColumn("internalid",null,null).setSort(),
      new nlobjSearchColumn("sequence", null, null),
      new nlobjSearchColumn("manufacturingworkcenter")
    ]
  );

  return results;
}
function searchFermMOTasks(prodOrdId){
  var FermResults = nlapiSearchRecord("manufacturingcosttemplate",null,
    [
       ["name","contains","Fermentation"], 
    ], 
    [
        new nlobjSearchColumn("internalid",null,null)
    ]
    );
    
    var array = [];
    for(var i = 0; i < FermResults.length; i++){
      array.push(FermResults[i].getValue('internalid'));
    }
    array.join();


    var results = nlapiSearchRecord("manufacturingoperationtask",null, 
    [
      ["workorder","anyof", prodOrdId], 
      "AND",
      ["manufacturingcosttemplate", "anyof", array],
    ], 
    [
        new nlobjSearchColumn("internalid",null,null).setSort(),
        new nlobjSearchColumn("sequence", null, null),
        new nlobjSearchColumn("manufacturingworkcenter")
    ]
    );

  return results;
    
}
function searchAllFermAndBrightTanks(location){
  var Filter = [], Column = [], Result;

  Filter.push(new nlobjSearchFilter('ismanufacturingworkcenter', null, 'is', 'T'));
  Filter.push(new nlobjSearchFilter('custentity_h_berp_equipment_type', null, 'anyof', 6, 8));
  Filter.push(new nlobjSearchFilter('custentity_h_berp_grouplocation', null, 'anyof', location));
  Column[0] = new nlobjSearchColumn('custentity_h_berp_live_tank_view_seq');
  Column[1] = new nlobjSearchColumn('custentity_h_berp_tank_name');
  Column[2] = new nlobjSearchColumn('custentity_h_berp_capacity');
  Column[3] = new nlobjSearchColumn('internalid');
  Column[0].setSort();

  Result = nlapiSearchRecord('entitygroup', null, Filter, Column);

  return Result;

}

function searchBatchProductionOrders(batchid) {
var Result = nlapiSearchRecord("workorder",null,
        [
           ["type","anyof","WorkOrd"], 
           "AND", 
          ["status","noneof","WorkOrd:C","WorkOrd:H"], 
           "AND", 
           ["custbody_h_berp_prod_batch","anyof",batchid], 
           "AND", 
           ["mainline","is","T"]
        ], 
        [
           new nlobjSearchColumn("internalid",null,null)
        ]
      );

  if (Result) { return Result; }

}

function accountActiveCheck() {

  var a = new Array();
    a['Content-Type'] = 'application/json';
    a['User-Agent-x'] = 'SuiteScript-Call';

    var url = "https://forms.na2.netsuite.com/app/site/hosting/scriptlet.nl?script=330&deploy=1&compid=1112431&h=ad0791c1dfe5631b86c3";

    //var activeAccounts = nlapiRequestURL(url, null, a);

    // nlapiLogExecution('DEBUG', 'Active Accounts', activeAccounts.getBody());

    //activeAccounts = JSON.parse(activeAccounts.getBody());
    // nlapiLogExecution('DEBUG', 'Active Accounts Length', activeAccounts.length);
    
    //HF CODE
    var context = nlapiGetContext();
    var activeAccounts = new Array();
      activeAccounts[0] = "AccountID";
      activeAccounts[1] = context.getSetting('SCRIPT', 'custscript_h_berp_ns_account_id');

    return activeAccounts;

}

function searchAllFermentationTanks(location) {

  var Filter = [], Column = [], Result;

  Filter.push(new nlobjSearchFilter('ismanufacturingworkcenter', null, 'is', 'T'));
  Filter.push(new nlobjSearchFilter('custentity_h_berp_equipment_type', null, 'anyof', 6));
  Filter.push(new nlobjSearchFilter('custentity_h_berp_grouplocation', null, 'anyof', location));
  Column[0] = new nlobjSearchColumn('custentity_h_berp_live_tank_view_seq');
  Column[1] = new nlobjSearchColumn('custentity_h_berp_tank_name');
  Column[2] = new nlobjSearchColumn('custentity_h_berp_capacity');
  Column[3] = new nlobjSearchColumn('internalid');
  Column[0].setSort();

  Result = nlapiSearchRecord('entitygroup', null, Filter, Column);

  return Result;

}

function searchAllBrightTanks(location) {

  var Filter = [], Column = [], Result;

  Filter.push(new nlobjSearchFilter('ismanufacturingworkcenter', null, 'is', 'T'));
  Filter.push(new nlobjSearchFilter('custentity_h_berp_equipment_type', null, 'anyof', 8));
  Filter.push(new nlobjSearchFilter('custentity_h_berp_grouplocation', null, 'anyof', location));
  Column[0] = new nlobjSearchColumn('custentity_h_berp_live_tank_view_seq');
  Column[1] = new nlobjSearchColumn('custentity_h_berp_tank_name');
  Column[2] = new nlobjSearchColumn('custentity_h_berp_capacity');
  Column[3] = new nlobjSearchColumn('internalid');
  Column[4] = new nlobjSearchColumn('groupname');
  Column[0].setSort();

  Result = nlapiSearchRecord('entitygroup', null, Filter, Column);

  return Result;

}
function searchOperationTaskCenters(prodOrdId, sequence, equipmentType) {
    var workCenterResults = nlapiSearchRecord("manufacturingoperationtask",null, 
    [
      ["workorder","anyof", prodOrdId],
      "AND",
      ["sequence", "equalto", sequence] 
    ], 
    [
        new nlobjSearchColumn("manufacturingworkcenter",null,null).setSort(),
    ]
    );
    
    var array = [];
    for(var i = 0; i < workCenterResults.length; i++){
      array.push(workCenterResults[i].getValue('manufacturingworkcenter'));
    }
    array.join();

    var results = nlapiSearchRecord("entitygroup", null, [
      ["internalid", "anyof", array],
      "AND",
      ["custentity_h_berp_equipment_type", "anyof", equipmentType],
    ],
    [
        new nlobjSearchColumn("custentity_h_berp_tank_name",null,null).setSort(),
        new nlobjSearchColumn("internalid"),
    ]
    );
    return results;
}
function lookUpSequence(prodOrdId, workcenter) {
    var results = nlapiSearchRecord("manufacturingoperationtask",null, 
    [
      ["workorder","anyof", prodOrdId],
      "AND",
      ["manufacturingworkcenter", "anyof", workcenter] 
    ], 
    [
        new nlobjSearchColumn("sequence",null,null)
    ]
    );

    return results;
}   
function isNull(s_string){
  return (s_string == null || s_string == '');
}
function isNotNull(field_val){
  return (field_val != '' && field_val != null);
}