function addButtonBeforeLoad(type, form){

	var commercialItem = nlapiGetFieldValue('custrecord_h_berp_bl_comm_item');
	var prodOrdCreated = nlapiGetFieldValue('custrecord_prod_ord_blendie');
    var prodQty = nlapiGetFieldValue('custrecord_work_ord_qty');
	if(isNotNull(commercialItem) && isNotNull(prodQty) && isNull(prodOrdCreated)){
		var recordId = nlapiGetRecordId();
		var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_blend_suitelet','customdeploy_h_berp_blend_suit');
		suiteletURL += '&custparam_recordid=' + recordId;
		form.setScript('customscript_h_berp_button_click');
		form.addButton('custpage_createCommercial', 'Create Blend', 'blendSuitelet(\'' + suiteletURL + '\');')
	}
}

function sumCostCalculateBBL(type, form){

	if(type == 'create' || type == 'edit'){

		//custscript_account_blendingz
		var blendId = nlapiGetRecordId();

		var makeCommercial = nlapiGetFieldValue('custrecord_h_berp_bl_make');
		var loc = nlapiGetFieldValue('custrecord_h_berp_bl_loc');
		var isAdjCreated = nlapiGetFieldValue('custrecord_inv_adj');

		var uomSample = nlapiGetFieldText('custrecord_h_berp_sample');

		var uomArray = uomSample.split(" ");

		var uomNum = uomArray[0];
		var uomMeasurement = uomArray[1];
		var calculateUOM = evaluateUOM(uomMeasurement);
		var BBLequivalent = (uomNum/calculateUOM);

		var searchBAitems = nlapiSearchRecord("customrecord_h_berp_barrels",null,
			[
			   ["custrecord_h_berp_blending","anyof",blendId]
			], 
			[
			   new nlobjSearchColumn("internalid",null,null), 
			   new nlobjSearchColumn("custrecord_h_berp_ba_item",null,null),
			   new nlobjSearchColumn("custrecord_h_berp_qty", null, null)
			]
			);

		for(var i =0; i < searchBAitems.length; i++){
			var ba_recId = searchBAitems[i].getValue('internalid');
			var ba_qty = searchBAitems[i].getValue('custrecord_h_berp_qty');
			var ba_item = searchBAitems[i].getValue('custrecord_h_berp_ba_item');
			var calcBBL = (ba_qty * BBLequivalent);
			var BBLset = BBLequivalent.toFixed(8);

			nlapiSubmitField('customrecord_h_berp_barrels', ba_recId, 'custrecord_h_berp_qty', BBLset);
		}

		if(isNotNull(searchBAitems) && isNull(isAdjCreated)){
			var params = {
		 		custscript_blend_id: blendId
		 	}

		 	nlapiScheduleScript('customscript_blend_adjust', 'customdeploy_blend_adjust', params);
	 	}
	}
}

function evaluateUOM(uomSample){

	//3968 oz in 1 barrel of beer.
	//117347.7658 mL in 1 barrel of beer
	
	var isOunce = new RegExp("oz");
	var ismL = new RegExp("mL");
	var barrel = new RegExp("Barrel");

	var res = isOunce.test(uomSample);
	var res2 = ismL.test(uomSample);
	var res3 = barrel.test(uomSample);

	if(res == true){
		return 3968;
	}
	else if(res2 == true){
		return 117347.7658;
	}
	else if(res3 == true){
		return 1;
	}
	else{
		return false;
	}
}

function isNotNull(field_val){
	return (field_val != '' && field_val != null);
}
function isNull(s_string){
	return (s_string == null || s_string == '');
}