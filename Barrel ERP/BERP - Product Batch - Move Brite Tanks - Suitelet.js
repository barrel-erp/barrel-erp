/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        09 Jun 2017     amillen          new functionality to allow users to move production batches between different Bright or Fermentation tank work centers
 *
 */


function moveTanks(request, response){


	var context = nlapiGetContext();
	var accountId = context.getSetting('SCRIPT', 'custscript_h_berp_ns_account_id');
	var activeAccounts = accountActiveCheck();
	nlapiLogExecution('DEBUG', 'Active Accounts', JSON.stringify(activeAccounts));
	nlapiLogExecution('DEBUG', 'Index', activeAccounts.indexOf(accountId));

    if (activeAccounts.indexOf(accountId) >= 0) {

		if (request.getMethod() == 'GET') {
			var location = request.getParameter('custparam_location');
			var recordId = request.getParameter('custparam_recordid');
			var origProdOrder = request.getParameter('custparam_origprod');
			var context = nlapiGetContext();
			var date = new Date();
			var defaultDate = nlapiDateToString(date);

			// OneWorld Check
			var isOneWorld;
			var companyInfo = nlapiLoadConfiguration('userpreferences'); //gets user preferences
			var acctSubsidiary = companyInfo.getFieldValue('subsidiary'); //gets subsidiary from user preferences
			if (acctSubsidiary != null) { // if subsidiary is not null
			   isOneWorld = true; //account is a OneWorld account
			} else {
			   isOneWorld = false; //account is NOT a OneWorld account
			}

			// Get Parameters
			var prodBatchId = request.getParameter('custparam_prodbatchid');
			var prodBatchLocation = request.getParameter('custparam_prodbatchlocation');

			var prodBatchLicense = '';
			var prodBatchSubsidiary = '';
			var brightBoolean = false;
			var fermBoolean = false;
			
			var origProdOrder = nlapiLookupField('customrecord_h_berp_prod_batch', prodBatchId, 'custrecord_h_berp_originating_prodord');
			if (origProdOrder) { var prodBatchLicense = nlapiLookupField('workorder', origProdOrder, 'custbody_h_berp_license'); }
			if (origProdOrder && isOneWorld == true) { var prodBatchSubsidiary = nlapiLookupField('workorder', origProdOrder, 'subsidiary'); }


			// Create the form and add fields to it 
			var form = nlapiCreateForm('Move Tank');

			// Create the Field Groups
			var primary = form.addFieldGroup('primary', 'Primary Information');
			var classification = form.addFieldGroup('classification', 'Classification');
			var tanks = form.addFieldGroup('tanks', 'Move Tanks');
 
			form.addField('custpage_prodorder', 'select', 'Production Order', 'workorder', 'primary').setDisplayType('inline').setDefaultValue(origProdOrder);
			form.addField('custpage_prodbatch', 'select', 'Production Batch', 'customrecord_h_berp_prod_batch', 'primary').setDisplayType('inline').setDefaultValue(prodBatchId);
			
			var tankSelection = form.addField('custpage_tank','select', 'Move From', null, 'tanks').setMandatory(true);
		    tankSelection.addSelectOption('','');
		    var moveToTank = form.addField('custpage_moveto', 'select', 'Move To', null, 'tanks').setMandatory(true);
		    moveToTank.addSelectOption('','');
	
		    var location = nlapiLookupField('workorder', origProdOrder, 'location');
			form.addField('custpage_locatn', 'select', 'Location', 'location', 'classification').setDisplayType('inline').setDefaultValue(location);
		
			//search Bright manufacturing operation tasks attached to original prod. order and return work center
		    var taskResults = searchBrightMOTasks(origProdOrder);
		    if(taskResults){
		    	for(var d = 0; d <taskResults.length; d++){
		    		var manWkCenter = taskResults[d].getText('manufacturingworkcenter');
		    		var TBDtest = manWkCenter.indexOf('TBD');
		    		if(TBDtest === -1){
		    			var sequence = taskResults[d].getValue('sequence');
		    			nlapiLogExecution('DEBUG', 'bright sequence', sequence)
		    			var equipmentType = 8;
		    			form.addField('custpage_sequence', 'integer', 'Os', null, 'tanks').setDisplayType('hidden').setDefaultValue(sequence);
		    			brightBoolean = true;
		    		}
		    	}
		    }
		    //if no results from Bright manufacturing tasks that are not TBD, then search Fermentation manufacturing tasks and return fermenetation work center
		    if(brightBoolean === false){
		    	var taskResults = searchFermMOTasks(origProdOrder);
		    	if(taskResults){
		    		for(var d = 0; d < taskResults.length; d++){
		    			var manWkCenter = taskResults[d].getText('manufacturingworkcenter');
		    			var TBDtest = manWkCenter.indexOf('TBD');
		    			if(TBDtest === -1){
		    				var sequence = taskResults[d].getValue("sequence");
		    				nlapiLogExecution('DEBUG', 'ferm sequence', sequence)
		    				var equipmentType = 6;
		    				form.addField('custpage_sequence', 'integer', 'Os', null, 'tanks').setDisplayType('hidden').setDefaultValue(sequence);
		    				fermBoolean = true;
		    			}
		    		}
		    	}
		    }
		    if(brightBoolean === false && fermBoolean === false){
		    	var equipmentType = '6,8';
		    	var sequence = '';
		    }

		    //search for one work center by sequence and equipment type variables set by earlier manufacturing operation task look ups
		    var centersResults = searchOperationTaskCenters(origProdOrder, sequence, equipmentType);
		    if (centersResults) {
		    	for(var a = 0; a < centersResults.length; a++){
					var string =  centersResults[a].getValue('custentity_h_berp_tank_name');
				    var TBD = string.indexOf('TBD');

				    //displaying the Bright tank work center result in the Move From tank field
				  	if(brightBoolean === true){
					    if(TBD === -1){	
					    	var compare = centersResults[a].getValue('custentity_h_berp_tank_name');
							tankSelection.addSelectOption(centersResults[a].getValue('internalid'), centersResults[a].getValue('custentity_h_berp_tank_name'));
						}
				  	}
				  	//displaying the Fermentation tank work center result in the Move From tank field
				  	if(fermBoolean === true){
				  		if(TBD === -1){
			  				var compare = centersResults[a].getValue('custentity_h_berp_tank_name');
							tankSelection.addSelectOption(centersResults[a].getValue('internalid'), centersResults[a].getValue('custentity_h_berp_tank_name'));	
						}	
				  	}
				  	//if no results returned, displaying Bright and Fermentation tank results in the Move From tank field
				  	if(fermBoolean === false && brightBoolean === false){
				  		var compare = centersResults[a].getValue('custentity_h_berp_tank_name');
				  		tankSelection.addSelectOption(centersResults[a].getValue('internalid'), centersResults[a].getValue('custentity_h_berp_tank_name')); 	
					}
				}
		    }
		    //if move from work center is Bright Tank, set select options for Move To tank field as any Bright Tank
		    if(brightBoolean === true){
		   		var tanksAvailable = searchAllBrightTanks(location);
			   	if (tanksAvailable) {
		   	    	for (var b = 0; b < tanksAvailable.length; b++) {
		   	    		var string =  tanksAvailable[b].getValue('custentity_h_berp_tank_name');
				    	var TBD = string.indexOf('TBD');
		    	    
		    	    	if(TBD === -1){
		    	    		if(compare !== string){
		    	    			moveToTank.addSelectOption(tanksAvailable[b].getValue('internalid'), tanksAvailable[b].getValue('custentity_h_berp_tank_name'))
		    	    		}
		    	    	}
	    	    	}
		    	}
		    }
		    //if move from work center is Fermentation Tank, set select options for Move To tank field as any Fermentation Tank
		    if(fermBoolean === true){
				var tanksAvailable = searchAllFermentationTanks(location);
			   	if (tanksAvailable) {
		   	    	for (var b = 0; b < tanksAvailable.length; b++) {
		   	    		var string =  tanksAvailable[b].getValue('custentity_h_berp_tank_name');
				    	var TBD = string.indexOf('TBD');
				    	if(TBD == -1){
				    		if(compare !== string){
		   		    			moveToTank.addSelectOption(tanksAvailable[b].getValue('internalid'), tanksAvailable[b].getValue('custentity_h_berp_tank_name'))
	    	    			}
	    	    		}
	    	    	}
		    	}
			}
			//if no results returned from Move From tank, set Move To tank field select options as any Fermentation or Bright tank
			if(brightBoolean === false && fermBoolean === false){
				var tanksAvailable = searchAllFermAndBrightTanks(location);
			   	if (tanksAvailable) {
		   	    	for (var b = 0; b < tanksAvailable.length; b++) {
	    				var string =  tanksAvailable[b].getValue('custentity_h_berp_tank_name');
				    	var TBD = string.indexOf('TBD');
				    	if(TBD == -1){
			    			moveToTank.addSelectOption(tanksAvailable[b].getValue('internalid'), tanksAvailable[b].getValue('custentity_h_berp_tank_name'))
	    	    		}
	    	    	}
		    	}
			}

			if (isOneWorld == true) {

				form.addField('custpage_subsidiary', 'select', 'Subsidiary', '-117', 'classification').setDisplayType('inline').setMandatory(true).setDefaultValue(prodBatchSubsidiary);

			}

	        form.addSubmitButton('Submit');

		    response.writePage(form);

		}
		else{
			var prodBatchId = request.getParameter('custpage_prodbatch');
			var moveToCenter = request.getParameter('custpage_moveto');
			var moveFromCenter = request.getParameter('custpage_tank');
			var operationSequence = request.getParameter('custpage_sequence');
			var origProdOrd = request.getParameter('custpage_prodorder');

			if(isNull(operationSequence)){
				var sequenceResults = lookUpSequence(origProdOrd, moveFromCenter);
				if(isNotNull(sequenceResults)){
					for(var a = 0; a < sequenceResults.length; a++){
						operationSequence = sequenceResults[a].getValue('sequence');
					}
				}
			}
			//search all related production orders to updated work center from production batch
			var prodOrderResults = searchBatchProductionOrders(prodBatchId);
			if(isNotNull(prodOrderResults)){
				for(var i = 0; i < prodOrderResults.length; i++){
					var relatedProdOrderId = prodOrderResults[i].getValue('internalid');
					if(relatedProdOrderId){
						//get internal id of manufacturing operation task to update to user selected work center
						var taskResults = searchMOTasks(relatedProdOrderId, operationSequence);
						if(taskResults){
							for(var h=0; h<taskResults.length;h++){
								var opTaskId = taskResults[h].getValue('internalid');
							}
						}
						var relatedProdRec = nlapiLoadRecord('workorder', relatedProdOrderId);
						var status = relatedProdRec.getFieldValue('orderstatus');
						var startDate = relatedProdRec.getFieldValue('startdate');
						var endDate = relatedProdRec.getFieldValue('enddate');
						//only update production order operation task work center if status is: B = Released, D = in progess,  G= Built
						try{
							if(status == 'D' || status == 'B' || status == 'G'){
								nlapiSubmitField('workorder', relatedProdOrderId, 'orderstatus', 'A');
								nlapiSubmitField('manufacturingoperationtask', opTaskId, 'manufacturingworkcenter', moveToCenter);
								nlapiSubmitField('workorder', relatedProdOrderId, 'orderstatus', status);
								nlapiSubmitField('workorder', relatedProdOrderId, 'enddate', endDate);
								nlapiSubmitField('workorder', relatedProdOrderId, 'startdate', startDate);
							}
							//if status is 'A' = Planned, then do not need to unlock production order to edit manufacturing operation task
							if(status == 'A'){
								nlapiSubmitField('manufacturingoperationtask', opTaskId, 'manufacturingworkcenter', moveToCenter);
							}
						}
						catch(e){
							nlapiLogExecution('ERROR', 'error', e.getDetails())
						}
					}
				}
			}
			response.write('<script>window.close();</script>');
		}
	}
}