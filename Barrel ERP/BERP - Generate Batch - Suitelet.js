/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.0        23 Dec 2015     jbrox
 * 1.0 ^ 2    12 Jun 2017     amillen          added functionality to tie the production batch to the manufacturing operation tasks
 *
 */

function generateBatch(request, response) {

	if (request.getMethod() == 'GET') { 
		var context = nlapiGetContext();
		var overrideDate = context.getSetting('SCRIPT', 'custscript_transdate');
		// Get Parameters
		nlapiLogExecution('DEBUG', 'tran date setting', overrideDate);
		var location = request.getParameter('custparam_location');
		var recordId = request.getParameter('custparam_recordid');
		var prodOrderFields = searchProdOrderFields(recordId);
		nlapiLogExecution('DEBUG', 'Prod Search', JSON.stringify(prodOrderFields));
		var recipeFancifulName = prodOrderFields[0].getText('item');
		var recipeQuantity = prodOrderFields[0].getValue('quantity');
		nlapiLogExecution('DEBUG', 'Prod Search Fanciful', JSON.stringify(recipeFancifulName));

		nlapiLogExecution('DEBUG', 'Record ID Param', recordId);

	    // Create Form with Fields
	    var generateBatch = nlapiCreateForm('Generate Production Batch');

	    // Create Field Groups on the Form
	    var primary = generateBatch.addFieldGroup('primary', 'Initiating Production Order Information');
	    generateBatch.addField('custpage_location', 'select', 'Location', '-103', 'primary').setDisplayType('disabled');
	   	generateBatch.addField('custpage_recordid', 'select', 'Record ID', '-30', 'primary').setDisplayType('disabled');
	   	var batch = generateBatch.addFieldGroup('batch', 'Production Batch Information');
	   	generateBatch.addField('custpage_description', 'textarea', null, null, 'batch').setDisplayType('inline').setDefaultValue('The Total BBL Quantity is the BBL quantity put into the Fermentation tank. The # of Production Orders in Batch is the number of Production Orders needed for the batch.');
	    generateBatch.addField('custpage_recipequantity', 'float', 'Total BBL Quantity', null, 'batch').setMandatory(true);
   	    generateBatch.addField('custpage_prodquantity', 'integer', '# of Production Orders in Batch ', null, 'batch').setMandatory(true);
   	    generateBatch.addField('custpage_prodbatchname', 'text', 'Production Batch Name', null, 'batch').setMandatory(true);
   	    if(overrideDate == 'T'){
   	    	generateBatch.addField('custpage_date', 'date', 'Date', null, 'batch').setMandatory(true);
   		}
   	    var fermTankSelection = generateBatch.addField('custpage_fermtank','select', 'Fermentation Tank', null, 'batch').setMandatory(true);
        fermTankSelection.addSelectOption('','');

        var fermTanksAvailable = searchAllFermentationTanks(location);

        if (fermTanksAvailable) {

        	for (var f = 0; f < fermTanksAvailable.length; f++) {

        		fermTankSelection.addSelectOption(fermTanksAvailable[f].getValue('internalid'), fermTanksAvailable[f].getValue('custentity_h_berp_tank_name'));

        	}

        }

		generateBatch.setFieldValues({custpage_recordid:recordId, custpage_location:location, custpage_recipequantity:recipeQuantity, custpage_prodbatchname:recipeFancifulName});

		generateBatch.addSubmitButton('Submit');

	    response.writePage(generateBatch);

	} else {
		// Get Context
		var context = nlapiGetContext();

		// Get Parameters
		var transDate = request.getParameter('custpage_date');
		var location = request.getParameter('custpage_location');
		var recordId = request.getParameter('custpage_recordid');
		var prodLookupFields = ['enddate', 'startdate'];
		var prodLookupColumns = nlapiLookupField('workorder', recordId, prodLookupFields);
		var prodEndDate = prodLookupColumns.enddate;
		var prodStartDate = prodLookupColumns.startdate;
		var recipeQuantity = request.getParameter('custpage_recipequantity');
		var prodQuantity = request.getParameter('custpage_prodquantity');
		var fermTank = request.getParameter('custpage_fermtank');
		var prodBatchName = request.getParameter('custpage_prodbatchname');
		nlapiLogExecution('DEBUG', 'Prod Batch Name', prodBatchName);
		prodQuantity = parseInt(prodQuantity);
		nlapiLogExecution('DEBUG', 'Recipe Quantity / Prod Quantity = ', recipeQuantity + ' / ' + prodQuantity + ' = ' + recipeQuantity / prodQuantity);
		recipeQuantity = recipeQuantity / prodQuantity;



		// Create Production Batch
		var prodBatch = nlapiCreateRecord('customrecord_h_berp_prod_batch');
		prodBatch.setFieldValue('altname', prodBatchName);
		prodBatch.setFieldValue('custrecord_h_berp_prb_location', location);
		prodBatch.setFieldValue('custrecord_h_berp_originating_prodord', recordId);
		var prodBatchId = nlapiSubmitRecord(prodBatch, true);

		for (var i = 0; i < prodQuantity - 1; i++) {

			var prodOrder = nlapiCopyRecord('workorder', recordId);
			prodOrder.setFieldValue('custbody_h_berp_prod_batch', prodBatchId);
			prodOrder.setFieldValue('enddate', prodEndDate);
			prodOrder.setFieldValue('startdate', prodStartDate);
			prodOrder.setFieldValue('quantity', recipeQuantity.toFixed(1));

			if(isNotNull(transDate)){
				prodOrder.setFieldValue('trandate', transDate);
			}

			var prodOrderId = nlapiSubmitRecord(prodOrder, true);

			var taskResults = searchMOTasks(prodOrderId);
          try{
			for (var j = 0; j < taskResults.length; j++){
				nlapiLogExecution('DEBUG', 'internal id', taskResults[j].getValue('internalid'));
				var taskId = taskResults[j].getValue('internalid');
				if( isNotNull(taskId) && isNotNull(prodBatchId) ) {
					nlapiSubmitField('manufacturingoperationtask', taskId, 'custevent_h_berp_mot_production_batch', prodBatchId);				
				}
			}
          }
          catch(E){
            nlapiLogExecution('ERROR', 'error', E);
          }
		}

		nlapiSubmitField('workorder', recordId, 'custbody_h_berp_prod_batch', prodBatchId);
		nlapiSubmitField('workorder', recordId, 'quantity', recipeQuantity.toFixed(1));

		if (prodBatchId) {

			var origProductionOrder = nlapiLookupField('customrecord_h_berp_prod_batch', prodBatchId, 'custrecord_h_berp_originating_prodord');
			var origProOrderWorkCenters = searchProOrderWorkCenters(origProductionOrder);

			var currentProOrderWorkCenters = searchProBatchWorkCenters(prodBatchId);
			nlapiLogExecution('DEBUG', 'Production Order Work Centers', JSON.stringify(currentProOrderWorkCenters));

			for (var b = 0; b < currentProOrderWorkCenters.length; b++) {

				if (nlapiLookupField('entitygroup', currentProOrderWorkCenters[b].getValue('manufacturingworkcenter', 'manufacturingOperationTask'), 'custentity_h_berp_equipment_type') == '6') {

					var productionOrderStatus = nlapiLookupField('workorder', recordId, 'status');
					nlapiLogExecution('AUDIT', 'Production Order Status', productionOrderStatus);

					if (productionOrderStatus == 'pendingApproval' || productionOrderStatus == 'Released' || productionOrderStatus == 'pendingBuild') {
					
						nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', fermTank);
						nlapiLogExecution('AUDIT', 'Setting Work Center', 'Manufacturing Operation Task = ' + currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask') + ' | ' + 'Orig Ferm Tank = ' + fermTank);

					} else {

						nlapiSubmitField('workorder', recordId, 'orderstatus', 'B');
						var productionOrderStatus = nlapiLookupField('workorder', recordId, 'status');
						nlapiLogExecution('AUDIT', 'Production Order Status After Unlock', productionOrderStatus);
						nlapiLogExecution('DEBUG', 'Manufacturing Operation | Orig Ferm Tank', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask') + ' | ' + fermTank);
						nlapiSubmitField('manufacturingoperationtask', currentProOrderWorkCenters[b].getValue('internalid', 'manufacturingOperationTask'), 'manufacturingworkcenter', fermTank);

					}

				}

			}

			var params = {
				custscript_h_berp_batchId: prodBatchId
			}

			nlapiScheduleScript('customscript_add_batch','customdeploy_add_batch', params);

		}

	}

	response.write('<script>window.close();</script>');

}