// document.onkeyup=function(e) {
// 	var e = e || window.event; // for IE to cover IEs window object

// 	if (e.which == 18 && e.which == 78) {
		
// 		document.activeElement.value = getTimeStamp();
// 		return false;
	
// 	}
// }

var timeFields = {

    'custrecord_h_berp_qcbrew_millstart': true, 
    'custrecord_h_berp_qcbrew_millend': true,
    'custrecord_h_berp_qcbrew_mashstart': true,
    'custrecord_h_berp_qcbrew_mashend': true,
    'custrecord_h_berp_qcbrew_mashrest1': true,
    'custrecord_h_berp_qcbrew_mashstrt1': true,
    'custrecord_h_berp_qcbrew_mashstrt2': true,
    'custrecord_h_berp_qcbrew_mashrest2': true,
    'custrecord_h_berp_qcbrew_mashstrt3': true,
    'custrecord_h_berp_qcbrew_mashend3': true,
    'custrecord_h_berp_qcbrew_vourlaufstart': true,
    'custrecord_h_berp_qcbrew_vorlaufendtime': true,
    'custrecord_h_berp_qcbrew_runoffstart': true,
    'custrecord_h_berp_qcbrew_runoffend': true,
    'custrecord_h_berp_qcbrew_spargestart': true,
    'custrecord_h_berp_qcbrew_spargeend': true,
    'custrecord_h_berp_qcbrew_boilstart': true,
    'custrecord_h_berp_qcbrew_boilend': true,
    'custrecord_h_berp_qcbrew_wpoolxferstart': true,
    'custrecord_h_berp_qcbrew_wpoolxferend': true,
    'custrecord_h_berp_qcbrew_knockoutstart': true,
    'custrecord_h_berp_qcbrew_knockoutend': true

};

var map = {78: false};
$(document).keydown(function(e) {
    if (e.keyCode in map) {
        map[e.keyCode] = true;
        if (map[78]) {
            // FIRE EVENT
            if (document.activeElement.id in timeFields) {

                document.activeElement.value = getTimeStamp();
                return false;

            }
        }
    }
}).keyup(function(e) {
    if (e.keyCode in map) {
        map[e.keyCode] = false;
    }
});

function getTimeStamp() {

    var d = new Date();
    var nsDate = nlapiDateToString(d);
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var amPM = "am";
    if (hours >= 12) {
        hours = hours-12;
        amPM = "pm";
    }
    if (hours == 0) {
        hours = 12;
    }
    minutes = minutes<10?"0"+minutes:minutes;

    var timeStamp = nsDate + '  ' + hours + ':' + minutes + ':' + '00';
    timeStamp += ' ' + amPM;    

    return timeStamp;
}